const _ = require('lodash');
const express = require('express');

const router = express.Router();
const PaymentGatewayService = require('../services/common/PaymentGateway');
const { objectIdSchema, orderChargeSchema } = require('../helpers/validation/schema');
// const OrderService = require('../services/core/OrderService');
const { ValidationError } = require('../helpers/errors/index');

// router.post('/:id', async (req, res, next) => {
//     try {
//         req.validate(objectIdSchema, req.params);
//         const payInfo = req.body;
//         await req.authorize(req.user, 'gateway', ['createOwn'], async () => {
//             const order = await OrderService.getOne(req.params.id);
//             if (_.isNil(order)) {
//                 throw new ValidationError(0, `Order id ${req.params.id} is not exist.`);
//             }
//             const flag = order.user._id.toString() === req.user_id;
//             return flag;
//         });

//         payInfo.merchantOrderId = req.params.id;
//         // req.validate(orderChargeSchema, payInfo);
//         const result = await PaymentGatewayService.chargeCustomer(payInfo, req.user);
//         res.send(result);
//     } catch (error) {
//         next(error);
//     }

// });


module.exports = router;
