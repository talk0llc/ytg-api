const _ = require('lodash');
const express = require('express');

const router = express.Router();

const { multerMW, multipartParser } = require('./middlewares/index');
// const { pServiceBasicData } = require('../helpers/projections/index');
// Validation
const { ValidationError, NotFoundError } = require('../helpers/errors/index');
const { objectIdSchema, ServiceSchema } = require('../helpers/validation/schema');
const Service = require('../services/core/Service');


const upload = multerMW({
    field: 'images',
    size: 2 * 1024 * 1024,
    count: 3,
});

router.post('/', upload, multipartParser, async (req, res, next) => {
    try {
        await req.authorize(req.user, 'services', ['createAny']);

        const data = req.body;
        req.validate(ServiceSchema, data);

        const service = await Service.create(data, req.files);
        res.json(service);
        // res.json(pServiceBasicData.pickFrom(employee));
    } catch (err) {
        next(err);
    }
});

router.get('/', async (req, res, next) => {
    try {
        // await req.authorize(req.user, 'services', ['readAny']);

        const params = req.query;
        params.sort = 'created_at';
        const result = await Service.getAll(params);
        res.json(result);
    } catch (err) {
        next(err);
    }
});

router.get('/:id', async (req, res, next) => {
    try {
        req.validate(objectIdSchema, req.params);
        // await req.authorize(req.user, 'services', ['readAny']);

        const service = await Service.getOne(req.params.id);
        if (_.isNil(service)) {
            throw new NotFoundError();
        }
        res.json(service);
        // res.json(pServiceBasicData.pickFrom(user));
    } catch (err) {
        next(err);
    }
});

router.put('/:id', upload, multipartParser, async (req, res, next) => {
    try {
        req.validate(objectIdSchema, req.params);
        await req.authorize(req.user, 'services', ['updateAny']);

        const data = req.body;
        req.validate(ServiceSchema, data, false);
        const service = await Service.update(req.params.id, data, req.files);

        res.json(service);
        // res.json(pServiceBasicData.pickFrom(user));
    } catch (err) {
        next(err);
    }
});

router.delete('/:id', async (req, res, next) => {
    try {
        req.validate(objectIdSchema, req.params);
        await req.authorize(req.user, 'services', ['deleteAny']);

        await Service.delete(req.params.id);
        res.sendStatus(200);
    } catch (err) {
        next(err);
    }
});

router.delete('/:id/sections/:sectionId', async (req, res, next) => {
    try {
        await req.authorize(req.user, 'services', ['deleteAny']);
        const section = await Service.deleteSection(req.params.id, req.params.sectionId);
        res.json(section);
    } catch (err) {
        next (err);
    }
});

module.exports = router;
