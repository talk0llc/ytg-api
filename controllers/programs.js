const _ = require('lodash');
const express = require('express');

const router = express.Router();
const { authenticationMW } = require('./middlewares');
const { multerMW, multipartParser } = require('./middlewares/index');
// const { pProgramBasicData } = require('../helpers/projections/index');
// Validation
const { ValidationError, NotFoundError } = require('../helpers/errors/index');
const { objectIdSchema, ProgramSchema } = require('../helpers/validation/schema');
const ProgramService = require('../services/core/ProgramService');


const upload = multerMW({
    field: 'images',
    size: 2 * 1024 * 1024,
    count: 3,
});

router.post('/', upload, multipartParser, async (req, res, next) => {
    try {
        await req.authorize(req.user, 'programs', ['createAny']);

        const data = req.body;
        req.validate(ProgramSchema, data);

        const program = await ProgramService.create(data, req.files);
        res.json(program);
        // res.json(pProgramBasicData.pickFrom(employee));
    } catch (err) {
        next(err);
    }
});

router.get('/', async (req, res, next) => {
    try {
        // await req.authorize(req.user, 'programs', ['readAny']);

        const params = req.query;
        params.sort = 'created_at';
        const result = await ProgramService.getAll(params);
        res.json(result);
    } catch (err) {
        next(err);
    }
});

router.get('/applications', async (req, res, next) => {
    try {
        await req.authorize(req.user, 'applications', ['readAny']);
        const params = req.query;
        params.sort = { created_at: 'desc' };
        const applications = await ProgramService.getUserApplications(params);
        res.json(applications);
    } catch (err) {
        next (err);
    }
});

router.get('/:id', async (req, res, next) => {
    try {
        req.validate(objectIdSchema, req.params);
        // await req.authorize(req.user, 'programs', ['readAny']);

        const program = await ProgramService.getOne(req.params.id);
        if (_.isNil(program)) {
            throw new NotFoundError();
        }
        res.json(program);
        // res.json(pProgramBasicData.pickFrom(user));
    } catch (err) {
        next(err);
    }
});

router.put('/:id', upload, multipartParser, async (req, res, next) => {
    try {
        req.validate(objectIdSchema, req.params);
        await req.authorize(req.user, 'programs', ['updateAny']);

        const data = req.body;
        req.validate(ProgramSchema, data, false);
        const program = await ProgramService.update(req.params.id, data, req.files);

        res.json(program);
        // res.json(pProgramBasicData.pickFrom(user));
    } catch (err) {
        next(err);
    }
});

router.delete('/:id', async (req, res, next) => {
    try {
        req.validate(objectIdSchema, req.params);
        await req.authorize(req.user, 'programs', ['deleteAny']);

        await ProgramService.delete(req.params.id);
        res.sendStatus(200);
    } catch (err) {
        next(err);
    }
});

router.put('/:id/questions', async (req, res, next) => {
    try {
        await req.authorize(req.user, 'programs', ['updateAny']);
        const program = await ProgramService.addQuestion(req.params.id, req.body);
        res.json(program);
    } catch (err) {
        next(err);
    }
});

router.get('/:id/questions', async (req, res, next) => {
    try {
        await req.authorize(req.user, 'programs', ['readAny']);
        const program = await ProgramService.getQuestions(req.params.id);
        res.json(program);
    } catch (err) {
        next(err);
    }
});

router.delete('/:id/questions/:questionId', async (req, res, next) => {
    try {
        await req.authorize(req.user, 'programs', ['deleteAny']);
        const program = await ProgramService.deleteQuestion(req.params.id, req.params.questionId);
        res.json(program);
    } catch (err) {
        next(err);
    }
});

router.post('/:id/questions', upload, multipartParser, async (req, res, next) => {
    try {
        await req.authorize(req.user, 'applications', ['createAny']);
        const answer = await ProgramService.submitAnswer(req.params.id, req.user_id, req.body, req.files);
        res.json(answer);

    } catch (err) {
        next (err);
    }
});

router.delete('/:id/sections/:sectionId', async (req, res, next) => {
    try {
        await req.authorize(req.user, 'programs', ['deleteAny']);
        const section = await ProgramService.deleteSection(req.params.id, req.params.sectionId);
        res.json(section);
    } catch (err) {
        next (err);
    }
});

router.get('/:id/applications/:applicationId', async (req, res, next) => {
    try {
        await req.authorize(req.user, 'applications', ['readAny']);
        const applications = await ProgramService.getUserApplication(req.params.id, req.params.applicationId);
        res.json(applications);
    } catch (err) {
        next (err);
    }
});

router.put('/:id/applications/:applicationId', async (req, res, next) => {
    try {
        const data = req.body;
        await req.authorize(req.user, 'applications', ['updateAny']);
        const application = ProgramService.updateUserApplicationStatus(req.params.applicationId, data);
        res.json(application);
    } catch (err) {
        next(err);
    }
});

router.delete('/:id/applications/:applicationId', async (req, res, next) => {
    try {
        // await req.authorize(req.user, 'applications', ['updateAny']);
        ProgramService.deleteUserApplicationStatus(req.params.applicationId);
        res.sendStatus(200);
    } catch (err) {
        next(err);
    }
});

module.exports = router;
