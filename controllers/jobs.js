const _ = require('lodash');
const express = require('express');

const router = express.Router();

const { multerMW, multipartParser } = require('./middlewares/index');
// const { pJobBasicData } = require('../helpers/projections/index');
// Validation
const { ValidationError, NotFoundError } = require('../helpers/errors/index');
const { objectIdSchema, JobSchema } = require('../helpers/validation/schema');
const JobService = require('../services/core/JobService');


const upload = multerMW({
    field: 'images',
    size: 2 * 1024 * 1024,
    count: 1,
});

router.post('/', upload, multipartParser, async (req, res, next) => {
    try {
        await req.authorize(req.user, 'jobs', ['createAny']);

        const data = req.body;
        req.validate(JobSchema, data);

        const job = await JobService.create(data, req.files);
        res.json(job);
        // res.json(pJobBasicData.pickFrom(employee));
    } catch (err) {
        next(err);
    }
});

router.get('/', async (req, res, next) => {
    try {
        // await req.authorize(req.user, 'jobs', ['readAny']);

        const params = req.query;
        params.sort = 'created_at';
        const result = await JobService.getAll(params);
        res.json(result);
    } catch (err) {
        next(err);
    }
});
router.get('/applications', async (req, res, next) => {
    try {
        await req.authorize(req.user, 'applications', ['readAny']);
        const params = req.query;
        params.sort = { created_at: 'desc' };
        const applications = await JobService.getUserApplications(params);
        res.json(applications);
    } catch (err) {
        next (err);
    }
});

router.get('/:id', async (req, res, next) => {
    try {
        req.validate(objectIdSchema, req.params);
        // await req.authorize(req.user, 'jobs', ['readAny']);

        const job = await JobService.getOne(req.params.id);
        if (_.isNil(job)) {
            throw new NotFoundError();
        }
        res.json(job);
        // res.json(pJobBasicData.pickFrom(user));
    } catch (err) {
        next(err);
    }
});

router.put('/:id', upload, multipartParser, async (req, res, next) => {
    try {
        req.validate(objectIdSchema, req.params);
        await req.authorize(req.user, 'jobs', ['updateAny']);

        const data = req.body;
        req.validate(JobSchema, data, false);
        const job = await JobService.update(req.params.id, data, req.files);

        res.json(job);
        // res.json(pJobBasicData.pickFrom(user));
    } catch (err) {
        next(err);
    }
});

router.delete('/:id', async (req, res, next) => {
    try {
        req.validate(objectIdSchema, req.params);
        await req.authorize(req.user, 'jobs', ['deleteAny']);

        await JobService.delete(req.params.id);
        res.sendStatus(200);
    } catch (err) {
        next(err);
    }
});

router.put('/:id/questions', async (req, res, next) => {
    try {
        await req.authorize(req.user, 'jobs', ['updateAny']);

        const job = await JobService.addQuestion(req.params.id, req.body);
        res.json(job);
    } catch (err) {
        next(err);
    }
});

router.get('/:id/questions', async (req, res, next) => {
    try {
        await req.authorize(req.user, 'jobs', ['readAny']);

        const job = await JobService.getQuestions(req.params.id);
        res.json(job);
    } catch (err) {
        next(err);
    }
});

router.delete('/:id/questions/:questionId', async (req, res, next) => {
    try {
        await req.authorize(req.user, 'jobs', ['deleteAny']);

        const job = await JobService.deleteQuestion(req.params.id, req.params.questionId);
        res.json(job);
    } catch (err) {
        next(err);
    }
});

router.post('/:id/questions', async (req, res, next) => {
    try {
        await req.authorize(req.user, 'applications', ['createAny']);

        const answer = await JobService.submitAnswer(req.params.id, req.user_id, req.body);
        res.json(answer);

    } catch (err) {
        next (err);
    }
});

router.delete('/:id/sections/:sectionId', async (req, res, next) => {
    try {
        await req.authorize(req.user, 'jobs', ['deleteAny']);

        const section = await JobService.deleteSection(req.params.id, req.params.sectionId);
        res.json(section);
    } catch (err) {
        next (err);
    }
});

router.get('/:id/applications/:applicationId', async (req, res, next) => {
    try {
        await req.authorize(req.user, 'applications', ['readAny']);

        const applications = await JobService.getUserApplication(req.params.id, req.params.applicationId);
        res.json(applications);
    } catch (err) {
        next (err);
    }
});

router.put('/:id/applications/:applicationId', async (req, res, next) => {
    try {
        await req.authorize(req.user, 'applications', ['updateAny']);

        const data = req.body;
        const application = JobService.updateUserApplicationStatus(req.params.applicationId, data);
        res.json(application);
    } catch (err) {
        next(err);
    }
});

router.delete('/:id/applications/:applicationId', async (req, res, next) => {
    try {
        // await req.authorize(req.user, 'applications', ['updateAny']);
        JobService.deleteUserApplicationStatus(req.params.applicationId);
        res.sendStatus(200);
    } catch (err) {
        next(err);
    }
});

module.exports = router;
