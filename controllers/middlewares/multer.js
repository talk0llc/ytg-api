const multer = require('multer');
const path = require('path')
const ImageProcessingUtils = require('../../utils/image-processing-utils');
const { ValidationError } = require('../../helpers/errors/index');

async function handleImageFileResizing(req, imageSize) {
    if (req.files && req.files.length > 0) {
        req.files = await ImageProcessingUtils.resizeImages(req.files, imageSize);
    }
}

module.exports = opts => async (req, res, next) => {
    const size = opts.size || 2 * 1024 * 1024; // required size limit or 500k as default
    const count = opts.count || 1;
    const field = opts.field || 'images';
    const dest = opts.dest || 'images/uploads';
    const imageSize = opts.imageSize || 'defaultImageDimensions';

    const storage = multer.diskStorage({
        destination(req, file, callback) {
            callback(null, dest);
        },
        filename(req, file, callback) {
            // callback(null, `${file.originalname}`);
            // callback(null, `${file.originalname}-${Date.now()}`);
            callback(null, `${Date.now()}${path.extname(file.originalname)}`);
        },
    });
    const mInstance = multer({
        storage,
        limits: {
            fileSize: size,
            files: count,
        },
    });

    const upload = mInstance.array(field, count);
    try {
        await new Promise((resolve, reject) => {
            upload(req, res, async (e) => {
                const error = new ValidationError();
                if (e) {
                    switch (e.code) {
                        case 'LIMIT_UNEXPECTED_FILE':
                            error.message = `Files count must be less than or equal to ${count}`;
                            break;
                        case 'LIMIT_FILE_SIZE':
                            error.message = `File size must be less than or equal to ${size} bytes`;
                            break;
                        default:
                            error.message = 'Error while uploading files';
                    }

                    reject(error);
                }

                // await handleImageFileResizing(req, imageSize);
                resolve();
            });
        });

        next();
    } catch (err) {
        next(err);
    }
};
