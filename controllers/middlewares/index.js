const authenticationMW = require('./authentication');
const multerMW = require('./multer');
const restaurantMW = require('./restaurant');
const hooks = require('./hooks');
const requestMutation = require('./requestMutation');
const multipartParser = require('./multipartParser');

module.exports = {
    authenticationMW,
    multerMW,
    restaurantMW,
    hooks,
    requestMutation,
    multipartParser,
};
