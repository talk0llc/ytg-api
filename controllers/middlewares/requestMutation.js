const _ = require('lodash');

const convertParams = (params) => {
    const result = {
        page: 0,
        limit: 100,
        _options: {},
    };

    let buffer;
    _.forEach(params, (value, key) => {
        switch (key) {
            case 'page':
                buffer = parseInt(value || 0, 10);
                result.page = buffer >= 0 ? buffer : 0;
                break;
            case 'limit':
                buffer = parseInt(value || 0, 10);
                result.limit = buffer > 100 ? 100 : buffer;
                break;
            case 'fields':
                result.fields = value.split(',')
                    .map(item => item.trim())
                    .join(' ');
                break;
            case 'sort':
                result.sort = {};
                value.split(',')
                    .forEach((item) => {
                        let field = item.trim();
                        const op = field.startsWith('-') ? -1 : 1;

                        if (_.isNil(field) || field.length === 0) {
                            return;
                        }

                        if (op < 0) {
                            field = field.substr(1);
                        }

                        result.sort[field] = op;
                    });
                break;
            default:
                result[key] = value;
        }
    });

    return result;
};

module.exports = async (req, res, next) => {
    try {
        // Get user ip
        req.ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;

        req.query = convertParams(req.query);
    } catch (e) {
        next(e);
    }

    next();
};
