const _ = require('lodash');
// const Restaurant = require('mongoose').model('Restaurant');
const { objectIdSchema } = require('../../helpers/validation/common');
const { Restaurant } = require('../../db/models');
const {
    ValidationError,
    NotFoundError,
} = require('../../helpers/errors');

module.exports = async (req, res, next) => {
    try {
        req.validate(objectIdSchema, req.params);
        const restaurant = await Restaurant.getById(req.params.restaurantId, { lean: true });

        if (_.isNil(restaurant)) {
            throw new NotFoundError(0, 'Restaurant does not exist');
        }

        req.restaurant = restaurant;
        req.restaurant_id = String(restaurant._id);

        // Hook hasEmployee func to check if this id is listed as employee in this restaurant
        req.restaurant.hasEmployee = (id) => {
            if (restaurant.employees) {
                for (let i = 0; i < restaurant.employees.length; i += 1) {
                    if (String(restaurant.employees[i]) === id) {
                        return true;
                    }
                }
            }

            return false;
        };
    } catch (e) {
        next(e);
    }

    next();
};
