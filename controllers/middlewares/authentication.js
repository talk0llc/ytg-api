const _ = require('lodash');
const jwt = require('jsonwebtoken');
// const User = require('mongoose').model('User');
// const Employee = require('mongoose').model('Employee');
const { User, Employee } = require('../../db/models');

const config = require('../../config');
const { pUserLogin } = require('../../helpers/projections');

const {
    UnauthenticatedError,
    UnauthorizedError,
    ErrorCodes,
} = require('../../helpers/errors/index');

const decorateUser = (user) => {
    user.isPublicUser = () => {
        const result = _.intersection(user.roles, ['user', 'guest']);
        return result.length > 0;
    };

    user.isUser = () => user.roles.length === 1 && user.roles[0] === 'user';
    user.isEmployee = () => {
        const result = _.intersection(user.roles, ['user', 'guest']);
        return result.length === 0;
    };
    user.hasRole = (role) => user.roles.indexOf(role) >= 0;
    user.isManager = () => user.roles.indexOf('manager') >= 0;
};

module.exports = async (req, res, next) => {
    if (_.isNil(req.headers.authorization)) {
        const guest = { roles: ['guest'] };
        decorateUser(guest);
        req.user = guest;
        req.user_id = '0';

        return next();
    }

    const { authorization } = req.headers;
    jwt.verify(authorization, config.auth.local.key, async (err, decoded) => {
        if (err || _.isNil(decoded)) {
            return next(new UnauthenticatedError());
        }

        try {
            const obj = decoded;

            if (_.isNil(obj.roles)) {
                return next(new UnauthorizedError());
            }

            let Model = User;
            if (_.isNil(obj.roles)) {
                return next(new UnauthorizedError());
            }

            // if (obj.roles.indexOf('user') === -1
            //     && obj.roles.indexOf('guest') === -1) {
            //     Model = Employee;
            // }
            if (obj.roles[0].name !== 'user') {
                Model = Employee;
            }

            const user = await Model.getById(obj.sub);
            // Check if active user
            // Match received token with stored token
            // Check for expired old token or in case we need to force expire tokens
            if (_.isNil(user)
                || !user.is_active
                || !user.is_verified
                || user.token !== authorization) {
                let message = '';
                if (user && !user.is_verified) {
                    message = 'User account is not verified';
                }

                return next(new UnauthorizedError(ErrorCodes.userInactive, message));
            }

            decorateUser(user);
            req.user = user;
            req.user_id = String(user._id);
            next();
        } catch (e) {
            next(e);
        }
    });
};
