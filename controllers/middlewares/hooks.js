const _ = require('lodash');
const Ajv = require('ajv');
const ac = require('../../config/auth/authorization');
const storage = require('../../helpers/storage/index');
const {
    ValidationError,
    UnauthorizedError,
} = require('../../helpers/errors/index');
const {
    Role,
} = require('../../db/models');

const accessType = ['createOwn', 'createAny', 'readOwn', 'readAny', 'updateOwn', 'updateAny', 'deleteOwn', 'deleteAny'];

module.exports = (req, res, next) => {
    // Validation /////////////////////////////////////////////////
    req.validate = (schema, data, strict = true) => {
        const ajv = new Ajv();
        const nSchema = schema;

        if (!strict) {
            delete nSchema.required;
        }
        const valid = ajv.validate(nSchema, data);

        if (!valid) {
            throw new ValidationError(null, ajv.errors);
        }
    };

    const checkFields = (fields = []) => {
        let count = fields.length;
        let positive = 0;
        let negative = 0;

        _.forEach(fields, (field) => {
            if (field === '_id' || (field.startsWith('-') && field.substr(1) === '_id')) {
                count -= 1;
                return;
            }

            if (field.startsWith('-')) {
                negative += 1;
            } else {
                positive += 1;
            }
        });

        let result = 0;
        if (negative === count) {
            result = -1;
        } else if (positive === count) {
            result = 1;
        }

        return (result);
    };

    const getAllowedFields = (userFields, permissionAttrs) => {
        const fields = _.isNil(userFields) ? [] : userFields.split(/\s/);
        const attributes = permissionAttrs;
        const fieldsType = checkFields(fields);
        let allowedAttrs = [];

        if (fieldsType === 0) {
            throw new ValidationError(0, 'Fields cannot have a mix of inclusion and exclusion');
        }

        if (attributes.indexOf('*') >= 0) {
            _.forEach(attributes, (attr) => {
                if (attr === '*') {
                    return;
                }

                const pAttr = attr.substr(1);
                const fIdx = fields.indexOf(pAttr);

                if (fIdx >= 0) {
                    fields.splice(fIdx, 1);
                }

                if (fieldsType === -1
                    || fields.length === 0
                    || (fields.indexOf('-_id') >= 0 && fields.length === 1)) {
                    fields.push(`-${pAttr}`);
                }
            });

            // add other fields
            _.forEach(fields, (field) => {
                allowedAttrs.push(field);
            });
        } else {
            allowedAttrs = attributes;
        }

        if (allowedAttrs.length > 0) {
            allowedAttrs = allowedAttrs.join(' ');
        } else {
            allowedAttrs = undefined;
        }

        return allowedAttrs;
    };

    /**
     * authorize user.roles to access select resource
     * if access needed only to own resources, will match user._id with required resource id
     * @param user: current logged in user
     * @param resource: accessControl resource
     * @param access: accessControl access string or array of strings - check accessType
     * @param predicate: predicate function
     */
    req.authorize = async (user, resource, access, predicate) => {
        try {
            if (_.isNil(user)) {
                throw new UnauthorizedError();
            }
            const permission = { granted: false, allowed: false };
            const { roles } = user;
            if (!roles) {
                throw new UnauthorizedError();
            } else {
                for (let i = 0; i < roles.length; i += 1) {
                    if (!_.isNil(roles[i])) {
                        if (!roles[i].resources) {
                            throw new UnauthorizedError();
                        } else {
                            for (let j = 0; j < roles[i].resources.length; j += 1) {
                                if (roles[i].resources[j].resource === resource) {
                                    permission.granted = true;
                                }
                                const result = _.intersection(roles[i].resources[j].accessType, access);
                                if (permission.granted && result && result.length > 0) {
                                    permission.allowed = true;
                                }
                                if (permission.granted === true && permission.allowed === true) {
                                    break;
                                } else {
                                    continue;
                                }
                            }
                            if (permission.granted === true && permission.allowed === true) {
                                break;
                            } else {
                                continue;
                            }
                        }
                    }
                }
                if (!permission.granted === true || !permission.allowed === true) {
                    throw new UnauthorizedError();
                }
            }
        } catch (err) {
            throw err;
        }
    };

    // Google storage
    req.storage = storage;

    next();
};
