const _ = require('lodash');
const express = require('express');

const router = express.Router();

const { multerMW, multipartParser } = require('./middlewares/index');
const { ValidationError, NotFoundError } = require('../helpers/errors/index');
const { objectIdSchema } = require('../helpers/validation/schema');
const PartnerService = require('../services/core/PartnerService');

const upload = multerMW({
    field: 'image',
    size: 2 * 1024 * 1024,
    count: 1,
});

router.post('/', upload, multipartParser, async (req, res, next) => {
    try {
        // await req.authorize(req.user, 'partners', ['createAny']);

        const data = req.body;

        const partner = await PartnerService.create(data, req.files);
        res.json(partner);
    } catch (err) {
        next(err);
    }
});

router.get('/', async (req, res, next) => {
    try {
        // await req.authorize(req.user, 'partners', ['readAny']);

        const params = req.query;
        params.sort = 'created_at';
        const result = await PartnerService.getAll(params);
        res.json(result);
    } catch (err) {
        next(err);
    }
});

router.get('/:id', async (req, res, next) => {
    try {
        req.validate(objectIdSchema, req.params);
        // await req.authorize(req.user, 'partners', ['readAny']);

        const partner = await PartnerService.getOne(req.params.id);
        if (_.isNil(partner)) {
            throw new NotFoundError();
        }

        res.json(partner);
    } catch (err) {
        next(err);
    }
});

router.put('/:id', upload, multipartParser, async (req, res, next) => {
    try {
        req.validate(objectIdSchema, req.params);
        // await req.authorize(req.user, 'partners', ['updateAny']);

        const data = req.body;
        const nPartner = await PartnerService.update(req.params.id, data, req.files);
        // if (_.isNil(nUser)) {
        //     throw new NotFoundError();
        // }

        res.json(nPartner);
    } catch (err) {
        next(err);
    }
});

router.delete('/:id', async (req, res, next) => {
    try {
        req.validate(objectIdSchema, req.params);
        await req.authorize(req.user, 'partners', ['deleteAny']);

        await PartnerService.delete(req.params.id);
        res.sendStatus(200);
    } catch (err) {
        next(err);
    }
});

module.exports = router;
