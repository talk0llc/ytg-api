const _ = require('lodash');
const express = require('express');

const router = express.Router();

const { multerMW, multipartParser } = require('./middlewares/index');
// const { pNewsBasicData } = require('../helpers/projections/index');
// Validation
const { ValidationError, NotFoundError } = require('../helpers/errors/index');
const { objectIdSchema, EventSchema } = require('../helpers/validation/schema');
const EventService = require('../services/core/EventService');


const upload = multerMW({
    field: 'images',
    size: 2 * 1024 * 1024,
    count: 3,
});

router.post('/', upload, multipartParser, async (req, res, next) => {
    try {
        await req.authorize(req.user, 'events', ['createAny']);

        const data = req.body;
        req.validate(EventSchema, data);

        const event = await EventService.create(data, req.files);
        res.json(event);
        // res.json(pNewsBasicData.pickFrom(employee));
    } catch (err) {
        next(err);
    }
});

router.get('/', async (req, res, next) => {
    try {
        // await req.authorize(req.user, 'events', ['readAny']);

        const params = req.query;
        params.sort = 'created_at';
        const result = await EventService.getAll(params);
        res.json(result);
    } catch (err) {
        next(err);
    }
});
router.get('/applications', async (req, res, next) => {
    try {
        await req.authorize(req.user, 'applications', ['readAny']);
        const params = req.query;
        params.sort = { created_at: 'desc' };
        const applications = await EventService.getUserApplications(params);
        res.json(applications);
    } catch (err) {
        next (err);
    }
});

router.get('/:id', async (req, res, next) => {
    try {
        req.validate(objectIdSchema, req.params);
        // await req.authorize(req.user, 'events', ['readAny']);

        const event = await EventService.getOne(req.params.id);
        if (_.isNil(event)) {
            throw new NotFoundError();
        }
        res.json(event);
        // res.json(pNewsBasicData.pickFrom(user));
    } catch (err) {
        next(err);
    }
});

router.put('/:id', upload, multipartParser, async (req, res, next) => {
    try {
        req.validate(objectIdSchema, req.params);
        await req.authorize(req.user, 'events', ['updateAny']);

        const data = req.body;
        req.validate(EventSchema, data, false);
        const event = await EventService.update(req.params.id, data, req.files);

        res.json(event);
        // res.json(pNewsBasicData.pickFrom(user));
    } catch (err) {
        next(err);
    }
});

router.delete('/:id', async (req, res, next) => {
    try {
        req.validate(objectIdSchema, req.params);
        await req.authorize(req.user, 'events', ['deleteAny']);

        await EventService.delete(req.params.id);
        res.sendStatus(200);
    } catch (err) {
        next(err);
    }
});

router.put('/:id/questions', async (req, res, next) => {
    try {
        await req.authorize(req.user, 'events', ['updateAny']);
        const event = await EventService.addQuestion(req.params.id, req.body);
        res.json(event);
    } catch (err) {
        next(err);
    }
});

router.get('/:id/questions', async (req, res, next) => {
    try {
        await req.authorize(req.user, 'events', ['readAny']);

        const event = await EventService.getQuestions(req.params.id);
        res.json(event);
    } catch (err) {
        next(err);
    }
});

router.delete('/:id/questions/:questionId', async (req, res, next) => {
    try {
        await req.authorize(req.user, 'events', ['deleteAny']);

        const event = await EventService.deleteQuestion(req.params.id, req.params.questionId);
        res.json(event);
    } catch (err) {
        next(err);
    }
});

router.post('/:id/questions', async (req, res, next) => {
    try {
        await req.authorize(req.user, 'applications', ['createAny']);

        const answer = await EventService.submitAnswer(req.params.id, req.user_id, req.body);
        res.json(answer);
    } catch (err) {
        next (err);
    }
});

router.delete('/:id/sections/:sectionId', async (req, res, next) => {
    try {
        await req.authorize(req.user, 'events', ['deleteAny']);

        const section = await EventService.deleteSection(req.params.id, req.params.sectionId);
        res.json(section);
    } catch (err) {
        next (err);
    }
});

router.get('/:id/applications/:applicationId', async (req, res, next) => {
    try {
        await req.authorize(req.user, 'applications', ['readAny']);

        const applications = await EventService.getUserApplication(req.params.id, req.params.applicationId);
        res.json(applications);
    } catch (err) {
        next (err);
    }
});

router.put('/:id/applications/:applicationId', async (req, res, next) => {
    try {
        await req.authorize(req.user, 'applications', ['updateAny']);

        const data = req.body;
        const application = EventService.updateUserApplicationStatus(req.params.applicationId, data);
        res.json(application);
    } catch (err) {
        next(err);
    }
});

router.delete('/:id/applications/:applicationId', async (req, res, next) => {
    try {
        // await req.authorize(req.user, 'applications', ['updateAny']);
        EventService.deleteUserApplicationStatus(req.params.applicationId);
        res.sendStatus(200);
    } catch (err) {
        next(err);
    }
});

module.exports = router;
