const _ = require('lodash');
const express = require('express');

const router = express.Router();

const { multerMW, multipartParser } = require('./middlewares/index');
// const { pNewsBasicData } = require('../helpers/projections/index');
// Validation
const { ValidationError, NotFoundError } = require('../helpers/errors/index');
const { objectIdSchema, NewsSchema } = require('../helpers/validation/schema');
const NewsService = require('../services/core/NewsService');


const upload = multerMW({
    field: 'images',
    size: 2 * 1024 * 1024,
    count: 3,
});

router.post('/', upload, multipartParser, async (req, res, next) => {
    try {
        await req.authorize(req.user, 'news', ['createAny']);

        const data = req.body;
        req.validate(NewsSchema, data);

        const news = await NewsService.create(data, req.files);
        res.json(news);
        // res.json(pNewsBasicData.pickFrom(employee));
    } catch (err) {
        next(err);
    }
});

router.get('/', async (req, res, next) => {
    try {
        // await req.authorize(req.user, 'news', ['readAny']);

        const params = req.query;
        params.sort = 'created_at';
        const result = await NewsService.getAll(params);
        res.json(result);
    } catch (err) {
        next(err);
    }
});

router.get('/:id', async (req, res, next) => {
    try {
        req.validate(objectIdSchema, req.params);
        // await req.authorize(req.user, 'news', ['readAny']);

        const news = await NewsService.getOne(req.params.id);
        if (_.isNil(news)) {
            throw new NotFoundError();
        }
        res.json(news);
        // res.json(pNewsBasicData.pickFrom(user));
    } catch (err) {
        next(err);
    }
});

router.put('/:id', upload, multipartParser, async (req, res, next) => {
    try {
        req.validate(objectIdSchema, req.params);
        await req.authorize(req.user, 'news', ['updateAny']);

        const data = req.body;
        req.validate(NewsSchema, data, false);
        const news = await NewsService.update(req.params.id, data, req.files);

        res.json(news);
        // res.json(pNewsBasicData.pickFrom(user));
    } catch (err) {
        next(err);
    }
});

router.delete('/:id', async (req, res, next) => {
    try {
        req.validate(objectIdSchema, req.params);
        await req.authorize(req.user, 'news', ['deleteAny']);

        await NewsService.delete(req.params.id);
        res.sendStatus(200);
    } catch (err) {
        next(err);
    }
});

router.delete('/:id/sections/:sectionId', async (req, res, next) => {
    try {
        await req.authorize(req.user, 'news', ['deleteAny']);

        const section = await NewsService.deleteSection(req.params.id, req.params.sectionId);
        res.json(section);
    } catch (err) {
        next (err);
    }
});
module.exports = router;
