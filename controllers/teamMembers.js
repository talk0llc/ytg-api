const _ = require('lodash');
const express = require('express');

const router = express.Router();

const { multerMW, multipartParser } = require('./middlewares/index');
// Validation
const { ValidationError, NotFoundError } = require('../helpers/errors/index');
const { objectIdSchema } = require('../helpers/validation/schema');
const TeamMemberService = require('../services/core/TeamMemberService');

// const upload = multerMW({
//     field: 'images',
//     size: 2 * 1024 * 1024,
//     count: 3,
// });

const upload = multerMW({
    field: 'images',
    size: 2 * 1024 * 1024,
    count: 3,
});

router.post('/', upload, multipartParser, async (req, res, next) => {
    try {
        await req.authorize(req.user, 'members', ['createAny']);

        const data = req.body;
        // req.validate(EmployeeSchema, data);

        const employee = await TeamMemberService.create(data, req.files);
        res.json(employee);
    } catch (err) {
        next(err);
    }
});

router.get('/', async (req, res, next) => {
    try {
        // await req.authorize(req.user, 'members', ['readAny']);

        const params = req.query;
        params.collation = { locale: 'en' };
        params.sort = { name: 'asc' };
        const result = await TeamMemberService.getAll(params);
        res.json(result);
    } catch (err) {
        next(err);
    }
});

router.get('/:id', async (req, res, next) => {
    try {
        req.validate(objectIdSchema, req.params);
        // await req.authorize(req.user, 'members', ['readAny']);

        const user = await TeamMemberService.getById(req.params.id);
        if (_.isNil(user)) {
            throw new NotFoundError();
        }

        res.json(user);
    } catch (err) {
        next(err);
    }
});

router.put('/:id', upload, multipartParser, async (req, res, next) => {
    try {
        req.validate(objectIdSchema, req.params);
        await req.authorize(req.user, 'members', ['updateAny']);

        const data = req.body;
        // req.validate(EmployeeSchema, data, false);
        const nUser = await TeamMemberService.update(req.params.id, data, req.files);
        // if (_.isNil(nUser)) {
        //     throw new NotFoundError();
        // }

        res.json(nUser);
    } catch (err) {
        next(err);
    }
});

router.delete('/:id', async (req, res, next) => {
    try {
        req.validate(objectIdSchema, req.params);
        await req.authorize(req.user, 'members', ['deleteAny']);

        await TeamMemberService.delete(req.params.id);
        res.sendStatus(200);
    } catch (err) {
        next(err);
    }
});

module.exports = router;
