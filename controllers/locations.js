const _ = require('lodash');
const express = require('express');

const router = express.Router();
const { NotFoundError } = require('../helpers/errors');
const { LocationService } = require('../services');

router.get('/countries', async (req, res, next) => {
    try {
        const params = req.query;
        params.fields = '-cities';

        const countries = await LocationService.getAll(params, false);
        return res.json(countries);
    } catch (err) {
        next(err);
    }
});

router.get('/countries/:id', async (req, res, next) => {
    try {
        const params = req.query;
        params.fields = '-cities';

        const country = await LocationService.getOne(req.params.id, params);
        if (_.isNil(country)) {
            throw new NotFoundError();
        }

        res.json(country);
    } catch (err) {
        next(err);
    }
});

router.get('/countries/:id/cities', async (req, res, next) => {
    try {
        const country = await LocationService.getOne(req.params.id, req.query);
        if (_.isNil(country)) {
            throw new NotFoundError();
        }

        const cities = _.filter(country.cities || [], { is_active: true });
        res.json(cities);
    } catch (err) {
        next(err);
    }
});

module.exports = router;
