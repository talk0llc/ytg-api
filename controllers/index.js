const _ = require('lodash');
const path = require('path');
const express = require('express');

const User = require('mongoose').model('User');
const Employee = require('mongoose').model('Employee');
const request = require('superagent');
const config = require('../config');
const utils = require('../helpers/utils');
const { authenticationMW } = require('./middlewares');

const constants = require('../config/constants');
// const recaptcha = require('../config/keys/recaptcha.json');


const { AuthService } = require('../services');

const { pUserBasicData } = require('../helpers/projections');
const {
    loginSchema,
    forgotPasswordSchema,
    userPasswordResetSchema,
    userVerifySchema,
    registerSchema,
} = require('../helpers/validation/user');
const {
    UnauthenticatedError,
} = require('../helpers/errors/index');

const router = express.Router();

const docsController = async (req, res, next) => {
    try {
        const view = path.join(__dirname, '../docs/swagger-ui/index.html');
        return res.sendFile(view);
    } catch (err) {
        return next(err);
    }
};

// const verifyGoogleCaptcha = req => new Promise((resolve) => {
//     const secretKey = recaptcha.SecretKey;
//     const verifyUrl = `${recaptcha.URL}?secret=${secretKey}&response=${req.body.captcha}&remoteip=${req.connection.remoteAddress}`;
//     request
//         .post(verifyUrl)
//         .set('accept', 'json')
//         .end((err, res) => {
//             if (!res.body && err) {
//                 resolve(false);
//             }
//             const result = res.body;
//             if (result.success !== undefined && !result.success) {
//                 resolve(false);
//             } else {
//                 resolve(true);
//             }
//         });
// });

// router.post('/register-web', async (req, res, next) => {
//     try {
//         const data = req.body;
//         // Validate data
//         req.validate(registerSchema, data);
//
//         const user = await User.findOne({
//             $or: [{
//                 email: { $ne: undefined, $eq: data.email },
//             }, {
//                 mobile: { $ne: undefined, $eq: data.mobile },
//             }],
//         }).exec();
//
//         if (!_.isNil(user)) {
//             throw new ValidationError(ErrorCodes.userExists, 'User already exists');
//         }
//
//         const uData = {
//             email: data.email,
//             mobile: data.mobile,
//             first_name: data.first_name,
//             last_name: data.last_name,
//             password: data.password,
//             roles: ['user'],
//             auth_type: 'local',
//         };
//
//         switch (data.auth_type) {
//             case 'google':
//                 uData.google = { id: data.google.id };
//                 break;
//             case 'facebook':
//                 uData.facebook = { id: data.facebook.id };
//                 break;
//             default:
//         }
//
//         // Create user
//         const userObj = new User(uData);
//         const userDoc = await userObj.save();
//
//         // // Notify user by sms asynchronously (no wait)
//         userNofications.sendVerificationSms(userDoc);
//
//         res.json(pUserBasicData.pickFrom(userDoc));
//     } catch (err) {
//         next(err);
//     }
// });

// router.post('/register-captcha', async (req, res, next) => {
//     try {
//         const data = req.body;
//         if (_.isNil(data.captcha)) {
//             return res.json({ success: false, msg: 'Please select captcha' });
//         }
//
//         const isVerified = await verifyGoogleCaptcha(req);
//         if (isVerified) {
//             // register user
//             res.json({ success: true });
//         } else {
//             return res.json({ success: false, msg: 'invalid Captcha' });
//         }
//     } catch (err) {
//         next(err);
//     }
// });

router.get('/', async (req, res) => {
    const view = path.join(`${__dirname}/client/index.html`);
    res.sendFile(view);
});

router.get('/swagger', async (req, res) => {
    const view = path.join(`${__dirname}/docs/swagger-ui/index.html`);
    res.sendFile(view);
});

router.get('/images/uploads/:imageId', async (req, res) => {
    const view = path.join(`${global.rootPath}/images/uploads/${req.params.imageId}`);
    res.sendFile(view);
});

router.get('/images/slider/:imageId', async (req, res) => {
    const view = path.join(`${global.rootPath}/images/slider/${req.params.imageId}`);
    res.sendFile(view);
});

router.get('/images/logos/:imageId', async (req, res) => {
    const view = path.join(`${global.rootPath}/images/logos/${req.params.imageId}`);
    res.sendFile(view);
});

router.post('/register', async (req, res, next) => {
    try {
        // await req.authorize(req.user, 'register', ['createOwn']);
        const data = req.body;
        req.validate(registerSchema, data);

        const user = await AuthService.register(data);
        res.json(user);
    } catch (err) {
        next(err);
    }
});

router.post('/login', async (req, res, next) => {
    try {
        const data = req.body;
        req.validate(loginSchema, data);

        const result = await AuthService.login(data);
        res.json(result);
    } catch (err) {
        next(err);
    }
});

router.post('/forgot_password', async (req, res, next) => {
    try {
        const data = req.body;
        req.validate(forgotPasswordSchema, data);

        await AuthService.forgotPassword(data);
        res.sendStatus(200);
    } catch (err) {
        next(err);
    }
});

router.post('/reset_password', async (req, res, next) => {
    try {
        const data = req.body;
        req.validate(userPasswordResetSchema, data);

        await AuthService.resetPassword(data);
        res.sendStatus(200);
    } catch (err) {
        next(err);
    }
});

router.post('/resend_sms', async (req, res, next) => {
    try {
        const token = req.headers.authorization;
        if (_.isNil(token)) {
            return next(new UnauthenticatedError('Missing authorization header'));
        }

        await AuthService.resendSms(token);
        res.sendStatus(200);
    } catch (err) {
        next(err);
    }
});

router.post('/verify', async (req, res, next) => {
    try {
        const { authorization } = req.headers;
        const data = {
            authorization,
            ...req.body,
        };

        req.validate(userVerifySchema, data);

        await AuthService.verify(data);
        res.sendStatus(200);
    } catch (err) {
        next(err);
    }
});

router.post('/logout', authenticationMW, async (req, res, next) => {
    try {
        const token = req.headers.authorization;
        if (_.isNil(token)) {
            return next(new UnauthenticatedError('Missing authorization header'));
        }
        await AuthService.logout(req.user);
        res.sendStatus(200);
    } catch (err) {
        next(err);
    }
});

router.get('/static/constants', authenticationMW, async (req, res, next) => {
    try {
        await req.authorize(req.user, 'constants', ['readAny']);
        res.json(constants);
    } catch (err) {
        next(err);
    }
});

// Kubernetes health check
router.get('/healthz', async (req, res) => {
    res.sendStatus(200);
});

if (utils.inDevelopment()) {
    router.get('/docs', docsController);
}

module.exports = router;
