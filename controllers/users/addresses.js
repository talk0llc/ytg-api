const _ = require('lodash');
const express = require('express');
const { ObjectId } = require('mongoose').mongo;
const { UserAddressService } = require('../../services');

const router = express.Router({ mergeParams: true });

// Validation
const {
    addressUserId,
    addressParams,
    addressSchema,
} = require('../../helpers/validation/user');
const {
    UnauthorizedError,
    ValidationError,
} = require('../../helpers/errors/index');

router.post('/', async (req, res, next) => {
    try {
        req.validate(addressUserId, req.params);
        req.validate(addressSchema, req.body);
        await req.authorize(req.user, 'userAddress', ['createAny', 'createOwn'], () => req.params.userId === req.user_id);

        const data = req.body;
        const address = await UserAddressService.create(req.params.userId, data);
        res.send(address);
    } catch (err) {
        next(err);
    }
});

router.get('/', async (req, res, next) => {
    try {
        req.validate(addressUserId, req.params);
        await req.authorize(req.user, 'userAddress', ['readAny', 'readOwn'], () => req.params.userId === req.user_id);

        const addresses = await UserAddressService.getAll(req.params.userId, req.query);
        res.send(addresses);
    } catch (err) {
        next(err);
    }
});

router.get('/:id', async (req, res, next) => {
    try {
        req.validate(addressParams, req.params);
        await req.authorize(req.user, 'userAddress', ['readAny', 'readOwn'], () => req.params.userId === req.user_id);

        const addresses = await UserAddressService.getById(req.params.userId, req.params.id, req.query);
        res.send(addresses);
    } catch (err) {
        next(err);
    }
});

router.put('/:id', async (req, res, next) => {
    try {
        req.validate(addressParams, req.params);
        req.validate(addressSchema, req.body, false);
        await req.authorize(req.user, 'userAddress', ['updateAny', 'updateOwn'], () => req.params.userId === req.user_id);

        const address = await UserAddressService.update(req.params.userId, req.params.id, req.body);
        res.json(address);
    } catch (err) {
        next(err);
    }
});

router.delete('/:id', async (req, res, next) => {
    try {
        req.validate(addressParams, req.params);
        await req.authorize(req.user, 'userAddress', ['deleteAny', 'deleteOwn'], () => req.params.userId === req.user_id);

        await UserAddressService.delete(req.params.userId, req.params.id);
        res.send(200);
    } catch (err) {
        next(err);
    }
});

module.exports = router;
