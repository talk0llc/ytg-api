const _ = require('lodash');
const express = require('express');
const UserService = require('../../services/core/UserService');

const router = express.Router();

const { multerMW, multipartParser } = require('../middlewares');
const { pUserLogin, pUserBasicData } = require('../../helpers/projections');
// Validation
const {
    objectIdSchema,
    userSchema,
    registerSchema,
} = require('../../helpers/validation/user');
const { NotFoundError } = require('../../helpers/errors/index');

const upload = multerMW({
    field: 'images',
    size: 2 * 1024 * 1024,
    count: 3,
});

// TODO - image upload
router.post('/', upload, multipartParser, async (req, res, next) => {
    try {
        await req.authorize(req.user, 'users', ['createAny']);

        const data = req.body;
        req.validate(registerSchema, data);

        const user = await UserService.create(data, req.files);
        res.json(pUserLogin.pickFrom(user));
    } catch (err) {
        next(err);
    }
});

router.get('/', async (req, res, next) => {
    try {
        await req.authorize(req.user, 'users', ['readAny']);
        const data = req.query;
        data.sort = 'created_at';
        const result = await UserService.getAll(data);
        res.json(result);
    } catch (err) {
        next(err);
    }
});

router.get('/:id', async (req, res, next) => {
    try {
        req.validate(objectIdSchema, req.params);
        await req.authorize(req.user, 'users', ['readAny'], () => req.user_id === req.params.id);

        const user = await UserService.getById(req.params.id, req.query);
        if (_.isNil(user)) {
            throw new NotFoundError();
        }
        // res.json(pUserBasicData.pickFrom(user));
        res.json(user);
    } catch (err) {
        next(err);
    }
});

router.put('/:id', upload, multipartParser, async (req, res, next) => {
    try {
        req.validate(objectIdSchema, req.params);
        // req.validate(userSchema, req.body, false);
        await req.authorize(req.user, 'users', ['updateAny'], () => req.user_id === req.params.id);

        const data = req.body;
        const user = await UserService.update(req.params.id, data, req.files);
        res.json(user);
    } catch (err) {
        next(err);
    }
});

router.delete('/:id', async (req, res, next) => {
    try {
        req.validate(objectIdSchema, req.params);
        await req.authorize(req.user, 'users', ['deleteAny']);

        await UserService.delete(req.params.id);
        res.send(200);
    } catch (err) {
        next(err);
    }
});

router.get('/:id/profile', async (req, res, next) => {
    try {
        req.validate(objectIdSchema, req.params);
        await req.authorize(req.user, 'users', ['readAny'], () => req.user_id === req.params.id);

        let user = await UserService.getProfileById(req.params.id, req.query);
        if (_.isNil(user)) {
            throw new NotFoundError();
        }

        user = _.omit(user, ['password', 'reset_token', 'reset_token_expiration', 'reset_token_time']);
        res.json(user);
    } catch (err) {
        next(err);
    }
});

module.exports = router;
