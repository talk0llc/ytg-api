const express = require('express');

const router = express.Router();
const { AuthService } = require('../../services');
const { pUserLogin } = require('../../helpers/projections');

router.post('/google', async (req, res, next) => {
    try {
        const data = req.body;
        const result = await AuthService.authGoogle(data, req.query);
        res.json(pUserLogin.pickFrom(result));
    } catch (err) {
        next(err);
    }
});

router.post('/facebook', async (req, res, next) => {
    try {
        const data = req.body;

        const result = await AuthService.authFacebook(data, req.query);
        res.json(pUserLogin.pickFrom(result));
    } catch (err) {
        next(err);
    }
});

module.exports = router;
