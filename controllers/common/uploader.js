const _ = require('lodash');
const { ObjectId } = require('mongoose').mongo;
const express = require('express');
const fs = require('fs');
const fileType = require('file-type');
const path = require('path');

const router = express.Router();
const ContentService = require('../../services/core/ContentService');
const { multerMW, multipartParser } = require('../middlewares/index');
const {
    Contents,
} = require('../../db/models');

const slider = multerMW({
    field: 'image',
    size: 5 * 1024 * 1024,
    count: 5,
    dest: 'images/slider',
});

router.post('/slider', slider, multipartParser, async (req, res, next) => {
    try {
        const data = req.body;
        data.image = '';
        if (req.files && req.files.length > 0) {
            for (let i = 0; i < req.files.length; i++) {
                data.image = `${req.files[i].destination}/${req.files[i].filename}`;
            }
        }
        data.key = 'slider';
        // if (_.isNil(data.index)) {
        //     const count = await Contents.getCount();
        //
        //     // Update data
        //     data.index = count;
        // }
        const content = await Contents.create(data);
        res.json(content);
    } catch (err) {
        next(err);
    }
});

router.get('/slider', async (req, res, next) => {
    try {
        const contents = await Contents.getAll({ key: 'slider' });
        res.json(contents);
        // const fileUrls = [];
        // const directoryPath = 'images/slider';
        // fs.readdir(directoryPath, async (err, files) => {
        //     files.forEach((file) => {
        //         fileUrls.push(`${directoryPath}/${file}`);
        //     });
        //     res.json(fileUrls);
        // });
        // res.sendStatus(200);
    } catch (err) {
        next(err);
    }
});

router.delete('/deleteSlider/:id', async (req, res, next) => {
    try {
        // Get content
        const content = await ContentService.delete(ObjectId(req.params.id));
        res.json(content);
    } catch (err) {
        next(err);
    }
});

const logos = multerMW({
    field: 'image',
    size: 5 * 1024 * 1024,
    count: 10,
    dest: 'images/logos',
});

router.post('/logos', logos, multipartParser, async (req, res, next) => {
    try {
        const data = req.body;
        if (req.files && req.files.length > 0) {
            for (let i = 0; i < req.files.length; i++) {
                data.image = `${req.files[i].destination}/${req.files[i].filename}`;
            }
        }
        data.key = 'logo';
        // if (_.isNil(data.index)) {
        //     const count = await Contents.getCount();
        //
        //     // Update data
        //     data.index = count;
        // }
        const content = await Contents.create(data);
        res.json(content);
    } catch (err) {
        next(err);
    }
});

router.get('/logos', async (req, res, next) => {
    try {
        // const fileUrls = [];
        // const directoryPath = 'images/logos';
        // fs.readdir(directoryPath, async (err, files) => {
        //     files.forEach((file) => {
        //         fileUrls.push(`${directoryPath}/${file}`);
        //     });
        //     res.json(fileUrls);
        // });
        // res.sendStatus(200);
        const contents = await Contents.getAll({ key: 'logo' });
        res.json(contents);
    } catch (err) {
        next(err);
    }
});

router.delete('/deleteLogos/:id', async (req, res, next) => {
    try {
        // Get content
        const content = await ContentService.delete(ObjectId(req.params.id));
        res.json(content);
    } catch (err) {
        next(err);
    }
});

router.put('/saveIVLink', async (req, res, next) => {
    try {
        const data = req.body;
        const result = await ContentService.saveIVLink(data);
        res.json(result);
    } catch (e) {
        next(e);
    }
});

module.exports = router;
