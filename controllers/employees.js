const _ = require('lodash');
const express = require('express');

const router = express.Router();

const { multerMW, multipartParser } = require('./middlewares/index');
const { pEmployeeBasicData } = require('../helpers/projections/index');
// Validation
const { ValidationError, NotFoundError } = require('../helpers/errors/index');
const { objectIdSchema, EmployeeSchema } = require('../helpers/validation/schema');
const EmployeeService = require('../services/core/EmployeeService');

const upload = multerMW({
    field: 'images',
    size: 2 * 1024 * 1024,
    count: 3,
});

router.post('/', upload, multipartParser, async (req, res, next) => {
    try {
        await req.authorize(req.user, 'employees', ['createAny']);

        const data = req.body;
        req.validate(EmployeeSchema, data);

        const employee = await EmployeeService.create(data, req.files);
        res.json(employee);
    } catch (err) {
        next(err);
    }
});

router.get('/', async (req, res, next) => {
    try {
        // await req.authorize(req.user, 'employees', ['readAny']);

        const params = req.query;
        params.sort = 'created_at';
        params.user = req.user;
        const result = await EmployeeService.getAll(params);
        res.json(result);
    } catch (err) {
        next(err);
    }
});

router.get('/:id', async (req, res, next) => {
    try {
        req.validate(objectIdSchema, req.params);
        // await req.authorize(req.user, 'employees', ['readAny']);

        const user = await EmployeeService.getById(req.params.id);
        if (_.isNil(user)) {
            throw new NotFoundError();
        }

        res.json(user);
    } catch (err) {
        next(err);
    }
});

router.put('/:id', upload, multipartParser, async (req, res, next) => {
    try {
        req.validate(objectIdSchema, req.params);
        await req.authorize(req.user, 'employees', ['updateAny'], async () => req.user_id === req.params.id);

        const data = req.body;
        req.validate(EmployeeSchema, data, false);
        const nUser = await EmployeeService.update(req.params.id, data, req.files);
        // if (_.isNil(nUser)) {
        //     throw new NotFoundError();
        // }

        res.json(nUser);
    } catch (err) {
        next(err);
    }
});

router.delete('/:id', async (req, res, next) => {
    try {
        req.validate(objectIdSchema, req.params);
        await req.authorize(req.user, 'employees', ['deleteAny']);

        await EmployeeService.delete(req.params.id);
        res.sendStatus(200);
    } catch (err) {
        next(err);
    }
});

module.exports = router;
