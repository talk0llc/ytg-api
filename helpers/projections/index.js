const Projection = require('./projection');

const baseUser = '_id email mobile first_name last_name roles token image images auth_type restaurant wallet referral_id preferences follow orders is_mobile_verified is_email_verified is_verified is_active paymentCards';

module.exports = {
    pUserLogin: new Projection(baseUser),
    pUserBasicData: new Projection(baseUser),
    pUserRegister: new Projection(baseUser),
};
