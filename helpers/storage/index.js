const _ = require('lodash');
const path = require('path');
const uniqid = require('uniqid');
const GoolgeStorage = require('@google-cloud/storage');
const config = require('../../config');

const storage = GoolgeStorage({
    projectId: config.storage.project_id,
    keyFilename: config.storage.keyFilename,
});

class Storage {
    constructor() {
        this.bucket = storage.bucket(config.storage.bucket);
    }

    getPublicUrl(filename) {
        return `${config.storage.url}/${filename}`;
    }

    /**
     * Upload single file to google cloud storage
     * @param file
     * @returns {Promise<void>}
     */
    async upload(file) {
        const ext = path.extname(file.originalname);

        const gcUrl = await new Promise((resolve, reject) => {
            const gcName = `${uniqid()}${ext}`;
            const gcFile = this.bucket.file(gcName);

            const stream = gcFile.createWriteStream({
                metadata: {
                    contentType: file.mimetype,
                },
            });

            stream.on('error', (err) => {
                reject(err);
            });

            stream.on('finish', () => {
                gcFile.makePublic().then(() => {
                    const url = this.getPublicUrl(gcName);
                    resolve(url);
                });
            });

            stream.end(file.buffer);
        });

        return gcUrl;
    }

    async delete(urls) {
        try {
            let rUrls = urls;

            if (_.isNil(urls) || urls.length === 0) {
                return;
            }

            if (!_.isArray(urls)) {
                rUrls = [urls];
            }

            const tasks = [];
            for (let i = 0; i < rUrls.length; i += 1) {
                if (_.isNil(rUrls[i])) {
                    continue;
                }
                const idx = rUrls[i].lastIndexOf('/') + 1;
                const fileName = rUrls[i].substring(idx);

                tasks.push(new Promise((resolve, reject) => {
                    const file = this.bucket.file(fileName);

                    file.delete((err, res) => {
                        if (err && err.code !== 404) {
                            reject(err);
                        }

                        resolve();
                    });
                }));
            }

            await Promise.all(tasks);
        } catch (err) {
            throw err;
        }
    }

    /**
     * Upload file(s) to google storage buckets
     * @param files: req.files
     * @returns {Promise<boolean>}
     */
    async save(files) {
        try {
            let rFiles = files;

            if (_.isNil(files)) {
                return;
            }

            if (!_.isArray(files)) {
                rFiles = [files];
            }

            const tasks = [];
            for (let i = 0; i < rFiles.length; i += 1) {
                const file = rFiles[i];
                tasks.push(this.upload(file));
            }

            return await Promise.all(tasks);
        } catch (err) {
            throw err;
        }
    }
}

module.exports = new Storage();
