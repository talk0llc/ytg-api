const _ = require('lodash');
const FCM = require('fcm-push');

const BaseNotification = require('./baseNotification');
const { serverKey } = require('../../config').firebase;

class PushNotification extends BaseNotification {
    constructor() {
        super();
        this.fcm = new FCM(serverKey);
    }

    /**
     * Config params:
     * @param to: array of user tokens or topics to send messages to
     * @param data: Object contains key value pair to be send as payload
     * @param notification: Object contains [title, body] to be sent as push notification
     * @returns {Promise<void>}
     */
    async send(config) {
        try {
            const obj = config;
            if (!_.isArray(obj.to)) {
                obj.to = [obj.to];
            }

            const nData = {
                registration_ids: obj.to,
                data: obj.data,
            };

            if (obj.notification) {
                nData.notification = obj.notification;
            }

            await this.fcm.send(nData);
        } catch (err) {
            console.log(err);
            throw err;
        }
    }
}

module.exports = new PushNotification();
