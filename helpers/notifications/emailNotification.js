const fs = require('fs');
const path = require('path');
const nodemailer = require('nodemailer');

const BaseNotification = require('./baseNotification');

class Email extends BaseNotification {
    constructor() {
        super();
        this.setupNodemailer();
        this.loadTemplates();
    }

    setupNodemailer() {
        this.transporter = nodemailer.createTransport({
            host: 'smtp.gmail.com',
            secure: true,
            auth: {
                type: 'OAuth2',
                user: 'info@ytg.eco', // Your gmail address.
                clientId: '16714531593-4f0uas2jfc4j79ujdaes2nasrnjfgs7o.apps.googleusercontent.com',
                clientSecret: '9ATqt3JhBFoQet3mlkmlzyZB',
                refreshToken: '1/hw-0OwE4BFoniFV0lRaQ1IEEbygoUpjwYXDowQ9yiJU',
                accessToken: 'ya29.GltoBrpxZ7xN4W3-Yrh6BZj9tyb4QbVUuAvd1l5vav9UTQVZem3bPHpovrp3rnEjFAVdG-fG2QQRkHOCB7zGUttw5GwqkCVc63ieveSkaPKfe2oh1Dfnqnii00FO',
            },
        });
    }

    loadTemplates() {
        this.templates = {
            emailVerification: fs.readFileSync(path.join(__dirname, '../../config/templates/email_verification.html'), 'utf8'),
            passwordReset: fs.readFileSync(path.join(__dirname, '../../config/templates/password_reset.html'), 'utf8'),
            applicationStatus: fs.readFileSync(path.join(__dirname, '../../config/templates/ytg-email.html'), 'utf8'),
        };
    }

    buildMessage(config) {
        const message = {
            to: config.to,
            from: 'youthinkgreen Egypt <info@ytg.eco>',
            subject: '',
            html: '',
        };

        switch (config.type) {
            case 'password_reset':
                message.subject = `${config.subject}`;
                message.html = this.templates.passwordReset;
                message.html = message.html.replace('{{username}}', config.name);
                message.html = message.html.replace('{{token}}', config.token);
                break;
            case 'application_status':
                message.subject = `${config.subject}`;
                message.html = this.templates.applicationStatus;
                message.html = message.html.replace('{{dear}}', config.dear);
                message.html = message.html.replace('{{body}}', config.body);
                message.html = message.html.replace('{{thanks}}', config.thanks);
                break;
            case 'email_verification':
                message.subject = `${config.subject}`;
                message.html = this.templates.emailVerification;
                message.html = message.html.replace('{{username}}', config.name);
                message.html = message.html.replace('{{token}}', config.token);
                break;
            default:
        }

        return message;
    }

    /**
     * @param config
     * {
     *    type: email type [ email_verification ,password_reset]
     *    to: user email
     *    name: user name
     *    token: reset token for user
     * }
     * @returns {Promise<[ClientResponse , {}]>}
     */
    async send(config) {
        const message = this.buildMessage(config);
        this.transporter.sendMail(message, (error, res, info) => {
            if (error) {
                console.log('Error', error);
            } else {
                console.log('Message %s sent: %s', info.messageId, info.response);
            }
        });
        this.transporter.close();
    }
}

module.exports = new Email();
