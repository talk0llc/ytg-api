const emailNotification = require('./emailNotification');
const smsNotification = require('./smsNotification');
const pushNotification = require('./pushNotification');

module.exports = {
    emailNotification,
    smsNotification,
    pushNotification,
};
