const bcrypt = require('bcrypt-nodejs');
const uniqid = require('uniqid');
const jwt = require('jsonwebtoken');

const config = require('../config');

const SALTCHARS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

class Utils {
    static createHash(text) {
        const salt = bcrypt.genSaltSync(5);
        return bcrypt.hashSync(text, salt);
    }

    static compareHash(text, hash) {
        return bcrypt.compareSync(text, hash);
    }

    static generateJwtToken(data) {
        const token = jwt.sign(data, config.auth.local.key);
        return token;
    }

    static generateRandomNumber() {
        const number = Math.floor(Math.random() * 900000) + 100000;

        return number;
    }

    static generateUniqueId() {
        return uniqid();
    }
}

module.exports = Utils;
