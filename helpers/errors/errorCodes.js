module.exports = {
    userInvalidCredentials: 10,
    userInvalidSocialCredentials: 11,
    userExists: 20,
    userInactive: 21,
    userSmsDelay: 22,
};
