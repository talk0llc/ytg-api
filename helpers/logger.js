const winston = require('winston');
const { LoggingWinston } = require('@google-cloud/logging-winston');
const { stackDriver } = require('../config');

const console = new winston.transports.Console();
const loggingWinston = new LoggingWinston(stackDriver);

// Create a Winston logger that streams to Stackdriver Logging
// Logs will be written to: "projects/YOUR_PROJECT_ID/logs/winston_log"
const logger = winston.createLogger({
    level: 'info',
    transports: [
        console,
        loggingWinston,
    ],
});

module.exports = logger;
