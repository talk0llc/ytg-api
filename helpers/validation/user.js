const { objectIdSchema } = require('./common');
const constants = require('../../config/constants/user');
const accessType = ['createOwn', 'createAny', 'readOwn', 'readAny', 'updateOwn', 'updateAny', 'deleteOwn', 'deleteAny'];

const authTypeEnum = ['local', 'google', 'facebook'];

module.exports = {
    objectIdSchema,
    loginSchema: {
        type: 'object',
        properties: {
            username: {
                type: 'string',
                minLength: 3,
            },
            password: {
                type: 'string',
                minLength: 3,
            },
            auth_type: {
                type: 'string',
                enum: authTypeEnum,
            },
            type: {
                type: 'string',
                enum: ['user', 'employee'],
            },
        },
        required: ['username', 'password'],
    },
    userSchema: {
        type: 'object',
        properties: {
            email: {
                type: 'string',
                format: 'email',
                minLength: 6,
                maxLength: 255,
            },
            mobile: {
                type: 'string',
                minLength: 6,
                maxLength: 20,
                pattern: '^(\\+|0)\\d+',
            },
            first_name: {
                type: 'string',
                minLength: 2,
                maxLength: 255,
            },
            last_name: {
                type: 'string',
                minLength: 2,
                maxLength: 255,
            },
            password: {
                type: 'string',
                minLength: 6,
                maxLength: 255,
            },
            auth_type: {
                type: 'string',
                enum: authTypeEnum,
            },
            gender: {
                type: 'string',
                enum: ['Male', 'Female', 'Other'],
            },
            roles: {
                type: 'array',
                minItems: 1,
                uniqueItems: true,
                items: {
                    name: {
                        type: 'string',
                    },
                    resources: {
                        type: 'array',
                        minItems: 1,
                        items: {
                            resource: {
                                type: 'string'
                            },
                            accessType: {
                                type: 'array',
                                minItems: 1,
                                items: {
                                    type: 'string',
                                    enum: accessType
                                }
                            }
                        }
                    }
                }
            },
        },
        additionalProperties: true,
        required: ['email', 'mobile', 'password', 'first_name', 'last_name'],
    },
    userRating: {
        type: 'object',
        properties: {
            order: { pattern: '^[0-9a-fA-F]{24}$' },
            rating: { type: 'number', minimum: 1, maximum: 5 },
        },
        required: ['order', 'rating'],
    },
    registerSchema: {
        type: 'object',
        properties: {
            first_name: {
                type: 'string',
                minLength: 2,
                maxLength: 255,
            },
            last_name: {
                type: 'string',
                minLength: 2,
                maxLength: 255,
            },
            mobile: {
                type: 'string',
                minLength: 6,
                maxLength: 20,
                pattern: '^(\\+|0)\\d+',
            },
            email: {
                type: 'string',
                format: 'email',
                minLength: 6,
                maxLength: 255,
            },
            password: {
                type: 'string',
                minLength: 6,
                maxLength: 255,
            },
            auth_type: {
                type: 'string',
                enum: authTypeEnum,
            },
            captcha: {
                type: 'string',
            },
            referrer: {
                type: 'string',
                maxLength: 128,
            },
        },
        additionalProperties: true,
        required: ['email', 'password', 'first_name', 'last_name'],
    },
    forgotPasswordSchema: {
        type: 'object',
        oneOf: [
            {
                properties: {
                    type: {
                        type: 'string',
                        enum: ['user', 'employee'],
                    },
                    username: {
                        type: 'string',
                        format: 'email',
                        minLength: 6,
                        maxLength: 255,
                    },
                },
                required: ['username'],
            },
            {
                properties: {
                    type: {
                        type: 'string',
                        enum: ['user', 'employee'],
                    },
                    username: {
                        type: 'string',
                        minLength: 6,
                        maxLength: 20,
                        pattern: '^(\\+|00)\\d+',
                    },
                },
                required: ['username', 'type'],
            },
        ],
    },
    userPasswordResetSchema: {
        type: 'object',
        properties: {
            type: { type: 'string', enum: ['user', 'employee'] },
            token: { type: 'string', minLength: 16 },
            password: { type: 'string', minLength: 6 },
        },
        required: ['token', 'password'],
    },
    userVerifySchema: {
        type: 'object',
        properties: {
            authorization: { type: 'string', minLength: 16 },
            type: { type: 'string', enum: ['user', 'employee'] },
            token: { type: 'string', minLength: 6 },
        },
        required: ['token'],
    },
    userSmsVerifySchema: {
        type: 'object',
        properties: {
            authorization: { type: 'string', minLength: 16 },
            token: { type: 'string', minLength: 4 },
        },
        required: ['authorization', 'token'],
    },
    addressUserId: {
        type: 'object',
        properties: {
            userId: { pattern: '^[0-9a-fA-F]{24}$' },
        },
        required: ['userId'],
    },
    addressParams: {
        type: 'object',
        properties: {
            id: { pattern: '^[0-9a-fA-F]{24}$' },
            userId: { pattern: '^[0-9a-fA-F]{24}$' },
        },
        required: ['id', 'userId'],
    },
    addressSchema: {
        type: 'object',
        properties: {
            name: { type: 'string', minLength: 3 },
            type: { type: 'string', enum: constants.address.type },
            country: { type: 'string', maxLength: 64 },
            city: { type: 'string', maxLength: 64 },
            area: { type: 'string', maxLength: 64 },
            street: { type: 'string', maxLength: 128 },
            building: { type: 'string', maxLength: 64 },
            floor: { type: 'string', maxLength: 32 },
            apartment: { type: 'string', maxLength: 32 },
            mobile: {
                type: 'string',
                minLength: 6,
                maxLength: 20,
                pattern: '^(\\+|00)\\d+',
            },
            location: {
                type: 'object',
                properties: {
                    coordinates: {
                        type: 'array',
                        minItems: 2,
                        maxItems: 2,
                        uniqueItems: true,
                        items: {
                            type: 'number',
                            pattern: '^(\-?\d+(\.\d+)?),\s*(\-?\d+(\.\d+)?)$',
                        },
                    },
                },
                required: ['coordinates'],
            },
            landmark: { type: 'string', maxLength: 32 },
            shipping_note: { type: 'string', maxLength: 128 },
        },
        required: ['name', 'type', 'country', 'city', 'street', 'mobile'],
    },
};
