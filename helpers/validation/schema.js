const { objectIdSchema } = require('./common');
const common = require('../../config/constants/common');
const { ADMINISTRATION } = require('../../config/constants/common');

const accessType = ['createOwn', 'createAny', 'readOwn', 'readAny', 'updateOwn', 'updateAny', 'deleteOwn', 'deleteAny'];

const defaultOrderProperties = {
    user: { type: 'string', pattern: '^[0-9a-fA-F]{24}$' },
    address: { type: 'string', pattern: '^[0-9a-fA-F]{24}$' },
};
module.exports = {
    objectIdSchema,
    EmployeeSchema: {
        type: 'object',
        properties: {
            email: {
                type: 'string',
                format: 'email',
                minLength: 6,
                maxLength: 255,
            },
            password: {
                type: 'string',
                minLength: 6,
                maxLength: 64,
            },
            roles: {
                type: 'array',
                minItems: 1,
                uniqueItems: true,
                items: {
                    name: {
                        type: 'string',
                        required: true,
                    },
                    resources: {
                        type: 'array',
                        minItems: 1,
                        required: true,
                        items: {
                            resource: {
                                type: 'string',
                                required: true,
                            },
                            accessType: {
                                type: 'array',
                                minItems: 1,
                                required: true,
                                items: {
                                    type: 'string',
                                    enum: accessType,
                                },
                            },
                        },
                    },
                },
            },
            mobile: {
                type: 'string',
                minLength: 6,
                maxLength: 64,
            },
            first_name: {
                type: 'string',
                minLength: 2,
                maxLength: 255,
            },
            last_name: {
                type: 'string',
                minLength: 2,
                maxLength: 255,
            }
        },
        required: ['email', 'password', 'first_name', 'last_name'],
    },
    ProgramSchema: {
        type: 'object',
        properties: {
            name: {
                type: 'string',
            },
            description: {
                type: 'string',
            },
        },
        required: ['name', 'description'],
    },
    ServiceSchema: {
        type: 'object',
        properties: {
            name: {
                type: 'string',
            },
            description: {
                type: 'string',
            },
        },
        required: ['name', 'description'],
    },
    JobSchema: {
        type: 'object',
        properties: {
            title: {
                type: 'string',
            },
            description: {
                type: 'string',
            },
            salary: {
                type: 'number',
            },
        },
        required: ['title', 'description'],
    },
    NewsSchema: {
        type: 'object',
        properties: {
            title: {
                type: 'string',
            },
            description: {
                type: 'string',
            },
            dueDate: {
                type: 'string',
            },
        },
        required: ['title', 'description'],
    },
    EventSchema: {
        type: 'object',
        properties: {
            title: {
                type: 'string',
            },
            description: {
                type: 'string',
            },
            dueDate: {
                type: 'string',
            },
        },
        required: ['title', 'description', 'dueDate'],
    },
};
