module.exports = {
    objectIdSchema: {
        type: 'object',
        properties: {
            id: { pattern: '^[0-9a-fA-F]{24}$' },
        },
    },
};
