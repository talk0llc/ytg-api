const chai = require('chai');
const request = require('./helpers/request');
const { restaurant, employee, review, user } = require('./data');
const accounts = require('./data/accounts');
const randomGenerator = require('./helpers/random-generator');
const uniqid = require('uniqid');


const expect = chai.expect;

describe('Employee API', () => {
    let cEmployee = {};

    it('Should create an employee as (admin)', async () => {
        employee.email = `employee${uniqid()}@talk.com`;
        employee.mobile = `+00${randomGenerator.generate('numeric', 10)}`;
        return request.post(`/employees`)
            .send(employee)
            .set('Authorization', accounts.admin.token)
            .then((res) => {
                if(res.error){
                    console.log(res.error.text);
                }
                cEmployee = res.body;
                expect(res).to.have.status(200);
                expect(res.body)
                    .to.be.an('object')
                    .to.have.own.property('email')
                    .to.be.equal(employee.email);
            })
            .catch((err) => {
                throw err;
            });
    });
    
    it('Should get all employees as (admin) \033[0;32m <-- SUCCESS CASE -->', async () => {
        return request.get('/employees')
        .set('Authorization', accounts.admin.token)
        .then((res) => {
            expect(res).to.have.status(200);
            expect(res.body)
                .to.be.an('object')
                .to.have.own.property('employees')
                .to.be.an('array');
        })
        .catch((err) => {
            throw err;
        });
    });
    it('Should get all employees filter by role name as (admin) \033[0;32m <-- SUCCESS CASE -->', async () => {
        return request.get('/employees?roles=admin')
        .set('Authorization', accounts.admin.token)
        .then((res) => {
            expect(res).to.have.status(200);
            expect(res.body)
                .to.be.an('object')
                .to.have.own.property('employees')
                .to.be.an('array');
        })
        .catch((err) => {
            throw err;
        });
    });
    it('Should get one employee as (admin) \033[0;32m <-- SUCCESS CASE -->', async () => {
        return request.get(`/employees/${sampleEmployee._id}`)
        .set('Authorization', accounts.admin.token)
        .then((res) => {
            expect(res).to.have.status(200);
            expect(res.body)
                .to.be.an('object')
                .to.have.property('first_name')
                .to.be.equal(sampleEmployee.first_name);
        })
        .catch((err) => {
            throw err;
        });
    });


    it('Should not get one employee as (admin) \033[0;31m <-- ERROR CASE --> [ID NOT FOUND]', async () => {
        return request.get(`/employees/5bb481897b2d6f1690e820d6`)
        .set('Authorization', accounts.admin.token)
        .then((res) => {
            expect(res).to.have.status(404);
            
        })
        .catch((err) => {
            throw err;
        });
    });

    it('Should update employee as (admin) \033[0;32m <-- SUCCESS CASE -->', async () => {
        return request.put(`/employees/${ cEmployee._id }`)
        .send(employee)
        .set('Authorization', accounts.admin.token)
        .then((res) => {
            expect(res).to.have.status(200);
            expect(res.body)
                .to.be.an('object')
                .to.have.own.property('email')
                .to.be.equal(employee.email);
        })
        .catch((err) => {
            throw err;
        });
    });

    it('Should not update employee as (admin) \033[0;31m <-- ERROR CASE --> [ID NOT FOUND]', async () => {
        return request.put(`/employees/5bb481897b2d6f1690e820d6`)
        .send(employee)
        .set('Authorization', accounts.admin.token)
        .then((res) => {
            expect(res).to.have.status(404);
        })
        .catch((err) => {
            throw err;
        });
    });    
    
    it('Should get all employee requests as (employee) \033[0;32m <-- SUCCESS CASE -->', async () => {
        return request.get(`/employees/${sampleEmployee._id}/requests`)
        .set('Authorization', sampleEmployee.token)
        .then((res) => {
            if(res.error) {
                console.log(res.error.text);
            }
            expect(res).to.have.status(200)
            expect(res.body)
            .to.be.an('object')
            .to.have.property('requests')
            .to.be.an('array');
        })
        .catch ((err) => {
            throw err;
        });
    });
    it('Should get all employee restaurants as (employee) \033[0;32m <-- SUCCESS CASE -->', async () => {
        return request.get(`/employees/${sampleEmployee._id}/restaurants`)
        .set('Authorization', sampleEmployee.token)
        .then((res) => {
            if(res.error) {
                console.log(res.error.text);
            }
            expect(res).to.have.status(200)
            expect(res.body)
            .to.be.an('array');
        })
        .catch ((err) => {
            throw err;
        });
    });
    it('Should delete employee as (admin)', async () => {
        return request.delete(`/employees/${ cEmployee._id }`)
        .set('Authorization', accounts.admin.token)
        .then((res) => {
            expect(res).to.have.status(200);
        })
        .catch((err) => {
            throw err;
        })
    });
});
