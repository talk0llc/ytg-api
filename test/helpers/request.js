const chai = require('chai');
const chaiHttp = require('chai-http');
const config = require('../../config');

chai.use(chaiHttp);
module.exports = chai.request.agent(`http://localhost:${config.port}`);
