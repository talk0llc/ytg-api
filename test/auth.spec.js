const expect = require('chai').expect;
const request = require('./helpers/request');
const { user } = require('./data');

describe('Public API', () => {
    let registeredUser;
    let UserService;
    it('Should Register as a User', async () => {
        return request.post('/register')
        .send(user)
        .then((res) => {
            if(res.error){
                console.log(res.error.text);
            }
            registeredUser = res.body;
            expect(res).to.have.status(200);
            expect(res.body)
            .to.be.an('object')
            .to.have.own.property('email')
            .to.be.equal(registeredUser.email);
        })
        .catch((err) => {
            throw err;
        });
    });
    it('Should Login as a User', async () => {
        return request.post('/login')
        .send({ username: user.email, password: user.password })
        .then((res) => {
            if(res.error){
                console.log(res.error.text);
            }
            expect(res).to.have.status(200);
            expect(res.body)
            .to.be.an('object')
            .to.have.property('email')
            .to.be.equal(user.email);      
        })
        .catch((err) => {
            throw err;
        });
    });
    // it('Should Login as a user with facebook', async () => {

    // });
    // it('Should Login as a user with google', async () => {
        
    // });
    it('Should Resend SMS for verification', async () => {
        return request.post('/resend_sms')
        .set('Authorization', String(registeredUser.token))
        .then((res) => {
            if(res.error){
                console.log(res.error.text);
            }
            expect(res).to.have.status(200);
        })
        .catch((err) => {
            throw err;
        });
    });
    it('Should get registered user reset_token', async () => {
        return request.get(`/users/${ registeredUser._id }`)
        .set('Authorization', String(registeredUser.token))
        .then((res) => {
            if(res.error) {
                console.log(res.error.text);
            }
            registeredUser = res.body;
            expect(res).to.have.status(200);
            expect(res.body).to.be.an('object');
        })
        .catch((err) => {
            throw err;
        });
    });
    it('Should Verify Token', async () => {
        return request.post('/verify')
        .send({ token: registeredUser.reset_token })
        .then((res) => {
            if(res.error){
                console.log(res.error.text);
            }
            expect(res).to.have.status(200);
        })
        .catch((err) => {
            throw err;
        });
    });
    it('Should send email on forget password', async () => {
        return request.post('/forgot_password')
        .send(user)
        .then((res) => {
            if(res.error){
                console.log(res.error.text);
            }
            expect(res).to.have.status(200);
        })
        .catch((err) => {
            throw err;
        });
    });
    it('Should get registered user reset_token', async () => {
        return request.get(`/users/${ registeredUser._id }`)
        .set('Authorization', String(registeredUser.token))
        .then((res) => {
            if(res.error) {
                console.log(res.error.text);
            }
            registeredUser = res.body;
            expect(res).to.have.status(200);
            expect(res.body).to.be.an('object');
        })
        .catch((err) => {
            throw err;
        });
    });
    it('Should reset user password', async () => {
        return request.post('/reset_password')
        .send({ token: registeredUser.reset_token, password: 'dsa12345' })
        .then((res) => {
            if(res.error){
                console.log(res.error.text);
            }
            expect(res).to.have.status(200);
        })
        .catch((err) => {
            throw err;
        });
    });
    it('Should Logout User', async () => {
        return request.post('/logout')
        .set('Authorization', String(registeredUser.token))
        .then((res) => {
            expect(res).to.have.status(200);
        })
        .catch((err) => {
            throw err;
        });
    });
    it('Feeling Lucky', async () => {
        return request.get(`/feel_lucky`)
            .then((res) => {
                expect(res).to.have.status(200);
                expect(res.body)
                    .to.be.an('array');                
            })
            .catch((err) => {
                throw err;
            });
    });
});