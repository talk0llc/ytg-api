const uniqid = require('uniqid');
const randomGenerator = require('../helpers/random-generator');

module.exports = {
    _id: '5bc8858bf429cb2d4c246411',
    restaurants: ['5bc88598f429cb2d4c246412', '5bc88598f429cb2d4c246412'],
    roles: ["manager"],
    serve: [],
    is_verified: true,
    is_active: true,
	is_mobile_verified: false,
    is_email_verified: false,
    auth_type: 'local',
    email: `employee${uniqid()}@talk.com`,
    password: "123456",
    mobile: `+00${randomGenerator.generate('numeric', 10)}`,
    first_name: "Admin",
    last_name: "Admin",
    gender: 'Male',
    image: 'string',
    preferences: ['preference1'],
    wallet: {
        points: 10,
    },
    facebook: {},
    google: {},
    twitter: {},
};
