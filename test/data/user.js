const uniqid = require('uniqid');
const randomGenerator = require('../helpers/random-generator');

module.exports = {
	first_name: "first",
	last_name: "last",
	mobile: `+00${ randomGenerator.generate('numeric', 10) }`,
	email: `user${ uniqid() }@talk.com`,
	password: "123456",
	is_verified: true,
	is_active: true,
	is_mobile_verified: false,
	is_email_verified: false,
	auth_type: 'local',
    gender: 'Male',
    image: 'string',
    preferences: ['preference1'],
    wallet: {
        points: 10,
    },
    rating: 20,
	referral_id: '4550sds45',
	roles: ["user"],
    addresses: [{
        default: true,
        name: 'new addresss',
        country: '5b72ec8bee60314fd9910bca',
        city: '5b72ec8bee60314fd9910bcb',
        area: '',
        street: '5 Anonymous St.',
        building: '112',
        floor: '10',
        apartment: '9',
        mobile: '01152578776',
        landmark: 'landmark',
        shipping_note: 'note',
        type: 'Home',
        location: {
            type: 'Point',
            coordinates: [10.1, 20.35]
        }
    }],
    facebook: {},
    google: {},
    twitter: {},


};
