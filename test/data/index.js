const restaurant = require('./restaurant');
const employee = require('./employee');
const review = require('./review');
const user = require('./user');
const table = require('./table');
const request = require('./request');
const category = require('./category');
const item = require('./item');
const meal = require('./meal');
const promotion = require('./promotion');
const advertisement = require('./advertisement');
const address = require('./address');
const mall = require('./mall');



module.exports = {
    restaurant,
    employee,
    review,
    user,
    table,
    request,
    category,
    meal,
    promotion,
    advertisement,
    item,
    address,
    mall
}
