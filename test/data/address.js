const uniqid = require('uniqid');
const randomGenerator = require('../helpers/random-generator');

module.exports = {
    type: 'Business',
    street: 'street',
    country: 'string',
    name: `address${ uniqid() }`,
    city: 'city',
    area: '',
    building: '112',
    floor: '10',
    apartment: '9',
    landmark: 'landmark',
    shipping_note: 'note',
    location: {
        type: 'Point',
        coordinates: [10.1, 20.35]
    },
    mobile: `+00${ randomGenerator.generate('numeric', 10) }`
};

