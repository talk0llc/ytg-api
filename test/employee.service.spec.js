const chai = require('chai');
const sinon = require('sinon');
const { employee } = require('./data');
const { Employee, Restaurant, Order,Request } = require('../db/models');

const expect = chai.expect;
let createEmployeeStub, getEmployeeStub, existEmployeeStub, getEmployeeCountStub, updateEmployeeStub, existRestaurantStub, deleteEmployeeStub, updateJoinRestaurantStub, updateRemoveFromRestaurantStub, getRestaurantStub, getOrderStub, getRequestStub;
const EmployeeService = require('../services/core/EmployeeService');

afterEach(async () => {
    if (createEmployeeStub)
        createEmployeeStub.restore();
    if (getEmployeeStub)
        getEmployeeStub.restore();
    if (getEmployeeCountStub)
        getEmployeeCountStub.restore();
    if (updateEmployeeStub)
        updateEmployeeStub.restore();
    if (deleteEmployeeStub)
        deleteEmployeeStub.restore();
    if (existEmployeeStub)
        existEmployeeStub.restore();
    if (existRestaurantStub)
        existRestaurantStub.restore();
    if (updateJoinRestaurantStub)
        updateJoinRestaurantStub.restore();
    if (updateRemoveFromRestaurantStub)
        updateRemoveFromRestaurantStub.restore();
    if (getRestaurantStub)
        getRestaurantStub.restore();
    if (getOrderStub)
        getOrderStub.restore();
    if (getRequestStub)
        getRequestStub.restore();
});

describe('Employees API Service', () => {
    describe('Create Employee', async () => {
        it('Should create employee \033[0;32m -->SUCCESS CASE<--', async () => {
            try {
                existEmployeeStub = sinon.stub(Employee, 'exists').returns(false);
                createEmployeeStub = sinon.stub(Employee, 'create').returns({ _id: '5bc8858bf429cb2d4c246411', email: 'm.talk@gmail.com' });
                updateJoinRestaurantStub = sinon.stub(Restaurant, 'update').returns({});
                const result = await EmployeeService.create(employee);
                expect(result)
                    .to.be.an('object');
            } catch (err) {
                console.log('\033[0;31m ERROR MESSAGE: ', err.message);
                throw err;
            }
        });
        it('Should create employee \033[0;32m -->SUCCESS CASE<--', async () => {
            try {
                existEmployeeStub = sinon.stub(Employee, 'exists').returns(false);
                createEmployeeStub = sinon.stub(Employee, 'create').returns({ _id: '5bc8858bf429cb2d4c246411', mobile: '+201157762228' });
                updateJoinRestaurantStub = sinon.stub(Restaurant, 'update').returns({});
                const result = await EmployeeService.create(employee);
                expect(result)
                    .to.be.an('object');
            } catch (err) {
                console.log('\033[0;31m ERROR MESSAGE: ', err.message);
                throw err;
            }
        });
        it('Should create employee \033[0;31m -->ERROR CASE<-- [EMPLOYEE ALREADY EXISTS]', async () => {
            try {
                existEmployeeStub = sinon.stub(Employee, 'exists').returns(true);
                createEmployeeStub = sinon.stub(Employee, 'create').returns({});
                updateJoinRestaurantStub = sinon.stub(Restaurant, 'update').returns({});
                await EmployeeService.create(employee);
            } catch (err) {
                expect(err)
                    .to.have.property('status').equals(400);
            }
        });
        it('Should create employee \033[0;31m -->ERROR CASE<-- [JOIN RESTAURANTS THROWS EXCEPTION]', async () => {
            try {
                existEmployeeStub = sinon.stub(Employee, 'exists').returns(false);
                createEmployeeStub = sinon.stub(Employee, 'create').returns({ _id: '5bc8858bf429cb2d4c246411', email: 'm.talk@gmail.com' });
                updateJoinRestaurantStub = sinon.stub(Restaurant, 'update').throws();
                await EmployeeService.create(employee);
            } catch (err) {
                expect(err).instanceOf(Error);
            }
        });
        it('Should create employee \033[0;31m -->ERROR CASE<-- [THROWS EXCEPTION]', async () => {
            try {
                existEmployeeStub = sinon.stub(Employee, 'exists').returns(false);
                createEmployeeStub = sinon.stub(Employee, 'create').throws();
                await EmployeeService.create(employee);
            } catch (err) {
                expect(err).instanceOf(Error);
            }
        });
    });
    describe('Create And Assign Employee', async () => {
        it('Should create and assign employee to restaurant \033[0;32m -->SUCCESS CASE<--', async () => {
            try {
                existRestaurantStub = sinon.stub(Restaurant, 'exists').returns(true);
                existEmployeeStub = sinon.stub(Employee, 'exists').returns(false);
                createEmployeeStub = sinon.stub(Employee, 'create').returns({ _id: '5bc8858bf429cb2d4c246411', email: 'm.talk@gmail.com' });
                updateJoinRestaurantStub = sinon.stub(Restaurant, 'update').returns({});
                const result = await EmployeeService.createAndAssign(employee, '5bc88598f429cb2d4c246412');
                expect (result)
                    .to.be.an('object');
            } catch (err) {
                console.log('\033[0;31m ERROR MESSAGE: ', err.message);
                throw err;
            }
        });
        it('Should create and assign employee to restaurant \033[0;31m -->ERROR CASE<-- [RESTAURANT DOESNT EXISTS]', async () => {
            try {
                existRestaurantStub = sinon.stub(Restaurant, 'exists').returns(false);
                existEmployeeStub = sinon.stub(Employee, 'exists').returns(false);
                createEmployeeStub = sinon.stub(Employee, 'create').returns({ _id: '5bc8858bf429cb2d4c246411', email: 'm.talk@gmail.com' });
                updateJoinRestaurantStub = sinon.stub(Restaurant, 'update').returns({});
                await EmployeeService.createAndAssign(employee, '5bc88598f429cb2d4c246412');
            } catch (err) {
                expect(err).to.have.property('status').equals(404);
            }

        });
        it('Should create and assign employee to restaurant \033[0;31m -->ERROR CASE<-- [EMPLOYEE ALREADY EXISTS]', async () => {
            try {
                existRestaurantStub = sinon.stub(Restaurant, 'exists').returns(true);
                existEmployeeStub = sinon.stub(Employee, 'exists').returns(true);
                createEmployeeStub = sinon.stub(Employee, 'create').returns({ _id: '5bc8858bf429cb2d4c246411', email: 'm.talk@gmail.com' });
                updateJoinRestaurantStub = sinon.stub(Restaurant, 'update').returns({});
                await EmployeeService.createAndAssign(employee, '5bc88598f429cb2d4c246412');
            } catch (err) {
                expect(err).to.have.property('status').equals(400);
            }

        });
    });
    describe('Get All Employees', async () => {
        it('Should get all employees \033[0;32m -->SUCCESS CASE<--', async () => {
            try {
                getEmployeeStub = sinon.stub(Employee, 'getAll').returns({});
                const result = await EmployeeService.getAll({ roles: 'manager, auditor' });
                expect(result)
                    .to.be.an('object');
            } catch (err) {
                console.log('\033[0;31m ERROR MESSAGE: ', err.message);
                throw err;
            }
        });
        it('Should get all employees \033[0;31m -->ERROR CASE<-- [THROWS EXCEPTION]', async () => {
            try {
                getEmployeeStub = sinon.stub(Employee, 'getAll').throws();
                await EmployeeService.getAll({ roles: 'manager, auditor' });
            } catch (err) {
                expect(err).instanceOf(Error);
            }
        });
    });
    describe('Get One Employee', async () => {
        it('Should get one Employee \033[0;32m -->SUCCESS CASE<--', async () => {
            try {
                getEmployeeStub = sinon.stub(Employee, 'getById').returns({});
                await EmployeeService.getById(employee._id)
            } catch (err) {
                console.log('\033[0;31m ERROR MESSAGE: ', err.message);
                throw err;
            }
        });
        it('Should get one Employee \033[0;31m -->ERROR CASE<-- [THROWS EXCEPTION]', async () => {
            try {
                getEmployeeStub = sinon.stub(Employee, 'getById').throws();
                await EmployeeService.getById(employee._id)
            } catch (err) {
                expect(err).instanceOf(Error);
            }
        });
        it('Should get one Employee \033[0;32m -->SUCCESS CASE<--', async () => {
            try {
                getEmployeeStub = sinon.stub(Employee, 'getById').returns({});
                await EmployeeService.getOne(employee._id)
            } catch (err) {
                console.log('\033[0;31m ERROR MESSAGE: ', err.message);
                throw err;
            }
        });
        it('Should get one Employee \033[0;31m -->ERROR CASE<-- [THROWS EXCEPTION]', async () => {
            try {
                getEmployeeStub = sinon.stub(Employee, 'getById').throws();
                await EmployeeService.getOne(employee._id)
            } catch (err) {
                expect(err).instanceOf(Error);
            }
        });
    });
    describe('Get Employee Restaurants', async () => {
        it('Should all employee restaurants \033[0;32m -->SUCCESS CASE<--', async () => {
            try {
                getRestaurantStub = sinon.stub(Restaurant, 'getAll').returns({});
                const result = await EmployeeService.getRestaurants(employee._id);
                expect(result)
                    .to.be.an('object');
            } catch (err) {
                console.log('\033[0;31m ERROR MESSAGE: ', err.message);
                throw err;
            }
        });
        it('Should all employee restaurants \033[0;31m -->ERROR CASE<-- [THROWS EXCEPTION]', async () => {
            try {
                getRestaurantStub = sinon.stub(Restaurant, 'getAll').throws();
                await EmployeeService.getRestaurants(employee._id);
            } catch (err) {
                expect(err).instanceOf(Error);
            }
        });
    });
    describe('Update Employee', async () => {
        it('Should update employee \033[0;32m -->SUCCESS CASE<--', async () => {
            try {
                getEmployeeStub = sinon.stub(Employee, 'getById').returns(false);
                existEmployeeStub = sinon.stub(Employee, 'exists').returns(false);
                updateEmployeeStub = sinon.stub(Employee, 'updateById').returns({});
                updateJoinRestaurantStub = sinon.stub(Restaurant, 'update').returns({});
                const result = await EmployeeService.update(employee._id, employee);
                expect(result)
                    .to.be.an('object');
            } catch (err) {
                console.log('\033[0;31m ERROR MESSAGE: ', err.message);
                throw err;
            }
        });
        it('Should update employee \033[0;31m -->ERROR CASE<-- [EMPLOYEE NOT FOUND]', async () => {
            try {
                getEmployeeStub = sinon.stub(Employee, 'getById').returns(true);
                existEmployeeStub = sinon.stub(Employee, 'exists').returns(false);
                updateEmployeeStub = sinon.stub(Employee, 'updateById').returns({});
                updateJoinRestaurantStub = sinon.stub(Restaurant, 'update').returns({});
                await EmployeeService.update(employee._id, employee);
            } catch (err) {
                expect(err).to.have.property('status').equals(404);
            }
        });
        it('Should update employee \033[0;31m -->ERROR CASE<-- [EMPLOYEE ALREADY EXISTS]', async () => {
            try {
                getEmployeeStub = sinon.stub(Employee, 'getById').returns(false);
                existEmployeeStub = sinon.stub(Employee, 'exists').returns(true);
                updateEmployeeStub = sinon.stub(Employee, 'updateById').returns({});
                updateJoinRestaurantStub = sinon.stub(Restaurant, 'update').returns({});
                await EmployeeService.update(employee._id, employee);
            } catch (err) {
                expect(err).to.have.property('status').equals(400);
            }
        });
        it('Should update employee \033[0;31m -->ERROR CASE<-- [THROWS EXCEPTION]', async () => {
            try {
                getEmployeeStub = sinon.stub(Employee, 'getById').returns(false);
                existEmployeeStub = sinon.stub(Employee, 'exists').returns(false);
                updateEmployeeStub = sinon.stub(Employee, 'updateById').returns({});
                updateJoinRestaurantStub = sinon.stub(Restaurant, 'update').throws();
                await EmployeeService.update(employee._id, employee);
            } catch (err) {
                expect(err).instanceOf(Error);
            }
        });
    });
    describe('Delete Employee', async () => {
        it('Should delete employee \033[0;32m -->SUCCESS CASE<--', async () => {
            try {
                getEmployeeStub = sinon.stub(Employee, 'getById').returns({});
                updateJoinRestaurantStub = sinon.stub(Restaurant, 'update').returns({});
                deleteEmployeeStub = sinon.stub(Employee, 'deleteById').returns({});
                const result = await EmployeeService.delete(employee._id);
                expect(result)
                    .to.be.an('object');
            } catch (err) {
                console.log('\033[0;31m ERROR MESSAGE: ', err.message);
                throw err;
            }
        });
        it('Should delete employee \033[0;31m -->ERROR CASE<-- [EMPLOYEE NOT FOUND]', async () => {
            try {
                getEmployeeStub = sinon.stub(Employee, 'getById').returns(null);
                updateJoinRestaurantStub = sinon.stub(Restaurant, 'update').returns({});
                deleteEmployeeStub = sinon.stub(Employee, 'deleteById').returns({});
                await EmployeeService.delete(employee._id);
            } catch (err) {
                expect(err).to.have.property('status').equals(404);
            }
        });
    });
    describe('Get Employee Orders', async () => {
        it('Should all employee orders \033[0;32m -->SUCCESS CASE<--', async () => {
            try {
                // getOrderStub = sinon.stub(Order, 'getAll').returns({ orders: [{ _id: '5bc88598f429cb2d4c246412', address: { name: 'address 1' }, user: { _id: '5bc88598f429cb2d4c246412', addresses: [{ _id: '5bc88598f429cb2d4c246412',name: 'address 1' }] } }] });
                getOrderStub = sinon.stub(Order, 'getAll').returns({});
                const result = await EmployeeService.getOrders(employee._id, '5bc88598f429cb2d4c246412', [], {});
                expect(result)
                    .to.be.an('object');
            } catch (err) {
                console.log('\033[0;31m ERROR MESSAGE: ', err.message);
                throw err;
            }
        });
        it('Should all employee orders \033[0;31m -->ERROR CASE<-- [THROWS EXCEPTION]', async () => {
            try {
                getOrderStub = sinon.stub(Order, 'getAll').throws();
                await EmployeeService.getOrders(employee._id, '5bc88598f429cb2d4c246412', [], {});
            } catch (err) {
                expect(err).instanceOf(Error);
            }
        });
    });
    describe('Get Employee Requests', async () => {
        it('Should all employee requests \033[0;32m -->SUCCESS CASE<--', async () => {
            try {
                getRequestStub = sinon.stub(Request, 'getAll').returns({});
                const result = await EmployeeService.getRequests(employee._id, '5bc88598f429cb2d4c246412', {});
                expect(result)
                    .to.be.an('object');
            } catch (err) {
                console.log('\033[0;31m ERROR MESSAGE: ', err.message);
                throw err;
            }
        });
        it('Should all employee requests \033[0;31m -->ERROR CASE<-- [THROWS EXCEPTION]', async () => {
            try {
                getRequestStub = sinon.stub(Request, 'getAll').throws();
                await EmployeeService.getRequests(employee._id, '5bc88598f429cb2d4c246412', {});
            } catch (err) {
                expect(err).instanceOf(Error);
            }
        });
    });
});
