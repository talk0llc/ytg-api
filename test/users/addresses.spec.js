const chai = require('chai');
const request = require('../helpers/request');
const { address } = require('../data');
const accounts = require('../data/accounts');
const randomGenerator = require('../helpers/random-generator');

const expect = chai.expect;

describe('User Adresses API', () => {
    global.sampleItem = {};
    let cAddress = {};
    it('Should create new address as (admin)', async () => {
        
        return request.post(`/users/${sampleUser._id}/addresses`)
            .send(address)
            .set('Authorization', accounts.admin.token)
            .then((res) => {
                if (res.error) {
                    console.log(res.error.text);
                }

                cAddress = res.body;
                expect(res).to.have.status(200);
                expect(res.body)
                    .to.be.an('object')
                    .to.have.own.property('name')
                    .to.be.equal(address.name);
            })
            .catch((err) => {
                throw err;
            });
    });

    it('Should get all addresses as (admin)', async () => {
        return request.get(`/users/${sampleUser._id}/addresses`)
            .set('Authorization', accounts.admin.token)
            .then((res) => {
                expect(res).to.have.status(200);
                expect(res.body)
                    .to.be.an('array');
            })
            .catch((err) => {
                throw err;
            });
    });


    it('Should get specific address as (admin)', async () => {
        return request.get(`/users/${sampleUser._id}/addresses/${cAddress._id}`)
            .set('Authorization', accounts.admin.token)
            .then((res) => {
                expect(res).to.have.status(200);
                expect(res.body)
                    .to.be.an('object');
            })
            .catch((err) => {
                throw err;
            });
    });

    it('Should update address as (admin)', async () => {
        return request.put(`/users/${sampleUser._id}/addresses/${cAddress._id}`)
            .send(address)
            .set('Authorization', accounts.admin.token)
            .then((res) => {
                if (res.error) {
                    console.log(res.error.text);
                }
                expect(res).to.have.status(200);
                expect(res.body)
                    .to.be.an('object')
                    .to.have.own.property('name')
                    .to.be.equal(address.name);
            })
            .catch((err) => {
                throw err;
            });
    });

    it('Should delete address as (admin)', async () => {

        return request.delete(`/users/${sampleUser._id}/addresses/${cAddress._id}`)
            .set('Authorization', accounts.admin.token)
            .then((res) => {
                if (res.error) {
                    console.log(res.error.text);
                }
                expect(res).to.have.status(200);
          
            })
            .catch((err) => {
                throw err;
            });
    });  
     
});
