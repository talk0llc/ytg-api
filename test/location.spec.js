const chai = require('chai');
const request = require('./helpers/request');
const accounts = require('./data/accounts');
const randomGenerator = require('./helpers/random-generator');
const { Country } = require('../db/models/common/country');
const Countries = require('../config/constants/countries');
const expect = chai.expect;

describe('Locations API', () => {
    global.locations = [];
    global.citizes = [];
    global.fakeId = '5bc859a9471b0d15097b7806';
    global.unatuthorizedToken = `eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI1YjlmYWFjNjRhN2JlNzVmNmNiMzQ5NWUiLCJlbWFpbCI6ImFkbWluQGFsZ29yaXRobS5hZSIsInJvbGVzIjpbImFkbWluIl0sImlhdCI6MTUzOTYxNzAxMX0.HAAtfStTl9KhzFigVLgDK2Mv6eVbGPF0hCKRsB4ZGro`;
    describe('GET ALL LOCATIONS', async () => {

        it('Should get all locations as (admin) \033[0;32m <-- SUCCESS CASE -->',  () => {
            return request.get(`/locations/countries`)
                .then((res) => {
                    locations = res.body;
                    expect(res).to.have.status(200);
                    expect(res.body)
                        .to.be.an('array');
                })
                .catch((err) => {
                    throw err;
                });
        });

        it('Should return unauthenticated error \033[0;31m <-- ERROR CASE -->', async () => {
            return request.get(`/locations/countries`)
                .set('Authorization', "")
                .then((res) => {
                    expect(res).to.have.status(403);

                })
                .catch((err) => {
                    // console.log(err);
                    throw err;
                });
        });

        it('Should return unauthorized error \033[0;31m <-- ERROR CASE -->', async () => {
            return request.get(`/locations/countries`)
                .set('Authorization', unatuthorizedToken)
                .then((res) => {
                    expect(res).to.have.status(401);

                })
                .catch((err) => {
                    // console.log(err);
                    throw err;
                });
        });

    });
    describe('Create new location if it does not exit', () => {

        it('create new location as (admin) \033[0;32m <-- SUCCESS CASE -->', async () => {
            Countries[0].is_active = true;
            if (locations.length === 0) {
                let country = new Country(Countries[0]);
                country.save();
            }

        });

        it('Should get all locations as (admin) \033[0;32m <-- SUCCESS CASE -->',  () => {
            return request.get(`/locations/countries`)
                .then((res) => {
                    locations = res.body;
                    expect(res).to.have.status(200);
                    expect(locations)
                        .to.be.an('array');
                })
                .catch((err) => {
                    throw err;
                });
        });

    });
    describe('GET SPECIFIC LOCATION',  () => {

        it('Should get specific location as (admin) \033[0;32m <-- SUCCESS CASE -->', async () => {
            return request.get(`/locations/countries/${locations[0]._id}`)
                .set('Authorization', accounts.admin.token)
                .then((res) => {
                    expect(res).to.have.status(200);
                    expect(res.body)
                        .to.be.an('object');
                })
                .catch((err) => {
                    throw err;
                });
        });

        it('Should return not found error \033[0;31m <-- ERROR CASE -->', async () => {
            return request.get(`/locations/countries/5bc859a9471b0d15097b7806`)
                .set('Authorization', accounts.admin.token)
                .then((res) => {
                    expect(res).to.have.status(404);

                })
                .catch((err) => {
                    throw err;
                });
        });



    });
    describe('GET ALL CITIZES',  () => {
        it('Should all citizes of specific country as (admin) \033[0;32m <-- SUCCESS CASE -->', async () => {
            return request.get(`/locations/countries/${locations[0]._id}/cities`)
                .set('Authorization', accounts.admin.token)
                .then((res) => {
                    citizes = res.body;
                    expect(res).to.have.status(200);
                    expect(citizes)
                        .to.be.an('array');
                })
                .catch((err) => {
                    throw err;
                });
        });

        it('Should return not found error \033[0;31m <-- ERROR CASE -->', async () => {
            return request.get(`/locations/countries/${fakeId}/cities`)
                .set('Authorization', accounts.admin.token)
                .then((res) => {
                    expect(res).to.have.status(404);

                })
                .catch((err) => {
                    throw err;
                });
        });
    });
    
});
