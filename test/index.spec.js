const chai = require('chai');
const server = require('../index');
const db = require('../db/index');
db._registerModels();
const app = server.app;
const expressRouter = require('../routes/');
const expressConfig = require('../config/express');

const config = require('../config');

before(async () => {
    expressConfig(app, expressRouter, config);
});

// require('./location.spec');
// require('./employee.spec');
// require('./users/addresses.spec');
// require('./location.service.spec');
// require('./employee.service.spec');
// require('./auth.spec');



