const chai = require('chai');
const sinon = require('sinon');
const { UnexpectedError, NotFoundError } = require('../helpers/errors/index');

const expect = chai.expect;
const { Country } = require('../db/models');
const LocationService = require('../services/core/LocationService');

let getCountryStub;
afterEach(() => {
    if(getCountryStub)
        getCountryStub.restore();
});

describe('Locations Service', () => {
    describe(`Should test get all countries`, ()=> {
        it('Should get all countries \033[0;32m -->SUCCESS CASE<--', async () => {
            getCountryStub = sinon.stub(Country, 'getAll').returns(['test']);
            try {
                
                const result = await LocationService.getAll();
                if (!result) {
                    throw new UnexpectedError();
                }
                expect(result)
                    .to.be.an('array');
            } catch (err) {
                console.log('\033[0;31m ERROR MESSAGE: ', err);
                throw err;
            }
        });
    
        it('Should throw exception \033[0;31m -->ERROR CASE<-- [THROWS EXCEPTION]', async () => {
            getCountryStub = sinon.stub(Country, 'getAll').throws();
            try {
                const result = await LocationService.getAll();
            } catch (error) {
                expect(error)
                .instanceof(Error);
            }
        });
    
    });

    describe(`Should test get country by id`, ()=> {
        const countryId = '5b72ec8bee60314fd9910bca';
        it('Should get one country \033[0;32m -->SUCCESS CASE<--', async () => {
            getCountryStub = sinon.stub(Country, 'getOne').returns({code: 'ae'});
            try {
                const result = await LocationService.getOne(getCountryStub._id, {});
                    if (!result) {
                        throw new NotFoundError();
                    }
                    expect(result)
                        .to.be.an('object')
                        .to.have.property('code')
                        .to.be.equal('ae')
            } catch (err) {
                console.log('\033[0;31m ERROR MESSAGE: ', err);
                throw err;
            }
        });
    
        it('Should get one country \033[0;31m -->ERROR CASE<-- [INVALID ID]', async () => {
            getCountryStub = sinon.stub(Country, 'getOne').returns(null);
            try {
                await LocationService.getOne(countryId, {})
            } catch (err) {
                expect(err.status)
                .to.be.equal(404);
            }
        });
    
        it('Should throw exception \033[0;31m -->ERROR CASE<-- [THROWS EXCEPTION]', async () => {
            try {
                getCountryStub = sinon.stub(Country, 'getOne').throws();
                await LocationService.getOne(countryId)
            } catch (err) {
                expect(err)
                .instanceof(Error)
            }
        });
    
    });
});
