const path = require('path');
const http = require('http');
const express = require('express');
const mongo = require('./db/index');
const config = require('./config');
const utils = require('./helpers/utils');

// const socket = require('./sockets/sockerManager');
// const logger = require('./helpers/logger');
const preConfiguaration = require('./db/preConfigurations');

class Server {
    constructor(_config) {
        this.config = _config;
        this.app = express();
        global.rootPath = path.dirname(require.main.filename);

        this.server = http.createServer(this.app);
    }

    async preInitialize() {
        // 1- Initialize mongo database
        try {
            await mongo.initialize(config.db);
        } catch (err) {
            console.log(err);
            process.exit(1);
        }

        // Apply express configurations
        const expressRouter = require('./routes/index');
        const expressConfig = require('./config/express');

        // Initialize socket.io
        // socket.initialize(this.server);
        expressConfig(this.app, expressRouter, this.config);
    }

    async start() {
        await this.preInitialize();
        await preConfiguaration.buildDefaultUsers();
        await new Promise((resolve, reject) => {
            this.server.on('error', (err) => {
                if (err.syscall !== 'listen') {
                    return reject(err);
                }

                // handle specific listen errors with friendly messages
                switch (err.code) {
                    case 'EACCES':
                        console.error(`port ${err.port} requires elevated privileges`);
                        process.exit(1);
                        break;
                    case 'EADDRINUSE':
                        console.error(`port ${err.port} is already in use`);
                        process.exit(1);
                        break;
                    default:
                        reject(err);
                }
            });

            this.server.on('listening', () => {
                resolve();
            });

            this.server.listen(process.env.PORT || this.config.port);
        });

        const info = this.server.address();
        console.log(`Running API server at ${info.address}:${info.port} on ${process.env.NODE_ENV}`);
        // logger.info(`Running API server at ${info.address}:${info.port}`);
    }

    async stop() {
        if (!utils.inDevelopment() && !utils.inTest()) {
            return;
        }

        if (this.server && this.server.listening) {
            await mongo.close();
            //await socket.close();
            this.server.close();
        }
    }
}

const server = new Server(config);

if (!module.parent) {
    server.start();
}

module.exports = server;
