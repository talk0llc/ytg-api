module.exports = {
    app: {
        name: 'YTG',
    },
    port: 3000,
    db: {
        host: 'mongodb://admin:123456r@ds259463.mlab.com:59463/ytg',
        // host: 'mongodb://localhost:27017/YTG',
        db: 'YTG',
    },
    api: {
        version: '1.0',
    },
    auth: {
        local: {
            issuer: 'talk.com',
            key: 'ZAZDp1IxnPigN9gX4VgiuFl5hSlqSpFaa103S4JsWPGhIKzkMh6vmEiDUbolPeEcVYpN0tN1zkdRE2S3GeOd',
        },
        google: {
            client_id: '16714531593-4f0uas2jfc4j79ujdaes2nasrnjfgs7o.apps.googleusercontent.com',
            client_secret: '9ATqt3JhBFoQet3mlkmlzyZB',
            callback: 'http://localhost:3000/api/social/google/callback',
        },
        facebook: {
            app_id: '470191943386802',
            app_secret: '1fb07b2c51ff555b31b3adc1bdfce751',
            callback: 'http://localhost:3000/api/social/facebook/callback',
        },
    },
    storage: {
        project_id: '',
        keyFilename: `${__dirname}/keys/storage.json`,
        bucket: 'test-development',
        url: 'https://storage.googleapis.com/test-development',
    },
    stackDriver: {
        projectId: '',
        keyFilename: `${__dirname}/keys/stack_driver.json`,
    },
    firebase: {
        serverKey: 'AAAAYKcmqEE:APA91bEdBGOm16DVvKiE3wO03YhhI46DLVVctvT4_Cx-1BNqzK1854nB-3EhV5UdCfJILSDGL9dR_3n7q8xZcv8YJOy0ztZc0nQCtmgbxYrBykWl-_DFnTl9u0b3AbA4Uo9OAzs7E03YRPZzipN7fPoWMyhROOZZiA',
    },
    expiration: {
        sms: 10 * 60 * 1000, // 10 minutes
        email: 24 * 60 * 60 * 1000, // 24 hours
        session: 7 * 24 * 60 * 60 * 1000, // 7 days
    },
    session: {
        expiration: 7, // Days
    },
    accounts: {
        admin: {
            email: 'admin@talk.com',
            password: '123456r',
        },
    },
    YTG: {
        rewards: {
            referral: 5,
            registration: 10,
        },
    },
    PAYMENT_GATEWAY: {
        SECRET_KEY: '',
    },
};
