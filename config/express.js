const bodyParser = require('body-parser');
const express = require('express');
const compress = require('compression');
const mongoSanitize = require('express-mongo-sanitize');
const cors = require('cors');
const logger = require('morgan');
//const logger = require('../helpers/logger');

const errors = require('../helpers/errors/index');
const utils = require('../helpers/utils');

const getLogError = (err, req, res, payload) => {
    try {
        const errCode = err.status || 500;
        const errData = {
            ...payload,
            source: req.getIp(),
            destination: req.originalUrl,
            method: req.method,
        };

        if (errCode === 500) {
            errData.stack = err.stack;
        }

        if (req.user_id) {
            errData.sub = req.user._id;
        }

        return errData;
    } catch (e) {
        throw err;
    }
};

module.exports = (app, router, config) => {
    // Disable powered by express
    app.disable('x-powered-by');
    app.disable('etag');

    // Express extensions
    if (utils.inDevelopment()) {
        app.use(logger('dev'));
    }
    app.use(express.static('client'));
    app.use(express.static('docs'));

    app.use(cors());
    app.use(bodyParser.json({ limit: '50mb' }));
    app.use(bodyParser.urlencoded({
        limit: '50mb',
        extended: true,
    }));
    app.use(mongoSanitize({
        replaceWith: '_',
    }));
    app.use(compress());

    // Finally we must initialize router
    router.initialize(app, config);

    // catch 404 and forwarding to error handler
    app.use((req, res, next) => {
        const err = new errors.NotFoundError();
        next(err);
    });

    // Error handling
    app.use((err, req, res, next) => {
        const payload = {
            code: err.code || 0,
            name: err.name,
            message: err.message || 'Error',
        };
        // Set res code based on error code or return 500
        res.statusCode = err.status || 500;
        // ajv errors
        if (err.status && err.status === 400) {
            // Handle ajv errors
            if (Array.isArray(err.message)) {
                payload.details = err.message;
                payload.message = 'validation error(s)';
            }
        }

        // const logData = getLogError(err, req, res, payload);
        // logger.error(logData);

        if (utils.inDevelopment()) {
            payload.stack = JSON.stringify(err.stack);
            return res.json(payload);
        }

        res.json(payload);
    });
};
