const AccessControl = require('accesscontrol');

const grantsObject = {
    admin: {
        constants: { 'read:any': ['*'] },
        user: {
            'create:any': ['*'], 'read:any': ['*', '!password'], 'update:any': ['*', '!email'], 'delete:any': ['*'],
        },
        userAddress: {
            'create:any': ['*'], 'read:any': ['*'], 'update:any': ['*'], 'delete:any': ['*'],
        },
        employees: {
            'create:any': ['*'],
            'read:any': ['*', '!password', '!token', '!reset_token'],
            'update:any': ['*', '!password', '!token', '!reset_token'],
            'delete:any': ['*'],
        },
    },
    operation: {
        constants: { 'read:any': ['*'] },
        employees: {
            'create:any': ['*'],
            'read:any': ['*', '!password', '!token', '!reset_token'],
            'update:any': ['*', '!password', '!token', '!reset_token'],
            'delete:any': ['*'],
        },
    },
    manager: {
        constants: { 'read:any': ['*'] },
        employees: {
            'create:own': ['*'], 'read:own': ['*'], 'update:own': ['mobile', 'password', 'first_name', 'last_name', 'image', 'supervisor', 'firebase_token'], 'delete:own': ['*'],
        },
    },
    user: {
        constants: { 'read:any': ['*'] },
        user: {
            'create:own': ['token', 'mobile', 'password', 'first_name', 'last_name', 'image', 'firebase_token'],
            'read:own': ['*', '!reset_toke'],
            'update:own': ['mobile', 'password', 'first_name', 'last_name', 'image', 'firebase_token', 'preferences'],
        },
        userAddress: {
            'create:own': ['*'], 'read:own': ['*'], 'update:own': ['*'], 'delete:own': ['*'],
        },
        gateway: {
            'create:own': ['*'],
        },
    },
    guest: {
        user: {
            'create:own': ['token', 'mobile', 'password', 'first_name', 'last_name', 'image', 'firebase_token'],
        },
    },
};

module.exports = new AccessControl(grantsObject);
