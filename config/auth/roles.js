const rolesName = [
    'admin',
    'manager',
    'user'
];

const ROLES_NAMES = {
    ADMIN: rolesName[0],
    MANAGER: rolesName[1],
    USER: rolesName[13],
};

module.exports = { rolesName, ROLES_NAMES };
