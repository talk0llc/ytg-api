const os = require('os');

const hostname = os.hostname();
const testDBName = hostname;

module.exports = {
    app: {
        name: 'YTG',
    },
    port: 3000,
    db: {
        host: `mongodb://localhost/${testDBName}`,
        db: testDBName,
    },
};
