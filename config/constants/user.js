const ADDRESS = {
    TYPE: ['Home', 'Business'],
};

module.exports = {
    ADDRESS,
    address: {
        type: ['Home', 'Business'],
    },
};
