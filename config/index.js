const development = require('./config.dev');
const test = require('./config.test');

const nodeEnv = process.env.NODE_ENV || 'development';

const configuration = {
    development,
    test,
};

module.exports = configuration['development'];
module.exports.env = 'development';
