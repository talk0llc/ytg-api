const _ = require('lodash');
const storage = require('../helpers/storage');
const errors = require('../helpers/errors');

class BaseService {
    constructor() {
        this.storage = storage;
        // Errors
        this.UnauthenticatedError = errors.UnauthenticatedError;
        this.UnauthorizedError = errors.UnauthorizedError;
        this.ValidationError = errors.ValidationError;
        this.NotFoundError = errors.NotFoundError;
    }

    async synchronizeFiles(oldFilesPaths = [], modifiedFilePaths = [], uploadedFiles) {
        const removedImages = _.difference(oldFilesPaths, modifiedFilePaths.images);
        if (removedImages && removedImages.length > 0) {
            await this.storage.delete(removedImages);
        }
        if (uploadedFiles && uploadedFiles.length > 0) {
            const urls = await this.storage.save(uploadedFiles);
            return _.concat(modifiedFilePaths, urls);
        }
    }

    async uploadFilesToCloud(files) {
        try {
            if (files && files.length > 0) {
                return await this.storage.save(files);
            }
        } catch (error) {
            throw error;
        }
    }

    validateExistence(object, message) {
        if (_.isNil(object)) {
            throw new this.NotFoundError(0, message);
        }
    }
}

module.exports = BaseService;
