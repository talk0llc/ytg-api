const _ = require('lodash');
const { ObjectId } = require('mongoose').mongo;
const User = require('../ModelsFactory').create('User');

const BaseService = require('../BaseService');

class UserAddressService extends BaseService {
    async create(userId, body) {
        try {
            const data = body;
            const user = await User.getById(userId);

            if (_.isNil(user)) {
                throw new this.NotFoundError();
            }

            // check if address exists
            const exists = _.find(user.addresses, (address) => {
                if (address.name.trim().toLowerCase() === data.name.trim().toLowerCase()) {
                    return true;
                }

                return false;
            });

            if (exists) {
                throw new this.ValidationError('Address with the same name already exists');
            }

            if (user.addresses.length >= 10) {
                throw new this.ValidationError('Each user is allowed to add up to 10 addresses');
            }

            // push new address, after appending _id
            data._id = new ObjectId();
            user.addresses.push(data);

            await user.save();
            return data;
        } catch (err) {
            throw err;
        }
    }

    async getAll(userId, params = {}, options = {}) {
        try {
            const user = await User.getById(userId, params);

            if (_.isNil(user)) {
                throw new NotFoundError();
            }

            return user.addresses || [];
        } catch (err) {
            throw err;
        }
    }

    async getById(userId, addressId, params = {}, options = {}) {
        try {
            const user = await User.getById(userId, params);
            if (_.isNil(user)) {
                throw new this.NotFoundError();
            }

            const address = _.find(user.addresses, (a)=> {
                return String(a._id) === addressId;
            });

            if (_.isNil(address)) {
                throw new this.NotFoundError('Invalid address id');
            }

            return address;
        } catch (err) {
            throw err;
        }
    }

    async update(userId, addressId, data, files) {
        try {
            const user = await User.getById(userId);

            if (_.isNil(user)) {
                throw new this.NotFoundError();
            }

            const address = _.find(user.addresses, { _id: ObjectId(addressId) });
            if (_.isNil(address)) {
                throw new this.ValidationError('Invalid address id');
            }

            address.set(data);
            await user.save();

            return address;
        } catch (err) {
            throw err;
        }
    }

    async delete(userId, addressId) {
        try {
            const address = await User.updateOne({
                _id: userId,
                'addresses._id': addressId,
            }, {
                $pull: { addresses: { _id: addressId } },
            }, { new: true });

            if (_.isNil(address)) {
                throw new this.NotFoundError();
            }
        } catch (err) {
            throw err;
        }
    }
}

module.exports = new UserAddressService();
