const _ = require('lodash');
const { ObjectId } = require('mongoose').mongo;

const {
    Program,
    ProgramQA,
    User,
} = require('../../db/models');
const notify = require('../common/notifications');
const BaseService = require('../BaseService');

class ProgramService extends BaseService {
    // _notifyUser(employee) {
    //     if (employee.email) {
    //         notify.sendVerificationEmail(employee);
    //     } else if (employee.mobile) {
    //         notify.sendVerificationSms(employee);
    //     }
    // }

    async _isProgramExists(programData) {
        const exists = await Program.exists({
            name: { $ne: undefined, $eq: programData.name },
        });

        return exists;
    }

    async create(data, files) {
        try {
            const nData = data;
            const exists = await this._isProgramExists(nData);
            if (exists) {
                throw new this.ValidationError(0, 'Program already exists');
            }

            nData.images = [];
            if (files && files.length > 0) {
                for (let i = 0; i < files.length; i++) {
                    nData.images.push(`${files[i].destination}/${files[i].filename}`);
                }
            }
            nData.created_at = new Date();
            const program = await Program.create(data);

            // this._notifyUser(employee);

            return program;
        } catch (error) {
            throw error;
        }
    }

    async getAll(params = {}, options = {}) {
        try {
            const nParams = params;
            const qRegEx = new RegExp(nParams.name, 'i');

            const query = {
                name: { $regex: qRegEx },
            };
            nParams.lean = true;
            return Program.getAll(query, nParams, true);
        } catch (err) {
            throw err;
        }
    }

    async getOne(query, params = {}) {
        try {
            return Program.getById(query, params);
        } catch (err) {
            throw err;
        }
    }

    async update(programId, data, files) {
        try {
            const nData = data;

            // Make sure employee exists, also we need to get his image
            const program = await Program.getById(programId);
            if (_.isNil(program)) {
                throw new this.NotFoundError();
            }
            if (nData.extraSections) {
                if (files && files.length > 0) {
                    nData.extraSections.images = [];
                    for (let i = 0; i < files.length; i++) {
                        nData.extraSections.images.push(`${files[i].destination}/${files[i].filename}`);
                    }
                }
                program.extraSections.push(nData.extraSections);
                return await program.save();
            }

            const exists = await Program.exists({
                _id: { $ne: programId },
                name: { $ne: undefined, $eq: nData.name },
            });

            if (exists) {
                throw new this.ValidationError(0, 'Program already exists');
            }

            // if (nData.image !== employee.image) {
            //     await this.storage.delete(employee.image);
            // }
            //
            nData.images = [];
            if (files && files.length > 0) {
                for (let i = 0; i < files.length; i++) {
                    // nData.images.push(`${files[i].destination}/${files[i].filename}`);
                    program.images.push(`${files[i].destination}/${files[i].filename}`);
                }
            }
            nData.images = program.images;
            nData.updated_at = new Date();
            const nProgram = await Program.updateById(programId, nData, {
                upsert: false,
                new: true,
            });

            // if (mobileChanged) {
            //     notify.sendVerificationSms(nEmployee);
            // }

            return nProgram;
        } catch (err) {
            throw err;
        }
    }

    async delete(id) {
        // Get program
        const program = await Program.getById(id);

        if (_.isNil(program)) {
            throw new this.NotFoundError();
        }
        return await Program.deleteById(id);
    }

    async addQuestion(id, data) {
        // Get program
        const program = await Program.getById(id);

        if (_.isNil(program)) {
            throw new this.NotFoundError();
        }

        program.questions.push(data);
        return await program.save();
    }

    async getQuestions(id) {
        // Get program
        const program = await Program.getById(id);

        if (_.isNil(program)) {
            throw new this.NotFoundError();
        }
        return program.questions;
    }

    async deleteQuestion(id, questionId) {
        // Get program
        const program = await Program.getById(id);

        if (_.isNil(program)) {
            throw new this.NotFoundError();
        }
        const questions = [];
        for (let i = 0; i < program.questions.length; i += 1) {
            if (program.questions[i]._id.toString() !== questionId) {
                questions.push(program.questions[i]);
            }
        }
        program.questions = questions;
        return await program.save();
    }

    async submitAnswer(id, userId, data, files) {
        // Get program
        const nData = data;
        const program = await Program.getById(id);

        if (_.isNil(program)) {
            throw new this.NotFoundError();
        }
        const questionAnswers = [];
        for (let i = 0; i < nData.questions.length; i += 1) {
            if (nData.questions[i].type === 'File Upload' && files && files.length > 0) {
                questionAnswers.push({
                    question: nData.questions[i].question,
                    answer: `https://ytg.eco/${files[0].destination}/${files[0].filename}`,
                });
            } else {
                questionAnswers.push({
                    question: nData.questions[i].question,
                    answer: nData.questions[i].answer,
                });
            }
        }
        const questionAnswer = {
            program: await Program.getById(id),
            user: await User.getById(userId),
            questionAnswers,
            created_at: new Date(),
        };
        const answer = await ProgramQA.create(questionAnswer);
        // Notify user
        await notify.sendApplicationUpdateStatusEmail(answer.user, answer.status, 'program', answer);
        return answer;
    }

    async deleteSection(id, sectionId) {
        // Get program
        const program = await Program.getById(id);

        if (_.isNil(program)) {
            throw new this.NotFoundError();
        }
        const sections = [];
        for (let i = 0; i < program.extraSections.length; i += 1) {
            if (program.extraSections[i]._id.toString() !== sectionId) {
                sections.push(program.extraSections[i]);
            }
        }
        program.extraSections = sections;
        return await program.save();
    }

    async getUserApplications(params = {}) {
        try {
            const nParams = params;
            let emailRegEx;
            let query = {};
            if (nParams.name) {
                query = {
                    ...query,
                    'program.name': nParams.name,
                };
            }
            if (nParams.email) {
                emailRegEx = new RegExp(nParams.email, 'i');
                query = {
                    ...query,
                    'user.email': { $regex: emailRegEx },
                };
            }
            if (nParams.status) {
                query = {
                    ...query,
                    status: nParams.status,
                };
            }
            nParams.lean = true;
            console.log('query', query);
            const result = await ProgramQA.getAll(query, nParams, true);
            return result;
        } catch (e) {
            throw e;
        }
    }

    async getUserApplication(id, applicationId, params = {}) {
        const nParams = params;
        const query = {
            _id: new ObjectId(applicationId),
        };
        nParams.lean = true;
        return await ProgramQA.getOne(query, nParams, true);
    }

    async updateUserApplicationStatus(applicationId, data) {
        try {
            const nProgramApplication = await ProgramQA.updateById(applicationId, data, {
                upsert: false,
                new: true,
            });
            // Notify user
            await notify.sendApplicationUpdateStatusEmail(nProgramApplication.user, nProgramApplication.status, 'program', nProgramApplication);
            return nProgramApplication;
        } catch (err) {
            throw err;
        }
    }
    async deleteUserApplicationStatus(id) {
        // Get programQA
        const programQA = await ProgramQA.getById(id);

        if (_.isNil(programQA)) {
            throw new this.NotFoundError();
        }
        return await ProgramQA.deleteById(id);
    }
}

module.exports = new ProgramService(Program);
