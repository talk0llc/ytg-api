const _ = require('lodash');
const request = require('superagent');
const uitl = require('util');
const { OAuth2Client } = require('google-auth-library');

const {
    User, Employee, Configuration,
} = require('../../db/models');
const BaseService = require('../BaseService');
const config = require('../../config/index');
const notify = require('../common/notifications');

const { pUserBasicData } = require('../../helpers/projections');
// Facebook
const facebookAPI = 'https://graph.facebook.com/v3.0/me?fields=id,email,first_name,last_name,picture&access_token=%s';

class AuthService extends BaseService {
    constructor() {
        super();
        const gClientId = config.auth.google.client_id;
        this.google = {
            client_id: gClientId,
            client: new OAuth2Client(gClientId),
        };

        this.reCaptcha = config.reCaptcha;
    }

    async _verifyGoogleCaptcha(captcha, client, userIp) {
        try {
            const { url } = this.reCaptcha;
            let reCaptchaConfig = this.reCaptcha.invisible;
            if (client && _.toLower(client) === 'android') {
                reCaptchaConfig = this.reCaptcha.android;
            }

            const verifyUrl = `${url}?secret=${reCaptchaConfig.secretKey}&response=${captcha}&remoteip=${userIp}`;
            const res = await request
                .post(verifyUrl)
                .set('accept', 'json');

            if (res.body && res.body.success === true) {
                return true;
            }

            return false;
        } catch (err) {
            return false;
        }
    }

    async register(body) {
        try {
            const data = body;

            // Validate reCapcha
            // const validCaptcha = await this._verifyGoogleCaptcha(data.captcha, data.clientType, '');
            //
            // if (!validCaptcha) {
            //     throw new this.ValidationError(0, 'Invalid reCaptcha code');
            // }

            // const exists = await User.exists({
            //     $or: [{
            //         email: { $ne: undefined, $eq: data.email },
            //     }, {
            //         mobile: { $ne: undefined, $eq: data.mobile },
            //     }],
            // });

            const exists = await User.exists({
                $or: [{
                    email: { $ne: undefined, $eq: data.email },
                }],
            });

            if (exists) {
                throw new this.ValidationError(0, 'User already exists');
            }

            const uData = {
                email: data.email,
                mobile: data.mobile,
                first_name: data.first_name,
                last_name: data.last_name,
                password: data.password,
                roles: [{
                    name: 'user',
                    resources: [
                        {
                            resource: 'programs',
                            accessType: ['readAny'],
                        },
                        {
                            resource: 'jobs',
                            accessType: ['readAny'],
                        },
                        {
                            resource: 'news',
                            accessType: ['readAny'],
                        },
                        {
                            resource: 'events',
                            accessType: ['readAny'],
                        },
                        {
                            resource: 'services',
                            accessType: ['readAny'],
                        },
                        {
                            resource: 'applications',
                            accessType: ['readAny', 'createAny', 'updateAny', 'deleteAny'],
                        },
                        {
                            resource: 'contents',
                            accessType: ['readAny'],
                        },
                        {
                            resource: 'users',
                            accessType: ['readAny', 'updateAny'],
                        },
                        {
                            resource: 'partners',
                            accessType: ['readAny'],
                        },
                        {
                            resource: 'members',
                            accessType: ['readAny'],
                        },
                    ],
                }],
                date_of_birth: data.date_of_birth,
                residence_address: data.residence_address,
                graduation_status: data.graduation_status,
                organization: data.organization,
                auth_type: data.auth_type ? data.auth_type : 'local',
                is_verified: data.auth_type && data.auth_type !== 'local' ? true : false,
            };

            // Create user
            const user = await User.create(uData);

            // Notify user by sms asynchronously (no wait)
            // await notify.sendVerificationSms(user);
            if (data.auth_type === 'local') {
                await notify.sendVerificationEmail(user);
            }

            return pUserBasicData.pickFrom(user);
        } catch (err) {
            throw err;
        }
    }

    async login(body) {
        try {
            const data = body;

            let Model = User;
            if (data.type === 'employee') {
                Model = Employee;
            }

            const user = await Model.getOne({
                $or: [
                    { email: data.username },
                    { mobile: data.username },
                ],
            });

            if (_.isNil(user) || user.is_verified === false) {
                throw new this.UnauthorizedError();
            }

            if (user.validPassword(data.password)) {
                user.last_login = Date.now();
                await user.save();

                const result = pUserBasicData.pickFrom(user);
                return result;
            }

            throw new this.UnauthenticatedError(0, 'Invalid username or password');
        } catch (err) {
            throw err;
        }
    }

    async logout(user) {
        try {
            const isUser = user.isUser();

            let Model = User;
            if (!isUser) {
                Model = Employee;
            }

            const userDoc = await Model.getById(String(user._id));
            if (!_.isNil(userDoc)) {
                userDoc.token = userDoc.generateToken();
                return userDoc.save();
            }

            throw new this.UnauthenticatedError();
        } catch (err) {
            throw err;
        }
    }

    async forgotPassword(body) {
        try {
            const data = body;

            let Model = User;
            if (data.type === 'employee') {
                Model = Employee;
            }

            const user = await Model.getOne({
                $or: [{
                    email: data.username,
                }, {
                    mobile: data.username,
                }],
            });

            if (_.isNil(user)) {
                throw new this.ValidationError(0, 'Invalid username');
            }

            // Set data
            user.reset_token = user.generateEmailToken();
            user.reset_token_time = Date.now();
            user.reset_token_expiration = Date.now() + config.expiration.email;

            await user.save();
            // Notify user
            notify.sendPasswordResetEmail(user);
        } catch (err) {
            throw err;
        }
    }

    async resetPassword(body) {
        try {
            const data = body;

            let Model = User;
            if (data.type === 'employee') {
                Model = Employee;
            }

            const user = await Model.getOne({
                reset_token: data.token,
            });

            if (!_.isNil(user)) {
                if (Date.now() >= Date.parse(user.reset_token_expiration)) {
                    throw new this.UnauthorizedError(0, 'Token expired');
                }

                user.reset_token = '';
                user.password = data.password;
                return await user.save();
            }

            throw new this.UnauthorizedError();
        } catch (err) {
            throw err;
        }
    }

    async resendSms(token) {
        try {
            const user = await User.getOne({ token });
            if (_.isNil(user)) {
                throw new this.UnauthorizedError();
            }

            if (user.is_verified) {
                throw new this.ValidationError(0, 'User is already verified');
            }

            // notify.sendVerificationSms(user);
        } catch (err) {
            throw err;
        }
    }

    async verify(data) {
        try {
            let Model = User;
            // if (data.type === 'employee') {
            //     Model = Employee;
            // }

            if (data.token.length <= 6 && _.isNil(data.authorization)) {
                throw new this.ValidationError(0, 'Authorization token is missing');
            }

            const query = {
                reset_token: data.token,
            };

            // if (!_.isNil(data.token)) {
            //     query.token = data.authorization;
            // }

            const user = await Model.getOne(query);

            if (!_.isNil(user)) {
                if (Date.now() >= Date.parse(user.reset_token_expiration)) {
                    throw new this.UnauthorizedError(0, 'Token expired');
                }

                if (data.token.length <= 6) {
                    user.is_mobile_verified = true;
                } else if (data.token > 6) {
                    user.is_email_verified = true;
                }

                user.reset_token = '';
                user.is_verified = true;

                await user.save();
                return;
            }

            throw new this.UnauthorizedError();
        } catch (err) {
            throw err;
        }
    }

    async _verifyGoogleToken(token) {
        return this.google.client.verifyIdToken({
            idToken: token,
            audience: this.google.clientId,
        });
    }

    async authGoogle(body) {
        try {
            const data = body;

            if (_.isNil(data.token)) {
                throw new this.ValidationError(0, 'Missing authorization token');
            }

            // 1- Verify token
            const ticket = await this._verifyGoogleToken(data.token);
            const payload = ticket.getPayload();

            const userData = {
                email: payload.email,
                first_name: payload.given_name,
                last_name: payload.family_name,
                mobile: data.mobile,
                image: payload.picture,
                auth_type: 'google',
                google: {
                    id: payload.sub,
                },
            };

            // 2- if user id exists, return user data
            const user = await User.getOne({
                $and: [
                    { 'google.id': { $exists: true } },
                    { 'google.id': userData.google.id },
                ],
            });

            if (!_.isNil(user)) {
                if (user.mobile !== userData.mobile && !user.is_email_verified) {
                    const exists = await User.exists({
                        mobile: { $ne: undefined, $eq: userData.mobile },
                    });

                    if (exists) {
                        throw new this.ValidationError(0, 'Account with the same mobile number already exists');
                    }

                    if (!_.isNil(data.mobile)) {
                        await user.set({ mobile: data.mobile });
                    }
                    // await notify.sendVerificationSms(user);
                }

                return user;
            }

            return User.create(userData);
        } catch (err) {
            throw err;
        }
    }

    async authFacebook(body) {
        try {
            const data = body;

            if (_.isNil(data.token)) {
                throw new this.ValidationError(0, 'Missing authorization token');
            }

            const rUri = uitl.format(facebookAPI, data.token);
            const rData = await request.get(rUri).accept('json');
            const facebookUser = rData.body;

            const userData = {
                email: facebookUser.email,
                first_name: facebookUser.first_name,
                last_name: facebookUser.last_name,
                image: facebookUser.picture.data.url,
                mobile: data.mobile,
                auth_type: 'facebook',
                facebook: {
                    id: facebookUser.id,
                },
            };

            // if user id exists, return user data
            const user = await User.getOne({
                $and: [
                    { 'facebook.id': { $exists: true } },
                    { 'facebook.id': userData.facebook.id },
                ],
            });

            if (!_.isNil(user)) {
                if (user.mobile !== userData.mobile && !user.is_email_verified) {
                    const exists = await User.exists({
                        mobile: { $ne: undefined, $eq: userData.mobile },
                    });

                    if (exists) {
                        throw new this.ValidationError(0, 'Account with the same mobile number already exists');
                    }

                    if (!_.isNil(data.mobile)) {
                        await user.set({ mobile: data.mobile });
                    }
                    // await notify.sendVerificationSms(user);
                }

                return user;
            }

            return User.create(userData);
        } catch (err) {
            throw err;
        }
    }
}

module.exports = new AuthService();
