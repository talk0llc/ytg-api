const _ = require('lodash');
const crypto = require('../../helpers/crypto');
const {
    User,
    ProgramQA,
    JobQA,
    EventQA,
} = require('../../db/models');
const BaseService = require('../BaseService');

class UserService extends BaseService {
    async create(body, files) {
        try {
            const data = body;

            // const user = await User.getOne({
            //     $or: [{
            //         email: { $ne: undefined, $eq: data.email },
            //     }, {
            //         mobile: { $ne: undefined, $eq: data.mobile },
            //     }],
            // });

            const user = await User.getOne({
                $or: [{
                    email: { $ne: undefined, $eq: data.email },
                }],
            });

            if (!_.isNil(user)) {
                throw new this.ValidationError(0, 'User already exists');
            }

            data.roles = [{
                name: 'user',
                resources: [
                    {
                        resource: 'programs',
                        accessType: ['readAny'],
                    },
                    {
                        resource: 'jobs',
                        accessType: ['readAny'],
                    },
                    {
                        resource: 'news',
                        accessType: ['readAny'],
                    },
                    {
                        resource: 'events',
                        accessType: ['readAny'],
                    },
                    {
                        resource: 'services',
                        accessType: ['readAny'],
                    },
                    {
                        resource: 'applications',
                        accessType: ['readAny', 'createAny', 'updateAny', 'deleteAny'],
                    },
                    {
                        resource: 'contents',
                        accessType: ['readAny'],
                    },
                    {
                        resource: 'users',
                        accessType: ['readAny', 'updateAny'],
                    },
                    {
                        resource: 'partners',
                        accessType: ['readAny'],
                    },
                    {
                        resource: 'members',
                        accessType: ['readAny'],
                    },
                ],
            }];
            data.image = '';
            if (files && files.length > 0) {
                data.image = `${files[0].destination}/${files[0].filename}`;
            }
            data.created_at = new Date();
            // data.is_verified = true;
            // data.is_email_verified = true;

            const nUser = await User.create(data);

            // // Notify User without waiting
            // if (data.email) {
            //     notify.sendVerificationEmail(nUser);
            // } else if (data.mobile) {
            //     notify.sendVerificationSms(nUser);
            // }

            // notify.sendVerificationEmail(nUser);
            // notify.sendVerificationSms(nUser);

            return nUser;
        } catch (err) {
            throw err;
        }
    }

    async getAll(params = {}, options = {}) {
        try {
            const nParams = params;
            nParams.lean = true;
            const emailRegEx = new RegExp(nParams.email, 'i');
            const nameRegEx = new RegExp(nParams.first_name, 'i');
            let query = {};
            if (nParams.email || nParams.first_name) {
                query = {
                    $or: [{ email: { $regex: emailRegEx } }, { first_name: { $regex: nameRegEx } }],
                };
            }
            return User.getAll(query, nParams, true);
        } catch (err) {
            throw err;
        }
    }

    async getById(id, params = {}) {
        try {
            return User.getById(id, params);
        } catch (err) {
            throw err;
        }
    }

    async getOne(query, params = {}) {
        try {
            return User.getById(query, params);
        } catch (err) {
            throw err;
        }
    }

    async getProfileById(id, params = {}) {
        try {
            const nParams = params;

            nParams.lean = true;
            const user = await User.getById(id, nParams);
            const programQAS = await ProgramQA.getAll({ 'user._id': id }, nParams, false);
            const jobQAS = await JobQA.getAll({ 'user._id': id }, nParams, false);
            const eventQAS = await EventQA.getAll({ 'user._id': id }, nParams, false);
            const userDetails = {
                programs: programQAS,
                jobs: jobQAS,
                events: eventQAS,
                ...user
            };
            return userDetails;
        } catch (err) {
            throw err;
        }
    }

    async update(id, data, files) {
        try {
            const nData = data;

            if (nData.password && nData.password.length < 6) {
                throw new this.ValidationError(0, 'Password cant be less than 6 characters.....');
            }

            if (nData.password) {
                nData.password = crypto.createHash(nData.password);
            }

            // Make sure user exists, also we need to get his image
            const user = await User.getById(id);
            if (_.isNil(user)) {
                throw new this.NotFoundError();
            }
            if (nData.currentPassword && !user.validPassword(nData.currentPassword)) {
                throw new this.ValidationError(0, 'Password Doesnt Match.....');
            }

            // const exists = await User.exists({
            //     _id: { $ne: id },
            //     $or: [{
            //         email: { $ne: undefined, $eq: nData.email },
            //     }, {
            //         mobile: { $ne: undefined, $eq: nData.mobile },
            //     }],
            // });

            const exists = await User.exists({
                _id: { $ne: id },
                $or: [{
                    email: { $ne: undefined, $eq: nData.email },
                }],
            });

            if (exists) {
                throw new this.ValidationError('User already exists');
            }
            // nData.image = '';
            // if (files && files.length > 0) {
            //     for (let i = 0; i < files.length; i++) {
            //         user.image = `${files[i].destination}/${files[i].filename}`;
            //     }
            // }
            // nData.image = user.image;
            if (nData.image) {
                nData.image = data.image;
            }
            // If mobile number changed
            const mobileChanged = !_.isNil(nData.mobile) && (user.mobile !== data.mobile);
            // if (mobileChanged) {
            //     nData.is_verified = false;
            // }
            nData.updated_at = new Date();
            const nUser = await User.updateById(id, nData, {
                upsert: false,
                new: true,
            });

            // if (mobileChanged) {
            //     notify.sendVerificationSms(nUser);
            // }

            return nUser;
        } catch (err) {
            throw err;
        }
    }

    async delete(id) {
        try {
            const user = await User.getById(id);

            if (_.isNil(user)) {
                throw new this.NotFoundError();
            }

            // Delete images related to this review
            // if (user.image && user.image.length > 0) {
            //     await this.storage.delete(user.image);
            // }

            return user.remove();
        } catch (err) {
            throw err;
        }
    }
}

module.exports = new UserService();
