const _ = require('lodash');
const { ObjectId } = require('mongoose').mongo;

const {
    Job,
    JobQA,
    User,
} = require('../../db/models');
const BaseService = require('../BaseService');
const notify = require('../common/notifications');
class JobService extends BaseService {
    // _notifyUser(employee) {
    //     if (employee.email) {
    //         notify.sendVerificationEmail(employee);
    //     } else if (employee.mobile) {
    //         notify.sendVerificationSms(employee);
    //     }
    // }

    async _isJobExists(jobData) {
        const exists = await Job.exists({
            name: { $ne: undefined, $eq: jobData.name },
        });

        return exists;
    }

    async create(data, files) {
        try {
            const nData = data;
            const exists = await this._isJobExists(nData);
            if (exists) {
                throw new this.ValidationError(0, 'Job already exists');
            }

            if (files && files.length > 0) {
                const [url] = await this.storage.save(files);
                nData.image = url;
            }
            nData.created_at = new Date();

            const job = await Job.create(data);

            // this._notifyUser(employee);

            return job;
        } catch (error) {
            throw error;
        }
    }

    async getAll(params = {}, options = {}) {
        try {
            const nParams = params;
            const qRegEx = new RegExp(nParams.title, 'i');

            const query = {
                title: { $regex: qRegEx },
            };
            nParams.lean = true;
            return Job.getAll(query, nParams, true);
        } catch (err) {
            throw err;
        }
    }

    async getOne(query, params = {}) {
        try {
            return Job.getById(query, params);
        } catch (err) {
            throw err;
        }
    }

    async update(jobId, data, files) {
        try {
            const nData = data;

            // Make sure employee exists, also we need to get his image
            const job = await Job.getById(jobId);
            if (_.isNil(job)) {
                throw new this.NotFoundError();
            }
            if (nData.extraSections) {
                if (files && files.length > 0) {
                    nData.extraSections.images = [];
                    for (let i = 0; i < files.length; i++) {
                        nData.extraSections.images.push(`${files[i].destination}/${files[i].filename}`);
                    }
                }
                job.extraSections.push(nData.extraSections);
                return await job.save();
            }

            const exists = await Job.exists({
                _id: { $ne: jobId },
                name: { $ne: undefined, $eq: data.name },
            });

            if (exists) {
                throw new this.ValidationError(0, 'Job already exists');
            }

            // if (nData.image !== employee.image) {
            //     await this.storage.delete(employee.image);
            // }
            //
            // if (files && files.length > 0) {
            //     const [url] = await this.storage.save(files);
            //     nData.image = url;
            // }
            nData.updated_at = new Date();
            const nJob = await Job.updateById(jobId, nData, {
                upsert: false,
                new: true,
            });

            // if (mobileChanged) {
            //     notify.sendVerificationSms(nEmployee);
            // }

            return nJob;
        } catch (err) {
            throw err;
        }
    }

    async delete(id) {
        // Get job
        const job = await Job.getById(id);

        if (_.isNil(job)) {
            throw new this.NotFoundError();
        }
        return await Job.deleteById(id);
    }

    async addQuestion(id, data) {
        // Get job
        const job = await Job.getById(id);

        if (_.isNil(job)) {
            throw new this.NotFoundError();
        }

        job.questions.push(data);
        return await job.save();
    }

    async getQuestions(id) {
        // Get job
        const job = await Job.getById(id);

        if (_.isNil(job)) {
            throw new this.NotFoundError();
        }
        return job.questions;
    }

    async deleteQuestion (id, questionId) {
        // Get job
        const job = await Job.getById(id);

        if (_.isNil(job)) {
            throw new this.NotFoundError();
        }
        const questions = [];
        for (let i = 0; i < job.questions.length; i += 1) {
            if (job.questions[i]._id.toString() !== questionId ) {
                questions.push(job.questions[i]);
            }
        }
        job.questions = questions;
        return await job.save();
    }

    async submitAnswer(id, userId, data) {
        // Get job
        const nData = data;
        const job = await Job.getById(id);

        if (_.isNil(job)) {
            throw new this.NotFoundError();
        }
        const questionAnswers = [];
        for (let i = 0; i < nData.questions.length; i += 1) {
            questionAnswers.push({
                question: nData.questions[i].question,
                answer: nData.questions[i].answer,
            });
        }
        const questionAnswer = {
            job: await Job.getById(id),
            user: await User.getById(userId),
            questionAnswers,
            created_at: new Date(),
        };
        const answer = await JobQA.create(questionAnswer);
        // Notify user
        await notify.sendApplicationUpdateStatusEmail(answer.user, answer.status, 'job', answer);
        return answer;
    }

    async deleteSection(id, sectionId) {
        // Get job
        const job = await Job.getById(id);

        if (_.isNil(job)) {
            throw new this.NotFoundError();
        }
        const sections = [];
        for (let i = 0; i < job.extraSections.length; i += 1) {
            if (job.extraSections[i]._id.toString() !== sectionId) {
                sections.push(job.extraSections[i]);
            }
        }
        job.extraSections = sections;
        return await job.save();
    }

    async getUserApplications(params = {}) {
        try {
            const nParams = params;
            let emailRegEx;
            let query = {};
            if (nParams.title) {
                query = {
                    ...query,
                    'job.title': nParams.title,
                };
            }
            if (nParams.email) {
                emailRegEx = new RegExp(nParams.email, 'i');
                query = {
                    ...query,
                    'user.email': { $regex: emailRegEx },
                };
            }
            if (nParams.status) {
                query = {
                    ...query,
                    status: nParams.status,
                };
            }
            nParams.lean = true;
            const result = await JobQA.getAll(query, nParams, true);
            return result;
        } catch (e) {
            throw e;
        }
    }

    async getUserApplication(id, applicationId, params = {}) {
        const nParams = params;
        const query = {
            _id: new ObjectId(applicationId),
        };
        nParams.lean = true;
        return await JobQA.getOne(query, nParams, true);
    }

    async updateUserApplicationStatus(applicationId, data) {
        try {
            const nJobApplication = await JobQA.updateById(applicationId, data, {
                upsert: false,
                new: true,
            });
            // Notify user
            await notify.sendApplicationUpdateStatusEmail(nJobApplication.user, nJobApplication.status, 'job', nJobApplication);
            return nJobApplication;
        } catch (err) {
            throw err;
        }
    }

    async deleteUserApplicationStatus(id) {
        // Get jobQA
        const jobQA = await JobQA.getById(id);

        if (_.isNil(jobQA)) {
            throw new this.NotFoundError();
        }
        return await JobQA.deleteById(id);
    }
}

module.exports = new JobService(Job);
