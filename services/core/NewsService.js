const _ = require('lodash');

const {
    News,
} = require('../../db/models');
const BaseService = require('../BaseService');

class NewsService extends BaseService {
    // _notifyUser(employee) {
    //     if (employee.email) {
    //         notify.sendVerificationEmail(employee);
    //     } else if (employee.mobile) {
    //         notify.sendVerificationSms(employee);
    //     }
    // }

    async _isNewsExists(newsData) {
        const exists = await News.exists({
            name: { $ne: undefined, $eq: newsData.name },
        });

        return exists;
    }

    async create(data, files) {
        try {
            const nData = data;
            nData.images = [];
            const exists = await this._isNewsExists(nData);
            if (exists) {
                throw new this.ValidationError(0, 'News already exists');
            }

            nData.images = [];
            if (files && files.length > 0) {
                for (let i = 0; i < files.length; i++) {
                    nData.images.push(`${files[i].destination}/${files[i].filename}`);
                }
            }
            nData.created_at = new Date();

            const news = await News.create(nData);

            // this._notifyUser(employee);

            return news;
        } catch (error) {
            throw error;
        }
    }

    async getAll(params = {}, options = {}) {
        try {
            const nParams = params;
            const qRegEx = new RegExp(nParams.title, 'i');

            const query = {
                title: { $regex: qRegEx },
            };

            nParams.lean = true;
            return News.getAll(query, nParams, true);
        } catch (err) {
            throw err;
        }
    }

    async getOne(query, params = {}) {
        try {
            return News.getById(query, params);
        } catch (err) {
            throw err;
        }
    }

    async update(newsId, data, files) {
        try {
            const nData = data;
            // Make sure employee exists, also we need to get his image
            const news = await News.getById(newsId);
            if (_.isNil(news)) {
                throw new this.NotFoundError();
            }
            if (nData.extraSections) {
                if (files && files.length > 0) {
                    nData.extraSections.images = [];
                    for (let i = 0; i < files.length; i++) {
                        nData.extraSections.images.push(`${files[i].destination}/${files[i].filename}`);
                    }
                }
                news.extraSections.push(nData.extraSections);
                return await news.save();
            }
            const exists = await News.exists({
                _id: { $ne: newsId },
                title: { $ne: undefined, $eq: nData.title },
            });

            if (exists) {
                throw new this.ValidationError(0, 'News already exists');
            }
            nData.images = [];
            if (files && files.length > 0) {
                for (let i = 0; i < files.length; i++) {
                    news.images.push(`${files[i].destination}/${files[i].filename}`);
                }
            }
            nData.images = news.images;
            // if (nData.image !== employee.image) {
            //     await this.storage.delete(employee.image);
            // }
            //
            // if (files && files.length > 0) {
            //     const [url] = await this.storage.save(files);
            //     nData.image = url;
            // }
            nData.updated_at = new Date();
            const nNews = await News.updateById(newsId, nData, {
                upsert: false,
                new: true,
            });

            // if (mobileChanged) {
            //     notify.sendVerificationSms(nEmployee);
            // }

            return nNews;
        } catch (err) {
            throw err;
        }
    }

    async delete(id) {
        // Get news
        const news = await News.getById(id);

        if (_.isNil(news)) {
            throw new this.NotFoundError();
        }
        return await News.deleteById(id);
    }

    async deleteSection(id, sectionId) {
        // Get news
        const news = await News.getById(id);

        if (_.isNil(news)) {
            throw new this.NotFoundError();
        }
        const sections = [];
        for (let i = 0; i < news.extraSections.length; i += 1) {
            if (news.extraSections[i]._id.toString() !== sectionId) {
                sections.push(news.extraSections[i]);
            }
        }
        news.extraSections = sections;
        return await news.save();
    }
}

module.exports = new NewsService(News);
