const _ = require('lodash');

const {
    TeamMember,
} = require('../../db/models');
const BaseService = require('../BaseService');

class TeamMemberService extends BaseService {
    // _notifyUser(employee) {
    //     if (employee.email) {
    //         notify.sendVerificationEmail(employee);
    //     } else if (employee.mobile) {
    //         notify.sendVerificationSms(employee);
    //     }
    // }

    async _isMemberExists(employeeData) {
        const exists = await TeamMember.exists({
                name: { $ne: undefined, $eq: employeeData.name },
            },
        );

        return exists;
    }

    async create(data, files) {
        try {
            const nData = data;
            // const exists = await this._isMemberExists(nData);
            // if (exists) {
            //     throw new this.ValidationError(0, 'Employee already exists');
            // }
            nData.image1 = '';
            nData.image2 = '';
            if (files && files.length > 0) {
                    nData.image1 = `${files[0].destination}/${files[0].filename}`;
                    nData.image2 = `${files[1].destination}/${files[1].filename}`;
            }
            nData.created_at = new Date();
            const employee = await TeamMember.create(nData);

            // this._notifyUser(employee);

            return employee;
        } catch (error) {
            throw error;
        }
    }

    async getAll(params = {}, options = {}) {
        try {
            const nParams = params;
            const nameRegEx = new RegExp(nParams.name, 'i');

            const query = nParams.name ? { name: { $regex: nameRegEx } } : {};

            // Roles filter
            // const { roles } = params;
            // if (roles && roles.length > 0) {
            //     const qRoles = roles.split(',').map(role => role.trim());
            //     query.roles = {
            //         $in: qRoles,
            //     };
            // }

            nParams.lean = true;
            return TeamMember.getAll(query, nParams, true);
        } catch (err) {
            throw err;
        }
    }

    async getById(id, params = {}) {
        try {
            return TeamMember.getById(id, params);
        } catch (err) {
            throw err;
        }
    }

    async getOne(query, params = {}) {
        try {
            return TeamMember.getById(query, params);
        } catch (err) {
            throw err;
        }
    }

    async update(employeeId, data, files) {
        try {
            const nData = data;

            // Make sure employee exists, also we need to get his image
            const employee = await TeamMember.getById(employeeId);
            if (_.isNil(employee)) {
                throw new this.NotFoundError();
            }

            // if(data.roles) {
            //     employee.roles = data.roles;
            // }

            // const exists = await TeamMember.exists({
            //     _id: { $ne: employeeId },
            //     $or: [{
            //         name: { $ne: undefined, $eq: nData.name },
            //     }],
            // });

            // if (exists) {
            //     throw new this.ValidationError(0, 'User already exists');
            // }

            if (files && files.length > 0) {
                nData.image1 = '';
                nData.image2 = '';
                nData.image1 = `${files[0].destination}/${files[0].filename}`;
                nData.image2 = `${files[1].destination}/${files[1].filename}`;
            }
            nData.updated_at = new Date();
            const nEmployee = await TeamMember.updateById(employeeId, nData, {
                upsert: false,
                new: true,
            });

            // if (mobileChanged) {
            //     notify.sendVerificationSms(nEmployee);
            // }

            return nEmployee;
        } catch (err) {
            throw err;
        }
    }

    async delete(id) {
        // Get employee
        const employee = await TeamMember.getById(id);

        if (_.isNil(employee)) {
            throw new this.NotFoundError();
        }
        return await TeamMember.deleteById(id);
    }
}

module.exports = new TeamMemberService(TeamMember);
