const _ = require('lodash');

const {
    Partner,
} = require('../../db/models');
const BaseService = require('../BaseService');

class PartnerService extends BaseService {
    // _notifyUser(employee) {
    //     if (employee.email) {
    //         notify.sendVerificationEmail(employee);
    //     } else if (employee.mobile) {
    //         notify.sendVerificationSms(employee);
    //     }
    // }

    async _isPartnerExists(partnerData) {
        const exists = await Partner.exists({
            name: { $ne: undefined, $eq: partnerData.name },
        });

        return exists;
    }

    async create(data, files) {
        try {
            const nData = data;
            // const exists = await this._isPartnerExists(nData);
            // if (exists) {
            //     throw new this.ValidationError(0, 'Partner already exists');
            // }
            nData.images = [];
            if (files && files.length > 0) {
                for (let i = 0; i < files.length; i++) {
                    nData.image = `${files[i].destination}/${files[i].filename}`;
                }
            }
            nData.created_at = new Date();
            const partner = await Partner.create(nData);

            // this._notifyUser(employee);

            return partner;
        } catch (error) {
            throw error;
        }
    }

    async getAll(params = {}, options = {}) {
        try {
            const nParams = params;
            const emailRegEx = new RegExp(nParams.email, 'i');
            const nameRegEx = new RegExp(nParams.name, 'i');

            const query = {
                $or: [{ email: { $regex: emailRegEx } }, { name: { $regex: nameRegEx } }],
            };
            nParams.lean = true;
            return Partner.getAll(query, nParams, true);
        } catch (err) {
            throw err;
        }
    }

    async getOne(query, params = {}) {
        try {
            return Partner.getById(query, params);
        } catch (err) {
            throw err;
        }
    }

    async update(partnerId, data, files) {
        try {
            const nData = data;

            // Make sure employee exists, also we need to get his image
            const partner = await Partner.getById(partnerId);
            if (_.isNil(partner)) {
                throw new this.NotFoundError();
            }

            // const exists = await Partner.exists({
            //     _id: { $ne: partnerId },
            //     name: { $ne: undefined, $eq: nData.name },
            // });
            //
            // if (exists) {
            //     throw new this.ValidationError(0, 'Partner already exists');
            // }
            nData.image = [];
            if (files && files.length > 0) {
                for (let i = 0; i < files.length; i++) {
                    partner.image = `${files[i].destination}/${files[i].filename}`;
                }
            }
            nData.image = partner.image;
            nData.updated_at = new Date();

            const nPartner = await Partner.updateById(partnerId, nData, {
                upsert: false,
                new: true,
            });

            // if (mobileChanged) {
            //     notify.sendVerificationSms(nEmployee);
            // }

            return nPartner;
        } catch (err) {
            throw err;
        }
    }

    async delete(id) {
        // Get partner
        const partner = await Partner.getById(id);

        if (_.isNil(partner)) {
            throw new this.NotFoundError();
        }
        return await Partner.deleteById(id);
    }
}

module.exports = new PartnerService(Partner);
