const _ = require('lodash');
const { ObjectId } = require('mongoose').mongo;

const {
    Events,
    EventQA,
    User,
} = require('../../db/models');
const BaseService = require('../BaseService');
const notify = require('../common/notifications');
class EventService extends BaseService {
    // _notifyUser(employee) {
    //     if (employee.email) {
    //         notify.sendVerificationEmail(employee);
    //     } else if (employee.mobile) {
    //         notify.sendVerificationSms(employee);
    //     }
    // }

    async _isEventsExists(eventData) {
        const exists = await Events.exists({
            name: { $ne: undefined, $eq: eventData.name },
        });

        return exists;
    }

    async create(data, files) {
        try {
            const nData = data;
            const exists = await this._isEventsExists(nData);
            if (exists) {
                throw new this.ValidationError(0, 'Events already exists');
            }

            nData.images = [];
            if (files && files.length > 0) {
                for (let i = 0; i < files.length; i++) {
                    nData.images.push(`${files[i].destination}/${files[i].filename}`);
                }
            }
            nData.created_at = new Date();

            const event = await Events.create(nData);

            // this._notifyUser(employee);

            return event;
        } catch (error) {
            throw error;
        }
    }

    async getAll(params = {}, options = {}) {
        try {
            const nParams = params;
            const qRegEx = new RegExp(nParams.title, 'i');

            const query = {
                title: { $regex: qRegEx },
            };

            nParams.lean = true;
            return Events.getAll(query, nParams, true);
        } catch (err) {
            throw err;
        }
    }

    async getOne(query, params = {}) {
        try {
            return Events.getById(query, params);
        } catch (err) {
            throw err;
        }
    }

    async update(eventId, data, files) {
        try {
            const nData = data;

            // Make sure employee exists, also we need to get his image
            const event = await Events.getById(eventId);
            if (_.isNil(event)) {
                throw new this.NotFoundError();
            }
            if (nData.extraSections) {
                if (files && files.length > 0) {
                    nData.extraSections.images = [];
                    for (let i = 0; i < files.length; i++) {
                        nData.extraSections.images.push(`${files[i].destination}/${files[i].filename}`);
                    }
                }
                event.extraSections.push(nData.extraSections);
                return await event.save();
            }

            const exists = await Events.exists({
                _id: { $ne: eventId },
                title: { $ne: undefined, $eq: data.title },
            });

            if (exists) {
                throw new this.ValidationError(0, 'Events already exists');
            }
            nData.images = [];
            if (files && files.length > 0) {
                for (let i = 0; i < files.length; i++) {
                    event.images.push(`${files[i].destination}/${files[i].filename}`);
                }
            }
            nData.images = event.images;
            nData.updated_at = new Date();
            const nEvents = await Events.updateById(eventId, nData, {
                upsert: false,
                new: true,
            });

            // if (mobileChanged) {
            //     notify.sendVerificationSms(nEmployee);
            // }

            return nEvents;
        } catch (err) {
            throw err;
        }
    }

    async delete(id) {
        // Get event
        const event = await Events.getById(id);

        if (_.isNil(event)) {
            throw new this.NotFoundError();
        }
        return await Events.deleteById(id);
    }

    async addQuestion(id, data) {
        // Get event
        const event = await Events.getById(id);

        if (_.isNil(event)) {
            throw new this.NotFoundError();
        }

        event.questions.push(data);
        return await event.save();
    }

    async getQuestions(id) {
        // Get event
        const event = await Events.getById(id);

        if (_.isNil(event)) {
            throw new this.NotFoundError();
        }
        return event.questions;
    }

    async deleteQuestion(id, questionId) {
        // Get event
        const event = await Events.getById(id);

        if (_.isNil(event)) {
            throw new this.NotFoundError();
        }
        const questions = [];
        for (let i = 0; i < event.questions.length; i += 1) {
            if (event.questions[i]._id.toString() !== questionId ) {
                questions.push(event.questions[i]);
            }
        }
        event.questions = questions;
        return await event.save();
    }

    async submitAnswer(id, userId, data) {
        // Get event
        const nData = data;
        const program = await Events.getById(id);

        if (_.isNil(program)) {
            throw new this.NotFoundError();
        }
        const questionAnswers = [];
        for (let i = 0; i < nData.questions.length; i += 1) {
            questionAnswers.push({
                question: nData.questions[i].question,
                answer: nData.questions[i].answer,
            });
        }
        const questionAnswer = {
            event: await Events.getById(id),
            user: await User.getById(userId),
            questionAnswers,
            created_at: new Date(),
        };
        const answer = await EventQA.create(questionAnswer);
        return answer;
    }

    async deleteSection(id, sectionId) {
        // Get event
        const event = await Events.getById(id);

        if (_.isNil(event)) {
            throw new this.NotFoundError();
        }
        const sections = [];
        for (let i = 0; i < event.extraSections.length; i += 1) {
            if (event.extraSections[i]._id.toString() !== sectionId) {
                sections.push(event.extraSections[i]);
            }
        }
        event.extraSections = sections;
        return await event.save();
    }

    async getUserApplications(params = {}) {
        try {
            const nParams = params;
            let emailRegEx;
            let query = {};
            if (nParams.title) {
                query = {
                    ...query,
                    'job.title': nParams.title,
                };
            }
            if (nParams.email) {
                emailRegEx = new RegExp(nParams.email, 'i');
                query = {
                    ...query,
                    'user.email': { $regex: emailRegEx },
                };
            }
            if (nParams.status) {
                query = {
                    ...query,
                    status: nParams.status,
                };
            }
            nParams.lean = true;
            const result = await EventQA.getAll(query, nParams, true);
            return result;
        } catch (e) {
            throw e;
        }
    }

    async getUserApplication(id, applicationId, params = {}) {
        const nParams = params;
        const query = {
            _id: new ObjectId(applicationId),
        };
        nParams.lean = true;
        return await EventQA.getOne(query, nParams, true);
    }

    async updateUserApplicationStatus(applicationId, data) {
        try {
            const nEventApplication = await EventQA.updateById(applicationId, data, {
                upsert: false,
                new: true,
            });
            // Notify user
            await notify.sendApplicationUpdateStatusEmail(nEventApplication.user, nEventApplication.status);
            return nEventApplication;
        } catch (err) {
            throw err;
        }
    }

    async deleteUserApplicationStatus(id) {
        // Get eventQA
        const eventQA = await EventQA.getById(id);

        if (_.isNil(eventQA)) {
            throw new this.NotFoundError();
        }
        return await EventQA.deleteById(id);
    }
}

module.exports = new EventService(Events);
