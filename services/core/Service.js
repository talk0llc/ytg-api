const _ = require('lodash');

const {
    Service,
} = require('../../db/models');
const BaseService = require('../BaseService');

class ServicesService extends BaseService {
    // _notifyUser(employee) {
    //     if (employee.email) {
    //         notify.sendVerificationEmail(employee);
    //     } else if (employee.mobile) {
    //         notify.sendVerificationSms(employee);
    //     }
    // }

    async _isServiceExists(serviceData) {
        const exists = await Service.exists({
            name: { $ne: undefined, $eq: serviceData.name },
        });

        return exists;
    }

    async create(data, files) {
        try {
            const nData = data;
            const exists = await this._isServiceExists(nData);
            if (exists) {
                throw new this.ValidationError(0, 'Service already exists');
            }

            nData.images = [];
            if (files && files.length > 0) {
                for (let i = 0; i < files.length; i++) {
                    nData.images.push(`${files[i].destination}/${files[i].filename}`);
                }
            }
            nData.created_at = new Date();
            const service = await Service.create(data);

            // this._notifyUser(employee);

            return service;
        } catch (error) {
            throw error;
        }
    }

    async getAll(params = {}, options = {}) {
        try {
            const nParams = params;
            const qRegEx = new RegExp(nParams.name, 'i');

            const query = {
                name: { $regex: qRegEx },
            };

            nParams.lean = true;
            return Service.getAll(query, nParams, true);
        } catch (err) {
            throw err;
        }
    }

    async getOne(query, params = {}) {
        try {
            return Service.getById(query, params);
        } catch (err) {
            throw err;
        }
    }

    async update(serviceId, data, files) {
        try {
            const nData = data;

            // Make sure employee exists, also we need to get his image
            const service = await Service.getById(serviceId);
            if (_.isNil(service)) {
                throw new this.NotFoundError();
            }
            if (nData.extraSections) {
                if (files && files.length > 0) {
                    nData.extraSections.images = [];
                    for (let i = 0; i < files.length; i++) {
                        nData.extraSections.images.push(`${files[i].destination}/${files[i].filename}`);
                    }
                }
                service.extraSections.push(nData.extraSections);
                return await service.save();
            }

            const exists = await Service.exists({
                _id: { $ne: serviceId },
                name: { $ne: undefined, $eq: nData.name },
            });

            if (exists) {
                throw new this.ValidationError(0, 'Service already exists');
            }
            nData.images = [];
            if (files && files.length > 0) {
                for (let i = 0; i < files.length; i++) {
                    service.images.push(`${files[i].destination}/${files[i].filename}`);
                }
            }
            nData.images = service.images;
            nData.updated_at = new Date();
            const nService = await Service.updateById(serviceId, nData, {
                upsert: false,
                new: true,
            });

            // if (mobileChanged) {
            //     notify.sendVerificationSms(nEmployee);
            // }

            return nService;
        } catch (err) {
            throw err;
        }
    }

    async delete(id) {
        // Get service
        const service = await Service.getById(id);

        if (_.isNil(service)) {
            throw new this.NotFoundError();
        }
        return await Service.deleteById(id);
    }

    async deleteSection(id, sectionId) {
        // Get event
        const service = await Service.getById(id);

        if (_.isNil(service)) {
            throw new this.NotFoundError();
        }
        const sections = [];
        for (let i = 0; i < service.extraSections.length; i += 1) {
            if (service.extraSections[i]._id.toString() !== sectionId) {
                sections.push(service.extraSections[i]);
            }
        }
        service.extraSections = sections;
        return await service.save();
    }
}

module.exports = new ServicesService(Service);
