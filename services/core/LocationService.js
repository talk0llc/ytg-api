const _ = require('lodash');
// const Country = require('../ModelsFactory').create('Country');
const BaseService = require('../BaseService');
const { Country } = require('../../db/models');
const { NotFoundError } = require('../../helpers/errors');

class LocationService extends BaseService {
    constructor() {
        super(Country);
    }

    async getAll(params = {}, pagination = true) {
        try {
            const nParams = params;
            nParams.lean = true;
            return Country.getAll({
                is_active: true,
            }, nParams, pagination);
        } catch (err) {
            throw (err);
        }
    }

    async getOne(countryId, params = {}) {
        try {
            const nParams = params;
            nParams.lean = true;
            const query = ({
                _id: countryId,
                is_active: true,
            });

            const country = Country.getOne(query, nParams);
            if (_.isNil(country)) {
                throw new NotFoundError();
            }
            return country;
        } catch (err) {
            throw err;
        }
    }
}


module.exports = new LocationService();
