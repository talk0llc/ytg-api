const _ = require('lodash');

const {
    Employee,
} = require('../../db/models');
const BaseService = require('../BaseService');

class EmployeeService extends BaseService {
    // _notifyUser(employee) {
    //     if (employee.email) {
    //         notify.sendVerificationEmail(employee);
    //     } else if (employee.mobile) {
    //         notify.sendVerificationSms(employee);
    //     }
    // }

    async _isEmployeeExists(employeeData) {
        const exists = await Employee.exists({
            $or: [{
                email: { $ne: undefined, $eq: employeeData.email },
            }, {
                mobile: { $ne: undefined, $eq: employeeData.mobile },
            }],
        });

        return exists;
    }

    async create(data, files) {
        try {
            const nData = data;
            const exists = await this._isEmployeeExists(nData);
            if (exists) {
                throw new this.ValidationError(0, 'Employee already exists');
            }
            nData.images = [];
            if (files && files.length > 0) {
                for (let i = 0; i < files.length; i++) {
                    nData.images.push(`${files[i].destination}/${files[i].filename}`);
                }
            }
            const employee = await Employee.create(nData);
            nData.created_at = new Date();
            // this._notifyUser(employee);

            return employee;
        } catch (error) {
            throw error;
        }
    }

    async getAll(params = {}, options = {}) {
        try {
            const nParams = params;
            const emailRegEx = new RegExp(nParams.email, 'i');
            const nameRegEx = new RegExp(nParams.name, 'i');

            const query = {
                $and: [{ email: { $ne: nParams.user.email } }, { email: { $ne: 'admin@talk.com' } }, { $or: [{ email: { $regex: emailRegEx } }, { name: { $regex: nameRegEx } }] }],
            };

            // Roles filter
            // const { roles } = params;
            // if (roles && roles.length > 0) {
            //     const qRoles = roles.split(',').map(role => role.trim());
            //     query.roles = {
            //         $in: qRoles,
            //     };
            // }
            nParams.lean = true;
            return Employee.getAll(query, nParams, true);
        } catch (err) {
            throw err;
        }
    }

    async getById(id, params = {}) {
        try {
            return Employee.getById(id, params);
        } catch (err) {
            throw err;
        }
    }

    async getOne(query, params = {}) {
        try {
            return Employee.getById(query, params);
        } catch (err) {
            throw err;
        }
    }

    async update(employeeId, data, files) {
        try {
            const nData = data;

            // Make sure employee exists, also we need to get his image
            const employee = await Employee.getById(employeeId);
            if (_.isNil(employee)) {
                throw new this.NotFoundError();
            }

            // if(data.roles) {
            //     employee.roles = data.roles;
            // }

            const exists = await Employee.exists({
                _id: { $ne: employeeId },
                $or: [{
                    email: { $ne: undefined, $eq: nData.email },
                }, {
                    mobile: { $ne: undefined, $eq: nData.mobile },
                }],
            });

            if (exists) {
                throw new this.ValidationError(0, 'User already exists');
            }
            nData.images = [];
            if (files && files.length > 0) {
                for (let i = 0; i < files.length; i++) {
                    employee.images.push(`${files[i].destination}/${files[i].filename}`);
                }
            }
            nData.images = employee.images;
            nData.updated_at = new Date();
            const nEmployee = await Employee.updateById(employeeId, nData, {
                upsert: false,
                new: true,
            });

            // if (mobileChanged) {
            //     notify.sendVerificationSms(nEmployee);
            // }

            return nEmployee;
        } catch (err) {
            throw err;
        }
    }

    async delete(id) {
        // Get employee
        const employee = await Employee.getById(id);

        if (_.isNil(employee)) {
            throw new this.NotFoundError();
        }
        return await Employee.deleteById(id);
    }
}

module.exports = new EmployeeService(Employee);
