const _ = require('lodash');

const {
    Contents,
} = require('../../db/models');
const BaseService = require('../BaseService');

class ContentService extends BaseService {

    async delete(id) {
        try {
            // Get content
            const content = await Contents.getOne({ _id: id });

            // if (_.isNil(content)) {
            //     throw new this.NotFoundError();
            // }
            await Contents.deleteById(id);
            return content;
        } catch (error) {
            throw error;
        }
    }

    async saveIVLink (data) {
        try {
            let content;
            const exists = await Contents.getOne({ key: data.key });
            if (exists) {
                content = await Contents.updateOne({
                    link: data.link,
                }, { new: true });
            } else {
                content = await Contents.create(data);
            }
            return content;
        } catch (e) {
            throw e;
        }
    }

}

module.exports = new ContentService(Contents);
