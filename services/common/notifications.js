/* eslint-disable no-param-reassign */
const config = require('../../config/index');
const { smsNotification, emailNotification } = require('../../helpers/notifications/index');
const { UnauthorizedError, ErrorCodes } = require('../../helpers/errors/index');
const {
    Contents,
} = require('../../db/models');
const smsDelay = 60 * 1000; // 1 min

class Notifications {
    // static async sendVerificationSms(user) {
    //     try {
    //         if (user.is_verified) {
    //             return;
    //         }
    //
    //         // Check last generation time, if less than a minute:
    //         const elapsedTime = Date.now() - (user.reset_token_time || 0);
    //
    //         if (elapsedTime < smsDelay) {
    //             user.reset_token_time = Date.now();
    //             await user.save();
    //
    //             const waitTime = Math.round((smsDelay - elapsedTime) / 1000);
    //             throw new UnauthorizedError(ErrorCodes.userSmsDelay, `Frequent requests are not allowed, you need to wait ${waitTime} seconds`);
    //         }
    //
    //         // Generate token
    //         const smsToken = user.generateSmsToken();
    //         const tokenTime = Date.now();
    //         const tokenExpiration = Date.now() + config.expiration.sms;
    //
    //         user.reset_token = smsToken;
    //         user.reset_token_time = tokenTime;
    //         user.reset_token_expiration = tokenExpiration;
    //
    //         await user.save();
    //
    //         // Send sms
    //         smsNotification.send({
    //             to: user.mobile,
    //             body: `Your verification code: ${user.reset_token} .`,
    //         });
    //     } catch (err) {
    //         console.log(err);
    //     }
    // }

    static async sendVerificationEmail(user) {
        // if (user.is_verified) {
        //     return;
        // }

        // Generate reset token
        user.reset_token = user.generateEmailToken();
        user.reset_token_time = Date.now();
        user.reset_token_expiration = Date.now() + config.expiration.email;

        await user.save();
        // Send email
        await emailNotification.send({
            subject: 'youthinkgreen Egypt - Account Verification',
            type: 'email_verification',
            to: user.email,
            name: `${user.first_name}`,
            token: user.reset_token,
        });
    }


    static async sendPasswordResetEmail(user) {
        // Generate reset token
        user.reset_token = user.generateEmailToken();
        user.reset_token_time = Date.now();
        user.reset_token_expiration = Date.now() + config.expiration.email;

        await user.save();
        // Send email
        await emailNotification.send({
            subject: 'youthinkgreen Egypt - Password Reset',
            type: 'password_reset',
            to: user.email,
            name: `${user.first_name}`,
            token: user.reset_token,
        });
    }

    static async sendApplicationUpdateStatusEmail(user, status, model, object) {
        // let content = await Contents.getOne({ key: 'link' });
        // if (!content.link) {
        //     content.link = '';
        // }
        if (model === 'program') {
            switch (status) {
                case 'submitted':
                    // Send email
                    await emailNotification.send({
                        dear: `Hello ${user.first_name}`,
                        subject: 'Thank you - we have received your ytg application',
                        type: 'application_status',
                        to: user.email,
                        name: `${user.first_name}`,
                        body: `Thank you for your interest in applying to ${object.program.name}.We’ve received your application, and we will be reviewing it shortly. 
Please keep an eye on your e-mail as we will get back to you soon.
We wish you the best of luck.`,
                        status: 'APP SUBMITTED',
                        thanks: 'Sincerely,',
                    });
                    break;
                case 'in-review':
                    // Send email
                    await emailNotification.send({
                        dear: `Hello ${user.first_name}`,
                        subject: 'youthinkgreen Egypt - We are reviewing your application',
                        type: 'application_status',
                        to: user.email,
                        name: `${user.first_name}`,
                        body: `We are sending this e-mail to let you know that we have started reviewing your application to ${object.program.name}. Please keep an eye on your email inbox for the updates.`,
                        status: 'APP IN REVIEW',
                        thanks: 'Sincerely,',
                    });
                    break;
                case 'invited':
                    // Send email
                    await emailNotification.send({
                        dear: `Hello ${user.first_name}`,
                        subject: 'youthinkgreen Egypt - you are called for an interview',
                        type: 'application_status',
                        to: user.email,
                        name: `${user.first_name}`,
                        body: `We are reaching out to tell you that your application to ${object.program.name} is accepeted and you are cordially invited to pick a suitable face to face interview time slot.
Please follow the link below to proceed with the scheduling process: ${object.link}`,
                        status: 'SCHEDULED FOR AN INTERVIEW',
                        thanks: 'Best of luck,',
                    });
                    break;
                case 'app-rejected':
                    // Send email
                    await emailNotification.send({
                        dear: `Hello ${user.first_name}`,
                        subject: 'youthinkgreen Egypt - We are sorry',
                        type: 'application_status',
                        to: user.email,
                        name: `${user.first_name}`,
                        body: `We are very sorry to tell you that we wouldn't go forward with your application to ${object.program.name}.
We encourage you to follow our updates and apply for our upcoming programs. `,
                        status: 'REJECTED',
                        thanks: 'Sincerely,',
                    });
                    break;
                case 'iv-rejected':
                    // Send email
                    await emailNotification.send({
                        dear: `Hello ${user.first_name}`,
                        subject: 'youthinkgreen Egypt - We are sorry',
                        type: 'application_status',
                        to: user.email,
                        name: `${user.first_name}`,
                        body: `We are very sorry to tell you that we wouldn't go forward with your application to ${object.program.name}.
We encourage you to follow our updates and apply for our upcoming programs. `,
                        status: 'REJECTED',
                        thanks: 'Sincerely,',
                    });
                    break;
                case 'accepted':
                    // Send email
                    await emailNotification.send({
                        dear: `${user.first_name}, CONGRATULATIONS!`,
                        subject: 'Congratulations - your application is accepted',
                        type: 'application_status',
                        to: user.email,
                        name: `${user.first_name}`,
                        body: `All of us at youthinkgreen Egypt are excited to tell you that your application to ${object.program.name} has been accepted.
One of our team will get in touch with you very soon to complete the on-boarding procedures.`,
                        status: 'ACCEPTED',
                        thanks: 'Yours,',
                    });
                    break;
                default:
            }
        }
        if (model === 'job') {
            switch (status) {
                case 'submitted':
                    // Send email
                    await emailNotification.send({
                        dear: `Hello ${user.first_name}`,
                        subject: 'Thank you - we have received your ytg application',
                        type: 'application_status',
                        to: user.email,
                        name: `${user.first_name}`,
                        body: `Thank you for your interest in joining the team at youthinkgreen Egypt. 
We’ve received your application to ${object.job.title}, and will be reviewing your qualifications shortly. Should your background and experience align with the needs of the position, one of our team will contact you.
Again, we appreciate your interest in youthinkgreen Egypt and wish you the best of luck in your career pursuits.`,
                        status: 'APP SUBMITTED',
                        thanks: 'Sincerely',
                    });
                    break;
                case 'in-review':
                    // Send email
                    await emailNotification.send({
                        dear: `Hello ${user.first_name}`,
                        subject: 'youthinkgreen Egypt - We are reviewing your application',
                        type: 'application_status',
                        to: user.email,
                        name: `${user.first_name}`,
                        body: `We started reviewing your application to ${object.job.title}. Should your background and experience align with the needs of the position, one of our team will contact you for an Interview.`,
                        status: 'APP IN REVIEW',
                        thanks: 'Sincerely',
                    });
                    break;
                case 'invited':
                    // Send email
                    await emailNotification.send({
                        dear: `Hello ${user.first_name}`,
                        subject: 'youthinkgreen Egypt - you are called for an interview',
                        type: 'application_status',
                        to: user.email,
                        name: `${user.first_name}`,
                        body: `We are reaching out to tell you that your application to ${object.job.title} is accepeted and you are cordially invited to pick a suitable face to face interview time.
Please follow the link below to proceed with the scheduling process: ${object.link}`,
                        status: 'SCHEDULED FOR AN INTERVIEW',
                        thanks: 'Best of luck,',
                    });
                    break;
                case 'app-rejected':
                    // Send email
                    await emailNotification.send({
                        dear: `Hello ${user.first_name}`,
                        subject: 'youthinkgreen Egypt - We are sorry',
                        type: 'application_status',
                        to: user.email,
                        name: `${user.first_name}`,
                        body: `We are very sorry to tell you that we wouldn't go forward with your application to ${object.job.title}.
We encourage you to apply for our current/future opening vacancies and follow our updates.
We appreciate your interest in youthinkgreen Egypt and wish you the best of luck in your career pursuits.`,
                        status: 'REJECTED',
                        thanks: 'Sincerely,',
                    });
                    break;
                case 'iv-rejected':
                    // Send email
                    await emailNotification.send({
                        dear: `Hello ${user.first_name}`,
                        subject: 'youthinkgreen Egypt - We are sorry',
                        type: 'application_status',
                        to: user.email,
                        name: `${user.first_name}`,
                        body: `We are very sorry to tell you that we wouldn't go forward with your application to ${object.job.title}.
We encourage you to apply for our current/future opening vacancies and follow our updates.
We appreciate your interest in youthinkgreen Egypt and wish you the best of luck in your career pursuits.`,
                        status: 'REJECTED',
                        thanks: 'Sincerely,',
                    });
                    break;
                case 'accepted':
                    // Send email
                    await emailNotification.send({
                        dear: `${user.first_name}, CONGRATULATIONS!`,
                        subject: 'Congratulations - your application is accepted',
                        type: 'application_status',
                        to: user.email,
                        name: `${user.first_name}`,
                        body: `All of us at youthinkgreen Egypt are delighted to tell you that your application to ${object.job.title} has been accepted.
One of our team will get in touch with you very soon to complete the hiring procedures.`,
                        status: 'ACCEPTED',
                        thanks: 'Yours,',
                    });
                    break;
                default:
            }

        }
    }
}

module.exports = Notifications;
