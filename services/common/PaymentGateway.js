
const _ = require('lodash');
const { PAYMENT_GATEWAY } = require('../../config/config.dev');

const stripe = require('stripe')(PAYMENT_GATEWAY.SECRET_KEY);
const { User } = require('../../db/models');
const { PaymentError } = require('../../helpers/errors');

class PaymentGateway {
    async saveCardInfo(cardNumber, user) {
        try {
            if (_.isNil(cardNumber) && _.isNil(user)) {
                throw Error('Must pass cardNumber and user arguments');
            }
            const paymentCustomerInfo = await stripe.customers.create({
                source: cardNumber,
                email: user.email,
            });
            if (_.isNil(user.paymentCards) || user.paymentCards.length === 0) {
                user.paymentCards = [];
            }
            const last4DigitsOfCard = cardNumber.substring(cardNumber.length - 4, cardNumber.length);
            const paymentCard = {
                last4DigitsOfCard,
                paymentToken: paymentCustomerInfo.id,
            };
            user.paymentCards.push(paymentCard);
            await user.save();
            return paymentCard;
        } catch (error) {
            throw error;
        }
    }

    async chargeCustomer(paymentToken, amount, currency) {
        try {
            const chargeResult = await stripe.charges.create({
                amount: (amount * 100),
                currency,
                customer: paymentToken,
            });
            return chargeResult;
        } catch (e) {
            throw e;
        }
    }

    getPaymentCard(user, paymentCardId) {
        if (!_.isNil(user) && user.paymentCards) {
            const paymentCard = user.paymentCards.find((paymentCard) => paymentCard._id.toString() === paymentCardId);
            if (_.isNil(paymentCard)) {
                throw new PaymentError(0, `card id doesn't belong to this customer: ${user._id}`);
            }
            return paymentCard;
        }
        throw new PaymentError(0, `card id doesn't belong to this customer: ${user._id}`);
    }

    async refundCharge(chargeId, amount) {
        try {
            const refundPayment = {
                charge: chargeId,
            };

            if (!_.isNil(amount)) {
                refundPayment.amount = amount;
            }

            const refund = stripe.refunds.create(refundPayment);
            return refund;
        } catch (e) {
            throw e;
        }
    }
}

// // TODO, to be refactored to set address values in the order
// async function generateCheckoutAddress(order, user) {
//     const addrLine1 = `${order.address.street} ${order.address.building} ${order.address.floor} ${order.address.apartment}`;

//     const country = await Country.getById(order.address.country);
//     const city = country.cities.find((item) => item._id.toString() === order.address.city );
//     const billingAddr = {
//         name: `${user.first_name} ${user.last_name}`,
//         addrLine1,
//         email: `${user.email}`,
//         phoneNumber: `${user.mobile}`,
//         country: `${country.name}`,
//         city: city.name,
//         street: `${order.address.street}`,
//         building: `${order.address.building}`,
//         floor: `${order.address.floor}`,
//         apartment: `${order.address.apartment}`,
//     };
//     return billingAddr;
// }


module.exports = new PaymentGateway();
