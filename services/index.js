
const EmployeeService = require('./core/EmployeeService');
const UserAddressService = require('./core/UserAddressService');
const UserService = require('./core/UserService');
const LocationService = require('./core/LocationService');
const AuthService = require('./core/AuthService');
const ContentService = require('./core/ContentService');
const ProgramService = require('./core/ProgramService');
const EventService = require('./core/EventService');
const NewsService = require('./core/NewsService');
const JobService = require('./core/JobService');
const PartnerService = require('./core/PartnerService');
const Service = require('./core/Service');
const TeamMemberService = require('./core/TeamMemberService');


module.exports = {
    AuthService,
    EmployeeService,
    UserService,
    UserAddressService,
    LocationService,
    ContentService,
    ProgramService,
    EventService,
    JobService,
    NewsService,
    PartnerService,
    Service,
    TeamMemberService
};
