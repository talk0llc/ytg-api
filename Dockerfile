FROM node:9.5

#RUN sudo apt-get install python
ADD package.json /tmp/package.json
RUN cd /tmp && npm install
RUN mkdir -p /usr/src/app && cp -a /tmp/node_modules /usr/src/app

WORKDIR /usr/src/app
ADD . /usr/src/app

HEALTHCHECK CMD curl --fail http://localhost:3000/HEALTHZ || exit 1

CMD [ "node", "index.js" ]
