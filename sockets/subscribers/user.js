const _ = require('lodash');
const sm = require('../sockerManager');
const Table = require('mongoose').model('Table');
const Request = require('mongoose').model('Request');

const SOCKET_UNIT = 'users';
const USER_REQUEST_EVENT = 'dine_in_request';

sm.subscribe(SOCKET_UNIT, USER_REQUEST_EVENT, async (client, data) => {
    const tableId = data.table;
    const userId = data.user;
    const requestType = data.type;

    let table;
    let tableEmployees;
    let employeeId;

    switch (requestType) {
        case 'help':
        case 'receipt':
            table = await Table.findById(tableId);

            if (_.isNil(table) && (!table.employees || table.employees.length === 0)) {
                return;
            }

            tableEmployees = table.employees;
            for (let i = 0; i < tableEmployees.length; i += 1) {
                sm.publishToUser(tableEmployees[i], 'data', {
                    event: USER_REQUEST_EVENT,
                    type: requestType,
                });
            }

            break;
        case 'shisha':
            table = await Table.findById(tableId)
                .populate('employees');

            if (_.isNil(table) && (!table.employees || table.employees.length === 0)) {
                return;
            }

            employeeId = table.employees[0]._id;
            tableEmployees = table.employees;
            for (let i = 0; i < tableEmployees.length; i += 1) {
                if (tableEmployees[i].roles.indexOf('shisha') >= 0) {
                    employeeId = tableEmployees[i];
                    break;
                }
            }

            sm.publishToUser(employeeId, 'data', {
                event: USER_REQUEST_EVENT,
                type: requestType,
            });

            break;

        default:
            break;
    }
});
