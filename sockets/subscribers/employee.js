const _ = require('lodash');
const sm = require('../sockerManager');

const SOCKET_UNIT = 'employees';
const E_LOCATION_CHANGED = 'location_changed';

sm.subscribe(SOCKET_UNIT, E_LOCATION_CHANGED, async (client, data) => {
    console.log(data);
    sm.publishToRole('admin', E_LOCATION_CHANGED, {
        event: E_LOCATION_CHANGED,
        _id: data._id,
        type: data.type,
        location: {
            lat: data.lat,
            lng: data.lng,
        }
    });
});
