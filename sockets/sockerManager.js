const _ = require('lodash');
const SocketServer = require('socket.io');
const { authenticate } = require('./authentication');

/**
 * You can only send "data" message to this server,
 * It must contain "event" then all other data you want to send
 * {
 *   event: 'help'
 * }
 */
class Server {
    constructor() {
        this.options = {
            pingTimeout: 5000,
            pingInterval: 2000,
            path: '/io',
        };

        this.io = null;
        this.connected = 0;
        this.clients = {};
        this.subscribers = [];
    }

    initialize(httpServer) {
        this.io = new SocketServer(httpServer, this.options);
        this.io.use(authenticate);
        this.io.on('connection', this.onClientConnected.bind(this));
    }

    addClient(socket) {
        const sId = socket.id;
        const sObj = this.clients[sId];
        // Check if we have this socket
        if (sObj) {
            return;
        }

        this.clients[sId] = {
            id: socket.request.user.sub,
            socket,
        };

        console.log(`${socket.id} added, connected ${this.connected}`);
    }

    removeClient(socket) {
        delete this.clients[socket.id];
        console.log(`socket removed, connected ${this.connected}`);
    }

    onClientConnected(socket) {
        const self = this;
        // Increment Count
        this.connected += 1;
        // Add to our client pool
        self.addClient(socket);
        // Attach required events to client socket
        socket.on('data', function (data) {
            self.onMessageReceived(this, data);
        });

        socket.on('disconnect', function () {
            self.connected -= 1;
            self.onClientDisconnected(this);
        });
    }

    onClientDisconnected(socket) {
        // Remove client from our clients list
        this.removeClient(socket);
    }

    onMessageReceived(client, message) {
        let mObject = null;
        try {
            mObject = JSON.parse(message);
        } catch (e) {}

        if (_.isNil(mObject) || _.isNil(mObject.event)) {
            return;
        }

        // Find subscribers for this event
        const eSubscribers = _.filter(this.subscribers, { event: mObject.event });
        if (_.isNil(eSubscribers) || eSubscribers.length === 0) {
            return;
        }

        // Publish to all subscribers
        for (let i = 0; i < eSubscribers.length; i += 1) {
            eSubscribers[i].callback(client, mObject);
        }
    }

    publishToSocket(clientSocket, event, data) {
        if (_.isNil(clientSocket)) {
            return;
        }

        clientSocket.emit(event, data);
    }

    /**
     * Publish specific event with payload data to user
     * Socket mapping for this user id will be done internally in this class
     * @param userId: Destination user id (DB _id)
     * @param event: Event type (String)
     * @param data: Payload data to include with event
     */
    publishToUser(userId, event, data) {
        const uId = userId.toString();

        // Get socket for this user
        const client = _.find(this.clients, { id: uId });

        if (_.isNil(client)) {
            return;
        }

        // Publish data
        this.publishToSocket(client.socket, event, data);
    }

    subscribe(id, event, callback) {
        const sExists = _.findIndex(this.subscribers, { id, event });
        // Add only if does not exists before
        if (sExists === -1) {
            this.subscribers.push({
                id,
                event,
                callback,
            });
        }
    }

    unsubscribe(id, event) {
        const sIdx = _.findIndex(this.subscribers, { id, event });
        if (sIdx >= 0) {
            delete this.subscribers[sIdx];
        }
    }

    getUserFromSocket(socket) {
        if (_.isNil(socket)) {
            return undefined;
        }

        return socket.request.user;
    }

    get socket() {
        return this.io;
    }

    async close() {
        return new Promise((resolve, reject) => {
            if (_.isNil(this.io)) {
                return resolve();
            }

            this.io.close((err) => {
                resolve();
            });
        });
    }
}

module.exports = new Server();
