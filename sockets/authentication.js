/**
 * Socket.io authentication middleware
 */
const _ = require('lodash');
const jwt = require('jsonwebtoken');
const { auth } = require('../config/index');
const {
    UnauthorizedError,
} = require('../helpers/errors');

const authenticate = (socket, next) => {
    const mSocket = socket;
    const token = mSocket.request._query.authorization;

    if (_.isNil(token)) {
        socket.disconnect();
        return (new UnauthorizedError());
    }

    jwt.verify(token, auth.local.key, (err, decoded) => {
        if (err || decoded === undefined || _.isNil(decoded)) {
            socket.disconnect();
            return next(new UnauthorizedError());
        }

        // TODO - limit for employees
        const user = decoded;
        if (_.isNil(user.roles)) {
            user.roles = ['user'];
        }

        mSocket.request.user = user;
        return next();
    });
};

module.exports = {
    authenticate,
};
