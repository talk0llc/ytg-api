const path = require('path');
const express = require('express');

const utils = require('../helpers/utils');
// Middlewares
const {
    hooks,
    authenticationMW,
    requestMutation,
} = require('../controllers/middlewares');

// Public
const index = require('../controllers/index');

// const user = require('../controllers/public/user');
const socialAuth = require('../controllers/common/auth');

const employees = require('../controllers/employees');
const teamMembers = require('../controllers/teamMembers');
const paymentRoute = require('../controllers/paymentGateway');
const locations = require('../controllers/locations');
const programs = require('../controllers/programs');
const jobs = require('../controllers/jobs');
const news = require('../controllers/news');
const events = require('../controllers/events');
const services = require('../controllers/services');
const partners = require('../controllers/partners');
const uploader = require('../controllers/common/uploader');
// const role = require('../controllers/roles');
// Users
const users = require('../controllers/users/users');
const userAddresses = require('../controllers/users/addresses');

class Router {
    static initialize(app, config) {
        // Hook validate func to req
        app.use(hooks);
        // app.use(upload);
        app.use(requestMutation);

        // API documentation static files (if in development mode)
        if (utils.inDevelopment()) {
            const files = path.join(__dirname, '../docs/swagger-ui');
            const oaSpecs = path.join(__dirname, '../api.yaml');
            const imageDir = path.join(__dirname, '../images/uploads');
            const sliderDir = path.join(__dirname, '../images/slider');
            const logoDir = path.join(__dirname, '../images/logos');

            app.use('/uploads', express.static(imageDir));
            app.use('/slider', express.static(sliderDir));
            app.use('/logos', express.static(logoDir));
            app.use('/swagger-ui', express.static(files));
            app.use('/swagger-ui/api.yaml', express.static(oaSpecs));
        }

        app.use('/api', index);
        app.use('/api/social', socialAuth);

        // Authentication
        app.use(authenticationMW);


        app.use('/api/employees', employees);
        app.use('/api/team-member', teamMembers);
        app.use('/api/locations', locations);
        app.use('/api/programs', programs);
        app.use('/api/jobs', jobs);
        app.use('/api/news', news);
        app.use('/api/events', events);
        app.use('/api/services', services);
        app.use('/api/partners', partners);
        // Users
        app.use('/api/users', users);
        app.use('/api/users/:userId/addresses', userAddresses);
        app.use('/api/gateway/orders/', paymentRoute);

        app.use('/api/upload', uploader);
    }
}

module.exports = Router;
