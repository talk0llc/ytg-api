module.exports = (schema, options) => {
    const opts = options || {};

    if (!schema.paths._id) {
        return;
    }

    // Options
    const fields = {};
    const createdAt = opts.createdAt || 'created_at';
    const updatedAt = opts.updatedAt || 'updated_at';

    // Add paths to schema if not present
    if (!schema.paths[createdAt]) {
        fields[createdAt] = { type: Date, default: Date.now() };
        fields[updatedAt] = { type: Date, default: Date.now() };
    }

    schema.add(fields);

    // Update the modified timestamp on save
    schema.pre('save', function (next) {
        if (this.parent) {
            return next();
        }

        this[updatedAt] = new Date();
        next();
    });
};
