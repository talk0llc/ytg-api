const mongoose = require('mongoose');
const crypto = require('../../helpers/crypto');

const { Schema } = mongoose;

const teamMemberSchema = new Schema({
    name: { type: String, trim: true },
    title: { type: String, trim: true },
    images: { type: String },
    image1: { type: String },
    image2: { type: String },
    linkedinUrl: { type: String },
    pinned: { type: Boolean, default: true },
});

// indexes
teamMemberSchema.index();

module.exports = mongoose.model('TeamMember', teamMemberSchema);
