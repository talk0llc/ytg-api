const mongoose = require('mongoose');

const { Schema } = mongoose;
const questionSchema = new Schema({
    question: { type: String },
    answers: { type: [String] },
    type: { type: String },
});
const extraSectionSchema = new Schema({
    images: { type: [String] },
    description: { type: String },
});
const programSchema = new Schema({
    name: { type: String, trim: true },
    images: { type: [String] },
    is_active: { type: Boolean, default: true },
    pinned: { type: Boolean, default: true },
    description: { type: String, trim: true },
    tags: { type: String },
    segment: { type: String },
    questions: { type: [questionSchema] },
    extraSections: { type: [extraSectionSchema], default: [] },
});

// indexes
programSchema.index();

module.exports = mongoose.model('Program', programSchema);
