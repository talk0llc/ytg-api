const mongoose = require('mongoose');

const { Schema } = mongoose;
const extraSectionSchema = new Schema({
    images: { type: [String] },
    description: { type: String },
});
const questionSchema = new Schema({
    question: { type: String },
    answers: { type: [String] },
    type: { type: String },
});
const eventsSchema = new Schema({
    title: { type: String, trim: true },
    description: { type: String, trim: true },
    images: { type: [String] },
    tags: { type: String },
    segment: { type: String },
    is_active: { type: Boolean, default: true },
    pinned: { type: Boolean, default: true },
    location: { type: String},
    dueDate: { type: Date },
    activities: { type: [String] },
    questions: { type: [questionSchema] },
    extraSections: { type: [extraSectionSchema], default: [] },
});

// indexes
eventsSchema.index();

module.exports = mongoose.model('Events', eventsSchema);
