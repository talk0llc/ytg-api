const mongoose = require('mongoose');

const { Schema } = mongoose;
const extraSectionSchema = new Schema({
    images: { type: [String] },
    description: { type: String },
});
const newsSchema = new Schema({
    title: { type: String, trim: true },
    description: { type: String, trim: true },
    images: { type: [String] },
    tags: { type: String },
    segment: { type: String },
    is_active: { type: Boolean, default: true },
    pinned: { type: Boolean, default: true },
    dueDate: { type: Date, default: Date.now() },
    extraSections: { type: [extraSectionSchema], default: [] },
});

// indexes
newsSchema.index();

module.exports = mongoose.model('News', newsSchema);
