const mongoose = require('mongoose');

const { Schema } = mongoose;

const programQASchema = new Schema({
    program: {
        _id: { type: String, pattern: '^[0-9a-fA-F]{24}$' },
        name: { type: String },
    },
    user: {
        _id: { type: String, pattern: '^[0-9a-fA-F]{24}$' },
        email: { type: String },
        mobile: { type: String },
        first_name: { type: String },
        last_name: { type: String },
    },
    questionAnswers: [{
        question: { type: String, required: true },
        answer: { type: String },
    }],
    status: { type: String, default: 'submitted' },
    responseNote: { type: String, default: '' },
    link: { type: String, default: '' },
    grade: { type: Number },
    grade_interview: { type: Number },
});

// indexes

module.exports = mongoose.model('ProgramQA', programQASchema);
