const mongoose = require('mongoose');

const { Schema } = mongoose;
const extraSectionSchema = new Schema({
    images: { type: [String] },
    description: { type: String },
});
const serviceSchema = new Schema({
    name: { type: String, trim: true },
    images: { type: [String] },
    is_active: { type: Boolean, default: true },
    pinned: { type: Boolean, default: true },
    description: { type: String, trim: true },
    tags: { type: String },
    segment: { type: String },
    extraSections: { type: [extraSectionSchema], default: [] },
});

// indexes
serviceSchema.index();

module.exports = mongoose.model('Service', serviceSchema);
