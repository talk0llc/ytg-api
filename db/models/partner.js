const mongoose = require('mongoose');
const crypto = require('../../helpers/crypto');

const { Schema } = mongoose;

const partnerSchema = new Schema({
    email: { type: String, lowercase: true, trim: true },
    mobile: { type: String, trim: true },
    profession: { type: String, trim: true },
    entity: { type: String, trim: true },
    interestedIn: { type: String, trim: true },
    message: { type: String, trim: true },
    name: { type: String, trim: true },
    image: { type: String, trim: true },
    is_active: { type: Boolean, default: true },
});

module.exports = mongoose.model('Partner', partnerSchema);
