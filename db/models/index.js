const Country = require('../ModelsFactory').create('Country');
const Employee = require('../ModelsFactory').create('Employee');
const TeamMember = require('../ModelsFactory').create('TeamMember');
const Partner = require('../ModelsFactory').create('Partner');
const User = require('../ModelsFactory').create('User');
const Configuration = require('../ModelsFactory').create('Configuration');
const Program = require('../ModelsFactory').create('Program');
const Service = require('../ModelsFactory').create('Service');
const Job = require('../ModelsFactory').create('Job');
const News = require('../ModelsFactory').create('News');
const Events = require('../ModelsFactory').create('Events');
const Role = require('../ModelsFactory').create('Role');
const ProgramQA = require('../ModelsFactory').create('ProgramQA');
const JobQA = require('../ModelsFactory').create('JobQA');
const EventQA = require('../ModelsFactory').create('EventQA');
const Contents = require('../ModelsFactory').create('Contents');

module.exports = {
    Country,
    Employee,
    User,
    Configuration,
    Program,
    Job,
    News,
    Role,
    Events,
    Service,
    Partner,
    ProgramQA,
    EventQA,
    TeamMember,
    Contents,
    JobQA
};
