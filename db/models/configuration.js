const mongoose = require('mongoose');

const { Schema } = mongoose;

const configurationSchema = new Schema({
    name: { type: String, default: 'YTG' },
    rewards: {
        referral: { type: Number },
        registration: { type: Number },
    },
});


configurationSchema.index({ name: 1 }, { collation: { locale: 'en', strength: 2 } });

module.exports = mongoose.model('Configuration', configurationSchema);
