const mongoose = require('mongoose');

const { Schema } = mongoose;

const EventQASchema = new Schema({
    event: {
        _id: { type: String, pattern: '^[0-9a-fA-F]{24}$' },
        title: { type: String },
    },
    user: {
        _id: { type: String, pattern: '^[0-9a-fA-F]{24}$' },
        email: { type: String },
        mobile: { type: String },
        first_name: { type: String },
        last_name: { type: String },
    },
    questionAnswers: [{
        question: { type: String, required: true },
        answer: { type: String },
    }],
    status: { type: String, default: 'submitted' },
    responseNote: { type: String, default: '' },
    answer: { type: String },
    grade: { type: Number },
});

// indexes
EventQASchema.index();

module.exports = mongoose.model('EventQA', EventQASchema);
