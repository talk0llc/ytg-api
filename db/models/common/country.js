const mongoose = require('mongoose');

const { Schema } = mongoose;

const areasSchema = new Schema({
    name: { type: String },
});

const citiesSchema = new Schema({
    name: { type: String },
    areas: [areasSchema],
    is_active: { type: Boolean, default: true },
});

const countriesSchema = new Schema({
    code: {
        type: String, required: true, lowercase: true, trim: true,
    },
    name: { type: String, required: true, trim: true },
    native: { type: String, trim: true },
    currency: { type: String, uppercase: true, trim: true },
    capital: { type: String, trim: true },
    phone: { type: String, trim: true },
    continent: { type: String, uppercase: true },
    languages: [{ type: String }],
    cities: [citiesSchema],
    is_active: { type: Boolean, default: false },
});

// indexes
countriesSchema.index({ code: 1 }, { collation: { locale: 'en', strength: 2 } });

module.exports = {
    Country: mongoose.model('Country', countriesSchema),
    City: mongoose.model('City', citiesSchema),
    Area: mongoose.model('Area', areasSchema),
};
