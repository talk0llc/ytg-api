const mongoose = require('mongoose');

const { Schema } = mongoose;
const resources = ['users', 'employees', 'programs', 'jobs', 'news', 'roles'];

const roleSchema = new Schema({
    name: { type: String, trim: true },
    resources: { resource: { type: String, enum: resources }, accessType: { type: [String] } },
});

// indexes
roleSchema.index();

module.exports = mongoose.model('Role', roleSchema);
