const mongoose = require('mongoose');
const crypto = require('../../helpers/crypto');

const { Schema } = mongoose;
const resources = ['users', 'employees', 'programs', 'jobs', 'news', 'events', 'partners', 'members', 'services', 'contents', 'applications'];
const employeeType = ['staff', 'team'];

const employeeSchema = new Schema({
    email: {
        type: String, unique: true, lowercase: true, trim: true, index: true,
    },
    password: { type: String, trim: true, required: true },
    first_name: { type: String, trim: true },
    last_name: { type: String, trim: true },
    mobile: { type: String, required: false, trim: true },
    roles: [{
        name: { type: String, trim: true },
        resources: [{
            resource: {
                type: String, enum: resources,
            },
            accessType: {
                type: [String],
            },
        }],
    }],
    type: { type: String, enum: employeeType },
    gender: { type: String, trim: true },
    images: { type: [String] },
    token: { type: String, trim: true },
    linkedinUrl: { type: String },
    reset_token: { type: String, trim: true },
    reset_token_time: { type: Date },
    reset_token_expiration: { type: Date },
    last_login: { type: Date, default: Date.now() },
    is_verified: { type: Boolean, default: true },
    is_active: { type: Boolean, default: true },
    pinned: { type: Boolean, default: true },
});

employeeSchema.pre('save', function (next) {
    if (this.isModified('password')) {
        this.password = crypto.createHash(this.password);
    }

    if (this.isModified('email')
        || this.isModified('mobile')
        || this.isModified('roles')) {
        this.token = this.generateToken();
    }

    next();
});

employeeSchema.methods.generateToken = function () {
    const token = crypto.generateJwtToken({
        sub: this._id,
        email: this.email,
        mobile: this.mobile,
        roles: this.roles,
    });

    return token;
};

employeeSchema.methods.generateEmailToken = function () {
    const token = crypto.generateJwtToken({
        sub: this._id,
    });

    return token;
};

employeeSchema.methods.generateSmsToken = function () {
    return crypto.generateRandomNumber();
};

employeeSchema.methods.validResetToken = function (token) {
    if (this.reset_token === token) {
        if (this.reset_token_time - Date.now() >= 0) {
            return true;
        }
    }

    return false;
};

employeeSchema.methods.validPassword = function (password) {
    return crypto.compareHash(password, this.password);
};

employeeSchema.methods.getType = function () {
    return 'employee';
};

// indexes
employeeSchema.index({ email: 1 });
employeeSchema.index({ mobile: 1 });

module.exports = mongoose.model('Employee', employeeSchema);
