const mongoose = require('mongoose');
const crypto = require('../../helpers/crypto');

const { Schema } = mongoose;
const resources = ['users', 'employees', 'programs', 'jobs', 'news', 'events', 'partners', 'members', 'services', 'applications', 'contents'];

const userSchema = new Schema({
    email: { type: String, lowercase: true, trim: true },
    mobile: { type: String, trim: false },
    auth_type: { type: String, trim: true, default: 'local' },
    password: { type: String, trim: true },
    roles: [{
        name: { type: String, trim: true },
        resources: [
            {
                resource: { type: String, enum: resources },
                accessType: { type: [String] },
            }],
    }],
    first_name: { type: String, trim: true },
    last_name: { type: String, trim: true },
    gender: { type: String, trim: true },
    image: { type: String, trim: true },
    token: { type: String, trim: true },
    reset_token: { type: String, trim: true },
    reset_token_time: { type: Date },
    reset_token_expiration: { type: Date },
    date_of_birth: { type: Date },
    residence_address: { type: String },
    graduation_status: { type: String },
    organization: { type: String },
    facebook: { type: Object },
    google: { type: Object },
    last_login: { type: Date },
    is_mobile_verified: { type: Boolean, default: false },
    is_email_verified: { type: Boolean, default: false },
    is_verified: { type: Boolean, default: false },
    is_active: { type: Boolean, default: true },
});

userSchema.pre('save', function (next) {
    if (this.isModified('password')) {
        this.password = crypto.createHash(this.password);
    }

    if (this.isModified('email')
        || this.isModified('mobile')
        || this.isModified('roles')) {
        this.token = this.generateToken();
    }

    if (this.isNew) {
        const name = this.first_name.toLowerCase();
        const hash = crypto.generateUniqueId();
        this.referral_id = `${name.substr(0, 1)}.${hash}`;
    }

    next();
});

userSchema.methods.generateToken = function () {
    const token = crypto.generateJwtToken({
        sub: this._id,
        email: this.email,
        mobile: this.mobile,
        roles: this.roles,
    });

    return token;
};

userSchema.methods.generateEmailToken = function () {
    const token = crypto.generateJwtToken({
        sub: this._id,
    });

    return token;
};

userSchema.methods.generateSmsToken = function () {
    return crypto.generateRandomNumber();
};

userSchema.methods.validResetToken = function (token) {
    if (this.reset_token === token) {
        if (this.reset_token_time - Date.now() >= 0) {
            return true;
        }
    }

    return false;
};

userSchema.methods.validPassword = function (password) {
    if (!this.password) {
        return false;
    }
    return crypto.compareHash(password, this.password);
};

userSchema.methods.getType = function () {
    return 'user';
};

// indexes
userSchema.index({ email: 1 });
userSchema.index({ mobile: 1 });

module.exports = mongoose.model('User', userSchema);
