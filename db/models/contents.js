const mongoose = require('mongoose');

const { Schema } = mongoose;

const contentSchema = new Schema({
    index: { type: Number },
    key: { type: String },
    number: { type: Number },
    comment: { type: String },
    buttonText: { type: String },
    link: { type: String },
    image: { type: String },
    description: { type: String },
});

// indexes
contentSchema.index();

module.exports = mongoose.model('Contents', contentSchema);
