const mongoose = require('mongoose');

const { Schema } = mongoose;
const questionSchema = new Schema({
    question: { type: String },
    answers: { type: [String] },
    type: { type: String },
});
const extraSectionSchema = new Schema({
    images: { type: [String] },
    description: { type: String },
});
const jobSchema = new Schema({
    title: { type: String, trim: true },
    description: { type: String, trim: true },
    is_active: { type: Boolean, default: true },
    segment: { type: String },
    questions: { type: [questionSchema] },
    deadline: { type: Date },
    pinned: { type: Boolean, default: true },
    salary: { type: Number },
    extraSections: { type: [extraSectionSchema], default: [] },
});

// indexes
jobSchema.index();

module.exports = mongoose.model('Job', jobSchema);
