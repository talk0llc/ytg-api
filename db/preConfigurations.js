const _ = require('lodash');
const mongoose = require('mongoose');
const jsonfile = require('jsonfile');

const config = require('../config');
const countries = require('../config/constants/countries');
const utils = require('../helpers/utils');

const APP_NAME = config.app.name;

class PreConfigurations {
    async initialize() {
        await this.buildConfigurations();
        await this.buildCountries();
        await this.buildDefaultUsers();
    }

    async buildConfigurations() {
        const Configuration = mongoose.model('Configuration');
        const document = await Configuration.findOne({ name: APP_NAME }).exec();

        if (!_.isNil(document)) {
            return;
        }

        const ytgConfig = new Configuration({
            name: APP_NAME,
            ...config[APP_NAME],
        });

        await ytgConfig.save();
    }

    async buildCountries() {
        const Country = mongoose.model('Country');
        const documents = await Country.countDocuments();

        if (documents > 0) {
            return;
        }

        for (let i = 0; i < countries.length; i += 1) {
            const country = new Country(countries[i]);
            await country.save();
        }
    }

    async buildDefaultUsers() {
        const { accounts } = config;

        if (accounts) {
            const Employee = mongoose.model('Employee');

            const adminExists = await Employee.findOne({
                email: accounts.admin.email,
            }).exec();

            if (!_.isNil(adminExists)) {
                this._createTestingAcountsJSONFile(adminExists);
                return;
            }

            // Create admin and guest accounts
            const adminData = new Employee({
                email: accounts.admin.email,
                password: accounts.admin.password,
                first_name: 'Admin',
                last_name: 'Admin',
                roles: [{
                    name: 'Admin Role',
                    resources: [
                        {
                            resource: 'programs',
                            accessType: [
                                'createAny',
                                'updateAny',
                                'readAny',
                                'deleteAny',
                            ],
                        },
                        {
                            resource: 'employees',
                            accessType: [
                                'createAny',
                                'updateAny',
                                'readAny',
                                'deleteAny',
                            ],
                        },
                        {
                            resource: 'users',
                            accessType: [
                                'createAny',
                                'updateAny',
                                'readAny',
                                'deleteAny',
                            ],
                        },
                        {
                            resource: 'jobs',
                            accessType: [
                                'createAny',
                                'updateAny',
                                'readAny',
                                'deleteAny',
                            ],
                        },
                        {
                            resource: 'news',
                            accessType: [
                                'createAny',
                                'updateAny',
                                'readAny',
                                'deleteAny',
                            ],
                        },
                        {
                            resource: 'events',
                            accessType: [
                                'createAny',
                                'updateAny',
                                'readAny',
                                'deleteAny',
                            ],
                        },
                        {
                            resource: 'applications',
                            accessType: [
                                'createAny',
                                'updateAny',
                                'readAny',
                                'deleteAny',
                            ],
                        },
                        {
                            resource: 'contents',
                            accessType: [
                                'createAny',
                                'updateAny',
                                'readAny',
                                'deleteAny',
                            ],
                        },
                        {
                            resource: 'services',
                            accessType: [
                                'createAny',
                                'updateAny',
                                'readAny',
                                'deleteAny',
                            ],
                        },
                        {
                            resource: 'members',
                            accessType: [
                                'createAny',
                                'updateAny',
                                'readAny',
                                'deleteAny',
                            ],
                        },
                        {
                            resource: 'partners',
                            accessType: [
                                'createAny',
                                'updateAny',
                                'readAny',
                                'deleteAny',
                            ],
                        },
                    ],
                }],
                is_verified: true,
            });

            const admin = await adminData.save();
            this._createTestingAcountsJSONFile(admin);
        }
    }

    _createTestingAcountsJSONFile(admin, guest) {
        if (utils.inTest()) {
            const aFile = utils.getPath(__dirname, '../test/data/accounts.json');
            jsonfile.writeFile(aFile, {
                admin,
                guest,
            }, (err) => {
                if (err) {
                    console.error(err);
                }
            });
        }
    }
}

module.exports = new PreConfigurations();
