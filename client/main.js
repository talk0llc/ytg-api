(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _auth_login_login_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./auth/login/login.component */ "./src/app/auth/login/login.component.ts");
/* harmony import */ var _layout_app_layout_app_layout_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./layout/app-layout/app-layout.component */ "./src/app/layout/app-layout/app-layout.component.ts");
/* harmony import */ var _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./dashboard/dashboard.component */ "./src/app/dashboard/dashboard.component.ts");
/* harmony import */ var _users_list_users_list_users_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./users/list-users/list-users.component */ "./src/app/users/list-users/list-users.component.ts");
/* harmony import */ var _auth_register_register_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./auth/register/register.component */ "./src/app/auth/register/register.component.ts");
/* harmony import */ var _auth_forget_password_forget_password_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./auth/forget-password/forget-password.component */ "./src/app/auth/forget-password/forget-password.component.ts");
/* harmony import */ var _auth_reset_password_reset_password_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./auth/reset-password/reset-password.component */ "./src/app/auth/reset-password/reset-password.component.ts");
/* harmony import */ var _users_create_user_create_user_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./users/create-user/create-user.component */ "./src/app/users/create-user/create-user.component.ts");
/* harmony import */ var _users_edit_user_edit_user_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./users/edit-user/edit-user.component */ "./src/app/users/edit-user/edit-user.component.ts");
/* harmony import */ var _staff_profile_profile_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./staff/profile/profile.component */ "./src/app/staff/profile/profile.component.ts");
/* harmony import */ var _staff_list_staff_list_staff_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./staff/list-staff/list-staff.component */ "./src/app/staff/list-staff/list-staff.component.ts");
/* harmony import */ var _staff_create_staff_create_staff_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./staff/create-staff/create-staff.component */ "./src/app/staff/create-staff/create-staff.component.ts");
/* harmony import */ var _staff_edit_staff_edit_staff_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./staff/edit-staff/edit-staff.component */ "./src/app/staff/edit-staff/edit-staff.component.ts");
/* harmony import */ var _partners_list_partners_list_partners_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./partners/list-partners/list-partners.component */ "./src/app/partners/list-partners/list-partners.component.ts");
/* harmony import */ var _partners_create_partner_create_partner_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./partners/create-partner/create-partner.component */ "./src/app/partners/create-partner/create-partner.component.ts");
/* harmony import */ var _partners_edit_partner_edit_partner_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./partners/edit-partner/edit-partner.component */ "./src/app/partners/edit-partner/edit-partner.component.ts");
/* harmony import */ var _events_list_events_list_events_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./events/list-events/list-events.component */ "./src/app/events/list-events/list-events.component.ts");
/* harmony import */ var _events_create_event_create_event_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./events/create-event/create-event.component */ "./src/app/events/create-event/create-event.component.ts");
/* harmony import */ var _events_edit_event_edit_event_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./events/edit-event/edit-event.component */ "./src/app/events/edit-event/edit-event.component.ts");
/* harmony import */ var _news_list_news_list_news_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./news/list-news/list-news.component */ "./src/app/news/list-news/list-news.component.ts");
/* harmony import */ var _news_create_news_create_news_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./news/create-news/create-news.component */ "./src/app/news/create-news/create-news.component.ts");
/* harmony import */ var _news_edit_news_edit_news_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./news/edit-news/edit-news.component */ "./src/app/news/edit-news/edit-news.component.ts");
/* harmony import */ var _jobs_list_jobs_list_jobs_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./jobs/list-jobs/list-jobs.component */ "./src/app/jobs/list-jobs/list-jobs.component.ts");
/* harmony import */ var _jobs_create_job_create_job_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./jobs/create-job/create-job.component */ "./src/app/jobs/create-job/create-job.component.ts");
/* harmony import */ var _jobs_edit_job_edit_job_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./jobs/edit-job/edit-job.component */ "./src/app/jobs/edit-job/edit-job.component.ts");
/* harmony import */ var _services_list_services_list_services_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./services/list-services/list-services.component */ "./src/app/services/list-services/list-services.component.ts");
/* harmony import */ var _services_create_service_create_service_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./services/create-service/create-service.component */ "./src/app/services/create-service/create-service.component.ts");
/* harmony import */ var _services_edit_service_edit_service_component__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./services/edit-service/edit-service.component */ "./src/app/services/edit-service/edit-service.component.ts");
/* harmony import */ var _programs_list_programs_list_programs_component__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./programs/list-programs/list-programs.component */ "./src/app/programs/list-programs/list-programs.component.ts");
/* harmony import */ var _programs_create_program_create_program_component__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ./programs/create-program/create-program.component */ "./src/app/programs/create-program/create-program.component.ts");
/* harmony import */ var _programs_edit_program_edit_program_component__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ./programs/edit-program/edit-program.component */ "./src/app/programs/edit-program/edit-program.component.ts");
/* harmony import */ var _programs_applications_create_application_create_application_component__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ./programs-applications/create-application/create-application.component */ "./src/app/programs-applications/create-application/create-application.component.ts");
/* harmony import */ var _events_applications_create_application_create_application_component__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ./events-applications/create-application/create-application.component */ "./src/app/events-applications/create-application/create-application.component.ts");
/* harmony import */ var _contents_contents_contents_component__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! ./contents/contents/contents.component */ "./src/app/contents/contents/contents.component.ts");
/* harmony import */ var _team_members_list_team_members_list_team_members_component__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! ./team-members/list-team-members/list-team-members.component */ "./src/app/team-members/list-team-members/list-team-members.component.ts");
/* harmony import */ var _team_members_create_team_member_create_team_member_component__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! ./team-members/create-team-member/create-team-member.component */ "./src/app/team-members/create-team-member/create-team-member.component.ts");
/* harmony import */ var _team_members_edit_team_member_edit_team_member_component__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! ./team-members/edit-team-member/edit-team-member.component */ "./src/app/team-members/edit-team-member/edit-team-member.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







































var routes = [
    { path: 'auth/login', component: _auth_login_login_component__WEBPACK_IMPORTED_MODULE_2__["LoginComponent"] },
    { path: 'auth/register', component: _auth_register_register_component__WEBPACK_IMPORTED_MODULE_6__["RegisterComponent"] },
    { path: 'auth/forget-password', component: _auth_forget_password_forget_password_component__WEBPACK_IMPORTED_MODULE_7__["ForgetPasswordComponent"] },
    { path: 'auth/reset-password', component: _auth_reset_password_reset_password_component__WEBPACK_IMPORTED_MODULE_8__["ResetPasswordComponent"] },
    { path: '', redirectTo: 'auth/login', pathMatch: 'full' },
    {
        path: '',
        component: _layout_app_layout_app_layout_component__WEBPACK_IMPORTED_MODULE_3__["AppLayoutComponent"],
        children: [
            { path: 'dashboard', component: _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_4__["DashboardComponent"] },
            { path: 'contents', component: _contents_contents_contents_component__WEBPACK_IMPORTED_MODULE_35__["ContentsComponent"] },
            { path: 'users/list', component: _users_list_users_list_users_component__WEBPACK_IMPORTED_MODULE_5__["ListUsersComponent"] },
            { path: 'users/create-user', component: _users_create_user_create_user_component__WEBPACK_IMPORTED_MODULE_9__["CreateUserComponent"] },
            { path: 'users/:id/edit-user', component: _users_edit_user_edit_user_component__WEBPACK_IMPORTED_MODULE_10__["EditUserComponent"] },
            { path: 'users', redirectTo: 'users/list', pathMatch: 'full' },
            { path: 'staff/profile', component: _staff_profile_profile_component__WEBPACK_IMPORTED_MODULE_11__["ProfileComponent"] },
            { path: 'staff/list', component: _staff_list_staff_list_staff_component__WEBPACK_IMPORTED_MODULE_12__["ListStaffComponent"] },
            { path: 'staff/create-staff', component: _staff_create_staff_create_staff_component__WEBPACK_IMPORTED_MODULE_13__["CreateStaffComponent"] },
            { path: 'staff/:id/edit-staff', component: _staff_edit_staff_edit_staff_component__WEBPACK_IMPORTED_MODULE_14__["EditStaffComponent"] },
            { path: 'staff', redirectTo: 'staff/list', pathMatch: 'full' },
            { path: 'team-member/list', component: _team_members_list_team_members_list_team_members_component__WEBPACK_IMPORTED_MODULE_36__["ListTeamMembersComponent"] },
            { path: 'team-member/create-team-member', component: _team_members_create_team_member_create_team_member_component__WEBPACK_IMPORTED_MODULE_37__["CreateTeamMemberComponent"] },
            { path: 'team-member/:id/edit-team-member', component: _team_members_edit_team_member_edit_team_member_component__WEBPACK_IMPORTED_MODULE_38__["EditTeamMemberComponent"] },
            { path: 'team-member', redirectTo: 'team-member/list', pathMatch: 'full' },
            { path: 'partners/list', component: _partners_list_partners_list_partners_component__WEBPACK_IMPORTED_MODULE_15__["ListPartnersComponent"] },
            { path: 'partners/create-partner', component: _partners_create_partner_create_partner_component__WEBPACK_IMPORTED_MODULE_16__["CreatePartnerComponent"] },
            { path: 'partners/:id/edit-partner', component: _partners_edit_partner_edit_partner_component__WEBPACK_IMPORTED_MODULE_17__["EditPartnerComponent"] },
            { path: 'partners', redirectTo: 'partners/list', pathMatch: 'full' },
            { path: 'events/list', component: _events_list_events_list_events_component__WEBPACK_IMPORTED_MODULE_18__["ListEventsComponent"] },
            { path: 'events/create-event', component: _events_create_event_create_event_component__WEBPACK_IMPORTED_MODULE_19__["CreateEventComponent"] },
            { path: 'events/:id/edit-event', component: _events_edit_event_edit_event_component__WEBPACK_IMPORTED_MODULE_20__["EditEventComponent"] },
            { path: 'events/:id/applications', component: _events_applications_create_application_create_application_component__WEBPACK_IMPORTED_MODULE_34__["CreateEventApplicationComponent"] },
            { path: 'events', redirectTo: 'events/list', pathMatch: 'full' },
            { path: 'news/list', component: _news_list_news_list_news_component__WEBPACK_IMPORTED_MODULE_21__["ListNewsComponent"] },
            { path: 'news/create-news', component: _news_create_news_create_news_component__WEBPACK_IMPORTED_MODULE_22__["CreateNewsComponent"] },
            { path: 'news/:id/edit-news', component: _news_edit_news_edit_news_component__WEBPACK_IMPORTED_MODULE_23__["EditNewsComponent"] },
            { path: 'news', redirectTo: 'news/list', pathMatch: 'full' },
            { path: 'jobs/list', component: _jobs_list_jobs_list_jobs_component__WEBPACK_IMPORTED_MODULE_24__["ListJobsComponent"] },
            { path: 'jobs/create-job', component: _jobs_create_job_create_job_component__WEBPACK_IMPORTED_MODULE_25__["CreateJobComponent"] },
            { path: 'jobs/:id/edit-job', component: _jobs_edit_job_edit_job_component__WEBPACK_IMPORTED_MODULE_26__["EditJobComponent"] },
            { path: 'jobs', redirectTo: 'jobs/list', pathMatch: 'full' },
            { path: 'services/list', component: _services_list_services_list_services_component__WEBPACK_IMPORTED_MODULE_27__["ListServicesComponent"] },
            { path: 'services/create-service', component: _services_create_service_create_service_component__WEBPACK_IMPORTED_MODULE_28__["CreateServiceComponent"] },
            { path: 'services/:id/edit-service', component: _services_edit_service_edit_service_component__WEBPACK_IMPORTED_MODULE_29__["EditServiceComponent"] },
            { path: 'services', redirectTo: 'services/list', pathMatch: 'full' },
            { path: 'programs/list', component: _programs_list_programs_list_programs_component__WEBPACK_IMPORTED_MODULE_30__["ListProgramsComponent"] },
            { path: 'programs/create-program', component: _programs_create_program_create_program_component__WEBPACK_IMPORTED_MODULE_31__["CreateProgramComponent"] },
            { path: 'programs/:id/edit-program', component: _programs_edit_program_edit_program_component__WEBPACK_IMPORTED_MODULE_32__["EditProgramComponent"] },
            { path: 'programs/:id/applications', component: _programs_applications_create_application_create_application_component__WEBPACK_IMPORTED_MODULE_33__["CreateProgramApplicationComponent"] },
            { path: 'programs', redirectTo: 'programs/list', pathMatch: 'full' },
        ]
    },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\n<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'ytg-adminPanel';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _kolkov_angular_editor__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @kolkov/angular-editor */ "./node_modules/@kolkov/angular-editor/fesm5/kolkov-angular-editor.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./dashboard/dashboard.component */ "./src/app/dashboard/dashboard.component.ts");
/* harmony import */ var _auth_login_login_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./auth/login/login.component */ "./src/app/auth/login/login.component.ts");
/* harmony import */ var _users_list_users_list_users_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./users/list-users/list-users.component */ "./src/app/users/list-users/list-users.component.ts");
/* harmony import */ var _layout_topnavbar_topnavbar_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./layout/topnavbar/topnavbar.component */ "./src/app/layout/topnavbar/topnavbar.component.ts");
/* harmony import */ var _layout_sidenavbar_sidenavbar_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./layout/sidenavbar/sidenavbar.component */ "./src/app/layout/sidenavbar/sidenavbar.component.ts");
/* harmony import */ var _layout_footernavbar_footernavbar_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./layout/footernavbar/footernavbar.component */ "./src/app/layout/footernavbar/footernavbar.component.ts");
/* harmony import */ var _layout_settingsnavbar_settingsnavbar_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./layout/settingsnavbar/settingsnavbar.component */ "./src/app/layout/settingsnavbar/settingsnavbar.component.ts");
/* harmony import */ var _layout_app_layout_app_layout_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./layout/app-layout/app-layout.component */ "./src/app/layout/app-layout/app-layout.component.ts");
/* harmony import */ var _auth_register_register_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./auth/register/register.component */ "./src/app/auth/register/register.component.ts");
/* harmony import */ var _auth_forget_password_forget_password_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./auth/forget-password/forget-password.component */ "./src/app/auth/forget-password/forget-password.component.ts");
/* harmony import */ var _auth_reset_password_reset_password_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./auth/reset-password/reset-password.component */ "./src/app/auth/reset-password/reset-password.component.ts");
/* harmony import */ var _users_create_user_create_user_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./users/create-user/create-user.component */ "./src/app/users/create-user/create-user.component.ts");
/* harmony import */ var _users_edit_user_edit_user_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./users/edit-user/edit-user.component */ "./src/app/users/edit-user/edit-user.component.ts");
/* harmony import */ var _staff_profile_profile_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./staff/profile/profile.component */ "./src/app/staff/profile/profile.component.ts");
/* harmony import */ var _staff_list_staff_list_staff_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./staff/list-staff/list-staff.component */ "./src/app/staff/list-staff/list-staff.component.ts");
/* harmony import */ var _staff_create_staff_create_staff_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./staff/create-staff/create-staff.component */ "./src/app/staff/create-staff/create-staff.component.ts");
/* harmony import */ var _staff_edit_staff_edit_staff_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./staff/edit-staff/edit-staff.component */ "./src/app/staff/edit-staff/edit-staff.component.ts");
/* harmony import */ var _jobs_list_jobs_list_jobs_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./jobs/list-jobs/list-jobs.component */ "./src/app/jobs/list-jobs/list-jobs.component.ts");
/* harmony import */ var _jobs_create_job_create_job_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./jobs/create-job/create-job.component */ "./src/app/jobs/create-job/create-job.component.ts");
/* harmony import */ var _jobs_edit_job_edit_job_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./jobs/edit-job/edit-job.component */ "./src/app/jobs/edit-job/edit-job.component.ts");
/* harmony import */ var _news_list_news_list_news_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./news/list-news/list-news.component */ "./src/app/news/list-news/list-news.component.ts");
/* harmony import */ var _news_create_news_create_news_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./news/create-news/create-news.component */ "./src/app/news/create-news/create-news.component.ts");
/* harmony import */ var _news_edit_news_edit_news_component__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./news/edit-news/edit-news.component */ "./src/app/news/edit-news/edit-news.component.ts");
/* harmony import */ var _events_list_events_list_events_component__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./events/list-events/list-events.component */ "./src/app/events/list-events/list-events.component.ts");
/* harmony import */ var _events_create_event_create_event_component__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ./events/create-event/create-event.component */ "./src/app/events/create-event/create-event.component.ts");
/* harmony import */ var _events_edit_event_edit_event_component__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ./events/edit-event/edit-event.component */ "./src/app/events/edit-event/edit-event.component.ts");
/* harmony import */ var _services_list_services_list_services_component__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ./services/list-services/list-services.component */ "./src/app/services/list-services/list-services.component.ts");
/* harmony import */ var _services_create_service_create_service_component__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ./services/create-service/create-service.component */ "./src/app/services/create-service/create-service.component.ts");
/* harmony import */ var _services_edit_service_edit_service_component__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! ./services/edit-service/edit-service.component */ "./src/app/services/edit-service/edit-service.component.ts");
/* harmony import */ var _programs_list_programs_list_programs_component__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! ./programs/list-programs/list-programs.component */ "./src/app/programs/list-programs/list-programs.component.ts");
/* harmony import */ var _programs_create_program_create_program_component__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! ./programs/create-program/create-program.component */ "./src/app/programs/create-program/create-program.component.ts");
/* harmony import */ var _programs_edit_program_edit_program_component__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! ./programs/edit-program/edit-program.component */ "./src/app/programs/edit-program/edit-program.component.ts");
/* harmony import */ var _programs_applications_create_application_create_application_component__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! ./programs-applications/create-application/create-application.component */ "./src/app/programs-applications/create-application/create-application.component.ts");
/* harmony import */ var _contents_contents_contents_component__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! ./contents/contents/contents.component */ "./src/app/contents/contents/contents.component.ts");
/* harmony import */ var _partners_create_partner_create_partner_component__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! ./partners/create-partner/create-partner.component */ "./src/app/partners/create-partner/create-partner.component.ts");
/* harmony import */ var _partners_edit_partner_edit_partner_component__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! ./partners/edit-partner/edit-partner.component */ "./src/app/partners/edit-partner/edit-partner.component.ts");
/* harmony import */ var _partners_list_partners_list_partners_component__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(/*! ./partners/list-partners/list-partners.component */ "./src/app/partners/list-partners/list-partners.component.ts");
/* harmony import */ var _events_applications_create_application_create_application_component__WEBPACK_IMPORTED_MODULE_44__ = __webpack_require__(/*! ./events-applications/create-application/create-application.component */ "./src/app/events-applications/create-application/create-application.component.ts");
/* harmony import */ var _team_members_create_team_member_create_team_member_component__WEBPACK_IMPORTED_MODULE_45__ = __webpack_require__(/*! ./team-members/create-team-member/create-team-member.component */ "./src/app/team-members/create-team-member/create-team-member.component.ts");
/* harmony import */ var _team_members_edit_team_member_edit_team_member_component__WEBPACK_IMPORTED_MODULE_46__ = __webpack_require__(/*! ./team-members/edit-team-member/edit-team-member.component */ "./src/app/team-members/edit-team-member/edit-team-member.component.ts");
/* harmony import */ var _team_members_list_team_members_list_team_members_component__WEBPACK_IMPORTED_MODULE_47__ = __webpack_require__(/*! ./team-members/list-team-members/list-team-members.component */ "./src/app/team-members/list-team-members/list-team-members.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
















































var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"],
                _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_7__["DashboardComponent"],
                _auth_login_login_component__WEBPACK_IMPORTED_MODULE_8__["LoginComponent"],
                _users_list_users_list_users_component__WEBPACK_IMPORTED_MODULE_9__["ListUsersComponent"],
                _layout_topnavbar_topnavbar_component__WEBPACK_IMPORTED_MODULE_10__["TopnavbarComponent"],
                _layout_sidenavbar_sidenavbar_component__WEBPACK_IMPORTED_MODULE_11__["SidenavbarComponent"],
                _layout_footernavbar_footernavbar_component__WEBPACK_IMPORTED_MODULE_12__["FooternavbarComponent"],
                _layout_settingsnavbar_settingsnavbar_component__WEBPACK_IMPORTED_MODULE_13__["SettingsnavbarComponent"],
                _layout_app_layout_app_layout_component__WEBPACK_IMPORTED_MODULE_14__["AppLayoutComponent"],
                _auth_register_register_component__WEBPACK_IMPORTED_MODULE_15__["RegisterComponent"],
                _auth_forget_password_forget_password_component__WEBPACK_IMPORTED_MODULE_16__["ForgetPasswordComponent"],
                _auth_reset_password_reset_password_component__WEBPACK_IMPORTED_MODULE_17__["ResetPasswordComponent"],
                _users_create_user_create_user_component__WEBPACK_IMPORTED_MODULE_18__["CreateUserComponent"],
                _users_edit_user_edit_user_component__WEBPACK_IMPORTED_MODULE_19__["EditUserComponent"],
                _staff_profile_profile_component__WEBPACK_IMPORTED_MODULE_20__["ProfileComponent"],
                _staff_list_staff_list_staff_component__WEBPACK_IMPORTED_MODULE_21__["ListStaffComponent"],
                _staff_create_staff_create_staff_component__WEBPACK_IMPORTED_MODULE_22__["CreateStaffComponent"],
                _staff_edit_staff_edit_staff_component__WEBPACK_IMPORTED_MODULE_23__["EditStaffComponent"],
                _jobs_list_jobs_list_jobs_component__WEBPACK_IMPORTED_MODULE_24__["ListJobsComponent"],
                _jobs_create_job_create_job_component__WEBPACK_IMPORTED_MODULE_25__["CreateJobComponent"],
                _jobs_edit_job_edit_job_component__WEBPACK_IMPORTED_MODULE_26__["EditJobComponent"],
                _news_list_news_list_news_component__WEBPACK_IMPORTED_MODULE_27__["ListNewsComponent"],
                _news_create_news_create_news_component__WEBPACK_IMPORTED_MODULE_28__["CreateNewsComponent"],
                _news_edit_news_edit_news_component__WEBPACK_IMPORTED_MODULE_29__["EditNewsComponent"],
                _events_list_events_list_events_component__WEBPACK_IMPORTED_MODULE_30__["ListEventsComponent"],
                _events_create_event_create_event_component__WEBPACK_IMPORTED_MODULE_31__["CreateEventComponent"],
                _events_edit_event_edit_event_component__WEBPACK_IMPORTED_MODULE_32__["EditEventComponent"],
                _services_list_services_list_services_component__WEBPACK_IMPORTED_MODULE_33__["ListServicesComponent"],
                _services_create_service_create_service_component__WEBPACK_IMPORTED_MODULE_34__["CreateServiceComponent"],
                _services_edit_service_edit_service_component__WEBPACK_IMPORTED_MODULE_35__["EditServiceComponent"],
                _programs_list_programs_list_programs_component__WEBPACK_IMPORTED_MODULE_36__["ListProgramsComponent"],
                _programs_create_program_create_program_component__WEBPACK_IMPORTED_MODULE_37__["CreateProgramComponent"],
                _programs_edit_program_edit_program_component__WEBPACK_IMPORTED_MODULE_38__["EditProgramComponent"],
                _programs_applications_create_application_create_application_component__WEBPACK_IMPORTED_MODULE_39__["CreateProgramApplicationComponent"],
                _contents_contents_contents_component__WEBPACK_IMPORTED_MODULE_40__["ContentsComponent"],
                _partners_create_partner_create_partner_component__WEBPACK_IMPORTED_MODULE_41__["CreatePartnerComponent"],
                _partners_edit_partner_edit_partner_component__WEBPACK_IMPORTED_MODULE_42__["EditPartnerComponent"],
                _partners_list_partners_list_partners_component__WEBPACK_IMPORTED_MODULE_43__["ListPartnersComponent"],
                _events_applications_create_application_create_application_component__WEBPACK_IMPORTED_MODULE_44__["CreateEventApplicationComponent"],
                _team_members_create_team_member_create_team_member_component__WEBPACK_IMPORTED_MODULE_45__["CreateTeamMemberComponent"],
                _team_members_edit_team_member_edit_team_member_component__WEBPACK_IMPORTED_MODULE_46__["EditTeamMemberComponent"],
                _team_members_list_team_members_list_team_members_component__WEBPACK_IMPORTED_MODULE_47__["ListTeamMembersComponent"],
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClientModule"],
                _kolkov_angular_editor__WEBPACK_IMPORTED_MODULE_3__["AngularEditorModule"]
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/auth/auth.service.ts":
/*!**************************************!*\
  !*** ./src/app/auth/auth.service.ts ***!
  \**************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../constants */ "./src/app/constants.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AuthService = /** @class */ (function () {
    function AuthService(http) {
        this.http = http;
        this.apiUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_0__["environment"].apiBaseUrl;
        this.userId = null;
        this._isAuthenticated = new rxjs__WEBPACK_IMPORTED_MODULE_4__["BehaviorSubject"](false);
    }
    Object.defineProperty(AuthService.prototype, "isAuthenticated", {
        // Providing a observable to listen the authentication state
        get: function () {
            return this._isAuthenticated.asObservable();
        },
        enumerable: true,
        configurable: true
    });
    AuthService.prototype.setUserId = function (id) {
        this.userId = id;
        // Dispatching to all listeners that the user is authenticated
        this._isAuthenticated.next(true);
    };
    AuthService.prototype.saveUserData = function (id, token) {
        localStorage.setItem(_constants__WEBPACK_IMPORTED_MODULE_3__["USER_ID"], id);
        localStorage.setItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"], token);
        this.setUserId(id);
    };
    AuthService.prototype.forgetPassword = function (email) {
        return this.http.post("/forget_password", { email: email });
    };
    AuthService.prototype.resetPassword = function (code, password) {
        return this.http.post("/reset_password", { code: code, password: password });
    };
    AuthService.prototype.login = function (username, password, type) {
        return this.http.post("/login", { username: username, password: password, type: type });
    };
    AuthService.prototype.signUp = function (data) {
        return this.http.post("/register", data);
    };
    AuthService.prototype.logout = function () {
        // Removing user data from local storage and the service
        localStorage.removeItem(_constants__WEBPACK_IMPORTED_MODULE_3__["USER_ID"]);
        localStorage.removeItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]);
        this.userId = null;
        // Dispatching to all listeners that the user is not authenticated
        this._isAuthenticated.next(false);
    };
    AuthService.prototype.autoLogin = function () {
        var id = localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["USER_ID"]);
        if (id) {
            this.setUserId(id);
        }
    };
    AuthService.prototype.getToken = function () {
        return localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]);
    };
    AuthService.prototype.isLoggednIn = function () {
        return this.getToken() !== null;
    };
    AuthService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/auth/forget-password/forget-password.component.css":
/*!********************************************************************!*\
  !*** ./src/app/auth/forget-password/forget-password.component.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2F1dGgvZm9yZ2V0LXBhc3N3b3JkL2ZvcmdldC1wYXNzd29yZC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/auth/forget-password/forget-password.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/auth/forget-password/forget-password.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<body class=\"hold-transition forget-page\">\n<div class=\"register-box\">\n  <div class=\"register-logo\">\n    <a href=\"#\"><b>YouThink</b><b style=\"color: green\">Green</b></a>\n  </div>\n\n  <div class=\"register-box-body\">\n    <p class=\"login-box-msg\">Send email for reseting your password</p>\n\n    <form action=\"../../index.html\" method=\"post\">\n      <div class=\"form-group has-feedback\">\n        <input type=\"email\" class=\"form-control\" placeholder=\"Email\">\n        <span class=\"glyphicon glyphicon-envelope form-control-feedback\"></span>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-xs-8\">\n        </div>\n        <!-- /.col -->\n        <div class=\"col-xs-4\">\n          <button type=\"submit\" class=\"btn btn-primary btn-block btn-flat\">Forget</button>\n        </div>\n        <!-- /.col -->\n      </div>\n    </form>\n    <a routerLink=\"/auth/login\">Back to Login</a><br>\n\n  </div>\n  <!-- /.form-box -->\n</div>\n<!-- /.register-box -->\n\n<!-- jQuery 3 -->\n<script src=\"../../bower_components/jquery/dist/jquery.min.js\"></script>\n<!-- Bootstrap 3.3.7 -->\n<script src=\"../../bower_components/bootstrap/dist/js/bootstrap.min.js\"></script>\n<!-- iCheck -->\n<script src=\"../../plugins/iCheck/icheck.min.js\"></script>\n<script>\n  $(function () {\n    $('input').iCheck({\n      checkboxClass: 'icheckbox_square-blue',\n      radioClass: 'iradio_square-blue',\n      increaseArea: '20%' /* optional */\n    });\n  });\n</script>\n\n\n</body>\n"

/***/ }),

/***/ "./src/app/auth/forget-password/forget-password.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/auth/forget-password/forget-password.component.ts ***!
  \*******************************************************************/
/*! exports provided: ForgetPasswordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForgetPasswordComponent", function() { return ForgetPasswordComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../auth.service */ "./src/app/auth/auth.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ForgetPasswordComponent = /** @class */ (function () {
    function ForgetPasswordComponent(fb, authService) {
        this.fb = fb;
        this.authService = authService;
    }
    ForgetPasswordComponent.prototype.ngOnInit = function () {
        this.buildForm();
    };
    Object.defineProperty(ForgetPasswordComponent.prototype, "email", {
        get: function () {
            return this.form.get('email');
        },
        enumerable: true,
        configurable: true
    });
    ForgetPasswordComponent.prototype.buildForm = function () {
        this.form = this.fb.group({
            email: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email]]
        });
    };
    ForgetPasswordComponent.prototype.forgetPassword = function () {
        var _this = this;
        this.authService.forgetPassword(this.form.value.email).subscribe(function (res) {
            _this.resMail = res.data.email;
            setTimeout(function () {
                _this.successMsg = '';
            }, 3000);
        }, function (err) {
            setTimeout(function () {
                _this.errorMsg = '';
            }, 3000);
        });
        this.form.reset();
    };
    ForgetPasswordComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-forget-password',
            template: __webpack_require__(/*! ./forget-password.component.html */ "./src/app/auth/forget-password/forget-password.component.html"),
            styles: [__webpack_require__(/*! ./forget-password.component.css */ "./src/app/auth/forget-password/forget-password.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]])
    ], ForgetPasswordComponent);
    return ForgetPasswordComponent;
}());



/***/ }),

/***/ "./src/app/auth/login/login.component.css":
/*!************************************************!*\
  !*** ./src/app/auth/login/login.component.css ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2F1dGgvbG9naW4vbG9naW4uY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/auth/login/login.component.html":
/*!*************************************************!*\
  !*** ./src/app/auth/login/login.component.html ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"login-box\">\n  <div class=\"login-logo\">\n    <a href=\"#\"><b>YouThink</b><b style=\"color: green\">Green</b></a>\n  </div>\n  <!-- /.login-logo -->\n  <div class=\"login-box-body\">\n    <p class=\"login-box-msg\">Sign in to start your session</p>\n\n    <form id=\"form-login\" class=\"p-t-15\" role=\"form\" [formGroup]=\"form\" (ngSubmit)=\"login()\">\n      <div class=\"form-group has-feedback\">\n        <input formControlName=\"email\" type=\"email\" class=\"form-control\" placeholder=\"Email\">\n        <span class=\"glyphicon glyphicon-envelope form-control-feedback\"></span>\n      </div>\n      <div class=\"form-group has-feedback\">\n        <input             formControlName=\"password\"\n                           type=\"password\" class=\"form-control\" placeholder=\"Password\">\n        <span class=\"glyphicon glyphicon-lock form-control-feedback\"></span>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-xs-8\">\n          <div class=\"checkbox icheck\">\n            <label class=\"\">\n              <div class=\"icheckbox_square-blue\" aria-checked=\"false\" aria-disabled=\"false\" style=\"position: relative;\"><input type=\"checkbox\" style=\"position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;\"><ins class=\"iCheck-helper\" style=\"position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;\"></ins></div> Remember Me\n            </label>\n          </div>\n        </div>\n        <!-- /.col -->\n        <div class=\"col-xs-4\">\n          <button type=\"button\" class=\"btn btn-primary btn-block btn-flat\" (click)=\"login()\">Sign In</button>\n        </div>\n        <!-- /.col -->\n      </div>\n    </form>\n\n    <a routerLink=\"/auth/forget-password\">I forgot my password</a><br>\n    <a routerLink=\"/auth/register\" href=\"register.html\" class=\"text-center\">Register a new membership</a>\n\n  </div>\n  <!-- /.login-box-body -->\n</div>\n"

/***/ }),

/***/ "./src/app/auth/login/login.component.ts":
/*!***********************************************!*\
  !*** ./src/app/auth/login/login.component.ts ***!
  \***********************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../auth.service */ "./src/app/auth/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LoginComponent = /** @class */ (function () {
    function LoginComponent(fb, router, route, authService) {
        this.fb = fb;
        this.router = router;
        this.route = route;
        this.authService = authService;
        this.ctrl = this;
        this.loading = false;
    }
    LoginComponent.prototype.ngOnInit = function () {
        if (this.authService.isLoggednIn()) {
            this.router.navigate(['/dashboard']);
        }
        this.buildForm();
        document.body.className = 'hold-transition login-page';
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
                increaseArea: '20%' /* optional */
            });
        });
    };
    Object.defineProperty(LoginComponent.prototype, "email", {
        get: function () {
            return this.form.get('email');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LoginComponent.prototype, "password", {
        get: function () {
            return this.form.get('password');
        },
        enumerable: true,
        configurable: true
    });
    LoginComponent.prototype.login = function () {
        var _this = this;
        // this.router.navigate(['dashboard']);
        this.loading = true;
        this.authService
            .login(this.form.value.email, this.form.value.password, 'employee')
            .subscribe(function (result) {
            _this.loading = false;
            var id = result._id;
            var token = result.token;
            _this.authService.saveUserData(id, token);
            var returnUrl = _this.route.snapshot.queryParamMap.get('returnUrl');
            _this.router.navigate([returnUrl || 'dashboard']);
            _this.message = '';
        }, function (error) {
            _this.loading = false;
        });
    };
    LoginComponent.prototype.buildForm = function () {
        this.form = this.fb.group({
            email: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email])],
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
        });
    };
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/auth/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.css */ "./src/app/auth/login/login.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/auth/register/register.component.css":
/*!******************************************************!*\
  !*** ./src/app/auth/register/register.component.css ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2F1dGgvcmVnaXN0ZXIvcmVnaXN0ZXIuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/auth/register/register.component.html":
/*!*******************************************************!*\
  !*** ./src/app/auth/register/register.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<body class=\"hold-transition register-page\">\n<div class=\"register-box\">\n  <div class=\"register-logo\">\n    <a href=\"#\"><b>YouThink</b><b style=\"color: green\">Green</b></a>\n  </div>\n\n  <div class=\"register-box-body\">\n    <p class=\"login-box-msg\">Register a new membership</p>\n\n    <form action=\"../../index.html\" method=\"post\">\n      <div class=\"form-group has-feedback\">\n        <input type=\"text\" class=\"form-control\" placeholder=\"Full name\">\n        <span class=\"glyphicon glyphicon-user form-control-feedback\"></span>\n      </div>\n      <div class=\"form-group has-feedback\">\n        <input type=\"email\" class=\"form-control\" placeholder=\"Email\">\n        <span class=\"glyphicon glyphicon-envelope form-control-feedback\"></span>\n      </div>\n      <div class=\"form-group has-feedback\">\n        <input type=\"password\" class=\"form-control\" placeholder=\"Password\">\n        <span class=\"glyphicon glyphicon-lock form-control-feedback\"></span>\n      </div>\n      <div class=\"form-group has-feedback\">\n        <input type=\"password\" class=\"form-control\" placeholder=\"Retype password\">\n        <span class=\"glyphicon glyphicon-log-in form-control-feedback\"></span>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-xs-8\">\n          <div class=\"checkbox icheck\">\n            <label class=\"\">\n              <div class=\"icheckbox_square-blue\" aria-checked=\"false\" aria-disabled=\"false\" style=\"position: relative;\"><input type=\"checkbox\" style=\"position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;\"><ins class=\"iCheck-helper\" style=\"position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;\"></ins></div> I agree to the <a href=\"#\">terms</a>\n            </label>\n          </div>\n        </div>\n        <!-- /.col -->\n        <div class=\"col-xs-4\">\n          <button type=\"submit\" class=\"btn btn-primary btn-block btn-flat\">Register</button>\n        </div>\n        <!-- /.col -->\n      </div>\n    </form>\n\n    <a routerLink=\"/auth/login\" class=\"text-center\">I already have a membership</a>\n  </div>\n  <!-- /.form-box -->\n</div>\n<!-- /.register-box -->\n\n<!-- jQuery 3 -->\n<script src=\"../../bower_components/jquery/dist/jquery.min.js\"></script>\n<!-- Bootstrap 3.3.7 -->\n<script src=\"../../bower_components/bootstrap/dist/js/bootstrap.min.js\"></script>\n<!-- iCheck -->\n<script src=\"../../plugins/iCheck/icheck.min.js\"></script>\n<script>\n  $(function () {\n    $('input').iCheck({\n      checkboxClass: 'icheckbox_square-blue',\n      radioClass: 'iradio_square-blue',\n      increaseArea: '20%' /* optional */\n    });\n  });\n</script>\n\n\n</body>\n"

/***/ }),

/***/ "./src/app/auth/register/register.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/auth/register/register.component.ts ***!
  \*****************************************************/
/*! exports provided: RegisterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterComponent", function() { return RegisterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../auth.service */ "./src/app/auth/auth.service.ts");
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../constants */ "./src/app/constants.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var RegisterComponent = /** @class */ (function () {
    function RegisterComponent(fb, router, authService) {
        this.fb = fb;
        this.router = router;
        this.authService = authService;
        this.ctrl = this;
        this.loading = false;
        this.myModel = [];
    }
    RegisterComponent.prototype.ngOnInit = function () {
        this.buildForm();
    };
    Object.defineProperty(RegisterComponent.prototype, "firstName", {
        get: function () {
            return this.form.get('firstName');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RegisterComponent.prototype, "lastName", {
        get: function () {
            return this.form.get('lastName');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RegisterComponent.prototype, "email", {
        get: function () {
            return this.form.get('email');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RegisterComponent.prototype, "mobileNumber", {
        get: function () {
            return this.form.get('mobileNumber');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RegisterComponent.prototype, "password", {
        get: function () {
            return this.form.get('password');
        },
        enumerable: true,
        configurable: true
    });
    RegisterComponent.prototype.signUp = function () {
        var _this = this;
        this.loading = true;
        this.authService.signUp(this.form.value).subscribe(function (res) {
            _this.loading = false;
            _this.successMsg = 'Success!, Your account created';
            setTimeout(function () {
                _this.successMsg = '';
                _this.router.navigate(['dashboard']);
            }, 2000);
            _this.saveUserData(res.data._id, res.data.token);
        }, function (error) {
            _this.loading = false;
            setTimeout(function () {
                _this.errorMsg = '';
            }, 2000);
        });
    };
    RegisterComponent.prototype.saveUserData = function (id, token) {
        this.authService.saveUserData(id, token);
    };
    RegisterComponent.prototype.buildForm = function () {
        this.form = this.fb.group({
            firstName: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            lastName: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            email: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email]],
            mobileNumber: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern(_constants__WEBPACK_IMPORTED_MODULE_4__["PHONE_REGEX"])]],
            password: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern(_constants__WEBPACK_IMPORTED_MODULE_4__["PASSWORD_REGEX"])]]
        });
    };
    RegisterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-register',
            template: __webpack_require__(/*! ./register.component.html */ "./src/app/auth/register/register.component.html"),
            styles: [__webpack_require__(/*! ./register.component.css */ "./src/app/auth/register/register.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]])
    ], RegisterComponent);
    return RegisterComponent;
}());



/***/ }),

/***/ "./src/app/auth/reset-password/reset-password.component.css":
/*!******************************************************************!*\
  !*** ./src/app/auth/reset-password/reset-password.component.css ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2F1dGgvcmVzZXQtcGFzc3dvcmQvcmVzZXQtcGFzc3dvcmQuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/auth/reset-password/reset-password.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/auth/reset-password/reset-password.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<body class=\"hold-transition reset-page\">\n<div class=\"register-box\">\n  <div class=\"register-logo\">\n    <a href=\"#\"><b>YouThink</b><b style=\"color: green\">Green</b></a>\n  </div>\n\n  <div class=\"register-box-body\">\n    <p class=\"login-box-msg\">Enter a new password</p>\n\n    <form action=\"../../index.html\" method=\"post\">\n      <div class=\"form-group has-feedback\">\n        <input type=\"password\" class=\"form-control\" placeholder=\"Password\">\n        <span class=\"glyphicon glyphicon-lock form-control-feedback\"></span>\n      </div>\n      <div class=\"form-group has-feedback\">\n        <input type=\"password\" class=\"form-control\" placeholder=\"Retype password\">\n        <span class=\"glyphicon glyphicon-log-in form-control-feedback\"></span>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-xs-8\">\n        </div>\n        <!-- /.col -->\n        <div class=\"col-xs-4\">\n          <button type=\"submit\" class=\"btn btn-primary btn-block btn-flat\">Reset</button>\n        </div>\n        <!-- /.col -->\n      </div>\n    </form>\n\n  </div>\n  <!-- /.form-box -->\n</div>\n<!-- /.register-box -->\n\n<!-- jQuery 3 -->\n<script src=\"../../bower_components/jquery/dist/jquery.min.js\"></script>\n<!-- Bootstrap 3.3.7 -->\n<script src=\"../../bower_components/bootstrap/dist/js/bootstrap.min.js\"></script>\n<!-- iCheck -->\n<script src=\"../../plugins/iCheck/icheck.min.js\"></script>\n<script>\n  $(function () {\n    $('input').iCheck({\n      checkboxClass: 'icheckbox_square-blue',\n      radioClass: 'iradio_square-blue',\n      increaseArea: '20%' /* optional */\n    });\n  });\n</script>\n\n\n</body>\n"

/***/ }),

/***/ "./src/app/auth/reset-password/reset-password.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/auth/reset-password/reset-password.component.ts ***!
  \*****************************************************************/
/*! exports provided: ResetPasswordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResetPasswordComponent", function() { return ResetPasswordComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../auth.service */ "./src/app/auth/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../constants */ "./src/app/constants.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ResetPasswordComponent = /** @class */ (function () {
    function ResetPasswordComponent(fb, authService, route, router) {
        this.fb = fb;
        this.authService = authService;
        this.route = route;
        this.router = router;
    }
    ResetPasswordComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.code = params.code;
        });
        this.buildForm();
    };
    Object.defineProperty(ResetPasswordComponent.prototype, "password", {
        get: function () {
            return this.form.get('password');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ResetPasswordComponent.prototype, "confirmPassword", {
        get: function () {
            return this.form.get('confirmPassword');
        },
        enumerable: true,
        configurable: true
    });
    ResetPasswordComponent.prototype.buildForm = function () {
        this.form = this.fb.group({
            password: [
                '',
                [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].minLength(8),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern(_constants__WEBPACK_IMPORTED_MODULE_3__["PASSWORD_REGEX"])
                ]
            ],
            confirmPassword: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, this.passwordConfirming]]
        });
    };
    ResetPasswordComponent.prototype.passwordConfirming = function (c) {
        if (!c.parent || !c) {
            return;
        }
        var pwd = c.parent.get('password');
        var cpwd = c.parent.get('confirmPassword');
        if (!pwd || !cpwd) {
            return;
        }
        if (pwd.value !== cpwd.value) {
            return { invalid: true };
        }
    };
    ResetPasswordComponent.prototype.resetPassword = function () {
        var _this = this;
        this.authService
            .resetPassword(this.code, this.form.value.password)
            .subscribe(function (res) {
            setTimeout(function () {
                _this.successMsg = '';
                _this.router.navigate(['login']);
            }, 3000);
        }, function (err) {
            setTimeout(function () {
                _this.errorMsg = '';
            }, 3000);
        });
        this.form.reset();
    };
    ResetPasswordComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-reset-password',
            template: __webpack_require__(/*! ./reset-password.component.html */ "./src/app/auth/reset-password/reset-password.component.html"),
            styles: [__webpack_require__(/*! ./reset-password.component.css */ "./src/app/auth/reset-password/reset-password.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"],
            _auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], ResetPasswordComponent);
    return ResetPasswordComponent;
}());



/***/ }),

/***/ "./src/app/constants.ts":
/*!******************************!*\
  !*** ./src/app/constants.ts ***!
  \******************************/
/*! exports provided: USER_ID, AUTH_TOKEN, googleMapsUrl, ITEMS_PER_PAGE, NAME_REGEX, PHONE_REGEX, EMAIL_REGEX, PASSWORD_REGEX */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "USER_ID", function() { return USER_ID; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AUTH_TOKEN", function() { return AUTH_TOKEN; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "googleMapsUrl", function() { return googleMapsUrl; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ITEMS_PER_PAGE", function() { return ITEMS_PER_PAGE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NAME_REGEX", function() { return NAME_REGEX; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PHONE_REGEX", function() { return PHONE_REGEX; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EMAIL_REGEX", function() { return EMAIL_REGEX; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PASSWORD_REGEX", function() { return PASSWORD_REGEX; });
var USER_ID = 'user-id';
var AUTH_TOKEN = 'auth-token';
var googleMapsUrl = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAVyrPpRVpBYwmDTYzZk0v0hp7B3KuhKpc?libraries=places';
var ITEMS_PER_PAGE = 5;
var NAME_REGEX = /^[\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z ]+[\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z-_ ]*$/;
var PHONE_REGEX = /^9665[0-9]{8}$$/;
var EMAIL_REGEX = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
var PASSWORD_REGEX = /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/;


/***/ }),

/***/ "./src/app/contents/contents.service.ts":
/*!**********************************************!*\
  !*** ./src/app/contents/contents.service.ts ***!
  \**********************************************/
/*! exports provided: ContentsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContentsService", function() { return ContentsService; });
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../constants */ "./src/app/constants.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ContentsService = /** @class */ (function () {
    function ContentsService(http) {
        this.http = http;
        this.apiUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_0__["environment"].apiBaseUrl;
        this.userId = null;
        this._isAuthenticated = new rxjs__WEBPACK_IMPORTED_MODULE_4__["BehaviorSubject"](false);
    }
    ContentsService.prototype.uploadSliderImages = function (data) {
        return this.http.post(this.apiUrl + "/upload/slider", data, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    ContentsService.prototype.uploadLogoImage = function (data) {
        return this.http.post(this.apiUrl + "/upload/logos", data, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    ContentsService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], ContentsService);
    return ContentsService;
}());



/***/ }),

/***/ "./src/app/contents/contents/contents.component.css":
/*!**********************************************************!*\
  !*** ./src/app/contents/contents/contents.component.css ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbnRlbnRzL2NvbnRlbnRzL2NvbnRlbnRzLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/contents/contents/contents.component.html":
/*!***********************************************************!*\
  !*** ./src/app/contents/contents/contents.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Content Header (Page header) -->\r\n<section class=\"content-header\">\r\n  <h1>\r\n    Events\r\n  </h1>\r\n  <ol class=\"breadcrumb\">\r\n    <li><a href=\"#\"><i class=\"fa fa-dashboard\"></i> Home</a></li>\r\n    <li><a href=\"#\">Contents</a></li>\r\n  </ol>\r\n</section>\r\n\r\n<!-- Main content -->\r\n<section class=\"content\">\r\n  <div class=\"row\">\r\n    <!-- left column -->\r\n    <div class=\"col-md-12\">\r\n      <!-- general form elements -->\r\n      <div class=\"box box-primary\">\r\n        <div class=\"box-header with-border\">\r\n          <h3 class=\"box-title\">Add Content</h3>\r\n        </div>\r\n        <!-- /.box-header -->\r\n        <!-- form start -->\r\n        <form id=\"form-slider-contents\" class=\"p-t-15\" role=\"form\" [formGroup]=\"form\" (ngSubmit)=\"uploadSlider()\">\r\n          <div class=\"box-body\">\r\n            <label for=\"exampleInputFile\">Slider</label>\r\n            <div class=\"form-group\">\r\n              <img *ngFor=\"let url1 of urls1\" [src]=\"url1\" height=\"200\"><br/>\r\n              <input type=\"file\" (change)=\"onFileChanged1($event)\" id=\"exampleInputFile\" multiple>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label>Comment</label>\r\n              <input formControlName=\"comment\" class=\"form-control\" placeholder=\"Enter Comment\">\r\n            </div>\r\n          </div>\r\n          <!-- /.box-body -->\r\n\r\n          <div class=\"box-footer\">\r\n            <button type=\"button\" class=\"btn btn-primary\" (click)=\"uploadSlider()\">Save</button>\r\n          </div>\r\n        </form>\r\n        <form id=\"form-logo-contents\" class=\"p-t-15\" role=\"form\" [formGroup]=\"form\" (ngSubmit)=\"uploadLogo()\">\r\n          <div class=\"box-body\">\r\n            <label for=\"exampleInputFile2\">Logo</label>\r\n            <div class=\"form-group\">\r\n              <img *ngFor=\"let url2 of urls2\" [src]=\"url2\" height=\"200\"><br/>\r\n              <input type=\"file\" (change)=\"onFileChanged2($event)\" id=\"exampleInputFile2\">\r\n            </div>\r\n          </div>\r\n          <!-- /.box-body -->\r\n\r\n          <div class=\"box-footer\">\r\n            <button type=\"button\" class=\"btn btn-primary\" (click)=\"uploadLogo()\">Save</button>\r\n          </div>\r\n        </form>\r\n      </div>\r\n      <!-- /.box -->\r\n\r\n    </div>\r\n    <!--/.col (left) -->\r\n  </div>\r\n  <!-- /.row -->\r\n</section>\r\n<!-- /.content -->\r\n\r\n<!-- Extra content -->\r\n<!--<section class=\"content\">-->\r\n  <!--<div class=\"row\">-->\r\n    <!--&lt;!&ndash; left column &ndash;&gt;-->\r\n    <!--<div class=\"col-md-12\">-->\r\n      <!--&lt;!&ndash; general form elements &ndash;&gt;-->\r\n      <!--<div class=\"box box-primary\">-->\r\n        <!--<div class=\"box-header with-border\">-->\r\n          <!--<h3 class=\"box-title\">Extra Content Sections</h3>-->\r\n        <!--</div>-->\r\n        <!--&lt;!&ndash; /.box-header &ndash;&gt;-->\r\n        <!--&lt;!&ndash; form start &ndash;&gt;-->\r\n        <!--<form id=\"form-extra-sections-events\" class=\"p-t-15\" role=\"form\" [formGroup]=\"form\" (ngSubmit)=\"updateSections()\">-->\r\n          <!--<div class=\"box-body\">-->\r\n            <!--<div class=\"form-group\">-->\r\n              <!--<label>Description</label>-->\r\n              <!--&lt;!&ndash;<textarea formControlName=\"extraDescription\"></textarea>&ndash;&gt;-->\r\n              <!--<angular-editor formControlName=\"extraDescription\" [config]=\"editorConfig\"></angular-editor>-->\r\n            <!--</div>-->\r\n            <!--<label for=\"exampleInputFile\">Upload Image</label>-->\r\n            <!--<div class=\"form-group\">-->\r\n              <!--<img *ngFor=\"let url of urls2\" [src]=\"url\" height=\"200\"><br/>-->\r\n              <!--<input type=\"file\" (change)=\"onFileChanged2($event)\" multiple>-->\r\n            <!--</div>-->\r\n          <!--</div>-->\r\n          <!--&lt;!&ndash; /.box-body &ndash;&gt;-->\r\n\r\n          <!--<div class=\"box-footer\">-->\r\n            <!--<button type=\"button\" (click)=\"updateSections()\" class=\"btn btn-primary\">Save</button>-->\r\n            <!--<button routerLink=\"/events\" class=\"btn btn-primary pull-right\">Back to List</button>-->\r\n          <!--</div>-->\r\n        <!--</form>-->\r\n      <!--</div>-->\r\n      <!--&lt;!&ndash; /.box &ndash;&gt;-->\r\n\r\n    <!--</div>-->\r\n    <!--&lt;!&ndash;/.col (left) &ndash;&gt;-->\r\n  <!--</div>-->\r\n  <!--&lt;!&ndash; /.row &ndash;&gt;-->\r\n<!--</section>-->\r\n<!-- /.content -->\r\n"

/***/ }),

/***/ "./src/app/contents/contents/contents.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/contents/contents/contents.component.ts ***!
  \*********************************************************/
/*! exports provided: ContentsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContentsComponent", function() { return ContentsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _contents_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../contents.service */ "./src/app/contents/contents.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ContentsComponent = /** @class */ (function () {
    function ContentsComponent(fb, router, route, contentService) {
        this.fb = fb;
        this.router = router;
        this.route = route;
        this.contentService = contentService;
        this.loading = false;
        this.selectedFiles = [];
        this.formData = new FormData();
        this.urls = [];
        this.urls1 = [];
        this.urls2 = [];
    }
    ContentsComponent.prototype.ngOnInit = function () {
        this.buildForm();
    };
    Object.defineProperty(ContentsComponent.prototype, "comment", {
        get: function () {
            return this.form.get('comment');
        },
        enumerable: true,
        configurable: true
    });
    ContentsComponent.prototype.onFileChanged = function (event) {
        var _this = this;
        this.loading = true;
        this.selectedFiles = event.target.files;
        // this.formData.append('images', this.selectedFiles, this.selectedFiles.name);
        if (this.selectedFiles.length > 0) {
            var _loop_1 = function (i) {
                var reader = new FileReader();
                reader.onload = function (event) {
                    _this.formData.append('images', _this.selectedFiles[i]);
                    _this.urls.push(event.target.result);
                };
                reader.readAsDataURL(this_1.selectedFiles[i]);
            };
            var this_1 = this;
            for (var i = 0; i < this.selectedFiles.length; i++) {
                _loop_1(i);
            }
        }
    };
    ContentsComponent.prototype.onFileChanged1 = function (event) {
        var _this = this;
        this.loading = true;
        this.selectedFiles = event.target.files;
        // this.formData.append('images', this.selectedFiles, this.selectedFiles.name);
        if (this.selectedFiles.length > 0) {
            var _loop_2 = function (i) {
                var reader = new FileReader();
                reader.onload = function (event) {
                    _this.formData.append('images', _this.selectedFiles[i]);
                    _this.urls1.push(event.target.result);
                };
                reader.readAsDataURL(this_2.selectedFiles[i]);
            };
            var this_2 = this;
            for (var i = 0; i < this.selectedFiles.length; i++) {
                _loop_2(i);
            }
        }
    };
    ContentsComponent.prototype.onFileChanged2 = function (event) {
        var _this = this;
        this.loading = true;
        this.selectedFiles = event.target.files;
        // this.formData.append('images', this.selectedFiles, this.selectedFiles.name);
        if (this.selectedFiles.length > 0) {
            var _loop_3 = function (i) {
                var reader = new FileReader();
                reader.onload = function (event) {
                    _this.formData.append('images', _this.selectedFiles[i]);
                    _this.urls2.push(event.target.result);
                };
                reader.readAsDataURL(this_3.selectedFiles[i]);
            };
            var this_3 = this;
            for (var i = 0; i < this.selectedFiles.length; i++) {
                _loop_3(i);
            }
        }
    };
    ContentsComponent.prototype.uploadSlider = function () {
        var _this = this;
        this.loading = true;
        var data = {
            key: 'slider',
            comment: this.form.value.comment
        };
        this.formData.append('data', JSON.stringify(data));
        this.contentService
            .uploadSliderImages(this.formData)
            .subscribe(function (result) {
            _this.loading = false;
            var returnUrl = _this.route.snapshot.queryParamMap.get('returnUrl');
            // this.router.navigate(['contents']);
            // setTimeout(() => {
            //   this.successMsg = 'News Created Successfully';
            // }, 2000);
        }, function (error) {
            _this.loading = false;
            // setTimeout(() => {
            //   this.errorMsg = error;
            // }, 2000);
        });
    };
    ContentsComponent.prototype.uploadLogo = function () {
        var _this = this;
        this.loading = true;
        this.contentService
            .uploadLogoImage(this.formData)
            .subscribe(function (result) {
            _this.loading = false;
            var returnUrl = _this.route.snapshot.queryParamMap.get('returnUrl');
            // this.router.navigate(['contents']);
            // setTimeout(() => {
            //   this.successMsg = 'News Created Successfully';
            // }, 2000);
        }, function (error) {
            _this.loading = false;
            // setTimeout(() => {
            //   this.errorMsg = error;
            // }, 2000);
        });
    };
    ContentsComponent.prototype.buildForm = function () {
        this.form = this.fb.group({
            comment: [''],
        });
    };
    ContentsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-contents',
            template: __webpack_require__(/*! ./contents.component.html */ "./src/app/contents/contents/contents.component.html"),
            styles: [__webpack_require__(/*! ./contents.component.css */ "./src/app/contents/contents/contents.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _contents_service__WEBPACK_IMPORTED_MODULE_2__["ContentsService"]])
    ], ContentsComponent);
    return ContentsComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/dashboard.component.css":
/*!***************************************************!*\
  !*** ./src/app/dashboard/dashboard.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Rhc2hib2FyZC9kYXNoYm9hcmQuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/dashboard/dashboard.component.html":
/*!****************************************************!*\
  !*** ./src/app/dashboard/dashboard.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n"

/***/ }),

/***/ "./src/app/dashboard/dashboard.component.ts":
/*!**************************************************!*\
  !*** ./src/app/dashboard/dashboard.component.ts ***!
  \**************************************************/
/*! exports provided: DashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardComponent", function() { return DashboardComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DashboardComponent = /** @class */ (function () {
    function DashboardComponent() {
    }
    DashboardComponent.prototype.ngOnInit = function () {
        document.body.className = 'sidebar-mini skin-green-light';
    };
    DashboardComponent.prototype.ngOnDestroy = function () {
        document.body.className = '';
    };
    DashboardComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-dashboard',
            template: __webpack_require__(/*! ./dashboard.component.html */ "./src/app/dashboard/dashboard.component.html"),
            styles: [__webpack_require__(/*! ./dashboard.component.css */ "./src/app/dashboard/dashboard.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "./src/app/events-applications/create-application/create-application.component.css":
/*!*****************************************************************************************!*\
  !*** ./src/app/events-applications/create-application/create-application.component.css ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2V2ZW50cy1hcHBsaWNhdGlvbnMvY3JlYXRlLWFwcGxpY2F0aW9uL2NyZWF0ZS1hcHBsaWNhdGlvbi5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/events-applications/create-application/create-application.component.html":
/*!******************************************************************************************!*\
  !*** ./src/app/events-applications/create-application/create-application.component.html ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Content Header (Page header) -->\r\n<section class=\"content-header\">\r\n  <h1>\r\n    Event Question\r\n  </h1>\r\n  <ol class=\"breadcrumb\">\r\n    <li><a href=\"#\"><i class=\"fa fa-dashboard\"></i> Home</a></li>\r\n    <li><a href=\"#\">Events</a></li>\r\n    <li class=\"active\">Create Question</li>\r\n  </ol>\r\n</section>\r\n\r\n<!-- Main content -->\r\n<div class=\"loader\" [hidden]=\"!loading\"></div>\r\n<section class=\"content\">\r\n  <div class=\"row\">\r\n    <!-- left column -->\r\n    <div class=\"col-md-12\">\r\n      <!-- general form elements -->\r\n      <div class=\"box box-primary\">\r\n        <div class=\"box-header with-border\">\r\n          <h3 class=\"box-title\">Add Event Question</h3>\r\n          <button class=\"btn btn-primary pull-right\" (click)=\"AddNewAnswer()\"><span class=\"glyphicon glyphicon-plus\"></span>Add Answer</button>\r\n        </div>\r\n        <!-- /.box-header -->\r\n        <!-- form start -->\r\n        <form id=\"form-programs-application\" class=\"p-t-15\" role=\"form\" [formGroup]=\"form\" (ngSubmit)=\"create()\">\r\n          <div class=\"box-body\">\r\n            <div class=\"form-group\">\r\n              <label for=\"exampleInputEmail1\">Question</label>\r\n              <input formControlName=\"question\" class=\"form-control\" id=\"exampleInputEmail1\" placeholder=\"Enter Question\">\r\n            </div>\r\n            <div id=\"questionAnswers\" class=\"form-group\">\r\n              <label id=\"answerLbl\" hidden=\"hidden\">Answer</label>\r\n              <!--<input class=\"form-control\" id=\"answer1\" placeholder=\"Enter Answer\">-->\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label>Type</label>\r\n              <select formControlName=\"type\" class=\"form-control\" [(ngModel)]=\"this.types[0]\" (change)=\"onSelectType(type)\">\r\n                <option [value]=\"type\" *ngFor=\"let type of types\">\r\n                  {{type}}</option>\r\n              </select>\r\n            </div>\r\n          </div>\r\n          <!-- /.box-body -->\r\n\r\n          <div class=\"box-footer\">\r\n            <button type=\"submit\" class=\"btn btn-primary\">Save</button>\r\n          </div>\r\n        </form>\r\n      </div>\r\n      <!-- /.box -->\r\n\r\n    </div>\r\n    <!--/.col (left) -->\r\n  </div>\r\n  <!-- /.row -->\r\n</section>\r\n<!-- /.content -->\r\n\r\n<style>\r\n  .btn {\r\n    background-color: DodgerBlue; /* Blue background */\r\n    border: none; /* Remove borders */\r\n    color: white; /* White text */\r\n    font-size: 10px; /* Set a font size */\r\n    cursor: pointer; /* Mouse pointer on hover */\r\n  }\r\n\r\n  /* Darker background on mouse-over */\r\n  .btn:hover {\r\n    background-color: RoyalBlue;\r\n  }\r\n</style>\r\n<!-- Content Header (Page header) -->\r\n<section class=\"content-header\">\r\n  <h1>\r\n    Questions\r\n  </h1>\r\n  <ol class=\"breadcrumb\">\r\n    <li><a href=\"#\"><i class=\"fa fa-dashboard\"></i> Home</a></li>\r\n    <li><a href=\"#\">Events</a></li>\r\n    <li><a href=\"#\">Event Questions</a></li>\r\n  </ol>\r\n</section>\r\n\r\n<!-- Main content -->\r\n<section class=\"content\">\r\n  <!-- /.row -->\r\n  <div class=\"row\">\r\n    <div class=\"col-xs-12\">\r\n      <div class=\"box\">\r\n        <div class=\"box-header\">\r\n          <h3 class=\"box-title\"></h3>\r\n\r\n          <div class=\"box-tools\">\r\n            <div class=\"input-group input-group-sm\" style=\"width: 150px;\">\r\n              <input type=\"text\" name=\"table_search\" class=\"form-control pull-right\" placeholder=\"Search\">\r\n\r\n              <div class=\"input-group-btn\">\r\n                <button type=\"submit\" class=\"btn btn-default\"><i class=\"fa fa-search\"></i></button>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <!-- /.box-header -->\r\n        <div class=\"box-body table-responsive no-padding\">\r\n          <table class=\"table table-hover\">\r\n            <thead>\r\n            <tr>\r\n              <th>Question</th>\r\n              <th># Answers</th>\r\n              <th>Type</th>\r\n            </tr>\r\n            </thead>\r\n            <tbody>\r\n            <tr *ngFor=\"let question of questions\" class=\"gradeX odd content-table-payment cursor\" role=\"row\">\r\n              <td class=\"mn-130 sorting_1\">{{question.question}}</td>\r\n              <td class=\"mn-130 sorting_1\">{{question.answers.length}}</td>\r\n              <td class=\"mn-130 sorting_1\">{{question.type}}</td>\r\n              <td>\r\n                <button class=\"btn\" (click)=\"delete(this.eventId, question._id)\"><i class=\"fa fa-trash\"></i></button>\r\n              </td>\r\n            </tr>\r\n            </tbody></table>\r\n        </div>\r\n        <!-- /.box-body -->\r\n      </div>\r\n      <!-- /.box -->\r\n    </div>\r\n  </div>\r\n</section>\r\n<!-- /.content -->\r\n\r\n"

/***/ }),

/***/ "./src/app/events-applications/create-application/create-application.component.ts":
/*!****************************************************************************************!*\
  !*** ./src/app/events-applications/create-application/create-application.component.ts ***!
  \****************************************************************************************/
/*! exports provided: CreateEventApplicationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateEventApplicationComponent", function() { return CreateEventApplicationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _events_events_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../events/events.service */ "./src/app/events/events.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CreateEventApplicationComponent = /** @class */ (function () {
    function CreateEventApplicationComponent(fb, router, route, eventService) {
        this.fb = fb;
        this.router = router;
        this.route = route;
        this.eventService = eventService;
        this.ctrl = this;
        this.eventId = this.route.snapshot.params.id;
        this.types = [
            'Checkbox',
            'RadioButton',
            'Paragraph',
            'Short Answer',
            'Dropdown Menu',
            'File Upload'
        ];
        this.answers = [];
        this.answerCount = 0;
        this.questions = [];
        this.loading = false;
    }
    CreateEventApplicationComponent.prototype.ngOnInit = function () {
        this.get(this.eventId);
        this.buildForm();
    };
    Object.defineProperty(CreateEventApplicationComponent.prototype, "question", {
        get: function () {
            return this.form.get('question');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreateEventApplicationComponent.prototype, "answer", {
        get: function () {
            return this.form.get('answer');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreateEventApplicationComponent.prototype, "type", {
        get: function () {
            return this.form.get('type');
        },
        enumerable: true,
        configurable: true
    });
    CreateEventApplicationComponent.prototype.create = function () {
        var _this = this;
        var prevElement = document.getElementById('answer' + this.answerCount);
        this.answers.push(prevElement.value);
        $('.content').find('*').attr('disabled', 'disabled');
        $('.content').find('a').click(function (e) { e.preventDefault(); });
        for (var answerIndex = 1; answerIndex <= this.answerCount; answerIndex++) {
            var prevAnswer = document.getElementById('answer' + answerIndex);
            this.answers.push(prevAnswer.value);
        }
        var data = {
            question: this.form.value.question,
            answers: this.answers,
            type: this.form.value.type
        };
        this.loading = true;
        this.eventService
            .addQuestion(this.eventId, data)
            .subscribe(function (result) {
            _this.loading = false;
            var returnUrl = _this.route.snapshot.queryParamMap.get('returnUrl');
            $('.content').find('*').removeAttr('disabled');
            $('.content').find('a').unbind('click');
            _this.get(_this.eventId);
            var parent = document.getElementById('questionAnswers');
            for (var answerIndex = 0; answerIndex <= _this.answerCount; answerIndex += 1) {
                parent.removeChild(parent.childNodes[answerIndex]);
            }
            _this.answerCount = 0;
        }, function (error) {
            _this.loading = false;
            var returnUrl = _this.route.snapshot.queryParamMap.get('returnUrl');
            $('.content').find('*').removeAttr('disabled');
            $('.content').find('a').unbind('click');
            _this.get(_this.eventId);
            var parent = document.getElementById('questionAnswers');
            for (var answerIndex = 0; answerIndex <= _this.answerCount; answerIndex += 1) {
                parent.removeChild(parent.childNodes[answerIndex]);
            }
            _this.answerCount = 0;
        });
    };
    CreateEventApplicationComponent.prototype.get = function (eventId) {
        var _this = this;
        this.loading = true;
        this.eventService
            .getQuestions(this.eventId)
            .subscribe(function (result) {
            _this.loading = false;
            _this.questions = result;
        }, function (error) {
            _this.loading = false;
        });
    };
    CreateEventApplicationComponent.prototype.delete = function (eventId, questionId) {
        var _this = this;
        this.loading = true;
        this.eventService
            .deleteQuestion(eventId, questionId)
            .subscribe(function (result) {
            _this.loading = false;
            _this.get(eventId);
        }, function (error) {
            _this.loading = false;
            _this.get(eventId);
        });
    };
    CreateEventApplicationComponent.prototype.AddNewAnswer = function () {
        var addAnswerLabel = document.getElementById('answerLbl');
        addAnswerLabel.removeAttribute('hidden');
        var prevElement = document.getElementById('answer' + this.answerCount);
        if (prevElement) {
            this.answers.push(prevElement.value);
        }
        this.answerCount += 1;
        var newAnswer = document.createElement('input');
        newAnswer.setAttribute('type', 'text');
        newAnswer.setAttribute('class', 'form-control');
        newAnswer.setAttribute('placeholder', 'Enter Answer');
        newAnswer.setAttribute('id', 'answer' + (this.answerCount));
        var parent = document.getElementById('questionAnswers');
        parent.appendChild(newAnswer);
    };
    CreateEventApplicationComponent.prototype.onSelectType = function (type) {
        var addAnswerButton = document.getElementById('addAnswerBtn');
        var addAnswerLabel = document.getElementById('answerLbl');
        if (type.value === 'Short Answer' || type.value === 'Paragraph' || type.value === 'FileUpload') {
            addAnswerButton.setAttribute('disabled', 'disabled');
            addAnswerLabel.setAttribute('hidden', 'hidden');
        }
        else if (type.value === 'Checkbox' || type.value === 'RadioButton' || type.value === 'Dropdown Menu') {
            addAnswerButton.removeAttribute('disabled');
            addAnswerLabel.removeAttribute('hidden');
        }
    };
    CreateEventApplicationComponent.prototype.buildForm = function () {
        this.form = this.fb.group({
            question: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            answer: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            type: [this.types]
        });
    };
    CreateEventApplicationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-create-application',
            template: __webpack_require__(/*! ./create-application.component.html */ "./src/app/events-applications/create-application/create-application.component.html"),
            styles: [__webpack_require__(/*! ./create-application.component.css */ "./src/app/events-applications/create-application/create-application.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _events_events_service__WEBPACK_IMPORTED_MODULE_3__["EventsService"]])
    ], CreateEventApplicationComponent);
    return CreateEventApplicationComponent;
}());



/***/ }),

/***/ "./src/app/events/create-event/create-event.component.css":
/*!****************************************************************!*\
  !*** ./src/app/events/create-event/create-event.component.css ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2V2ZW50cy9jcmVhdGUtZXZlbnQvY3JlYXRlLWV2ZW50LmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/events/create-event/create-event.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/events/create-event/create-event.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<!-- Content Header (Page header) -->\r\n  <section class=\"content-header\">\r\n    <h1>\r\n      Events\r\n    </h1>\r\n    <ol class=\"breadcrumb\">\r\n      <li><a routerLink=\"/dashboard\"><i class=\"fa fa-dashboard\"></i> Home</a></li>\r\n      <li><a routerLink=\"/events\">Events</a></li>\r\n      <li class=\"active\">Create</li>\r\n    </ol>\r\n  </section>\r\n\r\n  <!-- Main content -->\r\n<div class=\"loader\" [hidden]=\"!loading\"></div>\r\n\r\n  <section class=\"content\">\r\n    <div class=\"row\">\r\n      <!-- left column -->\r\n      <div class=\"col-md-12\">\r\n        <!-- general form elements -->\r\n        <div class=\"box box-primary\">\r\n          <div class=\"box-header with-border\">\r\n            <h3 class=\"box-title\">Create New Event</h3>\r\n          </div>\r\n          <!-- /.box-header -->\r\n          <!-- form start -->\r\n          <form id=\"form-events\" class=\"p-t-15\" role=\"form\" [formGroup]=\"form\" (ngSubmit)=\"create()\">\r\n            <div class=\"box-body\">\r\n              <div class=\"form-group\">\r\n                <label for=\"exampleInputEmail1\">Title</label>\r\n                <input formControlName=\"title\" class=\"form-control\" id=\"exampleInputEmail1\" placeholder=\"Enter Title\">\r\n              </div>\r\n              <div class=\"form-group\">\r\n                <label for=\"exampleInputEmail1\">Tags</label>\r\n                <input formControlName=\"tags\" class=\"form-control\" placeholder=\"Enter Tags separated comma\">\r\n              </div>\r\n              <div class=\"form-group\">\r\n                <label>Description</label>\r\n                <angular-editor formControlName=\"description\" [config]=\"editorConfig\"></angular-editor>\r\n              </div>\r\n              <div class=\"form-group\">\r\n                <label>Location</label>\r\n                <input formControlName=\"location\" class=\"form-control\" placeholder=\"Enter Location\">\r\n              </div>\r\n              <div class=\"form-group\">\r\n                <label>Date</label>\r\n                <div class=\"input-group date\">\r\n                  <div class=\"input-group-addon\">\r\n                    <i class=\"fa fa-calendar\"></i>\r\n                  </div>\r\n                  <input formControlName=\"dueDate\" type=\"text\" class=\"form-control pull-right\" id=\"datepicker\">\r\n                </div>\r\n              </div>\r\n              <!--<div class=\"form-group\">-->\r\n                <!--<label>Due Date</label>-->\r\n                <!--<input formControlName=\"dueDate\" class=\"form-control\" placeholder=\"Enter Date\">-->\r\n              <!--</div>-->\r\n              <label>Upload Image</label>\r\n              <div class=\"form-group\">\r\n                <img *ngFor='let url of urls'  [src]=\"url\" height=\"200\"> <br/>\r\n                <input type='file' (change)=\"onFileChanged($event)\" multiple>\r\n              </div>\r\n              <div class=\"form-group\">\r\n                <label>Segment</label>\r\n                <select formControlName=\"segment\" class=\"form-control\">\r\n                  <option [value]=\"segment.code\" *ngFor=\"let segment of segments\" [selected]=\"segment\">\r\n                    {{segment.color}}</option>\r\n                </select>\r\n              </div>\r\n              <div class=\"form-group\">\r\n                <label class=\"hover\">\r\n                  <div class=\"icheckbox_flat-green hover\" [ngClass]=\"{'checked': pinned}\" aria-checked=\"true\" aria-disabled=\"false\" style=\"position: relative;\"><input [checked]=\"pinned\" (change)=\"pinned = !pinned\" [value]='pinned' type=\"checkbox\" class=\"flat-red\" checked=\"\" style=\"position: absolute; opacity: 0;\"><ins class=\"iCheck-helper\" style=\"position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;\"></ins></div>\r\n                  Pin to dashboard\r\n                </label>\r\n              </div>\r\n              <div class=\"form-group\">\r\n                <label class=\"hover\">\r\n                  <div class=\"icheckbox_flat-green hover\" [ngClass]=\"{'checked': is_active}\" aria-checked=\"true\" aria-disabled=\"false\" style=\"position: relative;\"><input [checked]=\"is_active\" (change)=\"is_active = !is_active\" [value]='is_active' type=\"checkbox\" class=\"flat-red\" checked=\"\" style=\"position: absolute; opacity: 0;\"><ins class=\"iCheck-helper\" style=\"position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;\"></ins></div>\r\n                  Active\r\n                </label>\r\n              </div>\r\n            </div>\r\n            <!-- /.box-body -->\r\n\r\n            <div class=\"box-footer\">\r\n              <button type=\"submit\" class=\"btn btn-primary test\">Save</button>\r\n            </div>\r\n          </form>\r\n        </div>\r\n        <!-- /.box -->\r\n\r\n      </div>\r\n      <!--/.col (left) -->\r\n    </div>\r\n    <!-- /.row -->\r\n    </section>\r\n  <!-- /.content -->\r\n"

/***/ }),

/***/ "./src/app/events/create-event/create-event.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/events/create-event/create-event.component.ts ***!
  \***************************************************************/
/*! exports provided: CreateEventComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateEventComponent", function() { return CreateEventComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _events_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../events.service */ "./src/app/events/events.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CreateEventComponent = /** @class */ (function () {
    function CreateEventComponent(fb, router, route, eventsService) {
        this.fb = fb;
        this.router = router;
        this.route = route;
        this.eventsService = eventsService;
        this.ctrl = this;
        this.editorConfig = {
            editable: true,
            spellcheck: true,
            height: '25rem',
            minHeight: '5rem',
            placeholder: 'Enter text here...',
            translate: 'no',
            uploadUrl: 'v1/images',
        };
        this.pinned = false;
        this.is_active = false;
        this.loading = false;
        this.segments = [
            { color: 'Red', code: '#ff5a61' },
            { color: 'Blue', code: '#04adc7' },
            { color: 'Orange', code: '#fea601' },
            { color: 'Purple', code: '#8b199b' },
        ];
        this.selectedFiles = [];
        this.formData = new FormData();
        this.urls = [];
    }
    CreateEventComponent.prototype.ngOnInit = function () {
        $(function () {
            // Date picker
            $('#datepicker').datepicker({
                autoclose: true
            });
        });
        this.buildForm();
    };
    Object.defineProperty(CreateEventComponent.prototype, "title", {
        get: function () {
            return this.form.get('title');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreateEventComponent.prototype, "description", {
        get: function () {
            return this.form.get('description');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreateEventComponent.prototype, "location", {
        get: function () {
            return this.form.get('location');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreateEventComponent.prototype, "dueDate", {
        get: function () {
            return this.form.get('dueDate');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreateEventComponent.prototype, "tags", {
        get: function () {
            return this.form.get('tags');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreateEventComponent.prototype, "segment", {
        get: function () {
            return this.form.get('segment');
        },
        enumerable: true,
        configurable: true
    });
    CreateEventComponent.prototype.onFileChanged = function (event) {
        var _this = this;
        this.loading = true;
        this.selectedFiles = event.target.files;
        // this.formData.append('images', this.selectedFiles, this.selectedFiles.name);
        if (this.selectedFiles.length > 0) {
            var _loop_1 = function (i) {
                var reader = new FileReader();
                reader.onload = function (event) {
                    _this.formData.append('images', _this.selectedFiles[i]);
                    _this.urls.push(event.target.result);
                };
                reader.readAsDataURL(this_1.selectedFiles[i]);
            };
            var this_1 = this;
            for (var i = 0; i < this.selectedFiles.length; i++) {
                _loop_1(i);
            }
        }
    };
    CreateEventComponent.prototype.create = function () {
        var _this = this;
        this.loading = true;
        $('.content').find('*').attr('disabled', 'disabled');
        $('.content').find('a').click(function (e) { e.preventDefault(); });
        var data = {
            title: this.form.value.title,
            description: this.form.value.description,
            tags: this.form.value.tags,
            segment: this.form.value.segment,
            location: this.form.value.location,
            dueDate: this.form.value.dueDate,
            pinned: this.pinned,
            is_active: this.is_active
        };
        this.formData.append('data', JSON.stringify(data));
        this.eventsService
            .create(this.formData)
            .subscribe(function (result) {
            _this.loading = false;
            var returnUrl = _this.route.snapshot.queryParamMap.get('returnUrl');
            _this.router.navigate(['events']);
            // setTimeout(() => {
            //   this.successMsg = 'News Created Successfully';
            // }, 2000);
        }, function (error) {
            _this.loading = false;
            // setTimeout(() => {
            //   this.errorMsg = error;
            // }, 2000);
        });
    };
    CreateEventComponent.prototype.buildForm = function () {
        this.form = this.fb.group({
            title: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            description: [''],
            segment: [this.segments],
            tags: [''],
            location: [''],
            dueDate: [''],
        });
    };
    CreateEventComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-create-event',
            template: __webpack_require__(/*! ./create-event.component.html */ "./src/app/events/create-event/create-event.component.html"),
            styles: [__webpack_require__(/*! ./create-event.component.css */ "./src/app/events/create-event/create-event.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _events_service__WEBPACK_IMPORTED_MODULE_3__["EventsService"]])
    ], CreateEventComponent);
    return CreateEventComponent;
}());



/***/ }),

/***/ "./src/app/events/edit-event/edit-event.component.css":
/*!************************************************************!*\
  !*** ./src/app/events/edit-event/edit-event.component.css ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2V2ZW50cy9lZGl0LWV2ZW50L2VkaXQtZXZlbnQuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/events/edit-event/edit-event.component.html":
/*!*************************************************************!*\
  !*** ./src/app/events/edit-event/edit-event.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<!-- Content Header (Page header) -->\r\n  <section class=\"content-header\">\r\n    <h1>\r\n      Events <button routerLink=\"/events/{{this.eventId}}/applications\" type=\"submit\" class=\"btn btn-primary pull-right\">Add Question</button>\r\n    </h1>\r\n    <ol class=\"breadcrumb\">\r\n      <li><a routerLink=\"/dashboard\"><i class=\"fa fa-dashboard\"></i> Home</a></li>\r\n      <li><a routerLink=\"/events\">Events</a></li>\r\n      <li class=\"active\">Edit</li>\r\n    </ol>\r\n  </section>\r\n\r\n  <!-- Main content -->\r\n  <div class=\"loader\" [hidden]=\"!loading\"></div>\r\n  <section class=\"content\">\r\n    <div class=\"row\">\r\n      <!-- left column -->\r\n      <div class=\"col-md-12\">\r\n        <!-- general form elements -->\r\n        <div class=\"box box-primary\">\r\n          <div class=\"box-header with-border\">\r\n            <h3 class=\"box-title\">Edit Event</h3>\r\n          </div>\r\n          <!-- /.box-header -->\r\n          <!-- form start -->\r\n          <form id=\"form-events\" class=\"p-t-15\" role=\"form\" [formGroup]=\"form\" (ngSubmit)=\"update()\">\r\n            <div class=\"box-body\">\r\n              <div class=\"form-group\">\r\n                <label for=\"exampleInputEmail1\">Title</label>\r\n                <input class=\"form-control\" formControlName=\"title\" id=\"exampleInputEmail1\" placeholder=\"Enter Title\">\r\n              </div>\r\n              <div class=\"form-group\">\r\n                <label for=\"exampleInputEmail1\">Tags</label>\r\n                <input formControlName=\"tags\" class=\"form-control\" placeholder=\"Enter Tags separated comma\">\r\n              </div>\r\n              <div class=\"form-group\">\r\n                <label>Description</label>\r\n                <angular-editor formControlName=\"description\" [config]=\"editorConfig\"></angular-editor>\r\n              </div>\r\n              <div class=\"form-group\">\r\n                <label>Location</label>\r\n                <input formControlName=\"location\" class=\"form-control\" placeholder=\"Enter Location\">\r\n              </div>\r\n              <div class=\"form-group\">\r\n                <label>Date</label>\r\n                <div class=\"input-group date\">\r\n                  <div class=\"input-group-addon\">\r\n                    <i class=\"fa fa-calendar\"></i>\r\n                  </div>\r\n                  <input formControlName=\"dueDate\" type=\"text\" class=\"form-control pull-right\" id=\"datepicker\">\r\n                </div>\r\n              </div>\r\n              <label for=\"exampleInputFile\">Upload Image</label>\r\n              <div class=\"form-group\">\r\n                <img *ngFor=\"let url of urls\" [src]=\"url\" height=\"200\"><br/>\r\n                <input type=\"file\" (change)=\"onFileChanged($event)\" id=\"exampleInputFile\" multiple>\r\n              </div>\r\n              <div class=\"form-group\">\r\n                <label>Segment</label>\r\n                <select formControlName=\"segment\" class=\"form-control\">\r\n                  <option [value]=\"segment.code\" *ngFor=\"let segment of segments\">\r\n                    {{segment.color}}</option>\r\n                </select>\r\n              </div>\r\n              <div class=\"form-group\">\r\n                <label class=\"\">\r\n                  <div class=\"icheckbox_flat-green\" [ngClass]=\"{'checked': pinned}\" aria-checked=\"false\" aria-disabled=\"false\" style=\"position: relative;\"><input [checked]=\"pinned\" (change)=\"pinned = !pinned\" formControlName=\"pinned\" type=\"checkbox\" class=\"flat-red\" checked=\"\" style=\"position: absolute; opacity: 0;\"><ins class=\"iCheck-helper\" style=\"position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;\"></ins></div>\r\n                  Pin to dashboard\r\n                </label>\r\n              </div>\r\n              <div class=\"form-group\">\r\n                <label class=\"\">\r\n                  <div class=\"icheckbox_flat-green\" [ngClass]=\"{'checked': is_active}\" aria-checked=\"false\" aria-disabled=\"false\" style=\"position: relative;\"><input [checked]=\"is_active\" (change)=\"is_active = !is_active\" formControlName=\"is_active\" type=\"checkbox\" class=\"flat-red\" checked=\"\" style=\"position: absolute; opacity: 0;\"><ins class=\"iCheck-helper\" style=\"position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;\"></ins></div>\r\n                  Active\r\n                </label>\r\n              </div>\r\n            </div>\r\n            <!-- /.box-body -->\r\n\r\n            <div class=\"box-footer\">\r\n              <button type=\"submit\" class=\"btn btn-primary\">Save</button>\r\n            </div>\r\n          </form>\r\n        </div>\r\n        <!-- /.box -->\r\n\r\n      </div>\r\n      <!--/.col (left) -->\r\n    </div>\r\n    <!-- /.row -->\r\n  </section>\r\n  <!-- /.content -->\r\n\r\n<!-- Extra content -->\r\n<section class=\"content\">\r\n  <div class=\"row\">\r\n    <!-- left column -->\r\n    <div class=\"col-md-12\">\r\n      <!-- general form elements -->\r\n      <div class=\"box box-primary\">\r\n        <form id=\"form-extra-sections-events\" class=\"p-t-15\" role=\"form\" [formGroup]=\"form\" (ngSubmit)=\"updateSections()\">\r\n          <div class=\"box-header with-border\">\r\n            <h3 class=\"box-title\">Extra Program Sections</h3>\r\n            <div class=\"form-group pull-right\">\r\n              <select formControlName=\"section\" class=\"form-control\" (change)=\"onSelectType(section)\">\r\n                <option [value]=\"section\" *ngFor=\"let section of sections\">\r\n                  {{section}}</option>\r\n              </select>\r\n            </div>\r\n          </div>\r\n          <!-- /.box-header -->\r\n          <!-- form start -->\r\n          <div class=\"box-body\">\r\n            <div class=\"form-group\" id=\"description\" hidden=\"hidden\">\r\n              <label>Description</label>\r\n              <input formControlName=\"extraDescription\" class=\"form-control\" placeholder=\"Enter Title\" multiple>\r\n              <!--<angular-editor formControlName=\"extraDescription\" [config]=\"editorConfig\"></angular-editor>-->\r\n            </div>\r\n            <div class=\"form-group\" id=\"fileupload\" hidden=\"hidden\">\r\n              <label for=\"exampleInputFile\">Upload Image</label>\r\n              <img *ngFor=\"let url of urls\" [src]=\"url\" height=\"200\"><br/>\r\n              <input type=\"file\" (change)=\"onFileChanged($event)\" multiple>\r\n            </div>\r\n          </div>\r\n          <!-- /.box-body -->\r\n\r\n          <div class=\"box-footer\">\r\n            <button type=\"submit\" id=\"saveButton\" class=\"btn btn-primary\" disabled=\"disabled\">Save</button>\r\n            <button routerLink=\"/events\" class=\"btn btn-primary pull-right\">Back to List</button>\r\n          </div>\r\n        </form>\r\n      </div>\r\n      <!-- /.box -->\r\n\r\n    </div>\r\n    <!--/.col (left) -->\r\n  </div>\r\n  <!-- /.row -->\r\n</section>\r\n<!-- /.content -->\r\n"

/***/ }),

/***/ "./src/app/events/edit-event/edit-event.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/events/edit-event/edit-event.component.ts ***!
  \***********************************************************/
/*! exports provided: EditEventComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditEventComponent", function() { return EditEventComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _events_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../events.service */ "./src/app/events/events.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var EditEventComponent = /** @class */ (function () {
    function EditEventComponent(fb, router, route, eventsService) {
        this.fb = fb;
        this.router = router;
        this.route = route;
        this.eventsService = eventsService;
        this.eventId = this.route.snapshot.params.id;
        this.editorConfig = {
            editable: true,
            spellcheck: true,
            height: '25rem',
            minHeight: '5rem',
            placeholder: 'Enter text here...',
            translate: 'no',
            uploadUrl: "/app/images/uploads",
        };
        this.pinned = true;
        this.title = '';
        this.description = '';
        this.tags = '';
        this.segment = '';
        this.segments = [
            { color: 'Red', code: '#ff5a61' },
            { color: 'Blue', code: '#04adc7' },
            { color: 'Orange', code: '#fea601' },
            { color: 'Purple', code: '#8b199b' },
        ];
        this.sections = ['Text', 'File'];
        this.location = '';
        this.is_active = false;
        this.loading = false;
        this.selectedFiles = [];
        this.formData = new FormData();
        this.urls = [];
    }
    EditEventComponent.prototype.ngOnInit = function () {
        $(function () {
            // Date picker
            $('#datepicker').datepicker({
                autoclose: true
            });
        });
        this.buildForm();
        this.getOne(this.eventId);
    };
    Object.defineProperty(EditEventComponent.prototype, "section", {
        get: function () {
            return this.form.get('section');
        },
        enumerable: true,
        configurable: true
    });
    EditEventComponent.prototype.onFileChanged = function (event) {
        var _this = this;
        this.loading = true;
        this.selectedFiles = event.target.files;
        if (this.selectedFiles.length > 0) {
            var _loop_1 = function (i) {
                var reader = new FileReader();
                reader.onload = function (event) {
                    _this.formData.append('images', _this.selectedFiles[i]);
                    _this.urls.push(event.target.result);
                };
                reader.readAsDataURL(this_1.selectedFiles[i]);
            };
            var this_1 = this;
            for (var i = 0; i < this.selectedFiles.length; i++) {
                _loop_1(i);
            }
        }
    };
    EditEventComponent.prototype.update = function () {
        var _this = this;
        $('.content').find('*').attr('disabled', 'disabled');
        $('.content').find('a').click(function (e) { e.preventDefault(); });
        var data = {
            title: this.form.value.title,
            description: this.form.value.description,
            tags: this.form.value.tags,
            segment: this.form.value.segment,
            location: this.form.value.location,
            dueDate: this.form.value.dueDate,
            pinned: this.pinned,
            is_active: this.is_active
        };
        this.formData.append('data', JSON.stringify(data));
        this.loading = true;
        this.eventsService
            .update(this.formData, this.eventId)
            .subscribe(function (result) {
            _this.loading = false;
            var returnUrl = _this.route.snapshot.queryParamMap.get('returnUrl');
            _this.router.navigate(['events']);
        }, function (error) {
            _this.loading = false;
        });
    };
    EditEventComponent.prototype.getOne = function (id) {
        var _this = this;
        this.loading = true;
        this.eventsService
            .getOne(id)
            .subscribe(function (result) {
            _this.loading = false;
            _this.title = result.title;
            _this.description = result.description;
            _this.tags = result.tags;
            _this.segment = result.segment;
            _this.location = result.location;
            _this.dueDate = new Date(result.dueDate);
            // this.urls = result.images;
            _this.pinned = result.pinned;
            _this.is_active = result.is_active;
            _this.buildForm();
        }, function (error) {
            _this.loading = false;
        });
    };
    EditEventComponent.prototype.updateSections = function () {
        var _this = this;
        var data = {
            extraSections: {
                // images: this.urls,
                description: this.form.value.extraDescription
            }
        };
        this.formData.append('data', JSON.stringify(data));
        this.loading = true;
        this.eventsService
            .update(this.formData, this.eventId)
            .subscribe(function (result) {
            _this.loading = false;
            var returnUrl = _this.route.snapshot.queryParamMap.get('returnUrl');
            _this.urls = [];
            _this.form.controls['extraDescription'].setValue('');
        }, function (error) {
            _this.loading = false;
        });
    };
    EditEventComponent.prototype.onSelectType = function (type) {
        console.log(type.target);
        var description = document.getElementById('description');
        var file = document.getElementById('fileupload');
        var saveButton = document.getElementById('saveButton');
        if (type.value === 'Text') {
            description.removeAttribute('hidden');
            file.setAttribute('hidden', 'hidden');
        }
        else {
            file.removeAttribute('hidden');
            description.setAttribute('hidden', 'hidden');
        }
        saveButton.removeAttribute('disabled');
    };
    EditEventComponent.prototype.buildForm = function () {
        this.form = this.fb.group({
            title: [this.title, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            description: [this.description, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            tags: [this.tags],
            segment: [this.segment],
            section: [this.sections],
            location: [this.location, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            dueDate: [this.dueDate, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            is_active: [this.is_active],
            pinned: [this.pinned],
            extraDescription: ['']
        });
    };
    EditEventComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-edit-event',
            template: __webpack_require__(/*! ./edit-event.component.html */ "./src/app/events/edit-event/edit-event.component.html"),
            styles: [__webpack_require__(/*! ./edit-event.component.css */ "./src/app/events/edit-event/edit-event.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _events_service__WEBPACK_IMPORTED_MODULE_3__["EventsService"]])
    ], EditEventComponent);
    return EditEventComponent;
}());



/***/ }),

/***/ "./src/app/events/events.service.ts":
/*!******************************************!*\
  !*** ./src/app/events/events.service.ts ***!
  \******************************************/
/*! exports provided: EventsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EventsService", function() { return EventsService; });
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../constants */ "./src/app/constants.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var EventsService = /** @class */ (function () {
    function EventsService(http) {
        this.http = http;
        this.apiUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_0__["environment"].apiBaseUrl;
        this.userId = null;
        this._isAuthenticated = new rxjs__WEBPACK_IMPORTED_MODULE_4__["BehaviorSubject"](false);
    }
    EventsService.prototype.create = function (data) {
        return this.http.post(this.apiUrl + "/events", data, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    EventsService.prototype.update = function (data, id) {
        return this.http.put(this.apiUrl + "/events/" + id, data, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    EventsService.prototype.getOne = function (id) {
        return this.http.get(this.apiUrl + "/events/" + id, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    EventsService.prototype.getAll = function (query) {
        return this.http.get(this.apiUrl + "/events?" + query, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    EventsService.prototype.delete = function (id) {
        return this.http.delete(this.apiUrl + "/events/" + id, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    EventsService.prototype.addQuestion = function (id, data) {
        return this.http.put(this.apiUrl + "/events/" + id + "/questions", data, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    EventsService.prototype.getQuestions = function (id) {
        return this.http.get(this.apiUrl + "/events/" + id + "/questions", { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    EventsService.prototype.deleteQuestion = function (id, questionId) {
        return this.http.delete(this.apiUrl + "/events/" + id + "/questions/" + questionId, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    EventsService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], EventsService);
    return EventsService;
}());



/***/ }),

/***/ "./src/app/events/list-events/list-events.component.css":
/*!**************************************************************!*\
  !*** ./src/app/events/list-events/list-events.component.css ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2V2ZW50cy9saXN0LWV2ZW50cy9saXN0LWV2ZW50cy5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/events/list-events/list-events.component.html":
/*!***************************************************************!*\
  !*** ./src/app/events/list-events/list-events.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<style>\r\n  .btn {\r\n    background-color: DodgerBlue; /* Blue background */\r\n    border: none; /* Remove borders */\r\n    color: white; /* White text */\r\n    font-size: 10px; /* Set a font size */\r\n    cursor: pointer; /* Mouse pointer on hover */\r\n  }\r\n\r\n  /* Darker background on mouse-over */\r\n  .btn:hover {\r\n    background-color: RoyalBlue;\r\n  }\r\n  .loader {\r\n    position: absolute;\r\n    left: 50%;\r\n    top: 50%;\r\n    height:60px;\r\n    width:60px;\r\n    margin:0px auto;\r\n    -webkit-animation: rotation .6s infinite linear;\r\n    -moz-animation: rotation .6s infinite linear;\r\n    -o-animation: rotation .6s infinite linear;\r\n    animation: rotation .6s infinite linear;\r\n    border-left:6px solid rgba(0,174,239,.15);\r\n    border-right:6px solid rgba(0,174,239,.15);\r\n    border-bottom:6px solid rgba(0,174,239,.15);\r\n    border-top:6px solid rgba(0,174,239,.8);\r\n    border-radius:100%;\r\n  }\r\n\r\n  @-webkit-keyframes rotation {\r\n    from {-webkit-transform: rotate(0deg);}\r\n    to {-webkit-transform: rotate(359deg);}\r\n  }\r\n  @-moz-keyframes rotation {\r\n    from {-moz-transform: rotate(0deg);}\r\n    to {-moz-transform: rotate(359deg);}\r\n  }\r\n  @-o-keyframes rotation {\r\n    from {-o-transform: rotate(0deg);}\r\n    to {-o-transform: rotate(359deg);}\r\n  }\r\n  @keyframes rotation {\r\n    from {transform: rotate(0deg);}\r\n    to {transform: rotate(359deg);}\r\n  }\r\n</style>\r\n<!-- Content Header (Page header) -->\r\n  <section class=\"content-header\">\r\n    <h1>\r\n      Events <button routerLink=\"/events/create-event\" type=\"submit\" class=\"btn btn-primary pull-right\">New</button>\r\n    </h1>\r\n    <ol class=\"breadcrumb\">\r\n      <li><a routerLink=\"/dashboard\"><i class=\"fa fa-dashboard\"></i> Home</a></li>\r\n      <li><a routerLink=\"/events\">Events</a></li>\r\n    </ol>\r\n  </section>\r\n  <!-- Main content -->\r\n<div class=\"loader\" [hidden]=\"!loading\"></div>\r\n\r\n  <section class=\"content\">\r\n    <!-- /.row -->\r\n    <div class=\"row\">\r\n      <div class=\"col-xs-12\">\r\n        <div class=\"box\">\r\n          <div class=\"box-header\">\r\n            <h3 class=\"box-title\"></h3>\r\n\r\n            <div class=\"box-tools\">\r\n              <div class=\"input-group input-group-sm\" style=\"width: 150px;\">\r\n                <input (ngModelChange)=\"filter($event)\" [(ngModel)]=\"nameFilter\"  type=\"text\" name=\"table_search\" class=\"form-control pull-right\" placeholder=\"Search\">\r\n\r\n                <div class=\"input-group-btn\">\r\n                  <button type=\"submit\" class=\"btn btn-default\"><i class=\"fa fa-search\"></i></button>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <!-- /.box-header -->\r\n          <div class=\"box-body table-responsive no-padding\">\r\n            <table class=\"table table-hover\">\r\n              <thead>\r\n              <tr>\r\n                <th>Title</th>\r\n                <th>Description</th>\r\n                <th>Location</th>\r\n                <th>Status</th>\r\n                <th>Pinned</th>\r\n                <th>Interactions</th>\r\n                <th>Edit/Delete</th>\r\n              </tr>\r\n              </thead>\r\n              <tbody>\r\n              <tr *ngFor=\"let event of events\" class=\"gradeX odd content-table-payment cursor\" role=\"row\">\r\n                <td class=\"mn-130 sorting_1\">{{event.title}}</td>\r\n                <td class=\"mn-130 sorting_1\">{{event.description.substring(1,15)}}.....</td>\r\n                <td class=\"mn-130 sorting_1\">{{event.location}}</td>\r\n                <td class=\"mn-120 sorting_1\">\r\n                  <span *ngIf=\"event.is_active === true\"\r\n                        class=\"label label-success upper-case\">Active</span>\r\n                  <span *ngIf=\"event.is_active === false\"\r\n                        class=\"label label-danger upper-case\">Active</span>\r\n                </td>\r\n                <td class=\"mn-120 sorting_1\">\r\n                  <span *ngIf=\"event.pinned === true\"\r\n                        class=\"label label-success upper-case\">Pinned</span>\r\n                  <span *ngIf=\"event.pinned === false\"\r\n                        class=\"label label-danger upper-case\">Pinned</span>\r\n                </td>\r\n                <td></td>\r\n                <td>\r\n                  <button class=\"btn\" routerLink='/events/{{event._id}}/edit-event'><i class=\"fa fa-edit\"></i></button>\r\n                  <button class=\"btn\" (click)=\"delete(event._id)\"><i class=\"fa fa-trash\"></i></button>\r\n                </td>\r\n              </tr>\r\n              </tbody></table>\r\n          </div>\r\n          <!-- /.box-body -->\r\n        </div>\r\n        <!-- /.box -->\r\n      </div>\r\n    </div>\r\n    </section>\r\n  <!-- /.content -->\r\n"

/***/ }),

/***/ "./src/app/events/list-events/list-events.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/events/list-events/list-events.component.ts ***!
  \*************************************************************/
/*! exports provided: ListEventsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListEventsComponent", function() { return ListEventsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _events_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../events.service */ "./src/app/events/events.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_3__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ListEventsComponent = /** @class */ (function () {
    function ListEventsComponent(router, route, eventService) {
        this.router = router;
        this.route = route;
        this.eventService = eventService;
        this.ctrl = this;
        this.loading = false;
        this.events = [];
        this.nameFilter = '';
    }
    ListEventsComponent.prototype.ngOnInit = function () {
        this.getAll('');
    };
    ListEventsComponent.prototype.filter = function (value) {
        if (value !== '') {
            this.nameFilter = "title=" + value;
        }
        else {
            this.nameFilter = '';
        }
        this.getAll(this.nameFilter);
    };
    ListEventsComponent.prototype.getAll = function (query) {
        var _this = this;
        this.loading = true;
        $('.content').find('*').attr('disabled', 'disabled');
        $('.content').find('a').click(function (e) { e.preventDefault(); });
        this.eventService
            .getAll(query)
            .subscribe(function (result) {
            _this.loading = false;
            _this.events = result.events;
            $('.content').find('*').removeAttr('disabled');
            $('.content').find('a').unbind('click');
            // const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
            // this.router.navigate(['news']);
            // setTimeout(() => {
            //   this.successMsg = 'News Created Successfully';
            // }, 2000);
        }, function (error) {
            _this.loading = false;
            $('.content').find('*').removeAttr('disabled');
            $('.content').find('a').unbind('click');
            // setTimeout(() => {
            //   this.errorMsg = error;
            // }, 2000);
        });
    };
    ListEventsComponent.prototype.delete = function (id) {
        var _this = this;
        this.loading = true;
        sweetalert2__WEBPACK_IMPORTED_MODULE_3___default()({
            type: 'warning',
            title: 'Are you sure to Delete Event?',
            text: 'You will not be able to recover the data of Event',
            showCancelButton: true,
            confirmButtonColor: '#049F0C',
            cancelButtonColor: '#ff0000',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, keep it'
        }).then(function (result) {
            if (result.value) {
                _this.eventService
                    .delete(id)
                    .subscribe(function (result) {
                    _this.loading = false;
                    _this.getAll('');
                    // const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
                    // this.router.navigate(['news']);
                    // setTimeout(() => {
                    //   this.successMsg = 'News Created Successfully';
                    // }, 2000);
                }, function (error) {
                    _this.loading = false;
                    _this.getAll('');
                    // setTimeout(() => {
                    //   this.errorMsg = error;
                    // }, 2000);
                });
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_3___default()({
                    type: 'info',
                    title: 'Cancelled',
                    text: 'Your Event file is safe :)'
                });
            }
        });
    };
    ListEventsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-list-events',
            template: __webpack_require__(/*! ./list-events.component.html */ "./src/app/events/list-events/list-events.component.html"),
            styles: [__webpack_require__(/*! ./list-events.component.css */ "./src/app/events/list-events/list-events.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _events_service__WEBPACK_IMPORTED_MODULE_2__["EventsService"]])
    ], ListEventsComponent);
    return ListEventsComponent;
}());



/***/ }),

/***/ "./src/app/jobs/create-job/create-job.component.css":
/*!**********************************************************!*\
  !*** ./src/app/jobs/create-job/create-job.component.css ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2pvYnMvY3JlYXRlLWpvYi9jcmVhdGUtam9iLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/jobs/create-job/create-job.component.html":
/*!***********************************************************!*\
  !*** ./src/app/jobs/create-job/create-job.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<!-- Content Header (Page header) -->\r\n<section class=\"content-header\">\r\n  <h1>\r\n    Jobs\r\n  </h1>\r\n  <ol class=\"breadcrumb\">\r\n    <li><a routerLink=\"/dashboard\"><i class=\"fa fa-dashboard\"></i> Home</a></li>\r\n    <li><a routerLink=\"/jobs\">Jobs</a></li>\r\n    <li class=\"active\">Create</li>\r\n  </ol>\r\n</section>\r\n\r\n<!-- Main content -->\r\n<div class=\"loader\" [hidden]=\"!loading\"></div>\r\n<section class=\"content\">\r\n  <div class=\"row\">\r\n    <!-- left column -->\r\n    <div class=\"col-md-12\">\r\n      <!-- general form elements -->\r\n      <div class=\"box box-primary\">\r\n        <div class=\"box-header with-border\">\r\n          <h3 class=\"box-title\">Create New Job</h3>\r\n        </div>\r\n        <!-- /.box-header -->\r\n        <!-- form start -->\r\n        <form id=\"form-jobs\" class=\"p-t-15\" role=\"form\" [formGroup]=\"form\" (ngSubmit)=\"create()\">\r\n          <div class=\"box-body\">\r\n            <div class=\"form-group\">\r\n              <label for=\"exampleInputEmail1\">Title</label>\r\n              <input formControlName=\"title\" class=\"form-control\" id=\"exampleInputEmail1\" placeholder=\"Enter Title\">\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label>Description</label>\r\n              <angular-editor formControlName=\"description\" [config]=\"editorConfig\"></angular-editor>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label>Validation Deadline</label>\r\n              <div class=\"input-group date\">\r\n                <div class=\"input-group-addon\">\r\n                  <i class=\"fa fa-calendar\"></i>\r\n                </div>\r\n                <input formControlName=\"deadline\" type=\"text\" class=\"form-control pull-right\" id=\"datepicker\">\r\n              </div>\r\n            </div>\r\n            <!--<div class=\"form-group\">-->\r\n              <!--<label for=\"exampleInputEmail1\">Validation Deadline</label>-->\r\n              <!--<input formControlName=\"deadline\" class=\"form-control\" placeholder=\"DeadLine Date\">-->\r\n            <!--</div>-->\r\n            <div class=\"form-group\">\r\n              <label>Segment</label>\r\n              <select formControlName=\"segment\" class=\"form-control\">\r\n                <option [value]=\"segment.code\" *ngFor=\"let segment of segments\" [selected]=\"segment\">\r\n                  {{segment.color}}</option>\r\n              </select>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label>Category</label>\r\n              <select formControlName=\"category\" class=\"form-control\">\r\n                <option [value]=\"category\" *ngFor=\"let category of categories\" [selected]=\"category\">\r\n                  {{category}}</option>\r\n              </select>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label for=\"exampleInputEmail1\">Salary</label>\r\n              <input formControlName=\"salary\" class=\"form-control\" placeholder=\"Enter Salary\">\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label class=\"hover\">\r\n                <div class=\"icheckbox_flat-green hover\" [ngClass]=\"{'checked': pinned}\" aria-checked=\"true\" aria-disabled=\"false\" style=\"position: relative;\"><input [checked]=\"pinned\" (change)=\"pinned = !pinned\" [value]='pinned' type=\"checkbox\" class=\"flat-red\" checked=\"\" style=\"position: absolute; opacity: 0;\"><ins class=\"iCheck-helper\" style=\"position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;\"></ins></div>\r\n                Pin to dashboard\r\n              </label>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label class=\"hover\">\r\n                <div class=\"icheckbox_flat-green hover\" [ngClass]=\"{'checked': is_active}\" aria-checked=\"true\" aria-disabled=\"false\" style=\"position: relative;\"><input [checked]=\"is_active\" (change)=\"is_active = !is_active\" [value]='is_active' type=\"checkbox\" class=\"flat-red\" checked=\"\" style=\"position: absolute; opacity: 0;\"><ins class=\"iCheck-helper\" style=\"position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;\"></ins></div>\r\n                Active\r\n              </label>\r\n            </div>\r\n          </div>\r\n          <!-- /.box-body -->\r\n\r\n          <div class=\"box-footer\">\r\n            <button type=\"submit\" class=\"btn btn-primary\">Save</button>\r\n          </div>\r\n        </form>\r\n      </div>\r\n      <!-- /.box -->\r\n\r\n    </div>\r\n    <!--/.col (left) -->\r\n  </div>\r\n  <!-- /.row -->\r\n</section>\r\n<!-- /.content -->\r\n<script>\r\n  $(function () {\r\n    //Initialize Select2 Elements\r\n    $('.select2').select2()\r\n\r\n    //Datemask dd/mm/yyyy\r\n    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })\r\n    //Datemask2 mm/dd/yyyy\r\n    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })\r\n    //Money Euro\r\n    $('[data-mask]').inputmask()\r\n\r\n    //Date range picker\r\n    $('#reservation').daterangepicker()\r\n    //Date range picker with time picker\r\n    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })\r\n    //Date range as a button\r\n    $('#daterange-btn').daterangepicker(\r\n      {\r\n        ranges   : {\r\n          'Today'       : [moment(), moment()],\r\n          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],\r\n          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],\r\n          'Last 30 Days': [moment().subtract(29, 'days'), moment()],\r\n          'This Month'  : [moment().startOf('month'), moment().endOf('month')],\r\n          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]\r\n        },\r\n        startDate: moment().subtract(29, 'days'),\r\n        endDate  : moment()\r\n      },\r\n      function (start, end) {\r\n        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))\r\n      }\r\n    )\r\n\r\n    //Date picker\r\n    $('#datepicker').datepicker({\r\n      autoclose: true\r\n    })\r\n\r\n    //iCheck for checkbox and radio inputs\r\n    $('input[type=\"checkbox\"].minimal, input[type=\"radio\"].minimal').iCheck({\r\n      checkboxClass: 'icheckbox_minimal-blue',\r\n      radioClass   : 'iradio_minimal-blue'\r\n    })\r\n    //Red color scheme for iCheck\r\n    $('input[type=\"checkbox\"].minimal-red, input[type=\"radio\"].minimal-red').iCheck({\r\n      checkboxClass: 'icheckbox_minimal-red',\r\n      radioClass   : 'iradio_minimal-red'\r\n    })\r\n    //Flat red color scheme for iCheck\r\n    $('input[type=\"checkbox\"].flat-red, input[type=\"radio\"].flat-red').iCheck({\r\n      checkboxClass: 'icheckbox_flat-green',\r\n      radioClass   : 'iradio_flat-green'\r\n    })\r\n\r\n    //Colorpicker\r\n    $('.my-colorpicker1').colorpicker()\r\n    //color picker with addon\r\n    $('.my-colorpicker2').colorpicker()\r\n\r\n    //Timepicker\r\n    $('.timepicker').timepicker({\r\n      showInputs: false\r\n    })\r\n  })\r\n</script>\r\n"

/***/ }),

/***/ "./src/app/jobs/create-job/create-job.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/jobs/create-job/create-job.component.ts ***!
  \*********************************************************/
/*! exports provided: CreateJobComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateJobComponent", function() { return CreateJobComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _jobs_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../jobs.service */ "./src/app/jobs/jobs.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CreateJobComponent = /** @class */ (function () {
    function CreateJobComponent(fb, router, route, jobService) {
        this.fb = fb;
        this.router = router;
        this.route = route;
        this.jobService = jobService;
        this.ctrl = this;
        this.editorConfig = {
            editable: true,
            spellcheck: true,
            height: '25rem',
            minHeight: '5rem',
            placeholder: 'Enter text here...',
            translate: 'no',
            uploadUrl: 'v1/images',
        };
        this.pinned = true;
        this.is_active = false;
        this.loading = false;
        this.categories = ['Full-Time', 'Part-Time', 'Internship'];
        this.segments = [
            { color: 'Red', code: '#ff5a61' },
            { color: 'Blue', code: '#04adc7' },
            { color: 'Orange', code: '#fea601' },
            { color: 'Purple', code: '#8b199b' },
        ];
    }
    CreateJobComponent.prototype.ngOnInit = function () {
        $(function () {
            // Date picker
            $('#datepicker').datepicker({
                autoclose: true
            });
        });
        this.buildForm();
    };
    Object.defineProperty(CreateJobComponent.prototype, "title", {
        get: function () {
            return this.form.get('title');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreateJobComponent.prototype, "description", {
        get: function () {
            return this.form.get('description');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreateJobComponent.prototype, "salary", {
        get: function () {
            return this.form.get('salary');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreateJobComponent.prototype, "category", {
        get: function () {
            return this.form.get('category');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreateJobComponent.prototype, "deadline", {
        get: function () {
            return this.form.get('deadline');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreateJobComponent.prototype, "segment", {
        get: function () {
            return this.form.get('segment');
        },
        enumerable: true,
        configurable: true
    });
    CreateJobComponent.prototype.create = function () {
        var _this = this;
        this.loading = true;
        $('.content').find('*').attr('disabled', 'disabled');
        $('.content').find('a').click(function (e) { e.preventDefault(); });
        this.jobService
            .create({
            title: this.form.value.title,
            description: this.form.value.description,
            salary: Number(this.form.value.salary),
            segment: this.form.value.segment,
            category: this.form.value.category,
            deadline: this.form.value.deadline,
            pinned: this.pinned,
            is_active: this.is_active
        })
            .subscribe(function (result) {
            _this.loading = false;
            var returnUrl = _this.route.snapshot.queryParamMap.get('returnUrl');
            _this.router.navigate(['jobs']);
        }, function (error) {
            _this.loading = false;
        });
    };
    CreateJobComponent.prototype.buildForm = function () {
        this.form = this.fb.group({
            title: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            description: [''],
            segment: [this.segments],
            salary: [''],
            deadline: [''],
            category: [this.categories],
        });
    };
    CreateJobComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-create-job',
            template: __webpack_require__(/*! ./create-job.component.html */ "./src/app/jobs/create-job/create-job.component.html"),
            styles: [__webpack_require__(/*! ./create-job.component.css */ "./src/app/jobs/create-job/create-job.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _jobs_service__WEBPACK_IMPORTED_MODULE_3__["JobsService"]])
    ], CreateJobComponent);
    return CreateJobComponent;
}());



/***/ }),

/***/ "./src/app/jobs/edit-job/edit-job.component.css":
/*!******************************************************!*\
  !*** ./src/app/jobs/edit-job/edit-job.component.css ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2pvYnMvZWRpdC1qb2IvZWRpdC1qb2IuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/jobs/edit-job/edit-job.component.html":
/*!*******************************************************!*\
  !*** ./src/app/jobs/edit-job/edit-job.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<!-- Content Header (Page header) -->\r\n<section class=\"content-header\">\r\n  <h1>\r\n    Jobs\r\n  </h1>\r\n  <ol class=\"breadcrumb\">\r\n    <li><a routerLink=\"/dashboard\"><i class=\"fa fa-dashboard\"></i> Home</a></li>\r\n    <li><a routerLink=\"/jobs\">Jobs</a></li>\r\n    <li class=\"active\">Edit</li>\r\n  </ol>\r\n</section>\r\n\r\n<!-- Main content -->\r\n<div class=\"loader\" [hidden]=\"!loading\"></div>\r\n\r\n<section class=\"content\">\r\n  <div class=\"row\">\r\n    <!-- left column -->\r\n    <div class=\"col-md-12\">\r\n      <!-- general form elements -->\r\n      <div class=\"box box-primary\">\r\n        <div class=\"box-header with-border\">\r\n          <h3 class=\"box-title\">Edit Job</h3>\r\n        </div>\r\n        <!-- /.box-header -->\r\n        <!-- form start -->\r\n        <form id=\"form-jobs\" class=\"p-t-15\" role=\"form\" [formGroup]=\"form\" (ngSubmit)=\"update()\">\r\n          <div class=\"box-body\">\r\n            <div class=\"form-group\">\r\n              <label for=\"exampleInputEmail1\">Title</label>\r\n              <input formControlName=\"title\" class=\"form-control\" id=\"exampleInputEmail1\" placeholder=\"Enter Title\">\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label>Description</label>\r\n              <angular-editor formControlName=\"description\" [config]=\"editorConfig\"></angular-editor>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label for=\"exampleInputEmail1\">Validation Deadline</label>\r\n              <input formControlName=\"deadline\" class=\"form-control\" placeholder=\"DeadLine Date\">\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label>Segment</label>\r\n              <select formControlName=\"segment\" class=\"form-control\">\r\n                <option [value]=\"segment.code\" *ngFor=\"let segment of segments\">\r\n                  {{segment.color}}</option>\r\n              </select>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label>Category</label>\r\n              <select formControlName=\"category\" class=\"form-control\">\r\n                <option [value]=\"category\" *ngFor=\"let category of categories\">\r\n                  {{category}}</option>\r\n              </select>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label for=\"exampleInputEmail1\">Salary</label>\r\n              <input formControlName=\"salary\" class=\"form-control\" placeholder=\"Enter Title\">\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label class=\"hover\">\r\n                <div class=\"icheckbox_flat-green hover\" [ngClass]=\"{'checked': pinned}\" aria-checked=\"true\" aria-disabled=\"false\" style=\"position: relative;\"><input [checked]=\"pinned\" (change)=\"pinned = !pinned\" [value]='pinned' type=\"checkbox\" class=\"flat-red\" checked=\"\" style=\"position: absolute; opacity: 0;\"><ins class=\"iCheck-helper\" style=\"position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;\"></ins></div>\r\n                Pin to dashboard\r\n              </label>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label class=\"hover\">\r\n                <div class=\"icheckbox_flat-green hover\" [ngClass]=\"{'checked': is_active}\" aria-checked=\"true\" aria-disabled=\"false\" style=\"position: relative;\"><input [checked]=\"is_active\" (change)=\"is_active = !is_active\" [value]='is_active' type=\"checkbox\" class=\"flat-red\" checked=\"\" style=\"position: absolute; opacity: 0;\"><ins class=\"iCheck-helper\" style=\"position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;\"></ins></div>\r\n                Active\r\n              </label>\r\n            </div>\r\n          </div>\r\n          <!-- /.box-body -->\r\n\r\n          <div class=\"box-footer\">\r\n            <button type=\"submit\" class=\"btn btn-primary\">Save</button>\r\n          </div>\r\n        </form>\r\n      </div>\r\n      <!-- /.box -->\r\n\r\n    </div>\r\n    <!--/.col (left) -->\r\n  </div>\r\n  <!-- /.row -->\r\n</section>\r\n<!-- /.content -->\r\n\r\n<!-- Extra content -->\r\n<section class=\"content\">\r\n  <div class=\"row\">\r\n    <!-- left column -->\r\n    <div class=\"col-md-12\">\r\n      <!-- general form elements -->\r\n      <div class=\"box box-primary\">\r\n        <form id=\"form-extra-sections-jobs\" class=\"p-t-15\" role=\"form\" [formGroup]=\"form\" (ngSubmit)=\"updateSections()\">\r\n          <div class=\"box-header with-border\">\r\n            <h3 class=\"box-title\">Extra Program Sections</h3>\r\n            <div class=\"form-group pull-right\">\r\n              <select formControlName=\"section\" class=\"form-control\" (change)=\"onSelectType(section)\">\r\n                <option [value]=\"section\" *ngFor=\"let section of sections\">\r\n                  {{section}}</option>\r\n              </select>\r\n            </div>\r\n          </div>\r\n          <!-- /.box-header -->\r\n          <!-- form start -->\r\n          <div class=\"box-body\">\r\n            <div class=\"form-group\" id=\"description\" hidden=\"hidden\">\r\n              <label>Description</label>\r\n              <input formControlName=\"extraDescription\" class=\"form-control\" placeholder=\"Enter Title\" multiple>\r\n              <!--<angular-editor formControlName=\"extraDescription\" [config]=\"editorConfig\"></angular-editor>-->\r\n            </div>\r\n            <div class=\"form-group\" id=\"fileupload\" hidden=\"hidden\">\r\n              <label for=\"exampleInputFile\">Upload Image</label>\r\n              <img *ngFor=\"let url of urls\" [src]=\"url\" height=\"200\"><br/>\r\n              <input type=\"file\" (change)=\"onFileChanged($event)\" multiple>\r\n            </div>\r\n          </div>\r\n          <!-- /.box-body -->\r\n\r\n          <div class=\"box-footer\">\r\n            <button type=\"submit\" id=\"saveButton\" class=\"btn btn-primary\" disabled=\"disabled\">Save</button>\r\n            <button routerLink=\"/jobs\" class=\"btn btn-primary pull-right\">Back to List</button>\r\n          </div>\r\n        </form>\r\n      </div>\r\n      <!-- /.box -->\r\n\r\n    </div>\r\n    <!--/.col (left) -->\r\n  </div>\r\n  <!-- /.row -->\r\n</section>\r\n<!-- /.content -->\r\n\r\n<script>\r\n  $(function () {\r\n    //Initialize Select2 Elements\r\n    $('.select2').select2()\r\n\r\n    //Datemask dd/mm/yyyy\r\n    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })\r\n    //Datemask2 mm/dd/yyyy\r\n    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })\r\n    //Money Euro\r\n    $('[data-mask]').inputmask()\r\n\r\n    //Date range picker\r\n    $('#reservation').daterangepicker()\r\n    //Date range picker with time picker\r\n    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })\r\n    //Date range as a button\r\n    $('#daterange-btn').daterangepicker(\r\n      {\r\n        ranges   : {\r\n          'Today'       : [moment(), moment()],\r\n          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],\r\n          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],\r\n          'Last 30 Days': [moment().subtract(29, 'days'), moment()],\r\n          'This Month'  : [moment().startOf('month'), moment().endOf('month')],\r\n          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]\r\n        },\r\n        startDate: moment().subtract(29, 'days'),\r\n        endDate  : moment()\r\n      },\r\n      function (start, end) {\r\n        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))\r\n      }\r\n    )\r\n\r\n    //Date picker\r\n    $('#datepicker').datepicker({\r\n      autoclose: true\r\n    })\r\n\r\n    //iCheck for checkbox and radio inputs\r\n    $('input[type=\"checkbox\"].minimal, input[type=\"radio\"].minimal').iCheck({\r\n      checkboxClass: 'icheckbox_minimal-blue',\r\n      radioClass   : 'iradio_minimal-blue'\r\n    })\r\n    //Red color scheme for iCheck\r\n    $('input[type=\"checkbox\"].minimal-red, input[type=\"radio\"].minimal-red').iCheck({\r\n      checkboxClass: 'icheckbox_minimal-red',\r\n      radioClass   : 'iradio_minimal-red'\r\n    })\r\n    //Flat red color scheme for iCheck\r\n    $('input[type=\"checkbox\"].flat-red, input[type=\"radio\"].flat-red').iCheck({\r\n      checkboxClass: 'icheckbox_flat-green',\r\n      radioClass   : 'iradio_flat-green'\r\n    })\r\n\r\n    //Colorpicker\r\n    $('.my-colorpicker1').colorpicker()\r\n    //color picker with addon\r\n    $('.my-colorpicker2').colorpicker()\r\n\r\n    //Timepicker\r\n    $('.timepicker').timepicker({\r\n      showInputs: false\r\n    })\r\n  })\r\n</script>\r\n"

/***/ }),

/***/ "./src/app/jobs/edit-job/edit-job.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/jobs/edit-job/edit-job.component.ts ***!
  \*****************************************************/
/*! exports provided: EditJobComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditJobComponent", function() { return EditJobComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _jobs_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../jobs.service */ "./src/app/jobs/jobs.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var EditJobComponent = /** @class */ (function () {
    function EditJobComponent(fb, router, route, jobService) {
        this.fb = fb;
        this.router = router;
        this.route = route;
        this.jobService = jobService;
        this.jobId = this.route.snapshot.params.id;
        this.editorConfig = {
            editable: true,
            spellcheck: true,
            height: '25rem',
            minHeight: '5rem',
            placeholder: 'Enter text here...',
            translate: 'no',
            uploadUrl: 'v1/images',
        };
        this.pinned = true;
        this.title = '';
        this.description = '';
        this.category = '';
        this.categories = ['Full-Time', 'Part-Time', 'Internship'];
        this.segment = '';
        this.segments = [
            { color: 'Red', code: '#ff5a61' },
            { color: 'Blue', code: '#04adc7' },
            { color: 'Orange', code: '#fea601' },
            { color: 'Purple', code: '#8b199b' },
        ];
        this.sections = ['Text', 'File'];
        this.salary = '';
        this.is_active = false;
        this.loading = false;
        this.selectedFiles = [];
        this.formData = new FormData();
        this.urls = [];
    }
    EditJobComponent.prototype.ngOnInit = function () {
        $(function () {
            // Date picker
            $('#datepicker').datepicker({
                autoclose: true
            });
        });
        this.buildForm();
        this.getOne(this.jobId);
    };
    Object.defineProperty(EditJobComponent.prototype, "section", {
        get: function () {
            return this.form.get('section');
        },
        enumerable: true,
        configurable: true
    });
    EditJobComponent.prototype.onFileChanged = function (event) {
        var _this = this;
        this.loading = true;
        this.selectedFiles = event.target.files;
        // this.formData.append('images', this.selectedFiles, this.selectedFiles.name);
        if (this.selectedFiles.length > 0) {
            var _loop_1 = function (i) {
                var reader = new FileReader();
                reader.onload = function (event) {
                    _this.formData.append('images', _this.selectedFiles[i]);
                    _this.urls.push(event.target.result);
                };
                reader.readAsDataURL(this_1.selectedFiles[i]);
            };
            var this_1 = this;
            for (var i = 0; i < this.selectedFiles.length; i++) {
                _loop_1(i);
            }
        }
    };
    EditJobComponent.prototype.update = function () {
        var _this = this;
        $('.content').find('*').attr('disabled', 'disabled');
        $('.content').find('a').click(function (e) { e.preventDefault(); });
        var data = {
            title: this.form.value.title,
            description: this.form.value.description,
            salary: this.form.value.salary,
            segment: this.form.value.segment,
            category: this.form.value.category,
            deadline: this.form.value.deadline,
            pinned: this.pinned,
            is_active: this.is_active
        };
        this.loading = true;
        this.jobService
            .update(data, this.jobId)
            .subscribe(function (result) {
            _this.loading = false;
            var returnUrl = _this.route.snapshot.queryParamMap.get('returnUrl');
            _this.router.navigate(['jobs']);
        }, function (error) {
            _this.loading = false;
        });
    };
    EditJobComponent.prototype.getOne = function (id) {
        var _this = this;
        this.loading = true;
        this.jobService
            .getOne(id)
            .subscribe(function (result) {
            _this.loading = false;
            _this.title = result.title;
            _this.description = result.description;
            _this.salary = result.salary;
            _this.segment = result.segment;
            _this.category = result.category;
            _this.deadline = result.deadline;
            // this.urls = result.images;
            _this.pinned = result.pinned;
            _this.is_active = result.is_active;
            _this.buildForm();
        }, function (error) {
            _this.loading = false;
        });
    };
    EditJobComponent.prototype.updateSections = function () {
        var _this = this;
        var data = {
            extraSections: {
                // images: this.urls,
                description: this.form.value.extraDescription
            }
        };
        this.formData.append('data', JSON.stringify(data));
        this.loading = true;
        this.jobService
            .update(this.formData, this.jobId)
            .subscribe(function (result) {
            _this.loading = false;
            var returnUrl = _this.route.snapshot.queryParamMap.get('returnUrl');
            _this.urls = [];
            _this.form.controls['extraDescription'].setValue('');
        }, function (error) {
            _this.loading = false;
        });
    };
    EditJobComponent.prototype.onSelectType = function (type) {
        console.log(type.target);
        var description = document.getElementById('description');
        var file = document.getElementById('fileupload');
        var saveButton = document.getElementById('saveButton');
        if (type.value === 'Text') {
            description.removeAttribute('hidden');
            file.setAttribute('hidden', 'hidden');
        }
        else {
            file.removeAttribute('hidden');
            description.setAttribute('hidden', 'hidden');
        }
        saveButton.removeAttribute('disabled');
    };
    EditJobComponent.prototype.buildForm = function () {
        this.form = this.fb.group({
            title: [this.title, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            description: [this.description, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            is_active: [this.is_active],
            pinned: [this.pinned],
            category: [this.category],
            segment: [this.segment],
            section: [this.sections],
            salary: [this.salary],
            deadline: [this.deadline],
            extraDescription: ['']
        });
    };
    EditJobComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-edit-job',
            template: __webpack_require__(/*! ./edit-job.component.html */ "./src/app/jobs/edit-job/edit-job.component.html"),
            styles: [__webpack_require__(/*! ./edit-job.component.css */ "./src/app/jobs/edit-job/edit-job.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _jobs_service__WEBPACK_IMPORTED_MODULE_3__["JobsService"]])
    ], EditJobComponent);
    return EditJobComponent;
}());



/***/ }),

/***/ "./src/app/jobs/jobs.service.ts":
/*!**************************************!*\
  !*** ./src/app/jobs/jobs.service.ts ***!
  \**************************************/
/*! exports provided: JobsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JobsService", function() { return JobsService; });
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../constants */ "./src/app/constants.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var JobsService = /** @class */ (function () {
    function JobsService(http) {
        this.http = http;
        this.apiUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_0__["environment"].apiBaseUrl;
        this.userId = null;
        this._isAuthenticated = new rxjs__WEBPACK_IMPORTED_MODULE_4__["BehaviorSubject"](false);
    }
    JobsService.prototype.create = function (data) {
        return this.http.post(this.apiUrl + "/jobs", data, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    JobsService.prototype.update = function (data, id) {
        return this.http.put(this.apiUrl + "/jobs/" + id, data, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    JobsService.prototype.getOne = function (id) {
        return this.http.get(this.apiUrl + "/jobs/" + id, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    JobsService.prototype.getAll = function (query) {
        return this.http.get(this.apiUrl + "/jobs?" + query, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    JobsService.prototype.delete = function (id) {
        return this.http.delete(this.apiUrl + "/jobs/" + id, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    JobsService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], JobsService);
    return JobsService;
}());



/***/ }),

/***/ "./src/app/jobs/list-jobs/list-jobs.component.css":
/*!********************************************************!*\
  !*** ./src/app/jobs/list-jobs/list-jobs.component.css ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2pvYnMvbGlzdC1qb2JzL2xpc3Qtam9icy5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/jobs/list-jobs/list-jobs.component.html":
/*!*********************************************************!*\
  !*** ./src/app/jobs/list-jobs/list-jobs.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<style>\r\n  .btn {\r\n    background-color: DodgerBlue; /* Blue background */\r\n    border: none; /* Remove borders */\r\n    color: white; /* White text */\r\n    font-size: 10px; /* Set a font size */\r\n    cursor: pointer; /* Mouse pointer on hover */\r\n  }\r\n\r\n  /* Darker background on mouse-over */\r\n  .btn:hover {\r\n    background-color: RoyalBlue;\r\n  }\r\n  .loader {\r\n    position: absolute;\r\n    left: 50%;\r\n    top: 50%;\r\n    height:60px;\r\n    width:60px;\r\n    margin:0px auto;\r\n    -webkit-animation: rotation .6s infinite linear;\r\n    -moz-animation: rotation .6s infinite linear;\r\n    -o-animation: rotation .6s infinite linear;\r\n    animation: rotation .6s infinite linear;\r\n    border-left:6px solid rgba(0,174,239,.15);\r\n    border-right:6px solid rgba(0,174,239,.15);\r\n    border-bottom:6px solid rgba(0,174,239,.15);\r\n    border-top:6px solid rgba(0,174,239,.8);\r\n    border-radius:100%;\r\n  }\r\n\r\n  @-webkit-keyframes rotation {\r\n    from {-webkit-transform: rotate(0deg);}\r\n    to {-webkit-transform: rotate(359deg);}\r\n  }\r\n  @-moz-keyframes rotation {\r\n    from {-moz-transform: rotate(0deg);}\r\n    to {-moz-transform: rotate(359deg);}\r\n  }\r\n  @-o-keyframes rotation {\r\n    from {-o-transform: rotate(0deg);}\r\n    to {-o-transform: rotate(359deg);}\r\n  }\r\n  @keyframes rotation {\r\n    from {transform: rotate(0deg);}\r\n    to {transform: rotate(359deg);}\r\n  }\r\n</style>\r\n<!-- Content Header (Page header) -->\r\n<section class=\"content-header\">\r\n  <h1>\r\n    Jobs <button routerLink=\"/jobs/create-job\" type=\"submit\" class=\"btn btn-primary pull-right\">New</button>\r\n  </h1>\r\n  <ol class=\"breadcrumb\">\r\n    <li><a routerLink=\"/dashboard\"><i class=\"fa fa-dashboard\"></i> Home</a></li>\r\n    <li><a routerLink=\"/jobs\">Jobs</a></li>\r\n  </ol>\r\n</section>\r\n\r\n<!-- Main content -->\r\n<div class=\"loader\" [hidden]=\"!loading\"></div>\r\n<section class=\"content\">\r\n  <!-- /.row -->\r\n  <div class=\"row\">\r\n    <div class=\"col-xs-12\">\r\n      <div class=\"box\">\r\n        <div class=\"box-header\">\r\n          <h3 class=\"box-title\"></h3>\r\n\r\n          <div class=\"box-tools\">\r\n            <div class=\"input-group input-group-sm\" style=\"width: 150px;\">\r\n              <input (ngModelChange)=\"filter($event)\" [(ngModel)]=\"nameFilter\"  type=\"text\" name=\"table_search\" class=\"form-control pull-right\" placeholder=\"Search\">\r\n\r\n              <div class=\"input-group-btn\">\r\n                <button type=\"submit\" class=\"btn btn-default\"><i class=\"fa fa-search\"></i></button>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <!-- /.box-header -->\r\n        <div class=\"box-body table-responsive no-padding\">\r\n          <table class=\"table table-hover\">\r\n            <thead>\r\n            <tr>\r\n              <th>Title</th>\r\n              <th>Description</th>\r\n              <th>Category</th>\r\n              <th>Salary</th>\r\n              <th>Status</th>\r\n              <th>Pinned</th>\r\n              <th>Interactions</th>\r\n              <th>Edit/Delete</th>\r\n            </tr>\r\n            </thead>\r\n            <tbody>\r\n            <tr *ngFor=\"let job of jobs\" class=\"gradeX odd content-table-payment cursor\" role=\"row\">\r\n              <td class=\"mn-130 sorting_1\">{{job.title}}</td>\r\n              <td class=\"mn-130 sorting_1\">{{job.description.substring(1,15)}}.....</td>\r\n              <td class=\"mn-130 sorting_1\">{{job.category}}</td>\r\n              <td class=\"mn-130 sorting_1\">{{job.salary}}</td>\r\n              <td class=\"mn-120 sorting_1\">\r\n                  <span *ngIf=\"job.is_active === true\"\r\n                        class=\"label label-success upper-case\">Active</span>\r\n                <span *ngIf=\"job.is_active === false\"\r\n                      class=\"label label-danger upper-case\">Active</span>\r\n              </td>\r\n              <td class=\"mn-120 sorting_1\">\r\n                  <span *ngIf=\"job.pinned === true\"\r\n                        class=\"label label-success upper-case\">Pinned</span>\r\n                <span *ngIf=\"job.pinned === false\"\r\n                      class=\"label label-danger upper-case\">Pinned</span>\r\n              </td>\r\n              <td></td>\r\n              <td>\r\n                <button class=\"btn\" routerLink='/jobs/{{job._id}}/edit-job'><i class=\"fa fa-edit\"></i></button>\r\n                <button class=\"btn\" (click)=\"delete(job._id)\"><i class=\"fa fa-trash\"></i></button>\r\n              </td>\r\n            </tr>\r\n            </tbody></table>\r\n        </div>\r\n        <!-- /.box-body -->\r\n      </div>\r\n      <!-- /.box -->\r\n    </div>\r\n  </div>\r\n</section>\r\n<!-- /.content -->\r\n"

/***/ }),

/***/ "./src/app/jobs/list-jobs/list-jobs.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/jobs/list-jobs/list-jobs.component.ts ***!
  \*******************************************************/
/*! exports provided: ListJobsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListJobsComponent", function() { return ListJobsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _jobs_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../jobs.service */ "./src/app/jobs/jobs.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_3__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ListJobsComponent = /** @class */ (function () {
    function ListJobsComponent(router, route, jobService) {
        this.router = router;
        this.route = route;
        this.jobService = jobService;
        this.ctrl = this;
        this.loading = false;
        this.jobs = [];
        this.nameFilter = '';
    }
    ListJobsComponent.prototype.ngOnInit = function () {
        this.getAll('');
    };
    ListJobsComponent.prototype.filter = function (value) {
        if (value !== '') {
            this.nameFilter = "title=" + value;
        }
        else {
            this.nameFilter = '';
        }
        this.getAll(this.nameFilter);
    };
    ListJobsComponent.prototype.getAll = function (query) {
        var _this = this;
        this.loading = true;
        $('.content').find('*').attr('disabled', 'disabled');
        $('.content').find('a').click(function (e) { e.preventDefault(); });
        this.jobService
            .getAll(query)
            .subscribe(function (result) {
            _this.loading = false;
            _this.jobs = result.jobs;
            $('.content').find('*').removeAttr('disabled');
            $('.content').find('a').unbind('click');
            // const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
            // this.router.navigate(['news']);
            // setTimeout(() => {
            //   this.successMsg = 'News Created Successfully';
            // }, 2000);
        }, function (error) {
            _this.loading = false;
            $('.content').find('*').removeAttr('disabled');
            $('.content').find('a').unbind('click');
            // setTimeout(() => {
            //   this.errorMsg = error;
            // }, 2000);
        });
    };
    ListJobsComponent.prototype.delete = function (id) {
        var _this = this;
        this.loading = true;
        sweetalert2__WEBPACK_IMPORTED_MODULE_3___default()({
            type: 'warning',
            title: 'Are you sure to Delete Job?',
            text: 'You will not be able to recover the data of Job',
            showCancelButton: true,
            confirmButtonColor: '#049F0C',
            cancelButtonColor: '#ff0000',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, keep it'
        }).then(function (result) {
            if (result.value) {
                _this.jobService
                    .delete(id)
                    .subscribe(function (result) {
                    _this.loading = false;
                    _this.getAll('');
                    // const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
                    // this.router.navigate(['news']);
                    // setTimeout(() => {
                    //   this.successMsg = 'News Created Successfully';
                    // }, 2000);
                }, function (error) {
                    _this.loading = false;
                    _this.getAll('');
                    // setTimeout(() => {
                    //   this.errorMsg = error;
                    // }, 2000);
                });
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_3___default()({
                    type: 'info',
                    title: 'Cancelled',
                    text: 'Your Job file is safe :)'
                });
            }
        });
    };
    ListJobsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-list-jobs',
            template: __webpack_require__(/*! ./list-jobs.component.html */ "./src/app/jobs/list-jobs/list-jobs.component.html"),
            styles: [__webpack_require__(/*! ./list-jobs.component.css */ "./src/app/jobs/list-jobs/list-jobs.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _jobs_service__WEBPACK_IMPORTED_MODULE_1__["JobsService"]])
    ], ListJobsComponent);
    return ListJobsComponent;
}());



/***/ }),

/***/ "./src/app/layout/app-layout/app-layout.component.css":
/*!************************************************************!*\
  !*** ./src/app/layout/app-layout/app-layout.component.css ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9hcHAtbGF5b3V0L2FwcC1sYXlvdXQuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/layout/app-layout/app-layout.component.html":
/*!*************************************************************!*\
  !*** ./src/app/layout/app-layout/app-layout.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<body   class=\"sidebar-mini skin-green-light\">\r\n<div class=\"wrapper\" style=\"height: auto; min-height: 100%;\">\r\n  <app-sidenavbar></app-sidenavbar>\r\n  <app-topnavbar></app-topnavbar>\r\n  <div class=\"content-wrapper\" style=\"min-height: 990px;\">\r\n  <router-outlet></router-outlet>\r\n  </div>\r\n  <app-footernavbar></app-footernavbar>\r\n</div>\r\n</body>\r\n"

/***/ }),

/***/ "./src/app/layout/app-layout/app-layout.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/layout/app-layout/app-layout.component.ts ***!
  \***********************************************************/
/*! exports provided: AppLayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppLayoutComponent", function() { return AppLayoutComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AppLayoutComponent = /** @class */ (function () {
    function AppLayoutComponent() {
    }
    AppLayoutComponent.prototype.ngOnInit = function () {
    };
    AppLayoutComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-app-layout',
            template: __webpack_require__(/*! ./app-layout.component.html */ "./src/app/layout/app-layout/app-layout.component.html"),
            styles: [__webpack_require__(/*! ./app-layout.component.css */ "./src/app/layout/app-layout/app-layout.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], AppLayoutComponent);
    return AppLayoutComponent;
}());



/***/ }),

/***/ "./src/app/layout/footernavbar/footernavbar.component.css":
/*!****************************************************************!*\
  !*** ./src/app/layout/footernavbar/footernavbar.component.css ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9mb290ZXJuYXZiYXIvZm9vdGVybmF2YmFyLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/layout/footernavbar/footernavbar.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/layout/footernavbar/footernavbar.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<footer class=\"main-footer\">\n  <strong>Copyright © 2018 </strong> TALK, LLC original productions | Admin Panel\n</footer>\n"

/***/ }),

/***/ "./src/app/layout/footernavbar/footernavbar.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/layout/footernavbar/footernavbar.component.ts ***!
  \***************************************************************/
/*! exports provided: FooternavbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooternavbarComponent", function() { return FooternavbarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FooternavbarComponent = /** @class */ (function () {
    function FooternavbarComponent() {
    }
    FooternavbarComponent.prototype.ngOnInit = function () {
    };
    FooternavbarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-footernavbar',
            template: __webpack_require__(/*! ./footernavbar.component.html */ "./src/app/layout/footernavbar/footernavbar.component.html"),
            styles: [__webpack_require__(/*! ./footernavbar.component.css */ "./src/app/layout/footernavbar/footernavbar.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], FooternavbarComponent);
    return FooternavbarComponent;
}());



/***/ }),

/***/ "./src/app/layout/settingsnavbar/settingsnavbar.component.css":
/*!********************************************************************!*\
  !*** ./src/app/layout/settingsnavbar/settingsnavbar.component.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9zZXR0aW5nc25hdmJhci9zZXR0aW5nc25hdmJhci5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/layout/settingsnavbar/settingsnavbar.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/layout/settingsnavbar/settingsnavbar.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  settingsnavbar works!\n</p>\n"

/***/ }),

/***/ "./src/app/layout/settingsnavbar/settingsnavbar.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/layout/settingsnavbar/settingsnavbar.component.ts ***!
  \*******************************************************************/
/*! exports provided: SettingsnavbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsnavbarComponent", function() { return SettingsnavbarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SettingsnavbarComponent = /** @class */ (function () {
    function SettingsnavbarComponent() {
    }
    SettingsnavbarComponent.prototype.ngOnInit = function () {
    };
    SettingsnavbarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-settingsnavbar',
            template: __webpack_require__(/*! ./settingsnavbar.component.html */ "./src/app/layout/settingsnavbar/settingsnavbar.component.html"),
            styles: [__webpack_require__(/*! ./settingsnavbar.component.css */ "./src/app/layout/settingsnavbar/settingsnavbar.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], SettingsnavbarComponent);
    return SettingsnavbarComponent;
}());



/***/ }),

/***/ "./src/app/layout/sidenavbar/sidenavbar.component.css":
/*!************************************************************!*\
  !*** ./src/app/layout/sidenavbar/sidenavbar.component.css ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9zaWRlbmF2YmFyL3NpZGVuYXZiYXIuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/layout/sidenavbar/sidenavbar.component.html":
/*!*************************************************************!*\
  !*** ./src/app/layout/sidenavbar/sidenavbar.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<aside class=\"main-sidebar\">\r\n  <!-- sidebar: style can be found in sidebar.less -->\r\n  <section class=\"sidebar\" style=\"height: auto;\">\r\n    <!-- Sidebar users panel -->\r\n    <div class=\"user-panel\">\r\n      <div class=\"pull-left image\">\r\n        <img src=\"assets/dist/img/user2-160x160.jpg\" class=\"img-circle\" alt=\"User Image\">\r\n      </div>\r\n      <div class=\"pull-left info\">\r\n        <p>Alexander Pierce</p>\r\n        <a href=\"#\"><i class=\"fa fa-circle text-success\"></i> Online</a>\r\n      </div>\r\n    </div>\r\n    <!-- search form -->\r\n    <form action=\"#\" method=\"get\" class=\"sidebar-form\">\r\n      <div class=\"input-group\">\r\n        <input type=\"text\" name=\"q\" class=\"form-control\" placeholder=\"Search...\">\r\n        <span class=\"input-group-btn\">\r\n                <button type=\"submit\" name=\"search\" id=\"search-btn\" class=\"btn btn-flat\">\r\n                  <i class=\"fa fa-search\"></i>\r\n                </button>\r\n              </span>\r\n      </div>\r\n    </form>\r\n    <!-- /.search form -->\r\n    <!-- sidebar menu: : style can be found in sidebar.less -->\r\n    <ul class=\"sidebar-menu tree\" data-widget=\"tree\">\r\n      <li class=\"header\">MAIN NAVIGATION</li>\r\n      <li class=\"active treeview menu-open\">\r\n        <a routerLink=\"/dashboard\">\r\n          <i class=\"fa fa-dashboard\"></i> <span>Dashboard</span>\r\n        </a>\r\n      </li>\r\n      <li class=\"treeview\">\r\n        <a routerLink=\"/programs\">\r\n          <i class=\"fa fa-files-o\"></i>\r\n          <span>Programs</span>\r\n        </a>\r\n      </li>\r\n      <li>\r\n        <a routerLink=\"/services\">\r\n          <i class=\"fa fa-th\"></i>\r\n          <span>Services</span>\r\n        </a>\r\n      </li>\r\n      <li class=\"treeview\">\r\n        <a routerLink=\"/events\">\r\n          <i class=\"fa fa-pie-chart\"></i>\r\n          <span>Events</span>\r\n        </a>\r\n      </li>\r\n      <li class=\"treeview\">\r\n        <a routerLink=\"/news\">\r\n          <i class=\"fa fa-laptop\"></i>\r\n          <span>News</span>\r\n        </a>\r\n      </li>\r\n      <li class=\"treeview\">\r\n        <a routerLink=\"/jobs\">\r\n          <i class=\"fa fa-edit\"></i>\r\n          <span>Jobs</span>\r\n        </a>\r\n      </li>\r\n      <li class=\"treeview\">\r\n        <a routerLink=\"/staff\">\r\n          <i class=\"fa fa-table\"></i>\r\n          <span>Staff</span>\r\n        </a>\r\n      </li>\r\n      <li class=\"treeview\">\r\n        <a routerLink=\"/team-member\">\r\n          <i class=\"fa fa-table\"></i>\r\n          <span>Team Member</span>\r\n        </a>\r\n      </li>\r\n      <li class=\"treeview\">\r\n        <a routerLink=\"/partners\">\r\n          <i class=\"fa fa-table\"></i>\r\n          <span>Partners</span>\r\n        </a>\r\n      </li>\r\n      <li>\r\n        <a routerLink=\"/users\">\r\n          <i class=\"fa fa-calendar\"></i>\r\n          <span>Users</span>\r\n        </a>\r\n      </li>\r\n      <li>\r\n        <a routerLink=\"/contents\">\r\n          <i class=\"fa fa-calendar\"></i>\r\n          <span>Contents</span>\r\n        </a>\r\n      </li>\r\n    </ul>\r\n  </section>\r\n  <!-- /.sidebar -->\r\n</aside>\r\n"

/***/ }),

/***/ "./src/app/layout/sidenavbar/sidenavbar.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/layout/sidenavbar/sidenavbar.component.ts ***!
  \***********************************************************/
/*! exports provided: SidenavbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidenavbarComponent", function() { return SidenavbarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SidenavbarComponent = /** @class */ (function () {
    function SidenavbarComponent() {
    }
    SidenavbarComponent.prototype.ngOnInit = function () {
    };
    SidenavbarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-sidenavbar',
            template: __webpack_require__(/*! ./sidenavbar.component.html */ "./src/app/layout/sidenavbar/sidenavbar.component.html"),
            styles: [__webpack_require__(/*! ./sidenavbar.component.css */ "./src/app/layout/sidenavbar/sidenavbar.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], SidenavbarComponent);
    return SidenavbarComponent;
}());



/***/ }),

/***/ "./src/app/layout/topnavbar/topnavbar.component.css":
/*!**********************************************************!*\
  !*** ./src/app/layout/topnavbar/topnavbar.component.css ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC90b3BuYXZiYXIvdG9wbmF2YmFyLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/layout/topnavbar/topnavbar.component.html":
/*!***********************************************************!*\
  !*** ./src/app/layout/topnavbar/topnavbar.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"main-header\">\n\n  <!-- Logo -->\n  <a href=\"index2.html\" class=\"logo\">\n    <!-- mini logo for sidebar mini 50x50 pixels -->\n    <span class=\"logo-mini\"><b>A</b>LT</span>\n    <!-- logo for regular state and mobile devices -->\n    <span class=\"logo-lg\"><b>Admin</b>LTE</span>\n  </a>\n\n  <!-- Header Navbar: style can be found in header.less -->\n  <nav class=\"navbar navbar-static-top\">\n    <!-- Sidebar toggle button-->\n    <a href=\"#\" class=\"sidebar-toggle\" data-toggle=\"push-menu\" role=\"button\">\n      <span class=\"sr-only\">Toggle navigation</span>\n    </a>\n    <!-- Navbar Right Menu -->\n    <div class=\"navbar-custom-menu\">\n      <ul class=\"nav navbar-nav\">\n        <!-- User Account: style can be found in dropdown.less -->\n        <li class=\"dropdown user user-menu\">\n          <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">\n            <img src=\"assets/dist/img/user2-160x160.jpg\" class=\"user-image\" alt=\"User Image\">\n            <span class=\"hidden-xs\">Alexander Pierce</span>\n          </a>\n          <ul class=\"dropdown-menu\">\n            <!-- User image -->\n            <li class=\"user-header\">\n              <img src=\"assets/dist/img/user2-160x160.jpg\" class=\"img-circle\" alt=\"User Image\">\n\n              <p>\n                Alexander Pierce - Web Developer\n                <small>Member since Nov. 2012</small>\n              </p>\n            </li>\n            <!-- Menu Body -->\n            <li class=\"user-body\">\n              <div class=\"row\">\n                <div class=\"col-xs-4 text-center\">\n                  <a href=\"#\">Followers</a>\n                </div>\n                <div class=\"col-xs-4 text-center\">\n                  <a href=\"#\">Sales</a>\n                </div>\n                <div class=\"col-xs-4 text-center\">\n                  <a href=\"#\">Friends</a>\n                </div>\n              </div>\n              <!-- /.row -->\n            </li>\n            <!-- Menu Footer-->\n            <li class=\"user-footer\">\n              <div class=\"pull-left\">\n                <a routerLink=\"/staff/profile\" class=\"btn btn-default btn-flat\">Profile</a>\n              </div>\n              <div class=\"pull-right\">\n                <a routerLink=\"/auth/login\" class=\"btn btn-default btn-flat\">Sign out</a>\n              </div>\n            </li>\n          </ul>\n        </li>\n      </ul>\n    </div>\n\n  </nav>\n</header>\n"

/***/ }),

/***/ "./src/app/layout/topnavbar/topnavbar.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/layout/topnavbar/topnavbar.component.ts ***!
  \*********************************************************/
/*! exports provided: TopnavbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TopnavbarComponent", function() { return TopnavbarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TopnavbarComponent = /** @class */ (function () {
    function TopnavbarComponent() {
    }
    TopnavbarComponent.prototype.ngOnInit = function () {
    };
    TopnavbarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-topnavbar',
            template: __webpack_require__(/*! ./topnavbar.component.html */ "./src/app/layout/topnavbar/topnavbar.component.html"),
            styles: [__webpack_require__(/*! ./topnavbar.component.css */ "./src/app/layout/topnavbar/topnavbar.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], TopnavbarComponent);
    return TopnavbarComponent;
}());



/***/ }),

/***/ "./src/app/news/create-news/create-news.component.css":
/*!************************************************************!*\
  !*** ./src/app/news/create-news/create-news.component.css ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL25ld3MvY3JlYXRlLW5ld3MvY3JlYXRlLW5ld3MuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/news/create-news/create-news.component.html":
/*!*************************************************************!*\
  !*** ./src/app/news/create-news/create-news.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--<div class=\"row text-center\">-->\r\n  <!--<div class=\"col-lg-4 col-12 alert alert-success mx-auto fs-14\" role=\"alert\" *ngIf=\"successMsg\">-->\r\n    <!--<button class=\"close\" data-dismiss=\"alert\"></button>-->\r\n    <!--<strong> {{successMsg}}</strong>-->\r\n  <!--</div>-->\r\n<!--</div>-->\r\n\r\n<!--<div class=\"row text-center\">-->\r\n  <!--<div class=\"col-lg-4 col-12 alert alert-danger mx-auto fs-14\" role=\"alert\" *ngIf=\"errorMsg\">-->\r\n    <!--<button class=\"close\" data-dismiss=\"alert\"></button>-->\r\n    <!--<strong> {{errorMsg}}</strong>-->\r\n  <!--</div>-->\r\n<!--</div>-->\r\n\r\n<!-- Content Header (Page header) -->\r\n  <section class=\"content-header\">\r\n    <h1>\r\n      News\r\n    </h1>\r\n    <ol class=\"breadcrumb\">\r\n      <li><a routerLink=\"/dashboard\"><i class=\"fa fa-dashboard\"></i> Home</a></li>\r\n      <li><a routerLink=\"/news\">News</a></li>\r\n      <li class=\"active\">Create</li>\r\n    </ol>\r\n  </section>\r\n\r\n  <!-- Main content -->\r\n<div class=\"loader\" [hidden]=\"!loading\"></div>\r\n\r\n  <section class=\"content\">\r\n    <div class=\"row\">\r\n      <!-- left column -->\r\n      <div class=\"col-md-12\">\r\n        <!-- general form elements -->\r\n        <div class=\"box box-primary\">\r\n          <div class=\"box-header with-border\">\r\n            <h3 class=\"box-title\">Create New Feed</h3>\r\n          </div>\r\n          <!-- /.box-header -->\r\n          <!-- form start -->\r\n          <form id=\"form-news\" class=\"p-t-15\" role=\"form\" [formGroup]=\"form\" (ngSubmit)=\"create()\">\r\n            <div class=\"box-body\">\r\n              <div class=\"form-group\">\r\n                <label for=\"exampleInputEmail1\">Title</label>\r\n                <input formControlName=\"title\" class=\"form-control\" id=\"exampleInputEmail1\" placeholder=\"Enter Title\">\r\n              </div>\r\n              <div class=\"form-group\">\r\n                <label for=\"exampleInputEmail1\">Tags</label>\r\n                <input formControlName=\"tags\" class=\"form-control\" placeholder=\"Enter Tags separated comma\">\r\n              </div>\r\n              <div class=\"form-group\">\r\n                <label>Description</label>\r\n                <angular-editor formControlName=\"description\" [config]=\"editorConfig\"></angular-editor>\r\n              </div>\r\n              <label>Upload Image</label>\r\n              <div class=\"form-group\">\r\n                <img *ngFor='let url of urls'  [src]=\"url\" height=\"200\"> <br/>\r\n                <input type='file' (change)=\"onFileChanged($event)\" multiple>\r\n              </div>\r\n              <div class=\"form-group\">\r\n                <label>Segment</label>\r\n                <select formControlName=\"segment\" class=\"form-control\">\r\n                  <option [value]=\"segment.code\" *ngFor=\"let segment of segments\" [selected]=\"segment\">\r\n                    {{segment.color}}</option>\r\n                </select>\r\n              </div>\r\n              <div class=\"form-group\">\r\n                <label class=\"hover\">\r\n                  <div class=\"icheckbox_flat-green hover\" [ngClass]=\"{'checked': pinned}\" aria-checked=\"true\" aria-disabled=\"false\" style=\"position: relative;\"><input [checked]=\"pinned\" (change)=\"pinned = !pinned\" [value]='pinned' type=\"checkbox\" class=\"flat-red\" checked=\"\" style=\"position: absolute; opacity: 0;\"><ins class=\"iCheck-helper\" style=\"position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;\"></ins></div>\r\n                  Pin to dashboard\r\n                </label>\r\n              </div>\r\n              <div class=\"form-group\">\r\n                <label class=\"hover\">\r\n                  <div class=\"icheckbox_flat-green hover\" [ngClass]=\"{'checked': is_active}\" aria-checked=\"true\" aria-disabled=\"false\" style=\"position: relative;\"><input [checked]=\"is_active\" (change)=\"is_active = !is_active\" [value]='is_active' type=\"checkbox\" class=\"flat-red\" checked=\"\" style=\"position: absolute; opacity: 0;\"><ins class=\"iCheck-helper\" style=\"position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;\"></ins></div>\r\n                  Active\r\n                </label>\r\n              </div>\r\n            </div>\r\n            <!-- /.box-body -->\r\n\r\n            <div class=\"box-footer\">\r\n              <button type=\"submit\" class=\"btn btn-primary\">Save</button>\r\n            </div>\r\n          </form>\r\n        </div>\r\n        <!-- /.box -->\r\n\r\n      </div>\r\n      <!--/.col (left) -->\r\n    </div>\r\n    <!-- /.row -->\r\n  </section>\r\n  <!-- /.content -->\r\n<script>\r\n  $(function () {\r\n    //Initialize Select2 Elements\r\n    $('.select2').select2()\r\n\r\n    //Datemask dd/mm/yyyy\r\n    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })\r\n    //Datemask2 mm/dd/yyyy\r\n    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })\r\n    //Money Euro\r\n    $('[data-mask]').inputmask()\r\n\r\n    //Date range picker\r\n    $('#reservation').daterangepicker()\r\n    //Date range picker with time picker\r\n    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })\r\n    //Date range as a button\r\n    $('#daterange-btn').daterangepicker(\r\n      {\r\n        ranges   : {\r\n          'Today'       : [moment(), moment()],\r\n          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],\r\n          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],\r\n          'Last 30 Days': [moment().subtract(29, 'days'), moment()],\r\n          'This Month'  : [moment().startOf('month'), moment().endOf('month')],\r\n          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]\r\n        },\r\n        startDate: moment().subtract(29, 'days'),\r\n        endDate  : moment()\r\n      },\r\n      function (start, end) {\r\n        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))\r\n      }\r\n    )\r\n\r\n    //Date picker\r\n    $('#datepicker').datepicker({\r\n      autoclose: true\r\n    })\r\n\r\n    //iCheck for checkbox and radio inputs\r\n    $('input[type=\"checkbox\"].minimal, input[type=\"radio\"].minimal').iCheck({\r\n      checkboxClass: 'icheckbox_minimal-blue',\r\n      radioClass   : 'iradio_minimal-blue'\r\n    })\r\n    //Red color scheme for iCheck\r\n    $('input[type=\"checkbox\"].minimal-red, input[type=\"radio\"].minimal-red').iCheck({\r\n      checkboxClass: 'icheckbox_minimal-red',\r\n      radioClass   : 'iradio_minimal-red'\r\n    })\r\n    //Flat red color scheme for iCheck\r\n    $('input[type=\"checkbox\"].flat-red, input[type=\"radio\"].flat-red').iCheck({\r\n      checkboxClass: 'icheckbox_flat-green',\r\n      radioClass   : 'iradio_flat-green'\r\n    })\r\n\r\n    //Colorpicker\r\n    $('.my-colorpicker1').colorpicker()\r\n    //color picker with addon\r\n    $('.my-colorpicker2').colorpicker()\r\n\r\n    //Timepicker\r\n    $('.timepicker').timepicker({\r\n      showInputs: false\r\n    })\r\n  })\r\n</script>\r\n"

/***/ }),

/***/ "./src/app/news/create-news/create-news.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/news/create-news/create-news.component.ts ***!
  \***********************************************************/
/*! exports provided: CreateNewsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateNewsComponent", function() { return CreateNewsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _news_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../news.service */ "./src/app/news/news.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CreateNewsComponent = /** @class */ (function () {
    function CreateNewsComponent(fb, router, route, newsService) {
        this.fb = fb;
        this.router = router;
        this.route = route;
        this.newsService = newsService;
        this.ctrl = this;
        this.editorConfig = {
            editable: true,
            spellcheck: true,
            height: '25rem',
            minHeight: '5rem',
            placeholder: 'Enter text here...',
            translate: 'no',
            uploadUrl: 'v1/images',
        };
        this.pinned = true;
        this.is_active = false;
        this.loading = false;
        this.segments = [
            { color: 'Red', code: '#ff5a61' },
            { color: 'Blue', code: '#04adc7' },
            { color: 'Orange', code: '#fea601' },
            { color: 'Purple', code: '#8b199b' },
        ];
        this.selectedFiles = [];
        this.formData = new FormData();
        this.urls = [];
    }
    Object.defineProperty(CreateNewsComponent.prototype, "title", {
        get: function () {
            return this.form.get('title');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreateNewsComponent.prototype, "description", {
        get: function () {
            return this.form.get('description');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreateNewsComponent.prototype, "tags", {
        get: function () {
            return this.form.get('tags');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreateNewsComponent.prototype, "segment", {
        get: function () {
            return this.form.get('segment');
        },
        enumerable: true,
        configurable: true
    });
    CreateNewsComponent.prototype.ngOnInit = function () {
        this.buildForm();
    };
    CreateNewsComponent.prototype.onFileChanged = function (event) {
        var _this = this;
        this.loading = true;
        this.selectedFiles = event.target.files;
        // this.formData.append('images', this.selectedFiles, this.selectedFiles.name);
        if (this.selectedFiles.length > 0) {
            var _loop_1 = function (i) {
                var reader = new FileReader();
                reader.onload = function (event) {
                    _this.formData.append('images', _this.selectedFiles[i]);
                    _this.urls.push(event.target.result);
                };
                reader.readAsDataURL(this_1.selectedFiles[i]);
            };
            var this_1 = this;
            for (var i = 0; i < this.selectedFiles.length; i++) {
                _loop_1(i);
            }
        }
    };
    CreateNewsComponent.prototype.create = function () {
        var _this = this;
        this.loading = true;
        $('.content').find('*').attr('disabled', 'disabled');
        $('.content').find('a').click(function (e) { e.preventDefault(); });
        var data = {
            title: this.form.value.title,
            description: this.form.value.description,
            tags: this.form.value.tags,
            segment: this.form.value.segment,
            pinned: this.pinned,
            is_active: this.is_active
        };
        this.formData.append('data', JSON.stringify(data));
        this.newsService
            .create(this.formData)
            .subscribe(function (result) {
            _this.loading = false;
            var returnUrl = _this.route.snapshot.queryParamMap.get('returnUrl');
            _this.router.navigate(['news']);
        }, function (error) {
            _this.loading = false;
        });
    };
    CreateNewsComponent.prototype.buildForm = function () {
        this.form = this.fb.group({
            title: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            description: [''],
            tags: [''],
            segment: [this.segments]
        });
    };
    CreateNewsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-create-news',
            template: __webpack_require__(/*! ./create-news.component.html */ "./src/app/news/create-news/create-news.component.html"),
            styles: [__webpack_require__(/*! ./create-news.component.css */ "./src/app/news/create-news/create-news.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _news_service__WEBPACK_IMPORTED_MODULE_3__["NewsService"]])
    ], CreateNewsComponent);
    return CreateNewsComponent;
}());



/***/ }),

/***/ "./src/app/news/edit-news/edit-news.component.css":
/*!********************************************************!*\
  !*** ./src/app/news/edit-news/edit-news.component.css ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL25ld3MvZWRpdC1uZXdzL2VkaXQtbmV3cy5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/news/edit-news/edit-news.component.html":
/*!*********************************************************!*\
  !*** ./src/app/news/edit-news/edit-news.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<!-- Content Header (Page header) -->\r\n  <section class=\"content-header\">\r\n    <h1>\r\n      News\r\n    </h1>\r\n    <ol class=\"breadcrumb\">\r\n      <li><a routerLink=\"/dashboard\"><i class=\"fa fa-dashboard\"></i> Home</a></li>\r\n      <li><a routerLink=\"/news\">News</a></li>\r\n      <li class=\"active\">Edit</li>\r\n    </ol>\r\n  </section>\r\n\r\n  <!-- Main content -->\r\n<div class=\"loader\" [hidden]=\"!loading\"></div>\r\n  <section class=\"content\">\r\n    <div class=\"row\">\r\n      <!-- left column -->\r\n      <div class=\"col-md-12\">\r\n        <!-- general form elements -->\r\n        <div class=\"box box-primary\">\r\n          <div class=\"box-header with-border\">\r\n            <h3 class=\"box-title\">Edit News</h3>\r\n          </div>\r\n          <!-- /.box-header -->\r\n          <!-- form start -->\r\n          <form id=\"form-news\" class=\"p-t-15\" role=\"form\" [formGroup]=\"form\" (ngSubmit)=\"update()\">\r\n            <div class=\"box-body\">\r\n              <div class=\"form-group\">\r\n                <label for=\"exampleInputEmail1\">Title</label>\r\n                <input class=\"form-control\" formControlName=\"title\" id=\"exampleInputEmail1\" placeholder=\"Enter Title\">\r\n              </div>\r\n              <div class=\"form-group\">\r\n                <label for=\"exampleInputEmail1\">Tags</label>\r\n                <input formControlName=\"tags\" class=\"form-control\" placeholder=\"Enter Tags separated comma\">\r\n              </div>\r\n              <div class=\"form-group\">\r\n                <label>Description</label>\r\n                <angular-editor formControlName=\"description\" [config]=\"editorConfig\"></angular-editor>\r\n              </div>\r\n              <label for=\"exampleInputFile\">Upload Image</label>\r\n              <div class=\"form-group\">\r\n                <img *ngFor=\"let url of urls\" [src]=\"url\" height=\"200\"><br/>\r\n                <input type=\"file\" (change)=\"onFileChanged($event)\" id=\"exampleInputFile\" multiple>\r\n              </div>\r\n              <!--<div class=\"d-flex flex-wrap justify-content-center justify-content-sm-start m-b-30\">-->\r\n                <!--<img name='image' class=\"smile-face__wrapper d-flex justify-content-center align-items-center rounded-circle sm-m-b-20\" alt=\"User\">-->\r\n                <!--&lt;!&ndash;<div *ngIf=\"userImage ==''\" class=\"smile-face__wrapper d-flex justify-content-center align-items-center rounded-circle sm-m-b-20\">&ndash;&gt;-->\r\n                <!--&lt;!&ndash;<i class=\"fa fa-user-o\" style=\"font-size:48px\"></i>&ndash;&gt;-->\r\n                <!--&lt;!&ndash;</div>&ndash;&gt;-->\r\n                <!--<div class=\"m-l-20\">-->\r\n                  <!--<p class=\"bold fs-14 m-b-0\">title</p>-->\r\n                  <!--<p class=\"fs-12 text-muted\">subtitle</p>-->\r\n                  <!--<input style=\"display: none\"  type=\"file\"  accept=\"image/x-png,image/jpeg\"  (change)=\"onFileChanged($event)\"-->\r\n                         <!--#fileInput>-->\r\n                  <!--<button class=\"btn btn-muted\" (click)=\"fileInput.click()\">upload</button>-->\r\n                  <!--&lt;!&ndash;   {{'admin.staffForm.field.imageField.imageButton' | translate}}&ndash;&gt;-->\r\n\r\n                <!--</div>-->\r\n              <!--</div>-->\r\n              <div class=\"form-group\">\r\n                <label>Segment</label>\r\n                <select formControlName=\"segment\" class=\"form-control\">\r\n                  <option [value]=\"segment.code\" *ngFor=\"let segment of segments\">\r\n                    {{segment.color}}</option>\r\n                </select>\r\n              </div>\r\n              <div class=\"form-group\">\r\n                <label class=\"\">\r\n                  <div class=\"icheckbox_flat-green\" [ngClass]=\"{'checked': pinned}\" aria-checked=\"false\" aria-disabled=\"false\" style=\"position: relative;\"><input [checked]=\"pinned\" (change)=\"pinned = !pinned\" formControlName=\"pinned\" type=\"checkbox\" class=\"flat-red\" checked=\"\" style=\"position: absolute; opacity: 0;\"><ins class=\"iCheck-helper\" style=\"position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;\"></ins></div>\r\n                  Pin to dashboard\r\n                </label>\r\n              </div>\r\n              <div class=\"form-group\">\r\n                <label class=\"\">\r\n                  <div class=\"icheckbox_flat-green\" [ngClass]=\"{'checked': is_active}\" aria-checked=\"false\" aria-disabled=\"false\" style=\"position: relative;\"><input [checked]=\"is_active\" (change)=\"is_active = !is_active\" formControlName=\"is_active\" type=\"checkbox\" class=\"flat-red\" checked=\"\" style=\"position: absolute; opacity: 0;\"><ins class=\"iCheck-helper\" style=\"position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;\"></ins></div>\r\n                  Active\r\n                </label>\r\n              </div>\r\n            </div>\r\n            <!-- /.box-body -->\r\n\r\n            <div class=\"box-footer\">\r\n              <button type=\"submit\" class=\"btn btn-primary\">Save</button>\r\n            </div>\r\n          </form>\r\n        </div>\r\n        <!-- /.box -->\r\n\r\n      </div>\r\n      <!--/.col (left) -->\r\n    </div>\r\n    <!-- /.row -->\r\n  </section>\r\n  <!-- /.content -->\r\n\r\n<!-- Extra content -->\r\n<section class=\"content\">\r\n  <div class=\"row\">\r\n    <!-- left column -->\r\n    <div class=\"col-md-12\">\r\n      <!-- general form elements -->\r\n      <div class=\"box box-primary\">\r\n        <form id=\"form-extra-sections-news\" class=\"p-t-15\" role=\"form\" [formGroup]=\"form\" (ngSubmit)=\"updateSections()\">\r\n          <div class=\"box-header with-border\">\r\n            <h3 class=\"box-title\">Extra Program Sections</h3>\r\n            <div class=\"form-group pull-right\">\r\n              <select formControlName=\"section\" class=\"form-control\" (change)=\"onSelectType(section)\">\r\n                <option [value]=\"section\" *ngFor=\"let section of sections\">\r\n                  {{section}}</option>\r\n              </select>\r\n            </div>\r\n          </div>\r\n          <!-- /.box-header -->\r\n          <!-- form start -->\r\n          <div class=\"box-body\">\r\n            <div class=\"form-group\" id=\"description\" hidden=\"hidden\">\r\n              <label>Description</label>\r\n              <input formControlName=\"extraDescription\" class=\"form-control\" placeholder=\"Enter Title\" multiple>\r\n              <!--<angular-editor formControlName=\"extraDescription\" [config]=\"editorConfig\"></angular-editor>-->\r\n            </div>\r\n            <div class=\"form-group\" id=\"fileupload\" hidden=\"hidden\">\r\n              <label for=\"exampleInputFile\">Upload Image</label>\r\n              <img *ngFor=\"let url of urls\" [src]=\"url\" height=\"200\"><br/>\r\n              <input type=\"file\" (change)=\"onFileChanged($event)\" multiple>\r\n            </div>\r\n          </div>\r\n          <!-- /.box-body -->\r\n\r\n          <div class=\"box-footer\">\r\n            <button type=\"submit\" id=\"saveButton\" class=\"btn btn-primary\" disabled=\"disabled\">Save</button>\r\n            <button routerLink=\"/news\" class=\"btn btn-primary pull-right\">Back to List</button>\r\n          </div>\r\n        </form>\r\n      </div>\r\n      <!-- /.box -->\r\n\r\n    </div>\r\n    <!--/.col (left) -->\r\n  </div>\r\n  <!-- /.row -->\r\n</section>\r\n<!-- /.content -->\r\n"

/***/ }),

/***/ "./src/app/news/edit-news/edit-news.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/news/edit-news/edit-news.component.ts ***!
  \*******************************************************/
/*! exports provided: EditNewsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditNewsComponent", function() { return EditNewsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _news_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../news.service */ "./src/app/news/news.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var EditNewsComponent = /** @class */ (function () {
    function EditNewsComponent(fb, router, route, newsService) {
        this.fb = fb;
        this.router = router;
        this.route = route;
        this.newsService = newsService;
        this.newsId = this.route.snapshot.params.id;
        this.editorConfig = {
            editable: true,
            spellcheck: true,
            height: '25rem',
            minHeight: '5rem',
            placeholder: 'Enter text here...',
            translate: 'no',
            uploadUrl: 'v1/images',
        };
        this.pinned = true;
        this.title = '';
        this.description = '';
        this.tags = '';
        this.segment = '';
        this.segments = [
            { color: 'Red', code: '#ff5a61' },
            { color: 'Blue', code: '#04adc7' },
            { color: 'Orange', code: '#fea601' },
            { color: 'Purple', code: '#8b199b' },
        ];
        this.sections = ['Text', 'File'];
        this.is_active = false;
        this.loading = false;
        this.selectedFiles = [];
        this.formData = new FormData();
        this.urls = [];
    }
    EditNewsComponent.prototype.ngOnInit = function () {
        this.buildForm();
        this.getOne(this.newsId);
    };
    Object.defineProperty(EditNewsComponent.prototype, "section", {
        get: function () {
            return this.form.get('section');
        },
        enumerable: true,
        configurable: true
    });
    EditNewsComponent.prototype.onFileChanged = function (event) {
        var _this = this;
        this.loading = true;
        this.selectedFiles = event.target.files;
        // this.formData.append('images', this.selectedFiles, this.selectedFiles.name);
        if (this.selectedFiles.length > 0) {
            var _loop_1 = function (i) {
                var reader = new FileReader();
                reader.onload = function (event) {
                    _this.formData.append('images', _this.selectedFiles[i]);
                    _this.urls.push(event.target.result);
                };
                reader.readAsDataURL(this_1.selectedFiles[i]);
            };
            var this_1 = this;
            for (var i = 0; i < this.selectedFiles.length; i++) {
                _loop_1(i);
            }
        }
    };
    EditNewsComponent.prototype.update = function () {
        var _this = this;
        $('.content').find('*').attr('disabled', 'disabled');
        $('.content').find('a').click(function (e) { e.preventDefault(); });
        var data = {
            title: this.form.value.title,
            description: this.form.value.description,
            tags: this.form.value.tags,
            segment: this.form.value.segment,
            pinned: this.pinned,
            is_active: this.is_active
        };
        this.formData.append('data', JSON.stringify(data));
        this.loading = true;
        this.newsService
            .update(this.formData, this.newsId)
            .subscribe(function (result) {
            _this.loading = false;
            var returnUrl = _this.route.snapshot.queryParamMap.get('returnUrl');
            _this.router.navigate(['news']);
        }, function (error) {
            _this.loading = false;
        });
    };
    EditNewsComponent.prototype.getOne = function (id) {
        var _this = this;
        this.loading = true;
        this.newsService
            .getOne(id)
            .subscribe(function (result) {
            _this.loading = false;
            _this.title = result.title;
            _this.description = result.description;
            _this.tags = result.tags;
            _this.segment = result.segment;
            // this.urls = result.images;
            _this.pinned = result.pinned;
            _this.is_active = result.is_active;
            _this.buildForm();
        }, function (error) {
            _this.loading = false;
        });
    };
    EditNewsComponent.prototype.updateSections = function () {
        var _this = this;
        var data = {
            extraSections: {
                // images: this.urls,
                description: this.form.value.extraDescription
            }
        };
        this.formData.append('data', JSON.stringify(data));
        this.loading = true;
        this.newsService
            .update(this.formData, this.newsId)
            .subscribe(function (result) {
            _this.loading = false;
            var returnUrl = _this.route.snapshot.queryParamMap.get('returnUrl');
            _this.urls = [];
            _this.form.controls['extraDescription'].setValue('');
        }, function (error) {
            _this.loading = false;
        });
    };
    EditNewsComponent.prototype.onSelectType = function (type) {
        console.log(type.target);
        var description = document.getElementById('description');
        var file = document.getElementById('fileupload');
        var saveButton = document.getElementById('saveButton');
        if (type.value === 'Text') {
            description.removeAttribute('hidden');
            file.setAttribute('hidden', 'hidden');
        }
        else {
            file.removeAttribute('hidden');
            description.setAttribute('hidden', 'hidden');
        }
        saveButton.removeAttribute('disabled');
    };
    EditNewsComponent.prototype.buildForm = function () {
        this.form = this.fb.group({
            title: [this.title, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            description: [this.description, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            tags: [this.tags],
            segment: [this.segment],
            section: [this.sections],
            is_active: [this.is_active],
            pinned: [this.pinned],
            extraDescription: ['']
        });
    };
    EditNewsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-edit-news',
            template: __webpack_require__(/*! ./edit-news.component.html */ "./src/app/news/edit-news/edit-news.component.html"),
            styles: [__webpack_require__(/*! ./edit-news.component.css */ "./src/app/news/edit-news/edit-news.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _news_service__WEBPACK_IMPORTED_MODULE_3__["NewsService"]])
    ], EditNewsComponent);
    return EditNewsComponent;
}());



/***/ }),

/***/ "./src/app/news/list-news/list-news.component.css":
/*!********************************************************!*\
  !*** ./src/app/news/list-news/list-news.component.css ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL25ld3MvbGlzdC1uZXdzL2xpc3QtbmV3cy5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/news/list-news/list-news.component.html":
/*!*********************************************************!*\
  !*** ./src/app/news/list-news/list-news.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<style>\r\n  .btn {\r\n    background-color: DodgerBlue; /* Blue background */\r\n    border: none; /* Remove borders */\r\n    color: white; /* White text */\r\n    font-size: 10px; /* Set a font size */\r\n    cursor: pointer; /* Mouse pointer on hover */\r\n  }\r\n\r\n  /* Darker background on mouse-over */\r\n  .btn:hover {\r\n    background-color: RoyalBlue;\r\n  }\r\n\r\n  .loader {\r\n    position: absolute;\r\n    left: 50%;\r\n    top: 50%;\r\n    height:60px;\r\n    width:60px;\r\n    margin:0px auto;\r\n    -webkit-animation: rotation .6s infinite linear;\r\n    -moz-animation: rotation .6s infinite linear;\r\n    -o-animation: rotation .6s infinite linear;\r\n    animation: rotation .6s infinite linear;\r\n    border-left:6px solid rgba(0,174,239,.15);\r\n    border-right:6px solid rgba(0,174,239,.15);\r\n    border-bottom:6px solid rgba(0,174,239,.15);\r\n    border-top:6px solid rgba(0,174,239,.8);\r\n    border-radius:100%;\r\n  }\r\n\r\n  @-webkit-keyframes rotation {\r\n    from {-webkit-transform: rotate(0deg);}\r\n    to {-webkit-transform: rotate(359deg);}\r\n  }\r\n  @-moz-keyframes rotation {\r\n    from {-moz-transform: rotate(0deg);}\r\n    to {-moz-transform: rotate(359deg);}\r\n  }\r\n  @-o-keyframes rotation {\r\n    from {-o-transform: rotate(0deg);}\r\n    to {-o-transform: rotate(359deg);}\r\n  }\r\n  @keyframes rotation {\r\n    from {transform: rotate(0deg);}\r\n    to {transform: rotate(359deg);}\r\n  }\r\n</style>\r\n<!--<div class=\"row text-center\">-->\r\n<!--<div class=\"col-lg-4 col-12 alert alert-success mx-auto fs-14\" role=\"alert\" *ngIf=\"successMsg\">-->\r\n<!--<button class=\"close\" data-dismiss=\"alert\"></button>-->\r\n<!--<strong> {{successMsg}}</strong>-->\r\n<!--</div>-->\r\n<!--</div>-->\r\n\r\n<!--<div class=\"row text-center\">-->\r\n<!--<div class=\"col-lg-4 col-12 alert alert-danger mx-auto fs-14\" role=\"alert\" *ngIf=\"errorMsg\">-->\r\n<!--<button class=\"close\" data-dismiss=\"alert\"></button>-->\r\n<!--<strong> {{errorMsg}}</strong>-->\r\n<!--</div>-->\r\n<!--</div>-->\r\n<!-- Content Header (Page header) -->\r\n  <section class=\"content-header\">\r\n    <h1>\r\n      News <button routerLink=\"/news/create-news\" type=\"submit\" class=\"btn btn-primary pull-right\">New</button>\r\n    </h1>\r\n    <ol class=\"breadcrumb\">\r\n      <li><a routerLink=\"/dashboard\"><i class=\"fa fa-dashboard\"></i> Home</a></li>\r\n      <li><a routerLink=\"/news\">News</a></li>\r\n    </ol>\r\n  </section>\r\n\r\n  <!-- Main content -->\r\n<div class=\"loader\" [hidden]=\"!loading\"></div>\r\n  <section class=\"content\">\r\n    <!-- /.row -->\r\n    <div class=\"row\">\r\n      <div class=\"col-xs-12\">\r\n        <div class=\"box\">\r\n          <div class=\"box-header\">\r\n            <h3 class=\"box-title\"></h3>\r\n\r\n            <div class=\"box-tools\">\r\n              <div class=\"input-group input-group-sm\" style=\"width: 150px;\">\r\n                <input (ngModelChange)=\"filter($event)\" [(ngModel)]=\"nameFilter\"  type=\"text\" name=\"table_search\" class=\"form-control pull-right\" placeholder=\"Search\">\r\n\r\n                <div class=\"input-group-btn\">\r\n                  <button type=\"submit\" class=\"btn btn-default\"><i class=\"fa fa-search\"></i></button>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <!-- /.box-header -->\r\n          <div class=\"box-body table-responsive no-padding\">\r\n            <table class=\"table table-hover\">\r\n              <thead>\r\n              <tr>\r\n                <th>Title</th>\r\n                <th>Description</th>\r\n                <th>Status</th>\r\n                <th>Pinned</th>\r\n                <th>Edit/Delete</th>\r\n                <!--<th>Pinned</th>-->\r\n                <!--<th>Interactions</th>-->\r\n              </tr>\r\n              </thead>\r\n              <tbody>\r\n              <!--ui-sref=\"app.supplier.customer.detail({customerId:customer._id})\"-->\r\n              <tr *ngFor=\"let new of news\" class=\"gradeX odd content-table-payment cursor\" role=\"row\">\r\n                <td class=\"mn-130 sorting_1\">{{new.title}}</td>\r\n                <td class=\"mn-130 sorting_1\">{{new.description.substring(1,15)}}.....</td>\r\n                <td class=\"mn-120 sorting_1\">\r\n                  <span *ngIf=\"new.is_active === true\"\r\n                        class=\"label label-success upper-case\">Active</span>\r\n                  <span *ngIf=\"new.is_active === false\"\r\n                        class=\"label label-danger upper-case\">Active</span>\r\n                </td>\r\n                <td class=\"mn-120 sorting_1\">\r\n                  <span *ngIf=\"new.pinned === true\"\r\n                        class=\"label label-success upper-case\">Pinned</span>\r\n                  <span *ngIf=\"new.pinned === false\"\r\n                        class=\"label label-danger upper-case\">Pinned</span>\r\n                </td>\r\n                <td>\r\n                  <button class=\"btn\" routerLink='/news/{{new._id}}/edit-news'><i class=\"fa fa-edit\"></i></button>\r\n                  <button class=\"btn\" (click)=\"delete(new._id)\"><i class=\"fa fa-trash\"></i></button>\r\n                </td>\r\n              </tr>\r\n              </tbody>\r\n            </table>\r\n            <!--<list-pagination total-pages=\"$ctrl.totalPages\" on-page-click=\"$ctrl.setPage(pageNumber)\" current-page=\"$ctrl.currentPage\"></list-pagination>-->\r\n          </div>\r\n          <!-- /.box-body -->\r\n        </div>\r\n        <!-- /.box -->\r\n      </div>\r\n    </div>\r\n  </section>\r\n  <!-- /.content -->\r\n"

/***/ }),

/***/ "./src/app/news/list-news/list-news.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/news/list-news/list-news.component.ts ***!
  \*******************************************************/
/*! exports provided: ListNewsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListNewsComponent", function() { return ListNewsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _news_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../news.service */ "./src/app/news/news.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_3__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ListNewsComponent = /** @class */ (function () {
    function ListNewsComponent(router, route, newsService) {
        this.router = router;
        this.route = route;
        this.newsService = newsService;
        this.ctrl = this;
        this.loading = false;
        this.news = [];
        this.nameFilter = '';
    }
    ListNewsComponent.prototype.ngOnInit = function () {
        this.currentPage = 1;
        this.getAll('');
    };
    ListNewsComponent.prototype.filter = function (value) {
        if (value !== '') {
            this.nameFilter = "title=" + value;
        }
        else {
            this.nameFilter = '';
        }
        this.getAll(this.nameFilter);
    };
    ListNewsComponent.prototype.getAll = function (query) {
        var _this = this;
        this.loading = true;
        $('.content').find('*').attr('disabled', 'disabled');
        $('.content').find('a').click(function (e) { e.preventDefault(); });
        this.newsService
            .getAll(query)
            .subscribe(function (result) {
            _this.loading = false;
            _this.news = result.news;
            _this.totalPages = _this.news.length;
            $('.content').find('*').removeAttr('disabled');
            $('.content').find('a').unbind('click');
            // const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
            // this.router.navigate(['news']);
            // setTimeout(() => {
            //   this.successMsg = 'News Created Successfully';
            // }, 2000);
        }, function (error) {
            _this.loading = false;
            $('.content').find('*').removeAttr('disabled');
            $('.content').find('a').unbind('click');
            // setTimeout(() => {
            //   this.errorMsg = error;
            // }, 2000);
        });
    };
    ListNewsComponent.prototype.delete = function (id) {
        var _this = this;
        this.loading = true;
        sweetalert2__WEBPACK_IMPORTED_MODULE_3___default()({
            type: 'warning',
            title: 'Are you sure to Delete News?',
            text: 'You will not be able to recover the data of News',
            showCancelButton: true,
            confirmButtonColor: '#049F0C',
            cancelButtonColor: '#ff0000',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, keep it'
        }).then(function (result) {
            if (result.value) {
                _this.newsService
                    .delete(id)
                    .subscribe(function (result) {
                    _this.loading = false;
                    _this.getAll('');
                    // const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
                    // this.router.navigate(['news']);
                    // setTimeout(() => {
                    //   this.successMsg = 'News Created Successfully';
                    // }, 2000);
                }, function (error) {
                    _this.loading = false;
                    _this.getAll('');
                    // setTimeout(() => {
                    //   this.errorMsg = error;
                    // }, 2000);
                });
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_3___default()({
                    type: 'info',
                    title: 'Cancelled',
                    text: 'Your News file is safe :)'
                });
            }
        });
    };
    ListNewsComponent.prototype.setPage = function (pageNumber) {
        this.currentPage = pageNumber;
        this.query.page = (pageNumber - 1) * this.query.limit;
        this.getAll(this.query);
    };
    ListNewsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-list-news',
            template: __webpack_require__(/*! ./list-news.component.html */ "./src/app/news/list-news/list-news.component.html"),
            styles: [__webpack_require__(/*! ./list-news.component.css */ "./src/app/news/list-news/list-news.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _news_service__WEBPACK_IMPORTED_MODULE_2__["NewsService"]])
    ], ListNewsComponent);
    return ListNewsComponent;
}());



/***/ }),

/***/ "./src/app/news/news.service.ts":
/*!**************************************!*\
  !*** ./src/app/news/news.service.ts ***!
  \**************************************/
/*! exports provided: NewsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewsService", function() { return NewsService; });
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../constants */ "./src/app/constants.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var NewsService = /** @class */ (function () {
    function NewsService(http) {
        this.http = http;
        this.apiUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_0__["environment"].apiBaseUrl;
        this.userId = null;
        this._isAuthenticated = new rxjs__WEBPACK_IMPORTED_MODULE_4__["BehaviorSubject"](false);
    }
    NewsService.prototype.create = function (data) {
        return this.http.post(this.apiUrl + "/news", data, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    NewsService.prototype.update = function (data, id) {
        return this.http.put(this.apiUrl + "/news/" + id, data, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    NewsService.prototype.getOne = function (id) {
        return this.http.get(this.apiUrl + "/news/" + id, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    NewsService.prototype.getAll = function (query) {
        return this.http.get(this.apiUrl + "/news?" + query, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    NewsService.prototype.delete = function (id) {
        return this.http.delete(this.apiUrl + "/news/" + id, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    NewsService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], NewsService);
    return NewsService;
}());



/***/ }),

/***/ "./src/app/partners/create-partner/create-partner.component.css":
/*!**********************************************************************!*\
  !*** ./src/app/partners/create-partner/create-partner.component.css ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhcnRuZXJzL2NyZWF0ZS1wYXJ0bmVyL2NyZWF0ZS1wYXJ0bmVyLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/partners/create-partner/create-partner.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/partners/create-partner/create-partner.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<!-- Content Header (Page header) -->\r\n<section class=\"content-header\">\r\n  <h1>\r\n    Partner\r\n  </h1>\r\n  <ol class=\"breadcrumb\">\r\n    <li><a routerLink=\"/dashboard\"><i class=\"fa fa-dashboard\"></i> Home</a></li>\r\n    <li><a routerLink=\"/partners\">Partner</a></li>\r\n    <li class=\"active\">Create</li>\r\n  </ol>\r\n</section>\r\n\r\n<!-- Main content -->\r\n<div class=\"loader\" [hidden]=\"!loading\"></div>\r\n<section class=\"content\">\r\n  <div class=\"row\">\r\n    <!-- left column -->\r\n    <div class=\"col-md-12\">\r\n      <!-- general form elements -->\r\n      <div class=\"box box-primary\">\r\n        <div class=\"box-header with-border\">\r\n          <h3 class=\"box-title\">Create New Partner</h3>\r\n        </div>\r\n        <!-- /.box-header -->\r\n        <!-- form start -->\r\n        <form id=\"form-jobs\" class=\"p-t-15\" role=\"form\" [formGroup]=\"form\" (ngSubmit)=\"create()\">\r\n          <div class=\"box-body\">\r\n            <div class=\"form-group has-feedback\">\r\n              <label for=\"exampleInputEmail1\">Email</label>\r\n              <input type=\"email\" formControlName=\"email\" class=\"form-control\" id=\"exampleInputEmail1\" placeholder=\"Enter Email\">\r\n              <span class=\"glyphicon glyphicon-envelope form-control-feedback\"></span>\r\n            </div>\r\n            <div class=\"form-group has-feedback\">\r\n              <label for=\"exampleInputFirstName\">Name</label>\r\n              <input type=\"text\" formControlName=\"name\" class=\"form-control\" id=\"exampleInputFirstName\" placeholder=\"Enter Name\">\r\n              <span class=\"glyphicon glyphicon-user form-control-feedback\"></span>\r\n            </div>\r\n            <div class=\"form-group has-feedback\">\r\n              <label>Mobile</label>\r\n              <input type=\"text\" formControlName=\"mobile\" class=\"form-control\" placeholder=\"Enter Mobile\">\r\n              <span class=\"glyphicon glyphicon-phone form-control-feedback\"></span>\r\n            </div>\r\n            <div class=\"form-group has-feedback\">\r\n              <label>Entity</label>\r\n              <input type=\"text\" formControlName=\"entity\" class=\"form-control\" placeholder=\"Enter Entity\">\r\n              <span class=\"glyphicon glyphicon-phone form-control-feedback\"></span>\r\n            </div>\r\n            <div class=\"form-group has-feedback\">\r\n              <label>Profession</label>\r\n              <input type=\"text\" formControlName=\"profession\" class=\"form-control\" placeholder=\"Enter Profession\">\r\n              <span class=\"glyphicon glyphicon-phone form-control-feedback\"></span>\r\n            </div>\r\n            <div class=\"form-group has-feedback\">\r\n              <label>Interested In</label>\r\n              <input type=\"text\" formControlName=\"interestedIn\" class=\"form-control\" placeholder=\"Enter Interested In\">\r\n              <span class=\"glyphicon glyphicon-phone form-control-feedback\"></span>\r\n            </div>\r\n            <div class=\"form-group has-feedback\">\r\n              <label>Message</label>\r\n              <input type=\"text\" formControlName=\"message\" class=\"form-control\" placeholder=\"Enter Message\">\r\n              <span class=\"glyphicon glyphicon-phone form-control-feedback\"></span>\r\n            </div>\r\n            <label>Upload Image</label>\r\n            <div class=\"form-group\">\r\n              <img *ngFor='let url of urls'  [src]=\"url\" height=\"200\"> <br/>\r\n              <input type='file' (change)=\"onFileChanged($event)\">\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label class=\"hover\">\r\n                <div class=\"icheckbox_flat-green hover\" [ngClass]=\"{'checked': is_active}\" aria-checked=\"true\" aria-disabled=\"false\" style=\"position: relative;\"><input [checked]=\"is_active\" (change)=\"is_active = !is_active\" [value]='is_active' type=\"checkbox\" class=\"flat-red\" checked=\"\" style=\"position: absolute; opacity: 0;\"><ins class=\"iCheck-helper\" style=\"position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;\"></ins></div>\r\n                Active\r\n              </label>\r\n            </div>\r\n          </div>\r\n          <!-- /.box-body -->\r\n\r\n          <div class=\"box-footer\">\r\n            <button type=\"submit\" class=\"btn btn-primary\">Save</button>\r\n          </div>\r\n        </form>\r\n      </div>\r\n      <!-- /.box -->\r\n\r\n    </div>\r\n    <!--/.col (left) -->\r\n  </div>\r\n  <!-- /.row -->\r\n</section>\r\n<!-- /.content -->\r\n"

/***/ }),

/***/ "./src/app/partners/create-partner/create-partner.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/partners/create-partner/create-partner.component.ts ***!
  \*********************************************************************/
/*! exports provided: CreatePartnerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreatePartnerComponent", function() { return CreatePartnerComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _partners_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../partners.service */ "./src/app/partners/partners.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CreatePartnerComponent = /** @class */ (function () {
    function CreatePartnerComponent(fb, router, route, partnerService) {
        this.fb = fb;
        this.router = router;
        this.route = route;
        this.partnerService = partnerService;
        this.ctrl = this;
        this.is_active = false;
        this.loading = false;
        this.selectedFiles = [];
        this.formData = new FormData();
        this.urls = [];
    }
    CreatePartnerComponent.prototype.ngOnInit = function () {
        this.buildForm();
    };
    Object.defineProperty(CreatePartnerComponent.prototype, "email", {
        get: function () {
            return this.form.get('email');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreatePartnerComponent.prototype, "mobile", {
        get: function () {
            return this.form.get('mobile');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreatePartnerComponent.prototype, "profession", {
        get: function () {
            return this.form.get('profession');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreatePartnerComponent.prototype, "entity", {
        get: function () {
            return this.form.get('entity');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreatePartnerComponent.prototype, "interestedIn", {
        get: function () {
            return this.form.get('interestedIn');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreatePartnerComponent.prototype, "message", {
        get: function () {
            return this.form.get('message');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreatePartnerComponent.prototype, "name", {
        get: function () {
            return this.form.get('name');
        },
        enumerable: true,
        configurable: true
    });
    CreatePartnerComponent.prototype.onFileChanged = function (event) {
        var _this = this;
        this.loading = true;
        this.selectedFiles = event.target.files;
        // this.formData.append('images', this.selectedFiles, this.selectedFiles.name);
        if (this.selectedFiles.length > 0) {
            var _loop_1 = function (i) {
                var reader = new FileReader();
                reader.onload = function (event) {
                    _this.formData.append('image', _this.selectedFiles[i]);
                    _this.urls.push(event.target.result);
                };
                reader.readAsDataURL(this_1.selectedFiles[i]);
            };
            var this_1 = this;
            for (var i = 0; i < this.selectedFiles.length; i++) {
                _loop_1(i);
            }
        }
    };
    CreatePartnerComponent.prototype.create = function () {
        var _this = this;
        this.loading = true;
        $('.content').find('*').attr('disabled', 'disabled');
        $('.content').find('a').click(function (e) { e.preventDefault(); });
        var data = {
            email: this.form.value.email,
            name: this.form.value.name,
            mobile: this.form.value.mobile,
            profession: this.form.value.profession,
            entity: this.form.value.entity,
            interestedIn: this.form.value.interestedIn,
            message: this.form.value.message,
            is_active: this.is_active
        };
        this.formData.append('data', JSON.stringify(data));
        this.partnerService
            .create(this.formData)
            .subscribe(function (result) {
            _this.loading = false;
            var returnUrl = _this.route.snapshot.queryParamMap.get('returnUrl');
            _this.router.navigate(['partners']);
        }, function (error) {
            _this.loading = false;
        });
    };
    CreatePartnerComponent.prototype.buildForm = function () {
        this.form = this.fb.group({
            email: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            mobile: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            profession: [''],
            entity: [''],
            interestedIn: [''],
            message: [''],
        });
    };
    CreatePartnerComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-create-partner',
            template: __webpack_require__(/*! ./create-partner.component.html */ "./src/app/partners/create-partner/create-partner.component.html"),
            styles: [__webpack_require__(/*! ./create-partner.component.css */ "./src/app/partners/create-partner/create-partner.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _partners_service__WEBPACK_IMPORTED_MODULE_3__["PartnersService"]])
    ], CreatePartnerComponent);
    return CreatePartnerComponent;
}());



/***/ }),

/***/ "./src/app/partners/edit-partner/edit-partner.component.css":
/*!******************************************************************!*\
  !*** ./src/app/partners/edit-partner/edit-partner.component.css ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhcnRuZXJzL2VkaXQtcGFydG5lci9lZGl0LXBhcnRuZXIuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/partners/edit-partner/edit-partner.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/partners/edit-partner/edit-partner.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<!-- Content Header (Page header) -->\r\n<section class=\"content-header\">\r\n  <h1>\r\n    Partner\r\n  </h1>\r\n  <ol class=\"breadcrumb\">\r\n    <li><a routerLink=\"/dashboard\"><i class=\"fa fa-dashboard\"></i> Home</a></li>\r\n    <li><a routerLink=\"/partners\">Partner</a></li>\r\n    <li class=\"active\">Edit</li>\r\n  </ol>\r\n</section>\r\n\r\n<!-- Main content -->\r\n<div class=\"loader\" [hidden]=\"!loading\"></div>\r\n<section class=\"content\">\r\n  <div class=\"row\">\r\n    <!-- left column -->\r\n    <div class=\"col-md-12\">\r\n      <!-- general form elements -->\r\n      <div class=\"box box-primary\">\r\n        <div class=\"box-header with-border\">\r\n          <h3 class=\"box-title\">Edit Partner</h3>\r\n        </div>\r\n        <!-- /.box-header -->\r\n        <!-- form start -->\r\n        <form id=\"form-jobs\" class=\"p-t-15\" role=\"form\" [formGroup]=\"form\" (ngSubmit)=\"update()\">\r\n          <div class=\"box-body\">\r\n            <div class=\"form-group has-feedback\">\r\n              <label for=\"exampleInputEmail1\">Email</label>\r\n              <input type=\"email\" formControlName=\"email\" class=\"form-control\" id=\"exampleInputEmail1\" placeholder=\"Enter Email\">\r\n              <span class=\"glyphicon glyphicon-envelope form-control-feedback\"></span>\r\n            </div>\r\n            <div class=\"form-group has-feedback\">\r\n              <label>Name</label>\r\n              <input type=\"text\" formControlName=\"name\" class=\"form-control\" id=\"exampleInputFirstName\" placeholder=\"Enter Name\">\r\n              <span class=\"glyphicon glyphicon-user form-control-feedback\"></span>\r\n            </div>\r\n            <div class=\"form-group has-feedback\">\r\n              <label>Mobile</label>\r\n              <input type=\"text\" formControlName=\"mobile\" class=\"form-control\" placeholder=\"Enter Mobile\">\r\n              <span class=\"glyphicon glyphicon-phone form-control-feedback\"></span>\r\n            </div>\r\n            <div class=\"form-group has-feedback\">\r\n              <label>Entity</label>\r\n              <input type=\"text\" formControlName=\"entity\" class=\"form-control\" placeholder=\"Enter Mobile\">\r\n              <span class=\"glyphicon glyphicon-phone form-control-feedback\"></span>\r\n            </div>\r\n            <div class=\"form-group has-feedback\">\r\n              <label>Profession</label>\r\n              <input type=\"text\" formControlName=\"profession\" class=\"form-control\" placeholder=\"Enter Mobile\">\r\n              <span class=\"glyphicon glyphicon-phone form-control-feedback\"></span>\r\n            </div>\r\n            <div class=\"form-group has-feedback\">\r\n              <label>Interested In</label>\r\n              <input type=\"text\" formControlName=\"interestedIn\" class=\"form-control\" placeholder=\"Enter Mobile\">\r\n              <span class=\"glyphicon glyphicon-phone form-control-feedback\"></span>\r\n            </div>\r\n            <div class=\"form-group has-feedback\">\r\n              <label>Message</label>\r\n              <input type=\"text\" formControlName=\"message\" class=\"form-control\" placeholder=\"Enter Mobile\">\r\n              <span class=\"glyphicon glyphicon-phone form-control-feedback\"></span>\r\n            </div>\r\n            <label>Upload Image</label>\r\n            <div class=\"form-group\">\r\n              <img *ngFor='let url of urls'  [src]=\"url\" height=\"200\"> <br/>\r\n              <input type='file' (change)=\"onFileChanged($event)\">\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label class=\"hover\">\r\n                <div class=\"icheckbox_flat-green hover\" [ngClass]=\"{'checked': is_active}\" aria-checked=\"true\" aria-disabled=\"false\" style=\"position: relative;\"><input [checked]=\"is_active\" (change)=\"is_active = !is_active\" [value]='is_active' type=\"checkbox\" class=\"flat-red\" checked=\"\" style=\"position: absolute; opacity: 0;\"><ins class=\"iCheck-helper\" style=\"position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;\"></ins></div>\r\n                Active\r\n              </label>\r\n            </div>\r\n          </div>\r\n          <!-- /.box-body -->\r\n\r\n          <div class=\"box-footer\">\r\n            <button type=\"submit\" class=\"btn btn-primary\">Save</button>\r\n          </div>\r\n        </form>\r\n      </div>\r\n      <!-- /.box -->\r\n\r\n    </div>\r\n    <!--/.col (left) -->\r\n  </div>\r\n  <!-- /.row -->\r\n</section>\r\n<!-- /.content -->\r\n"

/***/ }),

/***/ "./src/app/partners/edit-partner/edit-partner.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/partners/edit-partner/edit-partner.component.ts ***!
  \*****************************************************************/
/*! exports provided: EditPartnerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditPartnerComponent", function() { return EditPartnerComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _partners_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../partners.service */ "./src/app/partners/partners.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var EditPartnerComponent = /** @class */ (function () {
    function EditPartnerComponent(fb, router, route, partnerService) {
        this.fb = fb;
        this.router = router;
        this.route = route;
        this.partnerService = partnerService;
        this.partnerId = this.route.snapshot.params.id;
        this.ctrl = this;
        this.is_active = false;
        this.loading = false;
        this.selectedFiles = [];
        this.email = '';
        this.name = '';
        this.mobile = '';
        this.profession = '';
        this.entity = '';
        this.interestedIn = '';
        this.message = '';
        this.formData = new FormData();
        this.urls = [];
    }
    EditPartnerComponent.prototype.ngOnInit = function () {
        this.buildForm();
        this.getOne(this.partnerId);
    };
    EditPartnerComponent.prototype.onFileChanged = function (event) {
        var _this = this;
        this.loading = true;
        this.selectedFiles = event.target.files;
        // this.formData.append('images', this.selectedFiles, this.selectedFiles.name);
        if (this.selectedFiles.length > 0) {
            var _loop_1 = function (i) {
                var reader = new FileReader();
                reader.onload = function (event) {
                    _this.formData.append('image', _this.selectedFiles[i]);
                    _this.urls.push(event.target.result);
                };
                reader.readAsDataURL(this_1.selectedFiles[i]);
            };
            var this_1 = this;
            for (var i = 0; i < this.selectedFiles.length; i++) {
                _loop_1(i);
            }
        }
    };
    EditPartnerComponent.prototype.update = function () {
        var _this = this;
        $('.content').find('*').attr('disabled', 'disabled');
        $('.content').find('a').click(function (e) { e.preventDefault(); });
        var data = {
            email: this.form.value.email,
            name: this.form.value.name,
            mobile: this.form.value.mobile,
            profession: this.form.value.profession,
            entity: this.form.value.entity,
            interestedIn: this.form.value.interestedIn,
            message: this.form.value.message,
            is_active: this.is_active
        };
        this.formData.append('data', JSON.stringify(data));
        this.loading = true;
        this.partnerService
            .update(this.formData, this.partnerId)
            .subscribe(function (result) {
            _this.loading = false;
            var returnUrl = _this.route.snapshot.queryParamMap.get('returnUrl');
            _this.router.navigate(['partners']);
        }, function (error) {
            _this.loading = false;
        });
    };
    EditPartnerComponent.prototype.getOne = function (id) {
        var _this = this;
        this.loading = true;
        this.partnerService
            .getOne(id)
            .subscribe(function (result) {
            _this.email = result.email;
            _this.name = result.name;
            _this.mobile = result.mobile;
            _this.profession = result.profession;
            _this.entity = result.entity;
            _this.interestedIn = result.interestedIn;
            // this.image =  this.urls;
            _this.message = result.message;
            _this.is_active = result.is_active;
            _this.loading = false;
            _this.buildForm();
        }, function (error) {
            _this.loading = false;
        });
    };
    EditPartnerComponent.prototype.buildForm = function () {
        this.form = this.fb.group({
            email: [this.email, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            name: [this.name, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            mobile: [this.mobile, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            profession: [this.profession],
            entity: [this.entity],
            interestedIn: [this.interestedIn],
            message: [this.message],
        });
    };
    EditPartnerComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-edit-partner',
            template: __webpack_require__(/*! ./edit-partner.component.html */ "./src/app/partners/edit-partner/edit-partner.component.html"),
            styles: [__webpack_require__(/*! ./edit-partner.component.css */ "./src/app/partners/edit-partner/edit-partner.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _partners_service__WEBPACK_IMPORTED_MODULE_3__["PartnersService"]])
    ], EditPartnerComponent);
    return EditPartnerComponent;
}());



/***/ }),

/***/ "./src/app/partners/list-partners/list-partners.component.css":
/*!********************************************************************!*\
  !*** ./src/app/partners/list-partners/list-partners.component.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhcnRuZXJzL2xpc3QtcGFydG5lcnMvbGlzdC1wYXJ0bmVycy5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/partners/list-partners/list-partners.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/partners/list-partners/list-partners.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<style>\r\n  .btn {\r\n    background-color: DodgerBlue; /* Blue background */\r\n    border: none; /* Remove borders */\r\n    color: white; /* White text */\r\n    font-size: 10px; /* Set a font size */\r\n    cursor: pointer; /* Mouse pointer on hover */\r\n  }\r\n\r\n  /* Darker background on mouse-over */\r\n  .btn:hover {\r\n    background-color: RoyalBlue;\r\n  }\r\n\r\n  .loader {\r\n    position: absolute;\r\n    left: 50%;\r\n    top: 50%;\r\n    height:60px;\r\n    width:60px;\r\n    margin:0px auto;\r\n    -webkit-animation: rotation .6s infinite linear;\r\n    -moz-animation: rotation .6s infinite linear;\r\n    -o-animation: rotation .6s infinite linear;\r\n    animation: rotation .6s infinite linear;\r\n    border-left:6px solid rgba(0,174,239,.15);\r\n    border-right:6px solid rgba(0,174,239,.15);\r\n    border-bottom:6px solid rgba(0,174,239,.15);\r\n    border-top:6px solid rgba(0,174,239,.8);\r\n    border-radius:100%;\r\n  }\r\n\r\n  @-webkit-keyframes rotation {\r\n    from {-webkit-transform: rotate(0deg);}\r\n    to {-webkit-transform: rotate(359deg);}\r\n  }\r\n  @-moz-keyframes rotation {\r\n    from {-moz-transform: rotate(0deg);}\r\n    to {-moz-transform: rotate(359deg);}\r\n  }\r\n  @-o-keyframes rotation {\r\n    from {-o-transform: rotate(0deg);}\r\n    to {-o-transform: rotate(359deg);}\r\n  }\r\n  @keyframes rotation {\r\n    from {transform: rotate(0deg);}\r\n    to {transform: rotate(359deg);}\r\n  }\r\n\r\n</style>\r\n<!-- Content Header (Page header) -->\r\n<section class=\"content-header\">\r\n  <h1>\r\n    Partners <button routerLink=\"/partners/create-partner\" type=\"submit\" class=\"btn btn-primary pull-right\">New</button>\r\n  </h1>\r\n  <ol class=\"breadcrumb\">\r\n    <li><a routerLink=\"/dashboard\"><i class=\"fa fa-dashboard\"></i> Home</a></li>\r\n    <li><a routerLink=\"/partners\">Partner</a></li>\r\n  </ol>\r\n</section>\r\n\r\n<!-- Main content -->\r\n<div class=\"loader\" [hidden]=\"!loading\"></div>\r\n<section class=\"content\">\r\n  <!-- /.row -->\r\n  <div class=\"row\">\r\n    <div class=\"col-xs-12\">\r\n      <div class=\"box\">\r\n        <div class=\"box-header\">\r\n          <h3 class=\"box-title\"></h3>\r\n\r\n          <div class=\"box-tools\">\r\n            <div class=\"input-group input-group-sm\" style=\"width: 150px;\">\r\n              <input (ngModelChange)=\"filter($event)\" [(ngModel)]=\"nameFilter\"  type=\"text\" name=\"table_search\" class=\"form-control pull-right\" placeholder=\"Search\">\r\n\r\n              <div class=\"input-group-btn\">\r\n                <button type=\"submit\" class=\"btn btn-default\"><i class=\"fa fa-search\"></i></button>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <!-- /.box-header -->\r\n        <div class=\"box-body table-responsive no-padding\">\r\n          <table class=\"table table-hover\">\r\n            <thead><tr>\r\n              <th>Email</th>\r\n              <th>Name</th>\r\n              <th>Mobile</th>\r\n              <th>Profession</th>\r\n              <th>Entity</th>\r\n              <th>InterestedIn</th>\r\n              <th>Message</th>\r\n              <th>Status</th>\r\n              <th>Edit/Delete</th>\r\n            </tr>\r\n            </thead>\r\n            <tbody>\r\n            <tr *ngFor=\"let partner of partners\" class=\"gradeX odd content-table-payment cursor\" role=\"row\">\r\n              <td class=\"mn-130 sorting_1\">{{partner.email}}</td>\r\n              <td class=\"mn-130 sorting_1\">{{partner.name}}</td>\r\n              <td class=\"mn-130 sorting_1\">{{partner.mobile}}</td>\r\n              <td class=\"mn-130 sorting_1\">{{partner.profession}}</td>\r\n              <td class=\"mn-130 sorting_1\">{{partner.entity}}</td>\r\n              <td class=\"mn-130 sorting_1\">{{partner.interestedIn}}</td>\r\n              <td class=\"mn-130 sorting_1\">{{partner.message}}</td>\r\n              <td class=\"mn-120 sorting_1\">\r\n                  <span *ngIf=\"partner.is_active === true\"\r\n                        class=\"label label-success upper-case\">Active</span>\r\n                <span *ngIf=\"partner.is_active === false\"\r\n                      class=\"label label-danger upper-case\">Active</span>\r\n              </td>\r\n              <td>\r\n                <button class=\"btn\" routerLink='/partners/{{partner._id}}/edit-partner'><i class=\"fa fa-edit\"></i></button>\r\n                <button class=\"btn\" (click)=\"delete(partner._id)\"><i class=\"fa fa-trash\"></i></button>\r\n              </td>\r\n            </tr>\r\n            </tbody></table>\r\n        </div>\r\n        <!-- /.box-body -->\r\n      </div>\r\n      <!-- /.box -->\r\n    </div>\r\n  </div>\r\n</section>\r\n<!-- /.content -->\r\n"

/***/ }),

/***/ "./src/app/partners/list-partners/list-partners.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/partners/list-partners/list-partners.component.ts ***!
  \*******************************************************************/
/*! exports provided: ListPartnersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListPartnersComponent", function() { return ListPartnersComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _partners_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../partners.service */ "./src/app/partners/partners.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_3__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ListPartnersComponent = /** @class */ (function () {
    function ListPartnersComponent(router, route, partnerService) {
        this.router = router;
        this.route = route;
        this.partnerService = partnerService;
        this.ctrl = this;
        this.loading = false;
        this.partners = [];
        this.nameFilter = '';
    }
    ListPartnersComponent.prototype.ngOnInit = function () {
        this.getAll('');
    };
    ListPartnersComponent.prototype.filter = function (value) {
        if (value !== '') {
            this.nameFilter = "email=" + value + "&name=" + value;
        }
        else {
            this.nameFilter = '';
        }
        this.getAll(this.nameFilter);
    };
    ListPartnersComponent.prototype.getAll = function (query) {
        var _this = this;
        this.loading = true;
        $('.content').find('*').attr('disabled', 'disabled');
        $('.content').find('a').click(function (e) { e.preventDefault(); });
        this.partnerService
            .getAll(query)
            .subscribe(function (result) {
            _this.loading = false;
            _this.partners = result.partners;
            $('.content').find('*').removeAttr('disabled');
            $('.content').find('a').unbind('click');
            // const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
            // this.router.navigate(['news']);
            // setTimeout(() => {
            //   this.successMsg = 'News Created Successfully';
            // }, 2000);
        }, function (error) {
            _this.loading = false;
            $('.content').find('*').removeAttr('disabled');
            $('.content').find('a').unbind('click');
            // setTimeout(() => {
            //   this.errorMsg = error;
            // }, 2000);
        });
    };
    ListPartnersComponent.prototype.delete = function (id) {
        var _this = this;
        this.loading = true;
        sweetalert2__WEBPACK_IMPORTED_MODULE_3___default()({
            type: 'warning',
            title: 'Are you sure to Delete Partner?',
            text: 'You will not be able to recover the data of Partner',
            showCancelButton: true,
            confirmButtonColor: '#049F0C',
            cancelButtonColor: '#ff0000',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, keep it'
        }).then(function (result) {
            if (result.value) {
                _this.partnerService
                    .delete(id)
                    .subscribe(function (result) {
                    _this.loading = false;
                    _this.getAll('');
                    // const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
                    // this.router.navigate(['news']);
                    // setTimeout(() => {
                    //   this.successMsg = 'News Created Successfully';
                    // }, 2000);
                }, function (error) {
                    _this.loading = false;
                    _this.getAll('');
                    // setTimeout(() => {
                    //   this.errorMsg = error;
                    // }, 2000);
                });
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_3___default()({
                    type: 'info',
                    title: 'Cancelled',
                    text: 'Your Partner file is safe :)'
                });
            }
        });
    };
    ListPartnersComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-list-partners',
            template: __webpack_require__(/*! ./list-partners.component.html */ "./src/app/partners/list-partners/list-partners.component.html"),
            styles: [__webpack_require__(/*! ./list-partners.component.css */ "./src/app/partners/list-partners/list-partners.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _partners_service__WEBPACK_IMPORTED_MODULE_2__["PartnersService"]])
    ], ListPartnersComponent);
    return ListPartnersComponent;
}());



/***/ }),

/***/ "./src/app/partners/partners.service.ts":
/*!**********************************************!*\
  !*** ./src/app/partners/partners.service.ts ***!
  \**********************************************/
/*! exports provided: PartnersService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PartnersService", function() { return PartnersService; });
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../constants */ "./src/app/constants.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var PartnersService = /** @class */ (function () {
    function PartnersService(http) {
        this.http = http;
        this.apiUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_0__["environment"].apiBaseUrl;
        this.userId = null;
        this._isAuthenticated = new rxjs__WEBPACK_IMPORTED_MODULE_4__["BehaviorSubject"](false);
    }
    PartnersService.prototype.create = function (data) {
        return this.http.post(this.apiUrl + "/partners", data, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    PartnersService.prototype.update = function (data, id) {
        return this.http.put(this.apiUrl + "/partners/" + id, data, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    PartnersService.prototype.getOne = function (id) {
        return this.http.get(this.apiUrl + "/partners/" + id, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    PartnersService.prototype.getAll = function (query) {
        return this.http.get(this.apiUrl + "/partners?" + query, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    PartnersService.prototype.delete = function (id) {
        return this.http.delete(this.apiUrl + "/partners/" + id, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    PartnersService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], PartnersService);
    return PartnersService;
}());



/***/ }),

/***/ "./src/app/programs-applications/create-application/create-application.component.css":
/*!*******************************************************************************************!*\
  !*** ./src/app/programs-applications/create-application/create-application.component.css ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Byb2dyYW1zLWFwcGxpY2F0aW9ucy9jcmVhdGUtYXBwbGljYXRpb24vY3JlYXRlLWFwcGxpY2F0aW9uLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/programs-applications/create-application/create-application.component.html":
/*!********************************************************************************************!*\
  !*** ./src/app/programs-applications/create-application/create-application.component.html ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Content Header (Page header) -->\r\n<section class=\"content-header\">\r\n  <h1>\r\n    Program Question\r\n  </h1>\r\n  <ol class=\"breadcrumb\">\r\n    <li><a href=\"#\"><i class=\"fa fa-dashboard\"></i> Home</a></li>\r\n    <li><a href=\"#\">Programs</a></li>\r\n    <li class=\"active\">Create Question</li>\r\n  </ol>\r\n</section>\r\n\r\n<!-- Main content -->\r\n<div class=\"loader\" [hidden]=\"!loading\"></div>\r\n<section class=\"content\">\r\n  <div class=\"row\">\r\n    <!-- left column -->\r\n    <div class=\"col-md-12\">\r\n      <!-- general form elements -->\r\n      <div class=\"box box-primary\">\r\n        <div class=\"box-header with-border\">\r\n          <h3 class=\"box-title\">Add Program Question</h3>\r\n          <button id=\"addAnswerBtn\" class=\"btn btn-primary pull-right\" (click)=\"AddNewAnswer()\"><span class=\"glyphicon glyphicon-plus\"></span>Add Answer</button>\r\n        </div>\r\n        <!-- /.box-header -->\r\n        <!-- form start -->\r\n        <form id=\"form-programs-application\" class=\"p-t-15\" role=\"form\" [formGroup]=\"form\" (ngSubmit)=\"create()\">\r\n          <div class=\"box-body\">\r\n            <div class=\"form-group\">\r\n              <label for=\"exampleInputEmail1\">Question</label>\r\n              <input formControlName=\"question\" class=\"form-control\" id=\"exampleInputEmail1\" placeholder=\"Enter Question\">\r\n            </div>\r\n            <div id=\"questionAnswers\" class=\"form-group\">\r\n              <label id=\"answerLbl\" hidden=\"hidden\">Answer</label>\r\n              <!--<input class=\"form-control\" id=\"answer1\" placeholder=\"Enter Answer\">-->\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label>Type</label>\r\n              <select formControlName=\"type\" class=\"form-control\" [(ngModel)]=\"this.types[0]\" (change)=\"onSelectType(type)\">\r\n                <option [value]=\"type\" *ngFor=\"let type of types\">\r\n                  {{type}}</option>\r\n              </select>\r\n            </div>\r\n          </div>\r\n          <!-- /.box-body -->\r\n\r\n          <div class=\"box-footer\">\r\n            <button type=\"submit\" class=\"btn btn-primary\">Save</button>\r\n          </div>\r\n        </form>\r\n      </div>\r\n      <!-- /.box -->\r\n\r\n    </div>\r\n    <!--/.col (left) -->\r\n  </div>\r\n  <!-- /.row -->\r\n</section>\r\n<!-- /.content -->\r\n\r\n<style>\r\n  .btn {\r\n    background-color: DodgerBlue; /* Blue background */\r\n    border: none; /* Remove borders */\r\n    color: white; /* White text */\r\n    font-size: 10px; /* Set a font size */\r\n    cursor: pointer; /* Mouse pointer on hover */\r\n  }\r\n\r\n  /* Darker background on mouse-over */\r\n  .btn:hover {\r\n    background-color: RoyalBlue;\r\n  }\r\n</style>\r\n<!-- Content Header (Page header) -->\r\n<section class=\"content-header\">\r\n  <h1>\r\n    Questions\r\n  </h1>\r\n  <ol class=\"breadcrumb\">\r\n    <li><a href=\"#\"><i class=\"fa fa-dashboard\"></i> Home</a></li>\r\n    <li><a href=\"#\">Programs</a></li>\r\n    <li><a href=\"#\">Program Questions</a></li>\r\n  </ol>\r\n</section>\r\n\r\n<!-- Main content -->\r\n<section class=\"content\">\r\n  <!-- /.row -->\r\n  <div class=\"row\">\r\n    <div class=\"col-xs-12\">\r\n      <div class=\"box\">\r\n        <div class=\"box-header\">\r\n          <h3 class=\"box-title\"></h3>\r\n\r\n          <div class=\"box-tools\">\r\n            <div class=\"input-group input-group-sm\" style=\"width: 150px;\">\r\n              <input type=\"text\" name=\"table_search\" class=\"form-control pull-right\" placeholder=\"Search\">\r\n\r\n              <div class=\"input-group-btn\">\r\n                <button type=\"submit\" class=\"btn btn-default\"><i class=\"fa fa-search\"></i></button>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <!-- /.box-header -->\r\n        <div class=\"box-body table-responsive no-padding\">\r\n          <table class=\"table table-hover\">\r\n            <thead>\r\n            <tr>\r\n              <th>Question</th>\r\n              <th># Answers</th>\r\n              <th>Type</th>\r\n            </tr>\r\n            </thead>\r\n            <tbody>\r\n            <tr *ngFor=\"let question of questions\" class=\"gradeX odd content-table-payment cursor\" role=\"row\">\r\n              <td class=\"mn-130 sorting_1\">{{question.question}}</td>\r\n              <td class=\"mn-130 sorting_1\">{{question.answers.length}}</td>\r\n              <td class=\"mn-130 sorting_1\">{{question.type}}</td>\r\n              <td>\r\n                <button class=\"btn\" (click)=\"delete(this.programId, question._id)\"><i class=\"fa fa-trash\"></i></button>\r\n              </td>\r\n            </tr>\r\n            </tbody></table>\r\n        </div>\r\n        <!-- /.box-body -->\r\n      </div>\r\n      <!-- /.box -->\r\n    </div>\r\n  </div>\r\n</section>\r\n<!-- /.content -->\r\n\r\n"

/***/ }),

/***/ "./src/app/programs-applications/create-application/create-application.component.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/programs-applications/create-application/create-application.component.ts ***!
  \******************************************************************************************/
/*! exports provided: CreateProgramApplicationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateProgramApplicationComponent", function() { return CreateProgramApplicationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _programs_programs_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../programs/programs.service */ "./src/app/programs/programs.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CreateProgramApplicationComponent = /** @class */ (function () {
    function CreateProgramApplicationComponent(fb, router, route, programService) {
        this.fb = fb;
        this.router = router;
        this.route = route;
        this.programService = programService;
        this.ctrl = this;
        this.programId = this.route.snapshot.params.id;
        this.types = [
            'Checkbox',
            'RadioButton',
            'Paragraph',
            'Short Answer',
            'Dropdown Menu',
            'File Upload'
        ];
        this.answers = [];
        this.answerCount = 0;
        this.questions = [];
        this.loading = false;
    }
    CreateProgramApplicationComponent.prototype.ngOnInit = function () {
        this.get(this.programId);
        this.buildForm();
    };
    Object.defineProperty(CreateProgramApplicationComponent.prototype, "question", {
        get: function () {
            return this.form.get('question');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreateProgramApplicationComponent.prototype, "answer", {
        get: function () {
            return this.form.get('answer');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreateProgramApplicationComponent.prototype, "type", {
        get: function () {
            return this.form.get('type');
        },
        enumerable: true,
        configurable: true
    });
    CreateProgramApplicationComponent.prototype.create = function () {
        var _this = this;
        var prevElement = document.getElementById('answer' + this.answerCount);
        this.answers.push(prevElement.value);
        $('.content').find('*').attr('disabled', 'disabled');
        $('.content').find('a').click(function (e) { e.preventDefault(); });
        var data = {
            question: this.form.value.question,
            answers: this.answers,
            type: this.form.value.type
        };
        this.loading = true;
        this.programService
            .addQuestion(this.programId, data)
            .subscribe(function (result) {
            _this.loading = false;
            var returnUrl = _this.route.snapshot.queryParamMap.get('returnUrl');
            $('.content').find('*').removeAttr('disabled');
            $('.content').find('a').unbind('click');
            _this.get(_this.programId);
            var parent = document.getElementById('questionAnswers');
            for (var answerIndex = 0; answerIndex <= _this.answerCount; answerIndex += 1) {
                parent.removeChild(parent.childNodes[answerIndex]);
            }
            _this.answerCount = 0;
        }, function (error) {
            _this.loading = false;
            var returnUrl = _this.route.snapshot.queryParamMap.get('returnUrl');
            $('.content').find('*').removeAttr('disabled');
            $('.content').find('a').unbind('click');
            _this.get(_this.programId);
            var parent = document.getElementById('questionAnswers');
            for (var answerIndex = 0; answerIndex <= _this.answerCount; answerIndex += 1) {
                parent.removeChild(parent.childNodes[answerIndex]);
            }
            _this.answerCount = 0;
        });
    };
    CreateProgramApplicationComponent.prototype.get = function (programId) {
        var _this = this;
        this.loading = true;
        this.programService
            .getQuestions(this.programId)
            .subscribe(function (result) {
            _this.loading = false;
            _this.questions = result;
        }, function (error) {
            _this.loading = false;
        });
    };
    CreateProgramApplicationComponent.prototype.delete = function (programsId, questionId) {
        var _this = this;
        this.loading = true;
        this.programService
            .deleteQuestion(programsId, questionId)
            .subscribe(function (result) {
            _this.loading = false;
            _this.get(programsId);
        }, function (error) {
            _this.loading = false;
            _this.get(programsId);
        });
    };
    CreateProgramApplicationComponent.prototype.AddNewAnswer = function () {
        var addAnswerLabel = document.getElementById('answerLbl');
        addAnswerLabel.removeAttribute('hidden');
        var prevElement = document.getElementById('answer' + this.answerCount);
        if (prevElement) {
            this.answers.push(prevElement.value);
        }
        this.answerCount += 1;
        var newAnswer = document.createElement('input');
        newAnswer.setAttribute('type', 'text');
        newAnswer.setAttribute('class', 'form-control');
        newAnswer.setAttribute('placeholder', 'Enter Answer');
        newAnswer.setAttribute('id', 'answer' + (this.answerCount));
        var parent = document.getElementById('questionAnswers');
        parent.appendChild(newAnswer);
    };
    CreateProgramApplicationComponent.prototype.onSelectType = function (type) {
        var addAnswerButton = document.getElementById('addAnswerBtn');
        var addAnswerLabel = document.getElementById('answerLbl');
        if (type.value === 'Short Answer' || type.value === 'Paragraph' || type.value === 'FileUpload') {
            addAnswerButton.setAttribute('disabled', 'disabled');
            addAnswerLabel.setAttribute('hidden', 'hidden');
        }
        else if (type.value === 'Checkbox' || type.value === 'RadioButton' || type.value === 'Dropdown Menu') {
            addAnswerButton.removeAttribute('disabled');
            addAnswerLabel.removeAttribute('hidden');
        }
    };
    CreateProgramApplicationComponent.prototype.buildForm = function () {
        this.form = this.fb.group({
            question: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            answer: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            type: [this.types]
        });
    };
    CreateProgramApplicationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-create-application',
            template: __webpack_require__(/*! ./create-application.component.html */ "./src/app/programs-applications/create-application/create-application.component.html"),
            styles: [__webpack_require__(/*! ./create-application.component.css */ "./src/app/programs-applications/create-application/create-application.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _programs_programs_service__WEBPACK_IMPORTED_MODULE_3__["ProgramsService"]])
    ], CreateProgramApplicationComponent);
    return CreateProgramApplicationComponent;
}());



/***/ }),

/***/ "./src/app/programs/create-program/create-program.component.css":
/*!**********************************************************************!*\
  !*** ./src/app/programs/create-program/create-program.component.css ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Byb2dyYW1zL2NyZWF0ZS1wcm9ncmFtL2NyZWF0ZS1wcm9ncmFtLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/programs/create-program/create-program.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/programs/create-program/create-program.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<!-- Content Header (Page header) -->\r\n<section class=\"content-header\">\r\n  <h1>\r\n    Programs\r\n  </h1>\r\n  <ol class=\"breadcrumb\">\r\n    <li><a routerLink=\"/dashboard\"><i class=\"fa fa-dashboard\"></i> Home</a></li>\r\n    <li><a routerLink=\"/programs\">Programs</a></li>\r\n    <li class=\"active\">Create</li>\r\n  </ol>\r\n</section>\r\n\r\n<!-- Main content -->\r\n<div class=\"loader\" [hidden]=\"!loading\"></div>\r\n<section class=\"content\">\r\n  <div class=\"row\">\r\n    <!-- left column -->\r\n    <div class=\"col-md-12\">\r\n      <!-- general form elements -->\r\n      <div class=\"box box-primary\">\r\n        <div class=\"box-header with-border\">\r\n          <h3 class=\"box-title\">Create New Program</h3>\r\n        </div>\r\n        <!-- /.box-header -->\r\n        <!-- form start -->\r\n        <form id=\"form-programs\" class=\"p-t-15\" role=\"form\" [formGroup]=\"form\" (ngSubmit)=\"create()\">\r\n          <div class=\"box-body\">\r\n            <div class=\"form-group\">\r\n              <label for=\"exampleInputEmail1\">Title</label>\r\n              <input formControlName=\"name\" class=\"form-control\" id=\"exampleInputEmail1\" placeholder=\"Enter Title\">\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label for=\"exampleInputEmail1\">Tags</label>\r\n              <input formControlName=\"tags\" class=\"form-control\" placeholder=\"Enter Tags separated comma\">\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label>Description</label>\r\n              <angular-editor formControlName=\"description\" [config]=\"editorConfig\"></angular-editor>\r\n            </div>\r\n            <label>Upload Image</label>\r\n            <div class=\"form-group\">\r\n              <img *ngFor='let url of urls'  [src]=\"url\" height=\"200\"> <br/>\r\n              <input type='file' (change)=\"onFileChanged($event)\" multiple>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label>Segment</label>\r\n              <select formControlName=\"segment\" class=\"form-control\">\r\n                <option [value]=\"segment.code\" *ngFor=\"let segment of segments\" [selected]=\"segment\">\r\n                  {{segment.color}}</option>\r\n              </select>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label class=\"hover\">\r\n                <div class=\"icheckbox_flat-green hover\" [ngClass]=\"{'checked': pinned}\" aria-checked=\"true\" aria-disabled=\"false\" style=\"position: relative;\"><input [checked]=\"pinned\" (change)=\"pinned = !pinned\" [value]='pinned' type=\"checkbox\" class=\"flat-red\" checked=\"\" style=\"position: absolute; opacity: 0;\"><ins class=\"iCheck-helper\" style=\"position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;\"></ins></div>\r\n                Pin to dashboard\r\n              </label>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label class=\"hover\">\r\n                <div class=\"icheckbox_flat-green hover\" [ngClass]=\"{'checked': is_active}\" aria-checked=\"true\" aria-disabled=\"false\" style=\"position: relative;\"><input [checked]=\"is_active\" (change)=\"is_active = !is_active\" [value]='is_active' type=\"checkbox\" class=\"flat-red\" checked=\"\" style=\"position: absolute; opacity: 0;\"><ins class=\"iCheck-helper\" style=\"position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;\"></ins></div>\r\n                Active\r\n              </label>\r\n            </div>\r\n          </div>\r\n          <!-- /.box-body -->\r\n\r\n          <div class=\"box-footer\">\r\n            <button type=\"submit\" class=\"btn btn-primary\">Save</button>\r\n          </div>\r\n        </form>\r\n      </div>\r\n      <!-- /.box -->\r\n\r\n    </div>\r\n    <!--/.col (left) -->\r\n  </div>\r\n  <!-- /.row -->\r\n</section>\r\n<!-- /.content -->\r\n<script>\r\n  $(function () {\r\n    //Initialize Select2 Elements\r\n    $('.select2').select2()\r\n\r\n    //Datemask dd/mm/yyyy\r\n    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })\r\n    //Datemask2 mm/dd/yyyy\r\n    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })\r\n    //Money Euro\r\n    $('[data-mask]').inputmask()\r\n\r\n    //Date range picker\r\n    $('#reservation').daterangepicker()\r\n    //Date range picker with time picker\r\n    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })\r\n    //Date range as a button\r\n    $('#daterange-btn').daterangepicker(\r\n      {\r\n        ranges   : {\r\n          'Today'       : [moment(), moment()],\r\n          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],\r\n          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],\r\n          'Last 30 Days': [moment().subtract(29, 'days'), moment()],\r\n          'This Month'  : [moment().startOf('month'), moment().endOf('month')],\r\n          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]\r\n        },\r\n        startDate: moment().subtract(29, 'days'),\r\n        endDate  : moment()\r\n      },\r\n      function (start, end) {\r\n        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))\r\n      }\r\n    )\r\n\r\n    //Date picker\r\n    $('#datepicker').datepicker({\r\n      autoclose: true\r\n    })\r\n\r\n    //iCheck for checkbox and radio inputs\r\n    $('input[type=\"checkbox\"].minimal, input[type=\"radio\"].minimal').iCheck({\r\n      checkboxClass: 'icheckbox_minimal-blue',\r\n      radioClass   : 'iradio_minimal-blue'\r\n    })\r\n    //Red color scheme for iCheck\r\n    $('input[type=\"checkbox\"].minimal-red, input[type=\"radio\"].minimal-red').iCheck({\r\n      checkboxClass: 'icheckbox_minimal-red',\r\n      radioClass   : 'iradio_minimal-red'\r\n    })\r\n    //Flat red color scheme for iCheck\r\n    $('input[type=\"checkbox\"].flat-red, input[type=\"radio\"].flat-red').iCheck({\r\n      checkboxClass: 'icheckbox_flat-green',\r\n      radioClass   : 'iradio_flat-green'\r\n    })\r\n\r\n    //Colorpicker\r\n    $('.my-colorpicker1').colorpicker()\r\n    //color picker with addon\r\n    $('.my-colorpicker2').colorpicker()\r\n\r\n    //Timepicker\r\n    $('.timepicker').timepicker({\r\n      showInputs: false\r\n    })\r\n  })\r\n</script>\r\n"

/***/ }),

/***/ "./src/app/programs/create-program/create-program.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/programs/create-program/create-program.component.ts ***!
  \*********************************************************************/
/*! exports provided: CreateProgramComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateProgramComponent", function() { return CreateProgramComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _programs_programs_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../programs/programs.service */ "./src/app/programs/programs.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CreateProgramComponent = /** @class */ (function () {
    function CreateProgramComponent(fb, router, route, programService) {
        this.fb = fb;
        this.router = router;
        this.route = route;
        this.programService = programService;
        this.ctrl = this;
        this.editorConfig = {
            editable: true,
            spellcheck: true,
            height: '25rem',
            minHeight: '5rem',
            placeholder: 'Enter text here...',
            translate: 'no',
            uploadUrl: 'v1/images',
        };
        this.pinned = true;
        this.is_active = false;
        this.loading = false;
        this.segments = [
            { color: 'Red', code: '#ff5a61' },
            { color: 'Blue', code: '#04adc7' },
            { color: 'Orange', code: '#fea601' },
            { color: 'Purple', code: '#8b199b' },
        ];
        this.selectedFiles = [];
        this.formData = new FormData();
        this.urls = [];
    }
    CreateProgramComponent.prototype.ngOnInit = function () {
        this.buildForm();
    };
    Object.defineProperty(CreateProgramComponent.prototype, "title", {
        get: function () {
            return this.form.get('name');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreateProgramComponent.prototype, "description", {
        get: function () {
            return this.form.get('description');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreateProgramComponent.prototype, "tag", {
        get: function () {
            return this.form.get('tag');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreateProgramComponent.prototype, "segment", {
        get: function () {
            return this.form.get('segment');
        },
        enumerable: true,
        configurable: true
    });
    CreateProgramComponent.prototype.onFileChanged = function (event) {
        var _this = this;
        this.loading = true;
        this.selectedFiles = event.target.files;
        // this.formData.append('images', this.selectedFiles, this.selectedFiles.name);
        if (this.selectedFiles.length > 0) {
            var _loop_1 = function (i) {
                var reader = new FileReader();
                reader.onload = function (event) {
                    _this.formData.append('images', _this.selectedFiles[i]);
                    _this.urls.push(event.target.result);
                };
                reader.readAsDataURL(this_1.selectedFiles[i]);
            };
            var this_1 = this;
            for (var i = 0; i < this.selectedFiles.length; i++) {
                _loop_1(i);
            }
        }
    };
    CreateProgramComponent.prototype.create = function () {
        var _this = this;
        this.loading = true;
        $('.content').find('*').attr('disabled', 'disabled');
        $('.content').find('a').click(function (e) { e.preventDefault(); });
        var data = {
            name: this.form.value.name,
            description: this.form.value.description,
            tag: this.form.value.tag,
            segment: this.form.value.segment,
            pinned: this.pinned,
            is_active: this.is_active
        };
        this.formData.append('data', JSON.stringify(data));
        this.programService
            .create(this.formData)
            .subscribe(function (result) {
            _this.loading = false;
            var returnUrl = _this.route.snapshot.queryParamMap.get('returnUrl');
            _this.router.navigate(['programs']);
        }, function (error) {
            _this.loading = false;
        });
    };
    CreateProgramComponent.prototype.buildForm = function () {
        this.form = this.fb.group({
            name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            description: [''],
            tags: [''],
            segment: [this.segments],
        });
    };
    CreateProgramComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-create-program',
            template: __webpack_require__(/*! ./create-program.component.html */ "./src/app/programs/create-program/create-program.component.html"),
            styles: [__webpack_require__(/*! ./create-program.component.css */ "./src/app/programs/create-program/create-program.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _programs_programs_service__WEBPACK_IMPORTED_MODULE_3__["ProgramsService"]])
    ], CreateProgramComponent);
    return CreateProgramComponent;
}());



/***/ }),

/***/ "./src/app/programs/edit-program/edit-program.component.css":
/*!******************************************************************!*\
  !*** ./src/app/programs/edit-program/edit-program.component.css ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Byb2dyYW1zL2VkaXQtcHJvZ3JhbS9lZGl0LXByb2dyYW0uY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/programs/edit-program/edit-program.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/programs/edit-program/edit-program.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<!-- Content Header (Page header) -->\r\n<section class=\"content-header\">\r\n  <h1>\r\n    Programs <button routerLink=\"/programs/{{this.programId}}/applications\" type=\"submit\" class=\"btn btn-primary pull-right\">Add Question</button>\r\n  </h1>\r\n  <ol class=\"breadcrumb\">\r\n    <li><a routerLink=\"/dashboard\"><i class=\"fa fa-dashboard\"></i> Home</a></li>\r\n    <li><a routerLink=\"/programs\">Programs</a></li>\r\n    <li class=\"active\">Edit</li>\r\n  </ol>\r\n</section>\r\n\r\n<!-- Main content -->\r\n<div class=\"loader\" [hidden]=\"!loading\"></div>\r\n<section class=\"content\">\r\n  <div class=\"row\">\r\n    <!-- left column -->\r\n    <div class=\"col-md-12\">\r\n      <!-- general form elements -->\r\n      <div class=\"box box-primary\">\r\n        <div class=\"box-header with-border\">\r\n          <h3 class=\"box-title\">Edit Progam</h3>\r\n        </div>\r\n        <!-- /.box-header -->\r\n        <!-- form start -->\r\n        <form id=\"form-programs\" class=\"p-t-15\" role=\"form\" [formGroup]=\"form\" (ngSubmit)=\"update()\">\r\n          <div class=\"box-body\">\r\n            <div class=\"form-group\">\r\n              <label for=\"exampleInputEmail1\">Title</label>\r\n              <input formControlName=\"name\" class=\"form-control\" id=\"exampleInputEmail1\" placeholder=\"Enter Title\">\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label for=\"exampleInputEmail1\">Tags</label>\r\n              <input formControlName=\"tags\" class=\"form-control\" placeholder=\"Enter Tags separated comma\">\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label>Description</label>\r\n              <angular-editor formControlName=\"description\" [config]=\"editorConfig\"></angular-editor>\r\n            </div>\r\n            <label for=\"exampleInputFile\">Upload Image</label>\r\n            <div class=\"form-group\">\r\n              <img *ngFor=\"let url of urls\" [src]=\"url\" height=\"200\"><br/>\r\n              <input type=\"file\" (change)=\"onFileChanged($event)\" id=\"exampleInputFile\" multiple>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label>Segment</label>\r\n              <select formControlName=\"segment\" class=\"form-control\">\r\n                <option [value]=\"segment.code\" *ngFor=\"let segment of segments\">\r\n                  {{segment.color}}</option>\r\n              </select>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label class=\"\">\r\n                <div class=\"icheckbox_flat-green\" [ngClass]=\"{'checked': pinned}\" aria-checked=\"false\" aria-disabled=\"false\" style=\"position: relative;\"><input [checked]=\"pinned\" (change)=\"pinned = !pinned\" formControlName=\"pinned\" type=\"checkbox\" class=\"flat-red\" checked=\"\" style=\"position: absolute; opacity: 0;\"><ins class=\"iCheck-helper\" style=\"position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;\"></ins></div>\r\n                Pin to dashboard\r\n              </label>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label class=\"\">\r\n                <div class=\"icheckbox_flat-green\" [ngClass]=\"{'checked': is_active}\" aria-checked=\"false\" aria-disabled=\"false\" style=\"position: relative;\"><input [checked]=\"is_active\" (change)=\"is_active = !is_active\" formControlName=\"is_active\" type=\"checkbox\" class=\"flat-red\" checked=\"\" style=\"position: absolute; opacity: 0;\"><ins class=\"iCheck-helper\" style=\"position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;\"></ins></div>\r\n                Active\r\n              </label>\r\n            </div>\r\n          </div>\r\n          <!-- /.box-body -->\r\n\r\n          <div class=\"box-footer\">\r\n            <button type=\"submit\" class=\"btn btn-primary\">Save</button>\r\n          </div>\r\n        </form>\r\n      </div>\r\n      <!-- /.box -->\r\n\r\n    </div>\r\n    <!--/.col (left) -->\r\n  </div>\r\n  <!-- /.row -->\r\n</section>\r\n<!-- /.content -->\r\n\r\n<!-- Extra content -->\r\n<section class=\"content\">\r\n  <div class=\"row\">\r\n    <!-- left column -->\r\n    <div class=\"col-md-12\">\r\n      <!-- general form elements -->\r\n      <div class=\"box box-primary\">\r\n        <form id=\"form-extra-sections-programs\" class=\"p-t-15\" role=\"form\" [formGroup]=\"form\" (ngSubmit)=\"updateSections()\">\r\n        <div class=\"box-header with-border\">\r\n          <h3 class=\"box-title\">Extra Program Sections</h3>\r\n          <div class=\"form-group pull-right\">\r\n            <select formControlName=\"section\" class=\"form-control\" (change)=\"onSelectType(section)\">\r\n            <option [value]=\"section\" *ngFor=\"let section of sections\">\r\n              {{section}}</option>\r\n          </select>\r\n          </div>\r\n        </div>\r\n        <!-- /.box-header -->\r\n        <!-- form start -->\r\n          <div class=\"box-body\">\r\n            <div class=\"form-group\" id=\"description\" hidden=\"hidden\">\r\n              <label>Description</label>\r\n              <input formControlName=\"extraDescription\" class=\"form-control\" placeholder=\"Enter Title\" multiple>\r\n              <!--<angular-editor formControlName=\"extraDescription\" [config]=\"editorConfig\"></angular-editor>-->\r\n            </div>\r\n            <div class=\"form-group\" id=\"fileupload\" hidden=\"hidden\">\r\n              <label for=\"exampleInputFile\">Upload Image</label>\r\n              <img *ngFor=\"let url of urls\" [src]=\"url\" height=\"200\"><br/>\r\n              <input type=\"file\" (change)=\"onFileChanged($event)\" multiple>\r\n            </div>\r\n          </div>\r\n          <!-- /.box-body -->\r\n\r\n          <div class=\"box-footer\">\r\n            <button type=\"submit\" id=\"saveButton\" class=\"btn btn-primary\" disabled=\"disabled\">Save</button>\r\n            <button routerLink=\"/progams\" class=\"btn btn-primary pull-right\">Back to List</button>\r\n          </div>\r\n        </form>\r\n      </div>\r\n      <!-- /.box -->\r\n\r\n    </div>\r\n    <!--/.col (left) -->\r\n  </div>\r\n  <!-- /.row -->\r\n</section>\r\n<!-- /.content -->\r\n"

/***/ }),

/***/ "./src/app/programs/edit-program/edit-program.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/programs/edit-program/edit-program.component.ts ***!
  \*****************************************************************/
/*! exports provided: EditProgramComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditProgramComponent", function() { return EditProgramComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _programs_programs_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../programs/programs.service */ "./src/app/programs/programs.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var EditProgramComponent = /** @class */ (function () {
    function EditProgramComponent(fb, router, route, programService) {
        this.fb = fb;
        this.router = router;
        this.route = route;
        this.programService = programService;
        this.programId = this.route.snapshot.params.id;
        this.editorConfig = {
            editable: true,
            spellcheck: true,
            height: '25rem',
            minHeight: '5rem',
            placeholder: 'Enter text here...',
            translate: 'no',
            uploadUrl: 'v1/images',
        };
        this.pinned = true;
        this.name = '';
        this.description = '';
        this.tags = '';
        this.location = '';
        this.segment = '';
        this.segments = [
            { color: 'Red', code: '#ff5a61' },
            { color: 'Blue', code: '#04adc7' },
            { color: 'Orange', code: '#fea601' },
            { color: 'Purple', code: '#8b199b' },
        ];
        this.sections = ['Text', 'File'];
        this.is_active = false;
        this.loading = false;
        this.selectedFiles = [];
        this.sec = '';
        this.formData = new FormData();
        this.urls = [];
    }
    EditProgramComponent.prototype.ngOnInit = function () {
        this.buildForm();
        this.getOne(this.programId);
    };
    Object.defineProperty(EditProgramComponent.prototype, "section", {
        get: function () {
            return this.form.get('section');
        },
        enumerable: true,
        configurable: true
    });
    EditProgramComponent.prototype.onFileChanged = function (event) {
        var _this = this;
        this.loading = true;
        this.selectedFiles = event.target.files;
        // this.formData.append('images', this.selectedFiles, this.selectedFiles.name);
        if (this.selectedFiles.length > 0) {
            var _loop_1 = function (i) {
                var reader = new FileReader();
                reader.onload = function (event) {
                    _this.formData.append('images', _this.selectedFiles[i]);
                    _this.urls.push(event.target.result);
                };
                reader.readAsDataURL(this_1.selectedFiles[i]);
            };
            var this_1 = this;
            for (var i = 0; i < this.selectedFiles.length; i++) {
                _loop_1(i);
            }
        }
    };
    EditProgramComponent.prototype.update = function () {
        var _this = this;
        $('.content').find('*').attr('disabled', 'disabled');
        $('.content').find('a').click(function (e) { e.preventDefault(); });
        var data = {
            title: this.form.value.name,
            description: this.form.value.description,
            tags: this.form.value.tags,
            segment: this.form.value.segment,
            pinned: this.pinned,
            is_active: this.is_active
        };
        this.formData.append('data', JSON.stringify(data));
        this.loading = true;
        this.programService
            .update(this.formData, this.programId)
            .subscribe(function (result) {
            _this.loading = false;
            var returnUrl = _this.route.snapshot.queryParamMap.get('returnUrl');
            _this.router.navigate(['programs']);
        }, function (error) {
            _this.loading = false;
        });
    };
    EditProgramComponent.prototype.getOne = function (id) {
        var _this = this;
        this.loading = true;
        this.programService
            .getOne(id)
            .subscribe(function (result) {
            _this.loading = false;
            _this.name = result.name;
            _this.description = result.description;
            _this.tags = result.tags;
            _this.segment = result.segment;
            _this.location = result.location;
            _this.dueDate = new Date(result.dueDate);
            _this.pinned = result.pinned;
            _this.is_active = result.is_active;
            _this.buildForm();
        }, function (error) {
            _this.loading = false;
        });
    };
    EditProgramComponent.prototype.updateSections = function () {
        var _this = this;
        var data = {
            extraSections: {
                // images: this.urls,
                description: this.form.value.extraDescription
            }
        };
        this.formData.append('data', JSON.stringify(data));
        this.loading = true;
        this.programService
            .update(this.formData, this.programId)
            .subscribe(function (result) {
            _this.loading = false;
            var returnUrl = _this.route.snapshot.queryParamMap.get('returnUrl');
            _this.urls = [];
            _this.form.controls['extraDescription'].setValue('');
        }, function (error) {
            _this.loading = false;
        });
    };
    EditProgramComponent.prototype.onSelectType = function (type) {
        console.log(type.target);
        var description = document.getElementById('description');
        var file = document.getElementById('fileupload');
        var saveButton = document.getElementById('saveButton');
        if (type.value === 'Text') {
            description.removeAttribute('hidden');
            file.setAttribute('hidden', 'hidden');
        }
        else {
            file.removeAttribute('hidden');
            description.setAttribute('hidden', 'hidden');
        }
        saveButton.removeAttribute('disabled');
    };
    EditProgramComponent.prototype.buildForm = function () {
        this.form = this.fb.group({
            name: [this.name, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            description: [this.description, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            tags: [this.tags],
            segment: [this.segment],
            section: [this.sections],
            is_active: [this.is_active],
            pinned: [this.pinned],
            extraDescription: ['']
        });
    };
    EditProgramComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-edit-program',
            template: __webpack_require__(/*! ./edit-program.component.html */ "./src/app/programs/edit-program/edit-program.component.html"),
            styles: [__webpack_require__(/*! ./edit-program.component.css */ "./src/app/programs/edit-program/edit-program.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _programs_programs_service__WEBPACK_IMPORTED_MODULE_3__["ProgramsService"]])
    ], EditProgramComponent);
    return EditProgramComponent;
}());



/***/ }),

/***/ "./src/app/programs/list-programs/list-programs.component.css":
/*!********************************************************************!*\
  !*** ./src/app/programs/list-programs/list-programs.component.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Byb2dyYW1zL2xpc3QtcHJvZ3JhbXMvbGlzdC1wcm9ncmFtcy5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/programs/list-programs/list-programs.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/programs/list-programs/list-programs.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<style>\r\n  .btn {\r\n    background-color: DodgerBlue; /* Blue background */\r\n    border: none; /* Remove borders */\r\n    color: white; /* White text */\r\n    font-size: 10px; /* Set a font size */\r\n    cursor: pointer; /* Mouse pointer on hover */\r\n  }\r\n\r\n  /* Darker background on mouse-over */\r\n  .btn:hover {\r\n    background-color: RoyalBlue;\r\n  }\r\n  .loader {\r\n    position: absolute;\r\n    left: 50%;\r\n    top: 50%;\r\n    height:60px;\r\n    width:60px;\r\n    margin:0px auto;\r\n    -webkit-animation: rotation .6s infinite linear;\r\n    -moz-animation: rotation .6s infinite linear;\r\n    -o-animation: rotation .6s infinite linear;\r\n    animation: rotation .6s infinite linear;\r\n    border-left:6px solid rgba(0,174,239,.15);\r\n    border-right:6px solid rgba(0,174,239,.15);\r\n    border-bottom:6px solid rgba(0,174,239,.15);\r\n    border-top:6px solid rgba(0,174,239,.8);\r\n    border-radius:100%;\r\n  }\r\n\r\n  @-webkit-keyframes rotation {\r\n    from {-webkit-transform: rotate(0deg);}\r\n    to {-webkit-transform: rotate(359deg);}\r\n  }\r\n  @-moz-keyframes rotation {\r\n    from {-moz-transform: rotate(0deg);}\r\n    to {-moz-transform: rotate(359deg);}\r\n  }\r\n  @-o-keyframes rotation {\r\n    from {-o-transform: rotate(0deg);}\r\n    to {-o-transform: rotate(359deg);}\r\n  }\r\n  @keyframes rotation {\r\n    from {transform: rotate(0deg);}\r\n    to {transform: rotate(359deg);}\r\n  }\r\n</style>\r\n<!-- Content Header (Page header) -->\r\n<section class=\"content-header\">\r\n  <h1>\r\n    Programs <button routerLink=\"/programs/create-program\" type=\"submit\" class=\"btn btn-primary pull-right\">New</button>\r\n  </h1>\r\n  <ol class=\"breadcrumb\">\r\n    <li><a routerLink=\"/dashboard\"><i class=\"fa fa-dashboard\"></i> Home</a></li>\r\n    <li><a routerLink=\"/programs\">Programs</a></li>\r\n  </ol>\r\n</section>\r\n\r\n<!-- Main content -->\r\n<div class=\"loader\" [hidden]=\"!loading\"></div>\r\n<section class=\"content\">\r\n  <!-- /.row -->\r\n  <div class=\"row\">\r\n    <div class=\"col-xs-12\">\r\n      <div class=\"box\">\r\n        <div class=\"box-header\">\r\n          <h3 class=\"box-title\"></h3>\r\n\r\n          <div class=\"box-tools\">\r\n            <div class=\"input-group input-group-sm\" style=\"width: 150px;\">\r\n              <input (ngModelChange)=\"filter($event)\" [(ngModel)]=\"nameFilter\"  type=\"text\" name=\"table_search\" class=\"form-control pull-right\" placeholder=\"Search\">\r\n\r\n              <div class=\"input-group-btn\">\r\n                <button type=\"submit\" class=\"btn btn-default\"><i class=\"fa fa-search\"></i></button>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <!-- /.box-header -->\r\n        <div class=\"box-body table-responsive no-padding\">\r\n          <table class=\"table table-hover\">\r\n            <thead>\r\n            <tr>\r\n              <th>Title</th>\r\n              <th>Description</th>\r\n              <th>Status</th>\r\n              <th>Interactions</th>\r\n            </tr>\r\n            </thead>\r\n            <tbody>\r\n            <tr *ngFor=\"let program of programs\" class=\"gradeX odd content-table-payment cursor\" role=\"row\">\r\n              <td class=\"mn-130 sorting_1\">{{program.name}}</td>\r\n              <td class=\"mn-130 sorting_1\">{{program.description.substring(1,15)}}.....</td>\r\n              <td class=\"mn-120 sorting_1\">\r\n                  <span *ngIf=\"program.is_active === true\"\r\n                        class=\"label label-success upper-case\">Active</span>\r\n                <span *ngIf=\"program.is_active === false\"\r\n                      class=\"label label-danger upper-case\">Active</span>\r\n              </td>\r\n              <td></td>\r\n              <td>\r\n                <button class=\"btn\" routerLink='/programs/{{program._id}}/edit-program'><i class=\"fa fa-edit\"></i></button>\r\n                <button class=\"btn\" (click)=\"delete(program._id)\"><i class=\"fa fa-trash\"></i></button>\r\n              </td>\r\n            </tr>\r\n            </tbody></table>\r\n        </div>\r\n        <!-- /.box-body -->\r\n      </div>\r\n      <!-- /.box -->\r\n    </div>\r\n  </div>\r\n</section>\r\n<!-- /.content -->\r\n"

/***/ }),

/***/ "./src/app/programs/list-programs/list-programs.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/programs/list-programs/list-programs.component.ts ***!
  \*******************************************************************/
/*! exports provided: ListProgramsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListProgramsComponent", function() { return ListProgramsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _programs_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../programs.service */ "./src/app/programs/programs.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_3__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ListProgramsComponent = /** @class */ (function () {
    function ListProgramsComponent(router, route, programService) {
        this.router = router;
        this.route = route;
        this.programService = programService;
        this.ctrl = this;
        this.loading = false;
        this.programs = [];
        this.nameFilter = '';
    }
    ListProgramsComponent.prototype.ngOnInit = function () {
        this.getAll('');
    };
    ListProgramsComponent.prototype.filter = function (value) {
        if (value !== '') {
            this.nameFilter = "name=" + value;
        }
        else {
            this.nameFilter = '';
        }
        this.getAll(this.nameFilter);
    };
    ListProgramsComponent.prototype.getAll = function (query) {
        var _this = this;
        this.loading = true;
        $('.content').find('*').attr('disabled', 'disabled');
        $('.content').find('a').click(function (e) { e.preventDefault(); });
        this.programService
            .getAll(query)
            .subscribe(function (result) {
            _this.loading = false;
            _this.programs = result.programs;
            $('.content').find('*').removeAttr('disabled');
            $('.content').find('a').unbind('click');
            // const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
            // this.router.navigate(['news']);
            // setTimeout(() => {
            //   this.successMsg = 'News Created Successfully';
            // }, 2000);
        }, function (error) {
            _this.loading = false;
            $('.content').find('*').removeAttr('disabled');
            $('.content').find('a').unbind('click');
            // setTimeout(() => {
            //   this.errorMsg = error;
            // }, 2000);
        });
    };
    ListProgramsComponent.prototype.delete = function (id) {
        var _this = this;
        this.loading = true;
        sweetalert2__WEBPACK_IMPORTED_MODULE_3___default()({
            type: 'warning',
            title: 'Are you sure to Delete Program?',
            text: 'You will not be able to recover the data of Program',
            showCancelButton: true,
            confirmButtonColor: '#049F0C',
            cancelButtonColor: '#ff0000',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, keep it'
        }).then(function (result) {
            if (result.value) {
                _this.programService
                    .delete(id)
                    .subscribe(function (result) {
                    _this.loading = false;
                    _this.getAll('');
                    // const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
                    // this.router.navigate(['news']);
                    // setTimeout(() => {
                    //   this.successMsg = 'News Created Successfully';
                    // }, 2000);
                }, function (error) {
                    _this.loading = false;
                    _this.getAll('');
                    // setTimeout(() => {
                    //   this.errorMsg = error;
                    // }, 2000);
                });
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_3___default()({
                    type: 'info',
                    title: 'Cancelled',
                    text: 'Your Program file is safe :)'
                });
            }
        });
    };
    ListProgramsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-list-programs',
            template: __webpack_require__(/*! ./list-programs.component.html */ "./src/app/programs/list-programs/list-programs.component.html"),
            styles: [__webpack_require__(/*! ./list-programs.component.css */ "./src/app/programs/list-programs/list-programs.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _programs_service__WEBPACK_IMPORTED_MODULE_2__["ProgramsService"]])
    ], ListProgramsComponent);
    return ListProgramsComponent;
}());



/***/ }),

/***/ "./src/app/programs/programs.service.ts":
/*!**********************************************!*\
  !*** ./src/app/programs/programs.service.ts ***!
  \**********************************************/
/*! exports provided: ProgramsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProgramsService", function() { return ProgramsService; });
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../constants */ "./src/app/constants.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ProgramsService = /** @class */ (function () {
    function ProgramsService(http) {
        this.http = http;
        this.apiUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_0__["environment"].apiBaseUrl;
        this.userId = null;
        this._isAuthenticated = new rxjs__WEBPACK_IMPORTED_MODULE_4__["BehaviorSubject"](false);
    }
    ProgramsService.prototype.create = function (data) {
        return this.http.post(this.apiUrl + "/programs", data, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    ProgramsService.prototype.update = function (data, id) {
        return this.http.put(this.apiUrl + "/programs/" + id, data, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    ProgramsService.prototype.getOne = function (id) {
        return this.http.get(this.apiUrl + "/programs/" + id, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    ProgramsService.prototype.getAll = function (query) {
        return this.http.get(this.apiUrl + "/programs?" + query, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    ProgramsService.prototype.delete = function (id) {
        return this.http.delete(this.apiUrl + "/programs/" + id, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    ProgramsService.prototype.addQuestion = function (id, data) {
        return this.http.put(this.apiUrl + "/programs/" + id + "/questions", data, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    ProgramsService.prototype.getQuestions = function (id) {
        return this.http.get(this.apiUrl + "/programs/" + id + "/questions", { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    ProgramsService.prototype.deleteQuestion = function (id, questionId) {
        return this.http.delete(this.apiUrl + "/programs/" + id + "/questions/" + questionId, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    ProgramsService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], ProgramsService);
    return ProgramsService;
}());



/***/ }),

/***/ "./src/app/services/create-service/create-service.component.css":
/*!**********************************************************************!*\
  !*** ./src/app/services/create-service/create-service.component.css ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NlcnZpY2VzL2NyZWF0ZS1zZXJ2aWNlL2NyZWF0ZS1zZXJ2aWNlLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/services/create-service/create-service.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/services/create-service/create-service.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<!-- Content Header (Page header) -->\r\n<section class=\"content-header\">\r\n  <h1>\r\n    Services\r\n  </h1>\r\n  <ol class=\"breadcrumb\">\r\n    <li><a routerLink=\"/dashboard\"><i class=\"fa fa-dashboard\"></i> Home</a></li>\r\n    <li><a routerLink=\"/services\">Services</a></li>\r\n    <li class=\"active\">Create</li>\r\n  </ol>\r\n</section>\r\n\r\n<!-- Main content -->\r\n<div class=\"loader\" [hidden]=\"!loading\"></div>\r\n<section class=\"content\">\r\n  <div class=\"row\">\r\n    <!-- left column -->\r\n    <div class=\"col-md-12\">\r\n      <!-- general form elements -->\r\n      <div class=\"box box-primary\">\r\n        <div class=\"box-header with-border\">\r\n          <h3 class=\"box-title\">Create New Service</h3>\r\n        </div>\r\n        <!-- /.box-header -->\r\n        <!-- form start -->\r\n        <form id=\"form-services\" class=\"p-t-15\" role=\"form\" [formGroup]=\"form\" (ngSubmit)=\"create()\">\r\n          <div class=\"box-body\">\r\n            <div class=\"form-group\">\r\n              <label for=\"exampleInputEmail1\">Title</label>\r\n              <input formControlName=\"name\" class=\"form-control\" id=\"exampleInputEmail1\" placeholder=\"Enter Title\">\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label for=\"exampleInputEmail1\">Tags</label>\r\n              <input formControlName=\"tags\" class=\"form-control\" placeholder=\"Enter Tags separated comma\">\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label>Description</label>\r\n              <angular-editor formControlName=\"description\" [config]=\"editorConfig\"></angular-editor>\r\n            </div>\r\n            <label>Upload Image</label>\r\n            <div class=\"form-group\">\r\n              <img *ngFor='let url of urls'  [src]=\"url\" height=\"200\"> <br/>\r\n              <input type='file' (change)=\"onFileChanged($event)\" multiple>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label>Segment</label>\r\n              <select formControlName=\"segment\" class=\"form-control\">\r\n                <option [value]=\"segment.code\" *ngFor=\"let segment of segments\" [selected]=\"segment\">\r\n                  {{segment.color}}</option>\r\n              </select>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label class=\"hover\">\r\n                <div class=\"icheckbox_flat-green hover\" [ngClass]=\"{'checked': pinned}\" aria-checked=\"true\" aria-disabled=\"false\" style=\"position: relative;\"><input [checked]=\"pinned\" (change)=\"pinned = !pinned\" [value]='pinned' type=\"checkbox\" class=\"flat-red\" checked=\"\" style=\"position: absolute; opacity: 0;\"><ins class=\"iCheck-helper\" style=\"position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;\"></ins></div>\r\n                Pin to dashboard\r\n              </label>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label class=\"hover\">\r\n                <div class=\"icheckbox_flat-green hover\" [ngClass]=\"{'checked': is_active}\" aria-checked=\"true\" aria-disabled=\"false\" style=\"position: relative;\"><input [checked]=\"is_active\" (change)=\"is_active = !is_active\" [value]='is_active' type=\"checkbox\" class=\"flat-red\" checked=\"\" style=\"position: absolute; opacity: 0;\"><ins class=\"iCheck-helper\" style=\"position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;\"></ins></div>\r\n                Active\r\n              </label>\r\n            </div>\r\n          </div>\r\n          <!-- /.box-body -->\r\n\r\n          <div class=\"box-footer\">\r\n            <button type=\"submit\" class=\"btn btn-primary\">Save</button>\r\n          </div>\r\n        </form>\r\n      </div>\r\n      <!-- /.box -->\r\n\r\n    </div>\r\n    <!--/.col (left) -->\r\n  </div>\r\n  <!-- /.row -->\r\n</section>\r\n<!-- /.content -->\r\n<script>\r\n  $(function () {\r\n    //Initialize Select2 Elements\r\n    $('.select2').select2()\r\n\r\n    //Datemask dd/mm/yyyy\r\n    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })\r\n    //Datemask2 mm/dd/yyyy\r\n    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })\r\n    //Money Euro\r\n    $('[data-mask]').inputmask()\r\n\r\n    //Date range picker\r\n    $('#reservation').daterangepicker()\r\n    //Date range picker with time picker\r\n    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })\r\n    //Date range as a button\r\n    $('#daterange-btn').daterangepicker(\r\n      {\r\n        ranges   : {\r\n          'Today'       : [moment(), moment()],\r\n          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],\r\n          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],\r\n          'Last 30 Days': [moment().subtract(29, 'days'), moment()],\r\n          'This Month'  : [moment().startOf('month'), moment().endOf('month')],\r\n          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]\r\n        },\r\n        startDate: moment().subtract(29, 'days'),\r\n        endDate  : moment()\r\n      },\r\n      function (start, end) {\r\n        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))\r\n      }\r\n    )\r\n\r\n    //Date picker\r\n    $('#datepicker').datepicker({\r\n      autoclose: true\r\n    })\r\n\r\n    //iCheck for checkbox and radio inputs\r\n    $('input[type=\"checkbox\"].minimal, input[type=\"radio\"].minimal').iCheck({\r\n      checkboxClass: 'icheckbox_minimal-blue',\r\n      radioClass   : 'iradio_minimal-blue'\r\n    })\r\n    //Red color scheme for iCheck\r\n    $('input[type=\"checkbox\"].minimal-red, input[type=\"radio\"].minimal-red').iCheck({\r\n      checkboxClass: 'icheckbox_minimal-red',\r\n      radioClass   : 'iradio_minimal-red'\r\n    })\r\n    //Flat red color scheme for iCheck\r\n    $('input[type=\"checkbox\"].flat-red, input[type=\"radio\"].flat-red').iCheck({\r\n      checkboxClass: 'icheckbox_flat-green',\r\n      radioClass   : 'iradio_flat-green'\r\n    })\r\n\r\n    //Colorpicker\r\n    $('.my-colorpicker1').colorpicker()\r\n    //color picker with addon\r\n    $('.my-colorpicker2').colorpicker()\r\n\r\n    //Timepicker\r\n    $('.timepicker').timepicker({\r\n      showInputs: false\r\n    })\r\n  })\r\n</script>\r\n"

/***/ }),

/***/ "./src/app/services/create-service/create-service.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/services/create-service/create-service.component.ts ***!
  \*********************************************************************/
/*! exports provided: CreateServiceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateServiceComponent", function() { return CreateServiceComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_services_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/services.service */ "./src/app/services/services.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CreateServiceComponent = /** @class */ (function () {
    function CreateServiceComponent(fb, router, route, serviceService) {
        this.fb = fb;
        this.router = router;
        this.route = route;
        this.serviceService = serviceService;
        this.ctrl = this;
        this.editorConfig = {
            editable: true,
            spellcheck: true,
            height: '25rem',
            minHeight: '5rem',
            placeholder: 'Enter text here...',
            translate: 'no',
            uploadUrl: 'v1/images',
        };
        this.pinned = true;
        this.is_active = false;
        this.loading = false;
        this.segments = [
            { color: 'Red', code: '#ff5a61' },
            { color: 'Blue', code: '#04adc7' },
            { color: 'Orange', code: '#fea601' },
            { color: 'Purple', code: '#8b199b' },
        ];
        this.selectedFiles = [];
        this.formData = new FormData();
        this.urls = [];
    }
    CreateServiceComponent.prototype.ngOnInit = function () {
        this.buildForm();
    };
    Object.defineProperty(CreateServiceComponent.prototype, "title", {
        get: function () {
            return this.form.get('name');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreateServiceComponent.prototype, "description", {
        get: function () {
            return this.form.get('description');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreateServiceComponent.prototype, "tags", {
        get: function () {
            return this.form.get('tags');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreateServiceComponent.prototype, "segment", {
        get: function () {
            return this.form.get('segment');
        },
        enumerable: true,
        configurable: true
    });
    CreateServiceComponent.prototype.onFileChanged = function (event) {
        var _this = this;
        this.loading = true;
        this.selectedFiles = event.target.files;
        // this.formData.append('images', this.selectedFiles, this.selectedFiles.name);
        if (this.selectedFiles.length > 0) {
            var _loop_1 = function (i) {
                var reader = new FileReader();
                reader.onload = function (event) {
                    _this.formData.append('images', _this.selectedFiles[i]);
                    _this.urls.push(event.target.result);
                };
                reader.readAsDataURL(this_1.selectedFiles[i]);
            };
            var this_1 = this;
            for (var i = 0; i < this.selectedFiles.length; i++) {
                _loop_1(i);
            }
        }
    };
    CreateServiceComponent.prototype.create = function () {
        var _this = this;
        this.loading = true;
        $('.content').find('*').attr('disabled', 'disabled');
        $('.content').find('a').click(function (e) { e.preventDefault(); });
        this.serviceService
            .create({
            name: this.form.value.name,
            description: this.form.value.description,
            tags: this.form.value.tags,
            segment: this.form.value.segment,
            pinned: this.pinned,
            is_active: this.is_active
        })
            .subscribe(function (result) {
            _this.loading = false;
            var returnUrl = _this.route.snapshot.queryParamMap.get('returnUrl');
            _this.router.navigate(['services']);
        }, function (error) {
            _this.loading = false;
        });
    };
    CreateServiceComponent.prototype.buildForm = function () {
        this.form = this.fb.group({
            name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            description: [''],
            segment: [this.segments],
            tags: ['']
        });
    };
    CreateServiceComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-create-service',
            template: __webpack_require__(/*! ./create-service.component.html */ "./src/app/services/create-service/create-service.component.html"),
            styles: [__webpack_require__(/*! ./create-service.component.css */ "./src/app/services/create-service/create-service.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _services_services_service__WEBPACK_IMPORTED_MODULE_3__["ServicesService"]])
    ], CreateServiceComponent);
    return CreateServiceComponent;
}());



/***/ }),

/***/ "./src/app/services/edit-service/edit-service.component.css":
/*!******************************************************************!*\
  !*** ./src/app/services/edit-service/edit-service.component.css ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NlcnZpY2VzL2VkaXQtc2VydmljZS9lZGl0LXNlcnZpY2UuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/services/edit-service/edit-service.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/services/edit-service/edit-service.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<!-- Content Header (Page header) -->\r\n<section class=\"content-header\">\r\n  <h1>\r\n    Services\r\n  </h1>\r\n  <ol class=\"breadcrumb\">\r\n    <li><a routerLink=\"/dashboard\"><i class=\"fa fa-dashboard\"></i> Home</a></li>\r\n    <li><a routerLink=\"/services\">Services</a></li>\r\n    <li class=\"active\">Edit</li>\r\n  </ol>\r\n</section>\r\n\r\n<!-- Main content -->\r\n<div class=\"loader\" [hidden]=\"!loading\"></div>\r\n<section class=\"content\">\r\n  <div class=\"row\">\r\n    <!-- left column -->\r\n    <div class=\"col-md-12\">\r\n      <!-- general form elements -->\r\n      <div class=\"box box-primary\">\r\n        <div class=\"box-header with-border\">\r\n          <h3 class=\"box-title\">Edit Service</h3>\r\n        </div>\r\n        <!-- /.box-header -->\r\n        <!-- form start -->\r\n        <form id=\"form-programs\" class=\"p-t-15\" role=\"form\" [formGroup]=\"form\" (ngSubmit)=\"update()\">\r\n          <div class=\"box-body\">\r\n            <div class=\"form-group\">\r\n              <label for=\"exampleInputEmail1\">Title</label>\r\n              <input formControlName=\"name\" class=\"form-control\" id=\"exampleInputEmail1\" placeholder=\"Enter Title\">\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label for=\"exampleInputEmail1\">Tags</label>\r\n              <input formControlName=\"tags\" class=\"form-control\" placeholder=\"Enter Tags separated comma\">\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label>Description</label>\r\n              <angular-editor formControlName=\"description\" [config]=\"editorConfig\"></angular-editor>\r\n            </div>\r\n            <label for=\"exampleInputFile\">Upload Image</label>\r\n            <div class=\"form-group\">\r\n              <img *ngFor=\"let url of urls\" [src]=\"url\" height=\"200\"><br/>\r\n              <input type=\"file\" (change)=\"onFileChanged($event)\" id=\"exampleInputFile\" multiple>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label>Segment</label>\r\n              <select formControlName=\"segment\" class=\"form-control\">\r\n                <option [value]=\"segment.code\" *ngFor=\"let segment of segments\">\r\n                  {{segment.color}}</option>\r\n              </select>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label class=\"\">\r\n                <div class=\"icheckbox_flat-green\" [ngClass]=\"{'checked': pinned}\" aria-checked=\"false\" aria-disabled=\"false\" style=\"position: relative;\"><input [checked]=\"pinned\" (change)=\"pinned = !pinned\" formControlName=\"pinned\" type=\"checkbox\" class=\"flat-red\" checked=\"\" style=\"position: absolute; opacity: 0;\"><ins class=\"iCheck-helper\" style=\"position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;\"></ins></div>\r\n                Pin to dashboard\r\n              </label>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label class=\"\">\r\n                <div class=\"icheckbox_flat-green\" [ngClass]=\"{'checked': is_active}\" aria-checked=\"false\" aria-disabled=\"false\" style=\"position: relative;\"><input [checked]=\"is_active\" (change)=\"is_active = !is_active\" formControlName=\"is_active\" type=\"checkbox\" class=\"flat-red\" checked=\"\" style=\"position: absolute; opacity: 0;\"><ins class=\"iCheck-helper\" style=\"position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;\"></ins></div>\r\n                Active\r\n              </label>\r\n            </div>\r\n          </div>\r\n          <!-- /.box-body -->\r\n\r\n          <div class=\"box-footer\">\r\n            <button type=\"submit\" class=\"btn btn-primary\">Save</button>\r\n          </div>\r\n        </form>\r\n      </div>\r\n      <!-- /.box -->\r\n\r\n    </div>\r\n    <!--/.col (left) -->\r\n  </div>\r\n  <!-- /.row -->\r\n</section>\r\n<!-- /.content -->\r\n\r\n<!-- Extra content -->\r\n<section class=\"content\">\r\n  <div class=\"row\">\r\n    <!-- left column -->\r\n    <div class=\"col-md-12\">\r\n      <!-- general form elements -->\r\n      <div class=\"box box-primary\">\r\n        <form id=\"form-extra-sections-services\" class=\"p-t-15\" role=\"form\" [formGroup]=\"form\" (ngSubmit)=\"updateSections()\">\r\n          <div class=\"box-header with-border\">\r\n            <h3 class=\"box-title\">Extra Program Sections</h3>\r\n            <div class=\"form-group pull-right\">\r\n              <select formControlName=\"section\" class=\"form-control\" (change)=\"onSelectType(section)\">\r\n                <option [value]=\"section\" *ngFor=\"let section of sections\">\r\n                  {{section}}</option>\r\n              </select>\r\n            </div>\r\n          </div>\r\n          <!-- /.box-header -->\r\n          <!-- form start -->\r\n          <div class=\"box-body\">\r\n            <div class=\"form-group\" id=\"description\" hidden=\"hidden\">\r\n              <label>Description</label>\r\n              <input formControlName=\"extraDescription\" class=\"form-control\" placeholder=\"Enter Title\" multiple>\r\n              <!--<angular-editor formControlName=\"extraDescription\" [config]=\"editorConfig\"></angular-editor>-->\r\n            </div>\r\n            <div class=\"form-group\" id=\"fileupload\" hidden=\"hidden\">\r\n              <label for=\"exampleInputFile\">Upload Image</label>\r\n              <img *ngFor=\"let url of urls\" [src]=\"url\" height=\"200\"><br/>\r\n              <input type=\"file\" (change)=\"onFileChanged($event)\" multiple>\r\n            </div>\r\n          </div>\r\n          <!-- /.box-body -->\r\n\r\n          <div class=\"box-footer\">\r\n            <button type=\"submit\" id=\"saveButton\" class=\"btn btn-primary\" disabled=\"disabled\">Save</button>\r\n            <button routerLink=\"/services\" class=\"btn btn-primary pull-right\">Back to List</button>\r\n          </div>\r\n        </form>\r\n      </div>\r\n      <!-- /.box -->\r\n\r\n    </div>\r\n    <!--/.col (left) -->\r\n  </div>\r\n  <!-- /.row -->\r\n</section>\r\n<!-- /.content -->\r\n"

/***/ }),

/***/ "./src/app/services/edit-service/edit-service.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/services/edit-service/edit-service.component.ts ***!
  \*****************************************************************/
/*! exports provided: EditServiceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditServiceComponent", function() { return EditServiceComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_services_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/services.service */ "./src/app/services/services.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var EditServiceComponent = /** @class */ (function () {
    function EditServiceComponent(fb, router, route, serviceService) {
        this.fb = fb;
        this.router = router;
        this.route = route;
        this.serviceService = serviceService;
        this.serviceId = this.route.snapshot.params.id;
        this.editorConfig = {
            editable: true,
            spellcheck: true,
            height: '25rem',
            minHeight: '5rem',
            placeholder: 'Enter text here...',
            translate: 'no',
            uploadUrl: 'v1/images',
        };
        this.pinned = true;
        this.name = '';
        this.description = '';
        this.tags = '';
        this.segment = '';
        this.segments = [
            { color: 'Red', code: '#ff5a61' },
            { color: 'Blue', code: '#04adc7' },
            { color: 'Orange', code: '#fea601' },
            { color: 'Purple', code: '#8b199b' },
        ];
        this.sections = ['Text', 'File'];
        this.location = '';
        this.is_active = false;
        this.loading = false;
        this.selectedFiles = [];
        this.formData = new FormData();
        this.urls = [];
    }
    EditServiceComponent.prototype.ngOnInit = function () {
        this.buildForm();
        this.getOne(this.serviceId);
    };
    Object.defineProperty(EditServiceComponent.prototype, "section", {
        get: function () {
            return this.form.get('section');
        },
        enumerable: true,
        configurable: true
    });
    EditServiceComponent.prototype.onFileChanged = function (event) {
        var _this = this;
        this.loading = true;
        this.selectedFiles = event.target.files;
        // this.formData.append('images', this.selectedFiles, this.selectedFiles.name);
        if (this.selectedFiles.length > 0) {
            var _loop_1 = function (i) {
                var reader = new FileReader();
                reader.onload = function (event) {
                    _this.formData.append('images', _this.selectedFiles[i]);
                    _this.urls.push(event.target.result);
                };
                reader.readAsDataURL(this_1.selectedFiles[i]);
            };
            var this_1 = this;
            for (var i = 0; i < this.selectedFiles.length; i++) {
                _loop_1(i);
            }
        }
    };
    EditServiceComponent.prototype.update = function () {
        var _this = this;
        $('.content').find('*').attr('disabled', 'disabled');
        $('.content').find('a').click(function (e) { e.preventDefault(); });
        var data = {
            title: this.form.value.name,
            description: this.form.value.description,
            tags: this.form.value.tags,
            segment: this.form.value.segment,
            pinned: this.pinned,
            is_active: this.is_active
        };
        this.formData.append('data', JSON.stringify(data));
        this.loading = true;
        this.serviceService
            .update(this.formData, this.serviceId)
            .subscribe(function (result) {
            _this.loading = false;
            var returnUrl = _this.route.snapshot.queryParamMap.get('returnUrl');
            _this.router.navigate(['services']);
        }, function (error) {
            _this.loading = false;
        });
    };
    EditServiceComponent.prototype.getOne = function (id) {
        var _this = this;
        this.loading = true;
        this.serviceService
            .getOne(id)
            .subscribe(function (result) {
            _this.loading = false;
            _this.name = result.name;
            _this.description = result.description;
            _this.tags = result.tags;
            _this.segment = result.segment;
            _this.location = result.location;
            _this.dueDate = new Date(result.dueDate);
            // this.urls = result.images;
            _this.pinned = result.pinned;
            _this.is_active = result.is_active;
            _this.buildForm();
        }, function (error) {
            _this.loading = false;
        });
    };
    EditServiceComponent.prototype.updateSections = function () {
        var _this = this;
        var data = {
            extraSections: {
                // images: this.urls,
                description: this.form.value.extraDescription
            }
        };
        this.formData.append('data', JSON.stringify(data));
        this.loading = true;
        this.serviceService
            .update(this.formData, this.serviceId)
            .subscribe(function (result) {
            _this.loading = false;
            var returnUrl = _this.route.snapshot.queryParamMap.get('returnUrl');
            _this.urls = [];
            _this.form.controls['extraDescription'].setValue('');
        }, function (error) {
            _this.loading = false;
        });
    };
    EditServiceComponent.prototype.onSelectType = function (type) {
        console.log(type.target);
        var description = document.getElementById('description');
        var file = document.getElementById('fileupload');
        var saveButton = document.getElementById('saveButton');
        if (type.value === 'Text') {
            description.removeAttribute('hidden');
            file.setAttribute('hidden', 'hidden');
        }
        else {
            file.removeAttribute('hidden');
            description.setAttribute('hidden', 'hidden');
        }
        saveButton.removeAttribute('disabled');
    };
    EditServiceComponent.prototype.buildForm = function () {
        this.form = this.fb.group({
            name: [this.name, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            description: [this.description, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            tags: [this.tags],
            segment: [this.segment],
            section: [this.sections],
            is_active: [this.is_active],
            pinned: [this.pinned],
            extraDescription: ['']
        });
    };
    EditServiceComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-edit-service',
            template: __webpack_require__(/*! ./edit-service.component.html */ "./src/app/services/edit-service/edit-service.component.html"),
            styles: [__webpack_require__(/*! ./edit-service.component.css */ "./src/app/services/edit-service/edit-service.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _services_services_service__WEBPACK_IMPORTED_MODULE_3__["ServicesService"]])
    ], EditServiceComponent);
    return EditServiceComponent;
}());



/***/ }),

/***/ "./src/app/services/list-services/list-services.component.css":
/*!********************************************************************!*\
  !*** ./src/app/services/list-services/list-services.component.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NlcnZpY2VzL2xpc3Qtc2VydmljZXMvbGlzdC1zZXJ2aWNlcy5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/services/list-services/list-services.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/services/list-services/list-services.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<style>\r\n  .btn {\r\n    background-color: DodgerBlue; /* Blue background */\r\n    border: none; /* Remove borders */\r\n    color: white; /* White text */\r\n    font-size: 10px; /* Set a font size */\r\n    cursor: pointer; /* Mouse pointer on hover */\r\n  }\r\n\r\n  /* Darker background on mouse-over */\r\n  .btn:hover {\r\n    background-color: RoyalBlue;\r\n  }\r\n  .loader {\r\n    position: absolute;\r\n    left: 50%;\r\n    top: 50%;\r\n    height:60px;\r\n    width:60px;\r\n    margin:0px auto;\r\n    -webkit-animation: rotation .6s infinite linear;\r\n    -moz-animation: rotation .6s infinite linear;\r\n    -o-animation: rotation .6s infinite linear;\r\n    animation: rotation .6s infinite linear;\r\n    border-left:6px solid rgba(0,174,239,.15);\r\n    border-right:6px solid rgba(0,174,239,.15);\r\n    border-bottom:6px solid rgba(0,174,239,.15);\r\n    border-top:6px solid rgba(0,174,239,.8);\r\n    border-radius:100%;\r\n  }\r\n\r\n  @-webkit-keyframes rotation {\r\n    from {-webkit-transform: rotate(0deg);}\r\n    to {-webkit-transform: rotate(359deg);}\r\n  }\r\n  @-moz-keyframes rotation {\r\n    from {-moz-transform: rotate(0deg);}\r\n    to {-moz-transform: rotate(359deg);}\r\n  }\r\n  @-o-keyframes rotation {\r\n    from {-o-transform: rotate(0deg);}\r\n    to {-o-transform: rotate(359deg);}\r\n  }\r\n  @keyframes rotation {\r\n    from {transform: rotate(0deg);}\r\n    to {transform: rotate(359deg);}\r\n  }\r\n</style>\r\n<!-- Content Header (Page header) -->\r\n<section class=\"content-header\">\r\n  <h1>\r\n    Services <button routerLink=\"/services/create-service\" type=\"submit\" class=\"btn btn-primary pull-right\">New</button>\r\n  </h1>\r\n  <ol class=\"breadcrumb\">\r\n    <li><a routerLink=\"/dashboard\"><i class=\"fa fa-dashboard\"></i> Home</a></li>\r\n    <li><a routerLink=\"/services\">Services</a></li>\r\n  </ol>\r\n</section>\r\n\r\n<!-- Main content -->\r\n<div class=\"loader\" [hidden]=\"!loading\"></div>\r\n<section class=\"content\">\r\n  <!-- /.row -->\r\n  <div class=\"row\">\r\n    <div class=\"col-xs-12\">\r\n      <div class=\"box\">\r\n        <div class=\"box-header\">\r\n          <h3 class=\"box-title\"></h3>\r\n\r\n          <div class=\"box-tools\">\r\n            <div class=\"input-group input-group-sm\" style=\"width: 150px;\">\r\n              <input (ngModelChange)=\"filter($event)\" [(ngModel)]=\"nameFilter\"  type=\"text\" name=\"table_search\" class=\"form-control pull-right\" placeholder=\"Search\">\r\n\r\n              <div class=\"input-group-btn\">\r\n                <button type=\"submit\" class=\"btn btn-default\"><i class=\"fa fa-search\"></i></button>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <!-- /.box-header -->\r\n        <div class=\"box-body table-responsive no-padding\">\r\n          <table class=\"table table-hover\">\r\n            <thead>\r\n            <tr>\r\n              <th>Title</th>\r\n              <th>Description</th>\r\n              <th>Status</th>\r\n              <th>Interactions</th>\r\n            </tr>\r\n            </thead>\r\n            <tbody>\r\n            <tr *ngFor=\"let service of services\" class=\"gradeX odd content-table-payment cursor\" role=\"row\">\r\n              <td class=\"mn-130 sorting_1\">{{service.name}}</td>\r\n              <td class=\"mn-130 sorting_1\">{{service.description.substring(1,15)}}.....</td>\r\n              <td class=\"mn-120 sorting_1\">\r\n                  <span *ngIf=\"service.is_active === true\"\r\n                        class=\"label label-success upper-case\">Active</span>\r\n                <span *ngIf=\"service.is_active === false\"\r\n                      class=\"label label-danger upper-case\">Active</span>\r\n              </td>\r\n              <td></td>\r\n              <td>\r\n                <button class=\"btn\" routerLink='/services/{{service._id}}/edit-service'><i class=\"fa fa-edit\"></i></button>\r\n                <button class=\"btn\" (click)=\"delete(service._id)\"><i class=\"fa fa-trash\"></i></button>\r\n              </td>\r\n            </tr>\r\n            </tbody></table>\r\n        </div>\r\n        <!-- /.box-body -->\r\n      </div>\r\n      <!-- /.box -->\r\n    </div>\r\n  </div>\r\n</section>\r\n<!-- /.content -->\r\n"

/***/ }),

/***/ "./src/app/services/list-services/list-services.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/services/list-services/list-services.component.ts ***!
  \*******************************************************************/
/*! exports provided: ListServicesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListServicesComponent", function() { return ListServicesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services.service */ "./src/app/services/services.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_3__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ListServicesComponent = /** @class */ (function () {
    function ListServicesComponent(router, route, serviceService) {
        this.router = router;
        this.route = route;
        this.serviceService = serviceService;
        this.ctrl = this;
        this.loading = false;
        this.services = [];
        this.nameFilter = '';
    }
    ListServicesComponent.prototype.ngOnInit = function () {
        this.getAll('');
    };
    ListServicesComponent.prototype.filter = function (value) {
        if (value !== '') {
            this.nameFilter = "name=" + value;
        }
        else {
            this.nameFilter = '';
        }
        this.getAll(this.nameFilter);
    };
    ListServicesComponent.prototype.getAll = function (query) {
        var _this = this;
        this.loading = true;
        $('.content').find('*').attr('disabled', 'disabled');
        $('.content').find('a').click(function (e) { e.preventDefault(); });
        this.serviceService
            .getAll(query)
            .subscribe(function (result) {
            _this.loading = false;
            _this.services = result.services;
            $('.content').find('*').removeAttr('disabled');
            $('.content').find('a').unbind('click');
            // const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
            // this.router.navigate(['news']);
            // setTimeout(() => {
            //   this.successMsg = 'News Created Successfully';
            // }, 2000);
        }, function (error) {
            _this.loading = false;
            $('.content').find('*').removeAttr('disabled');
            $('.content').find('a').unbind('click');
            // setTimeout(() => {
            //   this.errorMsg = error;
            // }, 2000);
        });
    };
    ListServicesComponent.prototype.delete = function (id) {
        var _this = this;
        this.loading = true;
        sweetalert2__WEBPACK_IMPORTED_MODULE_3___default()({
            type: 'warning',
            title: 'Are you sure to Delete Service?',
            text: 'You will not be able to recover the data of Service',
            showCancelButton: true,
            confirmButtonColor: '#049F0C',
            cancelButtonColor: '#ff0000',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, keep it'
        }).then(function (result) {
            if (result.value) {
                _this.serviceService
                    .delete(id)
                    .subscribe(function (result) {
                    _this.loading = false;
                    _this.getAll('');
                    // const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
                    // this.router.navigate(['news']);
                    // setTimeout(() => {
                    //   this.successMsg = 'News Created Successfully';
                    // }, 2000);
                }, function (error) {
                    _this.loading = false;
                    _this.getAll('');
                    // setTimeout(() => {
                    //   this.errorMsg = error;
                    // }, 2000);
                });
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_3___default()({
                    type: 'info',
                    title: 'Cancelled',
                    text: 'Your Service file is safe :)'
                });
            }
        });
    };
    ListServicesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-list-services',
            template: __webpack_require__(/*! ./list-services.component.html */ "./src/app/services/list-services/list-services.component.html"),
            styles: [__webpack_require__(/*! ./list-services.component.css */ "./src/app/services/list-services/list-services.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _services_service__WEBPACK_IMPORTED_MODULE_2__["ServicesService"]])
    ], ListServicesComponent);
    return ListServicesComponent;
}());



/***/ }),

/***/ "./src/app/services/services.service.ts":
/*!**********************************************!*\
  !*** ./src/app/services/services.service.ts ***!
  \**********************************************/
/*! exports provided: ServicesService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServicesService", function() { return ServicesService; });
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../constants */ "./src/app/constants.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ServicesService = /** @class */ (function () {
    function ServicesService(http) {
        this.http = http;
        this.apiUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_0__["environment"].apiBaseUrl;
        this.userId = null;
        this._isAuthenticated = new rxjs__WEBPACK_IMPORTED_MODULE_4__["BehaviorSubject"](false);
    }
    ServicesService.prototype.create = function (data) {
        return this.http.post(this.apiUrl + "/services", data, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    ServicesService.prototype.update = function (data, id) {
        return this.http.put(this.apiUrl + "/services/" + id, data, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    ServicesService.prototype.getOne = function (id) {
        return this.http.get(this.apiUrl + "/services/" + id, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    ServicesService.prototype.getAll = function (query) {
        return this.http.get(this.apiUrl + "/services?" + query, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    ServicesService.prototype.delete = function (id) {
        return this.http.delete(this.apiUrl + "/services/" + id, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    ServicesService.prototype.addQuestion = function (id, data) {
        return this.http.put(this.apiUrl + "/services/" + id + "/questions", data, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    ServicesService.prototype.getQuestions = function (id) {
        return this.http.get(this.apiUrl + "/services/" + id + "/questions", { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    ServicesService.prototype.deleteQuestion = function (id, questionId) {
        return this.http.delete(this.apiUrl + "/services/" + id + "/questions/" + questionId, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    ServicesService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], ServicesService);
    return ServicesService;
}());



/***/ }),

/***/ "./src/app/staff/create-staff/create-staff.component.css":
/*!***************************************************************!*\
  !*** ./src/app/staff/create-staff/create-staff.component.css ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3N0YWZmL2NyZWF0ZS1zdGFmZi9jcmVhdGUtc3RhZmYuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/staff/create-staff/create-staff.component.html":
/*!****************************************************************!*\
  !*** ./src/app/staff/create-staff/create-staff.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<!-- Content Header (Page header) -->\r\n<section class=\"content-header\">\r\n  <h1>\r\n    Staff\r\n  </h1>\r\n  <ol class=\"breadcrumb\">\r\n    <li><a routerLink=\"/dashboard\"><i class=\"fa fa-dashboard\"></i> Home</a></li>\r\n    <li><a routerLink=\"/staff\">Staff</a></li>\r\n    <li class=\"active\">Create</li>\r\n  </ol>\r\n</section>\r\n\r\n<!-- Main content -->\r\n<div class=\"loader\" [hidden]=\"!loading\"></div>\r\n<section class=\"content\">\r\n  <div class=\"row\">\r\n    <!-- left column -->\r\n    <div class=\"col-md-12\">\r\n      <!-- general form elements -->\r\n      <div class=\"box box-primary\">\r\n        <div class=\"box-header with-border\">\r\n          <h3 class=\"box-title\">Create New Staff</h3>\r\n        </div>\r\n        <!-- /.box-header -->\r\n        <!-- form start -->\r\n        <form id=\"form-jobs\" class=\"p-t-15\" role=\"form\" [formGroup]=\"form\" (ngSubmit)=\"create()\">\r\n          <div class=\"box-body\">\r\n            <div class=\"form-group has-feedback\">\r\n              <label for=\"exampleInputEmail1\">Email</label>\r\n              <input type=\"email\" formControlName=\"email\" class=\"form-control\" id=\"exampleInputEmail1\" placeholder=\"Enter Email\">\r\n              <span class=\"glyphicon glyphicon-envelope form-control-feedback\"></span>\r\n            </div>\r\n            <div class=\"form-group has-feedback\">\r\n              <label for=\"exampleInputPassword\">Password</label>\r\n              <input type=\"password\" formControlName=\"password\" class=\"form-control\" id=\"exampleInputPassword\" placeholder=\"Enter Password\">\r\n              <span class=\"glyphicon glyphicon-lock form-control-feedback\"></span>\r\n            </div>\r\n            <div class=\"form-group has-feedback\">\r\n              <label for=\"exampleInputFirstName\">First Name</label>\r\n              <input type=\"text\" formControlName=\"first_name\" class=\"form-control\" id=\"exampleInputFirstName\" placeholder=\"Enter First Name\">\r\n              <span class=\"glyphicon glyphicon-user form-control-feedback\"></span>\r\n            </div>\r\n            <div class=\"form-group has-feedback\">\r\n              <label for=\"exampleInputLastName\">Last Name</label>\r\n              <input type=\"text\" formControlName=\"last_name\" class=\"form-control\" id=\"exampleInputLastName\" placeholder=\"Enter Last Name\">\r\n              <span class=\"glyphicon glyphicon-user form-control-feedback\"></span>\r\n            </div>\r\n            <div class=\"form-group has-feedback\">\r\n              <label for=\"exampleInputMobile\">Mobile</label>\r\n              <input type=\"text\" formControlName=\"mobile\" class=\"form-control\" id=\"exampleInputMobile\" placeholder=\"Enter Mobile\">\r\n              <span class=\"glyphicon glyphicon-phone form-control-feedback\"></span>\r\n            </div>\r\n            <div class=\"form-group has-feedback\">\r\n              <label for=\"exampleInputMobile\">Linkedin Url</label>\r\n              <input type=\"text\" formControlName=\"linkedinUrl\" class=\"form-control\" id=\"exampleInputMobile\" placeholder=\"Enter Linkedin Url\">\r\n              <span class=\"glyphicon glyphicon-phone form-control-feedback\"></span>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label>Role</label>\r\n              <select formControlName=\"role\" class=\"form-control\">\r\n                <option [value]=\"role.value\" *ngFor=\"let role of roles\"[selected]=\"role\">\r\n                  {{role.key}}</option>\r\n              </select>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label>Gender</label>\r\n              <select formControlName=\"gender\" class=\"form-control\">\r\n                <option [value]=\"gender\" *ngFor=\"let gender of genders\" [selected]=\"gender\">\r\n                  {{gender}}</option>\r\n              </select>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label>Type</label>\r\n              <select formControlName=\"type\" class=\"form-control\">\r\n                <option [value]=\"type\" *ngFor=\"let type of types\" [selected]=\"type\">\r\n                  {{type}}</option>\r\n              </select>\r\n            </div>\r\n            <label>Upload Image</label>\r\n            <div class=\"form-group\">\r\n              <img *ngFor='let url of urls'  [src]=\"url\" height=\"200\"> <br/>\r\n              <input type='file' (change)=\"onFileChanged($event)\" multiple>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label class=\"hover\">\r\n                <div class=\"icheckbox_flat-green hover\" [ngClass]=\"{'checked': pinned}\" aria-checked=\"true\" aria-disabled=\"false\" style=\"position: relative;\"><input [checked]=\"pinned\" (change)=\"pinned = !pinned\" [value]='pinned' type=\"checkbox\" class=\"flat-red\" checked=\"\" style=\"position: absolute; opacity: 0;\"><ins class=\"iCheck-helper\" style=\"position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;\"></ins></div>\r\n                Pin to dashboard\r\n              </label>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label class=\"hover\">\r\n                <div class=\"icheckbox_flat-green hover\" [ngClass]=\"{'checked': is_active}\" aria-checked=\"true\" aria-disabled=\"false\" style=\"position: relative;\"><input [checked]=\"is_active\" (change)=\"is_active = !is_active\" [value]='is_active' type=\"checkbox\" class=\"flat-red\" checked=\"\" style=\"position: absolute; opacity: 0;\"><ins class=\"iCheck-helper\" style=\"position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;\"></ins></div>\r\n                Active\r\n              </label>\r\n            </div>\r\n          </div>\r\n          <!-- /.box-body -->\r\n\r\n          <div class=\"box-footer\">\r\n            <button type=\"submit\" class=\"btn btn-primary\">Save</button>\r\n          </div>\r\n        </form>\r\n      </div>\r\n      <!-- /.box -->\r\n\r\n    </div>\r\n    <!--/.col (left) -->\r\n  </div>\r\n  <!-- /.row -->\r\n</section>\r\n<!-- /.content -->\r\n"

/***/ }),

/***/ "./src/app/staff/create-staff/create-staff.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/staff/create-staff/create-staff.component.ts ***!
  \**************************************************************/
/*! exports provided: CreateStaffComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateStaffComponent", function() { return CreateStaffComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _staff_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../staff.service */ "./src/app/staff/staff.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CreateStaffComponent = /** @class */ (function () {
    function CreateStaffComponent(fb, router, route, staffService) {
        this.fb = fb;
        this.router = router;
        this.route = route;
        this.staffService = staffService;
        this.ctrl = this;
        this.roles = [
            { key: 'Programs', value: 'programs' },
            { key: 'News', value: 'news' },
            { key: 'Jobs', value: 'jobs' },
            { key: 'Events', value: 'events' },
            { key: 'Staff', value: 'staff' },
            { key: 'Users', value: 'users' },
            { key: 'Services', value: 'services' },
        ];
        this.genders = ['Male', 'Female'];
        this.types = ['staff', 'team'];
        this.pinned = false;
        this.is_active = false;
        this.loading = false;
        this.selectedFiles = [];
        this.formData = new FormData();
        this.urls = [];
    }
    CreateStaffComponent.prototype.ngOnInit = function () {
        this.buildForm();
    };
    Object.defineProperty(CreateStaffComponent.prototype, "email", {
        get: function () {
            return this.form.get('email');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreateStaffComponent.prototype, "password", {
        get: function () {
            return this.form.get('password');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreateStaffComponent.prototype, "first_name", {
        get: function () {
            return this.form.get('first_name');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreateStaffComponent.prototype, "last_name", {
        get: function () {
            return this.form.get('last_name');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreateStaffComponent.prototype, "mobile", {
        get: function () {
            return this.form.get('mobile');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreateStaffComponent.prototype, "role", {
        get: function () {
            return this.form.get('role');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreateStaffComponent.prototype, "gender", {
        get: function () {
            return this.form.get('gender');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreateStaffComponent.prototype, "type", {
        get: function () {
            return this.form.get('type');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreateStaffComponent.prototype, "linkedinUrl", {
        get: function () {
            return this.form.get('linkedinUrl');
        },
        enumerable: true,
        configurable: true
    });
    CreateStaffComponent.prototype.onFileChanged = function (event) {
        var _this = this;
        this.loading = true;
        this.selectedFiles = event.target.files;
        // this.formData.append('images', this.selectedFiles, this.selectedFiles.name);
        if (this.selectedFiles.length > 0) {
            var _loop_1 = function (i) {
                var reader = new FileReader();
                reader.onload = function (event) {
                    _this.formData.append('images', _this.selectedFiles[i]);
                    _this.urls.push(event.target.result);
                };
                reader.readAsDataURL(this_1.selectedFiles[i]);
            };
            var this_1 = this;
            for (var i = 0; i < this.selectedFiles.length; i++) {
                _loop_1(i);
            }
        }
    };
    CreateStaffComponent.prototype.create = function () {
        var _this = this;
        this.loading = true;
        $('.content').find('*').attr('disabled', 'disabled');
        $('.content').find('a').click(function (e) { e.preventDefault(); });
        var data = {
            email: this.form.value.email,
            password: this.form.value.password,
            first_name: this.form.value.first_name,
            last_name: this.form.value.last_name,
            mobile: this.form.value.mobile,
            linkedinUrl: this.form.value.linkedinUrl,
            role: this.form.value.role,
            gender: this.form.value.gender,
            type: this.form.value.type,
            pinned: this.pinned,
            is_active: this.is_active
        };
        this.formData.append('data', JSON.stringify(data));
        this.staffService
            .create(this.formData)
            .subscribe(function (result) {
            _this.loading = false;
            var returnUrl = _this.route.snapshot.queryParamMap.get('returnUrl');
            _this.router.navigate(['staff']);
        }, function (error) {
            _this.loading = false;
        });
    };
    CreateStaffComponent.prototype.buildForm = function () {
        this.form = this.fb.group({
            email: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            first_name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            last_name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            mobile: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            gender: [this.genders],
            type: [this.types],
            role: [this.roles],
            linkedinUrl: [''],
        });
    };
    CreateStaffComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-create-staff',
            template: __webpack_require__(/*! ./create-staff.component.html */ "./src/app/staff/create-staff/create-staff.component.html"),
            styles: [__webpack_require__(/*! ./create-staff.component.css */ "./src/app/staff/create-staff/create-staff.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _staff_service__WEBPACK_IMPORTED_MODULE_3__["StaffService"]])
    ], CreateStaffComponent);
    return CreateStaffComponent;
}());



/***/ }),

/***/ "./src/app/staff/edit-staff/edit-staff.component.css":
/*!***********************************************************!*\
  !*** ./src/app/staff/edit-staff/edit-staff.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3N0YWZmL2VkaXQtc3RhZmYvZWRpdC1zdGFmZi5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/staff/edit-staff/edit-staff.component.html":
/*!************************************************************!*\
  !*** ./src/app/staff/edit-staff/edit-staff.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<!-- Content Header (Page header) -->\r\n<section class=\"content-header\">\r\n  <h1>\r\n    Staff\r\n  </h1>\r\n  <ol class=\"breadcrumb\">\r\n    <li><a routerLink=\"/dashboard\"><i class=\"fa fa-dashboard\"></i> Home</a></li>\r\n    <li><a routerLink=\"/staff\">Staff</a></li>\r\n    <li class=\"active\">Edit</li>\r\n  </ol>\r\n</section>\r\n\r\n<!-- Main content -->\r\n<div class=\"loader\" [hidden]=\"!loading\"></div>\r\n<section class=\"content\">\r\n  <div class=\"row\">\r\n    <!-- left column -->\r\n    <div class=\"col-md-12\">\r\n      <!-- general form elements -->\r\n      <div class=\"box box-primary\">\r\n        <div class=\"box-header with-border\">\r\n          <h3 class=\"box-title\">Edit Staff</h3>\r\n        </div>\r\n        <!-- /.box-header -->\r\n        <!-- form start -->\r\n        <form id=\"form-jobs\" class=\"p-t-15\" role=\"form\" [formGroup]=\"form\" (ngSubmit)=\"update()\">\r\n          <div class=\"box-body\">\r\n            <div class=\"form-group has-feedback\">\r\n              <label for=\"exampleInputEmail1\">Email</label>\r\n              <input type=\"email\" formControlName=\"email\" class=\"form-control\" id=\"exampleInputEmail1\" placeholder=\"Enter Email\">\r\n              <span class=\"glyphicon glyphicon-envelope form-control-feedback\"></span>\r\n            </div>\r\n            <div class=\"form-group has-feedback\">\r\n              <label for=\"exampleInputPassword\">Password</label>\r\n              <input type=\"password\" formControlName=\"password\" class=\"form-control\" id=\"exampleInputPassword\" placeholder=\"Enter Password\">\r\n              <span class=\"glyphicon glyphicon-lock form-control-feedback\"></span>\r\n            </div>\r\n            <div class=\"form-group has-feedback\">\r\n              <label for=\"exampleInputFirstName\">First Name</label>\r\n              <input type=\"text\" formControlName=\"first_name\" class=\"form-control\" id=\"exampleInputFirstName\" placeholder=\"Enter First Name\">\r\n              <span class=\"glyphicon glyphicon-user form-control-feedback\"></span>\r\n            </div>\r\n            <div class=\"form-group has-feedback\">\r\n              <label for=\"exampleInputLastName\">Last Name</label>\r\n              <input type=\"text\" formControlName=\"last_name\" class=\"form-control\" id=\"exampleInputLastName\" placeholder=\"Enter Last Name\">\r\n              <span class=\"glyphicon glyphicon-user form-control-feedback\"></span>\r\n            </div>\r\n            <div class=\"form-group has-feedback\">\r\n              <label>Mobile</label>\r\n              <input type=\"text\" formControlName=\"mobile\" class=\"form-control\" placeholder=\"Enter Mobile\">\r\n              <span class=\"glyphicon glyphicon-phone form-control-feedback\"></span>\r\n            </div>\r\n            <div class=\"form-group has-feedback\">\r\n              <label>Linkedin Url</label>\r\n              <input type=\"text\" formControlName=\"linkedinUrl\" class=\"form-control\" placeholder=\"Enter Linkedin Url\">\r\n              <span class=\"glyphicon glyphicon-phone form-control-feedback\"></span>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label>Role</label>\r\n              <select formControlName=\"role\" class=\"form-control\">\r\n                <option [value]=\"role.value\" *ngFor=\"let role of roles\">\r\n                  {{role.key}}</option>\r\n              </select>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label>Gender</label>\r\n              <select formControlName=\"gender\" class=\"form-control\">\r\n                <option [value]=\"gender\" *ngFor=\"let gender of genders\">\r\n                  {{gender}}</option>\r\n              </select>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label>Type</label>\r\n              <select formControlName=\"type\" class=\"form-control\">\r\n                <option [value]=\"type\" *ngFor=\"let type of types\">\r\n                  {{type}}</option>\r\n              </select>\r\n            </div>\r\n            <label>Upload Image</label>\r\n            <div class=\"form-group\">\r\n              <img *ngFor='let url of urls'  [src]=\"url\" height=\"200\"> <br/>\r\n              <input type='file' (change)=\"onFileChanged($event)\" multiple>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label class=\"hover\">\r\n                <div class=\"icheckbox_flat-green hover\" [ngClass]=\"{'checked': pinned}\" aria-checked=\"true\" aria-disabled=\"false\" style=\"position: relative;\"><input [checked]=\"pinned\" (change)=\"pinned = !pinned\" [value]='pinned' type=\"checkbox\" class=\"flat-red\" checked=\"\" style=\"position: absolute; opacity: 0;\"><ins class=\"iCheck-helper\" style=\"position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;\"></ins></div>\r\n                Pin to dashboard\r\n              </label>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label class=\"hover\">\r\n                <div class=\"icheckbox_flat-green hover\" [ngClass]=\"{'checked': is_active}\" aria-checked=\"true\" aria-disabled=\"false\" style=\"position: relative;\"><input [checked]=\"is_active\" (change)=\"is_active = !is_active\" [value]='is_active' type=\"checkbox\" class=\"flat-red\" checked=\"\" style=\"position: absolute; opacity: 0;\"><ins class=\"iCheck-helper\" style=\"position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;\"></ins></div>\r\n                Active\r\n              </label>\r\n            </div>\r\n          </div>\r\n          <!-- /.box-body -->\r\n\r\n          <div class=\"box-footer\">\r\n            <button type=\"submit\" class=\"btn btn-primary\">Save</button>\r\n          </div>\r\n        </form>\r\n      </div>\r\n      <!-- /.box -->\r\n\r\n    </div>\r\n    <!--/.col (left) -->\r\n  </div>\r\n  <!-- /.row -->\r\n</section>\r\n<!-- /.content -->\r\n"

/***/ }),

/***/ "./src/app/staff/edit-staff/edit-staff.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/staff/edit-staff/edit-staff.component.ts ***!
  \**********************************************************/
/*! exports provided: EditStaffComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditStaffComponent", function() { return EditStaffComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _staff_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../staff.service */ "./src/app/staff/staff.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var EditStaffComponent = /** @class */ (function () {
    function EditStaffComponent(fb, router, route, staffService) {
        this.fb = fb;
        this.router = router;
        this.route = route;
        this.staffService = staffService;
        this.staffId = this.route.snapshot.params.id;
        this.email = '';
        this.password = '';
        this.first_name = '';
        this.last_name = '';
        this.mobile = '';
        this.linkedinUrl = '';
        this.role = '';
        this.gender = '';
        this.type = '';
        this.image = [];
        this.roles = [
            { key: 'Programs', value: 'programs' },
            { key: 'News', value: 'news' },
            { key: 'Jobs', value: 'jobs' },
            { key: 'Events', value: 'events' },
            { key: 'Staff', value: 'staff' },
            { key: 'Users', value: 'users' },
            { key: 'Services', value: 'services' },
        ];
        this.genders = ['Male', 'Female'];
        this.types = ['staff', 'team'];
        this.pinned = false;
        this.is_active = false;
        this.loading = false;
        this.selectedFiles = [];
        this.formData = new FormData();
        this.urls = [];
    }
    EditStaffComponent.prototype.ngOnInit = function () {
        this.buildForm();
        this.getOne(this.staffId);
    };
    EditStaffComponent.prototype.onFileChanged = function (event) {
        var _this = this;
        this.loading = true;
        this.selectedFiles = event.target.files;
        // this.formData.append('images', this.selectedFiles, this.selectedFiles.name);
        if (this.selectedFiles.length > 0) {
            var _loop_1 = function (i) {
                var reader = new FileReader();
                reader.onload = function (event) {
                    _this.formData.append('images', _this.selectedFiles[i]);
                    _this.urls.push(event.target.result);
                };
                reader.readAsDataURL(this_1.selectedFiles[i]);
            };
            var this_1 = this;
            for (var i = 0; i < this.selectedFiles.length; i++) {
                _loop_1(i);
            }
        }
    };
    EditStaffComponent.prototype.update = function () {
        var _this = this;
        $('.content').find('*').attr('disabled', 'disabled');
        $('.content').find('a').click(function (e) { e.preventDefault(); });
        var data = {
            email: this.form.value.email,
            first_name: this.form.value.first_name,
            last_name: this.form.value.last_name,
            mobile: this.form.value.mobile,
            linkedinUrl: this.form.value.linkedinUrl,
            role: this.form.value.role,
            gender: this.form.value.gender,
            types: this.form.value.type,
            pinned: this.pinned,
            is_active: this.is_active
        };
        this.formData.append('data', JSON.stringify(data));
        this.loading = true;
        this.staffService
            .update(this.formData, this.staffId)
            .subscribe(function (result) {
            _this.loading = false;
            var returnUrl = _this.route.snapshot.queryParamMap.get('returnUrl');
            _this.router.navigate(['staff']);
        }, function (error) {
            _this.loading = false;
        });
    };
    EditStaffComponent.prototype.getOne = function (id) {
        var _this = this;
        this.loading = true;
        this.staffService
            .getOne(id)
            .subscribe(function (result) {
            _this.email = result.email;
            _this.password = result.password;
            _this.first_name = result.first_name;
            _this.last_name = result.last_name;
            _this.mobile = result.mobile;
            _this.linkedinUrl = result.linkedinUrl;
            _this.role = result.role;
            _this.gender = result.gender;
            _this.type = result.type;
            _this.image = _this.urls;
            _this.pinned = result.pinned;
            _this.is_active = result.is_active;
            _this.loading = false;
            _this.buildForm();
        }, function (error) {
            _this.loading = false;
        });
    };
    EditStaffComponent.prototype.buildForm = function () {
        this.form = this.fb.group({
            email: [this.email, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            password: [this.password, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            first_name: [this.first_name, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            last_name: [this.last_name, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            mobile: [this.mobile, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            gender: [this.gender],
            type: [this.type],
            role: [this.roles],
            linkedinUrl: [this.linkedinUrl],
        });
    };
    EditStaffComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-edit-staff',
            template: __webpack_require__(/*! ./edit-staff.component.html */ "./src/app/staff/edit-staff/edit-staff.component.html"),
            styles: [__webpack_require__(/*! ./edit-staff.component.css */ "./src/app/staff/edit-staff/edit-staff.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _staff_service__WEBPACK_IMPORTED_MODULE_3__["StaffService"]])
    ], EditStaffComponent);
    return EditStaffComponent;
}());



/***/ }),

/***/ "./src/app/staff/list-staff/list-staff.component.css":
/*!***********************************************************!*\
  !*** ./src/app/staff/list-staff/list-staff.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3N0YWZmL2xpc3Qtc3RhZmYvbGlzdC1zdGFmZi5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/staff/list-staff/list-staff.component.html":
/*!************************************************************!*\
  !*** ./src/app/staff/list-staff/list-staff.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<style>\r\n  .btn {\r\n    background-color: DodgerBlue; /* Blue background */\r\n    border: none; /* Remove borders */\r\n    color: white; /* White text */\r\n    font-size: 10px; /* Set a font size */\r\n    cursor: pointer; /* Mouse pointer on hover */\r\n  }\r\n\r\n  /* Darker background on mouse-over */\r\n  .btn:hover {\r\n    background-color: RoyalBlue;\r\n  }\r\n  .loader {\r\n    position: absolute;\r\n    left: 50%;\r\n    top: 50%;\r\n    height:60px;\r\n    width:60px;\r\n    margin:0px auto;\r\n    -webkit-animation: rotation .6s infinite linear;\r\n    -moz-animation: rotation .6s infinite linear;\r\n    -o-animation: rotation .6s infinite linear;\r\n    animation: rotation .6s infinite linear;\r\n    border-left:6px solid rgba(0,174,239,.15);\r\n    border-right:6px solid rgba(0,174,239,.15);\r\n    border-bottom:6px solid rgba(0,174,239,.15);\r\n    border-top:6px solid rgba(0,174,239,.8);\r\n    border-radius:100%;\r\n  }\r\n\r\n  @-webkit-keyframes rotation {\r\n    from {-webkit-transform: rotate(0deg);}\r\n    to {-webkit-transform: rotate(359deg);}\r\n  }\r\n  @-moz-keyframes rotation {\r\n    from {-moz-transform: rotate(0deg);}\r\n    to {-moz-transform: rotate(359deg);}\r\n  }\r\n  @-o-keyframes rotation {\r\n    from {-o-transform: rotate(0deg);}\r\n    to {-o-transform: rotate(359deg);}\r\n  }\r\n  @keyframes rotation {\r\n    from {transform: rotate(0deg);}\r\n    to {transform: rotate(359deg);}\r\n  }\r\n</style>\r\n<!-- Content Header (Page header) -->\r\n<section class=\"content-header\">\r\n  <h1>\r\n    Staff <button routerLink=\"/staff/create-staff\" type=\"submit\" class=\"btn btn-primary pull-right\">New</button>\r\n  </h1>\r\n  <ol class=\"breadcrumb\">\r\n    <li><a routerLink=\"/dashboard\"><i class=\"fa fa-dashboard\"></i> Home</a></li>\r\n    <li><a routerLink=\"/staff\">Staff</a></li>\r\n  </ol>\r\n</section>\r\n\r\n<!-- Main content -->\r\n<div class=\"loader\" [hidden]=\"!loading\"></div>\r\n<section class=\"content\">\r\n  <!-- /.row -->\r\n  <div class=\"row\">\r\n    <div class=\"col-xs-12\">\r\n      <div class=\"box\">\r\n        <div class=\"box-header\">\r\n          <h3 class=\"box-title\"></h3>\r\n\r\n          <div class=\"box-tools\">\r\n            <div class=\"input-group input-group-sm\" style=\"width: 150px;\">\r\n              <input (ngModelChange)=\"filter($event)\" [(ngModel)]=\"nameFilter\"  type=\"text\" name=\"table_search\" class=\"form-control pull-right\" placeholder=\"Search\">\r\n\r\n              <div class=\"input-group-btn\">\r\n                <button type=\"submit\" class=\"btn btn-default\"><i class=\"fa fa-search\"></i></button>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <!-- /.box-header -->\r\n        <div class=\"box-body table-responsive no-padding\">\r\n          <table class=\"table table-hover\">\r\n            <thead><tr>\r\n              <th>Email</th>\r\n              <th>First Name</th>\r\n              <th>Last Name</th>\r\n              <th>Mobile</th>\r\n              <th>Gender</th>\r\n              <th>Status</th>\r\n              <th>Pinned</th>\r\n              <th>Edit/Delete</th>\r\n            </tr>\r\n            </thead>\r\n            <tbody>\r\n            <tr *ngFor=\"let staff of employees\" class=\"gradeX odd content-table-payment cursor\" role=\"row\">\r\n              <td class=\"mn-130 sorting_1\">{{staff.email}}</td>\r\n              <td class=\"mn-130 sorting_1\">{{staff.first_name}}</td>\r\n              <td class=\"mn-130 sorting_1\">{{staff.last_name}}</td>\r\n              <td class=\"mn-130 sorting_1\">{{staff.mobile}}</td>\r\n              <td class=\"mn-130 sorting_1\">{{staff.gender}}</td>\r\n              <td class=\"mn-120 sorting_1\">\r\n                  <span *ngIf=\"staff.is_active === true\"\r\n                        class=\"label label-success upper-case\">Active</span>\r\n                <span *ngIf=\"staff.is_active === false\"\r\n                      class=\"label label-danger upper-case\">Active</span>\r\n              </td>\r\n              <td class=\"mn-120 sorting_1\">\r\n                  <span *ngIf=\"staff.pinned === true\"\r\n                        class=\"label label-success upper-case\">Pinned</span>\r\n                <span *ngIf=\"staff.pinned === false\"\r\n                      class=\"label label-danger upper-case\">Pinned</span>\r\n              </td>\r\n              <td>\r\n                <button class=\"btn\" routerLink='/staff/{{staff._id}}/edit-staff'><i class=\"fa fa-edit\"></i></button>\r\n                <button class=\"btn\" (click)=\"delete(staff._id)\"><i class=\"fa fa-trash\"></i></button>\r\n              </td>\r\n            </tr>\r\n            </tbody>\r\n          </table>\r\n        </div>\r\n        <!-- /.box-body -->\r\n      </div>\r\n      <!-- /.box -->\r\n    </div>\r\n  </div>\r\n</section>\r\n<!-- /.content -->\r\n"

/***/ }),

/***/ "./src/app/staff/list-staff/list-staff.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/staff/list-staff/list-staff.component.ts ***!
  \**********************************************************/
/*! exports provided: ListStaffComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListStaffComponent", function() { return ListStaffComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _staff_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../staff.service */ "./src/app/staff/staff.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_3__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ListStaffComponent = /** @class */ (function () {
    function ListStaffComponent(router, route, staffService) {
        this.router = router;
        this.route = route;
        this.staffService = staffService;
        this.ctrl = this;
        this.loading = false;
        this.employees = [];
        this.nameFilter = '';
    }
    ListStaffComponent.prototype.ngOnInit = function () {
        this.getAll('');
    };
    ListStaffComponent.prototype.filter = function (value) {
        if (value !== '') {
            this.nameFilter = "email=" + value + "&first_name=" + value;
        }
        else {
            this.nameFilter = '';
        }
        this.getAll(this.nameFilter);
    };
    ListStaffComponent.prototype.getAll = function (query) {
        var _this = this;
        this.loading = true;
        $('.content').find('*').attr('disabled', 'disabled');
        $('.content').find('a').click(function (e) { e.preventDefault(); });
        this.staffService
            .getAll(query)
            .subscribe(function (result) {
            _this.loading = false;
            _this.employees = result.employees;
            $('.content').find('*').removeAttr('disabled');
            $('.content').find('a').unbind('click');
            // const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
            // this.router.navigate(['news']);
            // setTimeout(() => {
            //   this.successMsg = 'News Created Successfully';
            // }, 2000);
        }, function (error) {
            _this.loading = false;
            $('.content').find('*').removeAttr('disabled');
            $('.content').find('a').unbind('click');
            // setTimeout(() => {
            //   this.errorMsg = error;
            // }, 2000);
        });
    };
    ListStaffComponent.prototype.delete = function (id) {
        var _this = this;
        this.loading = true;
        sweetalert2__WEBPACK_IMPORTED_MODULE_3___default()({
            type: 'warning',
            title: 'Are you sure to Delete Staff?',
            text: 'You will not be able to recover the data of Staff',
            showCancelButton: true,
            confirmButtonColor: '#049F0C',
            cancelButtonColor: '#ff0000',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, keep it'
        }).then(function (result) {
            if (result.value) {
                _this.staffService
                    .delete(id)
                    .subscribe(function (result) {
                    _this.loading = false;
                    _this.getAll('');
                    // const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
                    // this.router.navigate(['news']);
                    // setTimeout(() => {
                    //   this.successMsg = 'News Created Successfully';
                    // }, 2000);
                }, function (error) {
                    _this.loading = false;
                    _this.getAll('');
                    // setTimeout(() => {
                    //   this.errorMsg = error;
                    // }, 2000);
                });
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_3___default()({
                    type: 'info',
                    title: 'Cancelled',
                    text: 'Your Staff file is safe :)'
                });
            }
        });
    };
    ListStaffComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-list-staff',
            template: __webpack_require__(/*! ./list-staff.component.html */ "./src/app/staff/list-staff/list-staff.component.html"),
            styles: [__webpack_require__(/*! ./list-staff.component.css */ "./src/app/staff/list-staff/list-staff.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _staff_service__WEBPACK_IMPORTED_MODULE_2__["StaffService"]])
    ], ListStaffComponent);
    return ListStaffComponent;
}());



/***/ }),

/***/ "./src/app/staff/profile/profile.component.css":
/*!*****************************************************!*\
  !*** ./src/app/staff/profile/profile.component.css ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3N0YWZmL3Byb2ZpbGUvcHJvZmlsZS5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/staff/profile/profile.component.html":
/*!******************************************************!*\
  !*** ./src/app/staff/profile/profile.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"content-header\">\n  <h1>\n    User Profile\n  </h1>\n  <ol class=\"breadcrumb\">\n    <li><a href=\"#\"><i class=\"fa fa-dashboard\"></i> Home</a></li>\n    <li><a href=\"#\">Examples</a></li>\n    <li class=\"active\">User profile</li>\n  </ol>\n</section>\n<section class=\"content\">\n\n  <div class=\"row\">\n    <div class=\"col-md-3\">\n\n      <!-- Profile Image -->\n      <div class=\"box box-primary\">\n        <div class=\"box-body box-profile\">\n          <img class=\"profile-user-img img-responsive img-circle\" src=\"assets/dist/img/user4-128x128.jpg\" alt=\"User profile picture\">\n\n          <h3 class=\"profile-username text-center\">Nina Mcintire</h3>\n\n          <p class=\"text-muted text-center\">Software Engineer</p>\n\n          <ul class=\"list-group list-group-unbordered\">\n            <li class=\"list-group-item\">\n              <b>Followers</b> <a class=\"pull-right\">1,322</a>\n            </li>\n            <li class=\"list-group-item\">\n              <b>Following</b> <a class=\"pull-right\">543</a>\n            </li>\n            <li class=\"list-group-item\">\n              <b>Friends</b> <a class=\"pull-right\">13,287</a>\n            </li>\n          </ul>\n\n          <a href=\"#\" class=\"btn btn-primary btn-block\"><b>Follow</b></a>\n        </div>\n        <!-- /.box-body -->\n      </div>\n      <!-- /.box -->\n\n      <!-- About Me Box -->\n      <div class=\"box box-primary\">\n        <div class=\"box-header with-border\">\n          <h3 class=\"box-title\">About Me</h3>\n        </div>\n        <!-- /.box-header -->\n        <div class=\"box-body\">\n          <strong><i class=\"fa fa-book margin-r-5\"></i> Education</strong>\n\n          <p class=\"text-muted\">\n            B.S. in Computer Science from the University of Tennessee at Knoxville\n          </p>\n\n          <hr>\n\n          <strong><i class=\"fa fa-map-marker margin-r-5\"></i> Location</strong>\n\n          <p class=\"text-muted\">Malibu, California</p>\n\n          <hr>\n\n          <strong><i class=\"fa fa-pencil margin-r-5\"></i> Skills</strong>\n\n          <p>\n            <span class=\"label label-danger\">UI Design</span>\n            <span class=\"label label-success\">Coding</span>\n            <span class=\"label label-info\">Javascript</span>\n            <span class=\"label label-warning\">PHP</span>\n            <span class=\"label label-primary\">Node.js</span>\n          </p>\n\n          <hr>\n\n          <strong><i class=\"fa fa-file-text-o margin-r-5\"></i> Notes</strong>\n\n          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>\n        </div>\n        <!-- /.box-body -->\n      </div>\n      <!-- /.box -->\n    </div>\n    <!-- /.col -->\n    <div class=\"col-md-9\">\n      <div class=\"nav-tabs-custom\">\n        <ul class=\"nav nav-tabs\">\n          <li class=\"active\"><a href=\"#settings\" data-toggle=\"tab\">Settings</a></li>\n        </ul>\n        <div class=\"tab-content\">\n\n          <div class=\"active tab-pane\" id=\"settings\">\n            <form class=\"form-horizontal\">\n              <div class=\"form-group\">\n                <label for=\"inputName\" class=\"col-sm-2 control-label\">Name</label>\n\n                <div class=\"col-sm-10\">\n                  <input type=\"email\" class=\"form-control\" id=\"inputName\" placeholder=\"Name\">\n                </div>\n              </div>\n              <div class=\"form-group\">\n                <label for=\"inputEmail\" class=\"col-sm-2 control-label\">Email</label>\n\n                <div class=\"col-sm-10\">\n                  <input type=\"email\" class=\"form-control\" id=\"inputEmail\" placeholder=\"Email\">\n                </div>\n              </div>\n              <div class=\"form-group\">\n                <label for=\"inputName\" class=\"col-sm-2 control-label\">Name</label>\n\n                <div class=\"col-sm-10\">\n                  <input type=\"text\" class=\"form-control\" id=\"inputName\" placeholder=\"Name\">\n                </div>\n              </div>\n              <div class=\"form-group\">\n                <label for=\"inputExperience\" class=\"col-sm-2 control-label\">Experience</label>\n\n                <div class=\"col-sm-10\">\n                  <textarea class=\"form-control\" id=\"inputExperience\" placeholder=\"Experience\"></textarea>\n                </div>\n              </div>\n              <div class=\"form-group\">\n                <label for=\"inputSkills\" class=\"col-sm-2 control-label\">Skills</label>\n\n                <div class=\"col-sm-10\">\n                  <input type=\"text\" class=\"form-control\" id=\"inputSkills\" placeholder=\"Skills\">\n                </div>\n              </div>\n              <div class=\"form-group\">\n                <div class=\"col-sm-offset-2 col-sm-10\">\n                  <div class=\"checkbox\">\n                    <label>\n                      <input type=\"checkbox\"> I agree to the <a href=\"#\">terms and conditions</a>\n                    </label>\n                  </div>\n                </div>\n              </div>\n              <div class=\"form-group\">\n                <div class=\"col-sm-offset-2 col-sm-10\">\n                  <button type=\"submit\" class=\"btn btn-danger\">Submit</button>\n                </div>\n              </div>\n            </form>\n          </div>\n          <!-- /.tab-pane -->\n        </div>\n        <!-- /.tab-content -->\n      </div>\n      <!-- /.nav-tabs-custom -->\n    </div>\n    <!-- /.col -->\n  </div>\n  <!-- /.row -->\n"

/***/ }),

/***/ "./src/app/staff/profile/profile.component.ts":
/*!****************************************************!*\
  !*** ./src/app/staff/profile/profile.component.ts ***!
  \****************************************************/
/*! exports provided: ProfileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileComponent", function() { return ProfileComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ProfileComponent = /** @class */ (function () {
    function ProfileComponent() {
    }
    ProfileComponent.prototype.ngOnInit = function () {
    };
    ProfileComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-profile',
            template: __webpack_require__(/*! ./profile.component.html */ "./src/app/staff/profile/profile.component.html"),
            styles: [__webpack_require__(/*! ./profile.component.css */ "./src/app/staff/profile/profile.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ProfileComponent);
    return ProfileComponent;
}());



/***/ }),

/***/ "./src/app/staff/staff.service.ts":
/*!****************************************!*\
  !*** ./src/app/staff/staff.service.ts ***!
  \****************************************/
/*! exports provided: StaffService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StaffService", function() { return StaffService; });
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../constants */ "./src/app/constants.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var StaffService = /** @class */ (function () {
    function StaffService(http) {
        this.http = http;
        this.apiUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_0__["environment"].apiBaseUrl;
        this.userId = null;
        this._isAuthenticated = new rxjs__WEBPACK_IMPORTED_MODULE_4__["BehaviorSubject"](false);
    }
    StaffService.prototype.create = function (data) {
        return this.http.post(this.apiUrl + "/employees", data, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    StaffService.prototype.update = function (data, id) {
        return this.http.put(this.apiUrl + "/employees/" + id, data, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    StaffService.prototype.getOne = function (id) {
        return this.http.get(this.apiUrl + "/employees/" + id, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    StaffService.prototype.getAll = function (query) {
        return this.http.get(this.apiUrl + "/employees?" + query, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    StaffService.prototype.delete = function (id) {
        return this.http.delete(this.apiUrl + "/employees/" + id, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    StaffService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], StaffService);
    return StaffService;
}());



/***/ }),

/***/ "./src/app/team-members/create-team-member/create-team-member.component.css":
/*!**********************************************************************************!*\
  !*** ./src/app/team-members/create-team-member/create-team-member.component.css ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RlYW0tbWVtYmVycy9jcmVhdGUtdGVhbS1tZW1iZXIvY3JlYXRlLXRlYW0tbWVtYmVyLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/team-members/create-team-member/create-team-member.component.html":
/*!***********************************************************************************!*\
  !*** ./src/app/team-members/create-team-member/create-team-member.component.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<!-- Content Header (Page header) -->\r\n<section class=\"content-header\">\r\n  <h1>\r\n    Team Member\r\n  </h1>\r\n  <ol class=\"breadcrumb\">\r\n    <li><a routerLink=\"/dashboard\"><i class=\"fa fa-dashboard\"></i> Home</a></li>\r\n    <li><a routerLink=\"/team-member\">Team Memeber</a></li>\r\n    <li class=\"active\">Create</li>\r\n  </ol>\r\n</section>\r\n\r\n<!-- Main content -->\r\n<div class=\"loader\" [hidden]=\"!loading\"></div>\r\n<section class=\"content\">\r\n  <div class=\"row\">\r\n    <!-- left column -->\r\n    <div class=\"col-md-12\">\r\n      <!-- general form elements -->\r\n      <div class=\"box box-primary\">\r\n        <div class=\"box-header with-border\">\r\n          <h3 class=\"box-title\">Create New Team Member</h3>\r\n        </div>\r\n        <!-- /.box-header -->\r\n        <!-- form start -->\r\n        <form id=\"form-team-member\" class=\"p-t-15\" role=\"form\" [formGroup]=\"form\" (ngSubmit)=\"create()\">\r\n          <div class=\"box-body\">\r\n            <div class=\"form-group has-feedback\">\r\n              <label for=\"exampleInputFirstName\">Name</label>\r\n              <input type=\"text\" formControlName=\"name\" class=\"form-control\" id=\"exampleInputFirstName\" placeholder=\"Enter Name\">\r\n              <span class=\"glyphicon glyphicon-user form-control-feedback\"></span>\r\n            </div>\r\n            <div class=\"form-group has-feedback\">\r\n              <label for=\"exampleInputLastName\">Title</label>\r\n              <input type=\"text\" formControlName=\"title\" class=\"form-control\" id=\"exampleInputLastName\" placeholder=\"Enter Title\">\r\n              <span class=\"glyphicon glyphicon-user form-control-feedback\"></span>\r\n            </div>\r\n            <div class=\"form-group has-feedback\">\r\n              <label for=\"exampleInputMobile\">Linkedin Url</label>\r\n              <input type=\"text\" formControlName=\"linkedinUrl\" class=\"form-control\" id=\"exampleInputMobile\" placeholder=\"Enter Linkedin Url\">\r\n              <span class=\"glyphicon glyphicon-phone form-control-feedback\"></span>\r\n            </div>\r\n            <label>Upload Photo</label>\r\n            <div class=\"form-group\">\r\n              <img *ngFor='let url of urls'  [src]=\"url\" height=\"200\"> <br/>\r\n              <input type='file' (change)=\"onFileChanged($event)\" multiple>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label class=\"hover\">\r\n                <div class=\"icheckbox_flat-green hover\" [ngClass]=\"{'checked': pinned}\" aria-checked=\"true\" aria-disabled=\"false\" style=\"position: relative;\"><input [checked]=\"pinned\" (change)=\"pinned = !pinned\" [value]='pinned' type=\"checkbox\" class=\"flat-red\" checked=\"\" style=\"position: absolute; opacity: 0;\"><ins class=\"iCheck-helper\" style=\"position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;\"></ins></div>\r\n                Pin to dashboard\r\n              </label>\r\n            </div>\r\n          </div>\r\n          <!-- /.box-body -->\r\n\r\n          <div class=\"box-footer\">\r\n            <button type=\"submit\" class=\"btn btn-primary\">Save</button>\r\n          </div>\r\n        </form>\r\n      </div>\r\n      <!-- /.box -->\r\n\r\n    </div>\r\n    <!--/.col (left) -->\r\n  </div>\r\n  <!-- /.row -->\r\n</section>\r\n<!-- /.content -->\r\n"

/***/ }),

/***/ "./src/app/team-members/create-team-member/create-team-member.component.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/team-members/create-team-member/create-team-member.component.ts ***!
  \*********************************************************************************/
/*! exports provided: CreateTeamMemberComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateTeamMemberComponent", function() { return CreateTeamMemberComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _team_members_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../team-members.service */ "./src/app/team-members/team-members.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CreateTeamMemberComponent = /** @class */ (function () {
    function CreateTeamMemberComponent(fb, router, route, teamMemberService) {
        this.fb = fb;
        this.router = router;
        this.route = route;
        this.teamMemberService = teamMemberService;
        this.ctrl = this;
        this.pinned = false;
        this.loading = false;
        this.selectedFiles = [];
        this.formData = new FormData();
        this.urls = [];
    }
    CreateTeamMemberComponent.prototype.ngOnInit = function () {
        this.buildForm();
    };
    Object.defineProperty(CreateTeamMemberComponent.prototype, "name", {
        get: function () {
            return this.form.get('name');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreateTeamMemberComponent.prototype, "title", {
        get: function () {
            return this.form.get('title');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreateTeamMemberComponent.prototype, "linkedinUrl", {
        get: function () {
            return this.form.get('linkedinUrl');
        },
        enumerable: true,
        configurable: true
    });
    CreateTeamMemberComponent.prototype.onFileChanged = function (event) {
        var _this = this;
        this.loading = true;
        this.selectedFiles = event.target.files;
        // this.formData.append('images', this.selectedFiles, this.selectedFiles.name);
        if (this.selectedFiles.length > 0) {
            var _loop_1 = function (i) {
                var reader = new FileReader();
                reader.onload = function (event) {
                    _this.formData.append('images', _this.selectedFiles[i]);
                    _this.urls.push(event.target.result);
                };
                reader.readAsDataURL(this_1.selectedFiles[i]);
            };
            var this_1 = this;
            for (var i = 0; i < this.selectedFiles.length; i++) {
                _loop_1(i);
            }
        }
    };
    CreateTeamMemberComponent.prototype.create = function () {
        var _this = this;
        this.loading = true;
        $('.content').find('*').attr('disabled', 'disabled');
        $('.content').find('a').click(function (e) { e.preventDefault(); });
        var data = {
            name: this.form.value.name,
            title: this.form.value.title,
            linkedinUrl: this.form.value.linkedinUrl,
            pinned: this.pinned,
        };
        this.formData.append('data', JSON.stringify(data));
        this.teamMemberService
            .create(this.formData)
            .subscribe(function (result) {
            _this.loading = false;
            var returnUrl = _this.route.snapshot.queryParamMap.get('returnUrl');
            _this.router.navigate(['team-member']);
        }, function (error) {
            _this.loading = false;
        });
    };
    CreateTeamMemberComponent.prototype.buildForm = function () {
        this.form = this.fb.group({
            name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            title: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            linkedinUrl: [''],
        });
    };
    CreateTeamMemberComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-create-team-member',
            template: __webpack_require__(/*! ./create-team-member.component.html */ "./src/app/team-members/create-team-member/create-team-member.component.html"),
            styles: [__webpack_require__(/*! ./create-team-member.component.css */ "./src/app/team-members/create-team-member/create-team-member.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _team_members_service__WEBPACK_IMPORTED_MODULE_3__["TeamMembersService"]])
    ], CreateTeamMemberComponent);
    return CreateTeamMemberComponent;
}());



/***/ }),

/***/ "./src/app/team-members/edit-team-member/edit-team-member.component.css":
/*!******************************************************************************!*\
  !*** ./src/app/team-members/edit-team-member/edit-team-member.component.css ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RlYW0tbWVtYmVycy9lZGl0LXRlYW0tbWVtYmVyL2VkaXQtdGVhbS1tZW1iZXIuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/team-members/edit-team-member/edit-team-member.component.html":
/*!*******************************************************************************!*\
  !*** ./src/app/team-members/edit-team-member/edit-team-member.component.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<!-- Content Header (Page header) -->\r\n<section class=\"content-header\">\r\n  <h1>\r\n    Team Member\r\n  </h1>\r\n  <ol class=\"breadcrumb\">\r\n    <li><a routerLink=\"/dashboard\"><i class=\"fa fa-dashboard\"></i> Home</a></li>\r\n    <li><a routerLink=\"/team-member\">Team Memeber</a></li>\r\n    <li class=\"active\">Edit</li>\r\n  </ol>\r\n</section>\r\n\r\n<!-- Main content -->\r\n<div class=\"loader\" [hidden]=\"!loading\"></div>\r\n\r\n<section class=\"content\">\r\n  <div class=\"row\">\r\n    <!-- left column -->\r\n    <div class=\"col-md-12\">\r\n      <!-- general form elements -->\r\n      <div class=\"box box-primary\">\r\n        <div class=\"box-header with-border\">\r\n          <h3 class=\"box-title\">Edit Team Member</h3>\r\n        </div>\r\n        <!-- /.box-header -->\r\n        <!-- form start -->\r\n        <form id=\"form-team-member\" class=\"p-t-15\" role=\"form\" [formGroup]=\"form\" (ngSubmit)=\"update()\">\r\n          <div class=\"box-body\">\r\n            <div class=\"form-group has-feedback\">\r\n              <label for=\"exampleInputFirstName\">Name</label>\r\n              <input type=\"text\" formControlName=\"name\" class=\"form-control\" id=\"exampleInputFirstName\" placeholder=\"Enter Name\">\r\n              <span class=\"glyphicon glyphicon-user form-control-feedback\"></span>\r\n            </div>\r\n            <div class=\"form-group has-feedback\">\r\n              <label for=\"exampleInputLastName\">Title</label>\r\n              <input type=\"text\" formControlName=\"title\" class=\"form-control\" id=\"exampleInputLastName\" placeholder=\"Enter Title\">\r\n              <span class=\"glyphicon glyphicon-user form-control-feedback\"></span>\r\n            </div>\r\n            <div class=\"form-group has-feedback\">\r\n              <label for=\"exampleInputMobile\">Linkedin Url</label>\r\n              <input type=\"text\" formControlName=\"linkedinUrl\" class=\"form-control\" id=\"exampleInputMobile\" placeholder=\"Enter Linkedin Url\">\r\n              <span class=\"glyphicon glyphicon-phone form-control-feedback\"></span>\r\n            </div>\r\n            <label>Upload Photo</label>\r\n            <div class=\"form-group\">\r\n              <img *ngFor='let url of urls'  [src]=\"url\" height=\"200\"> <br/>\r\n              <input type='file' (change)=\"onFileChanged($event)\" multiple>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label class=\"hover\">\r\n                <div class=\"icheckbox_flat-green hover\" [ngClass]=\"{'checked': pinned}\" aria-checked=\"true\" aria-disabled=\"false\" style=\"position: relative;\"><input [checked]=\"pinned\" (change)=\"pinned = !pinned\" [value]='pinned' type=\"checkbox\" class=\"flat-red\" checked=\"\" style=\"position: absolute; opacity: 0;\"><ins class=\"iCheck-helper\" style=\"position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;\"></ins></div>\r\n                Pin to dashboard\r\n              </label>\r\n            </div>\r\n          </div>\r\n          <!-- /.box-body -->\r\n\r\n          <div class=\"box-footer\">\r\n            <button type=\"submit\" class=\"btn btn-primary\">Save</button>\r\n          </div>\r\n        </form>\r\n      </div>\r\n      <!-- /.box -->\r\n\r\n    </div>\r\n    <!--/.col (left) -->\r\n  </div>\r\n  <!-- /.row -->\r\n</section>\r\n<!-- /.content -->\r\n"

/***/ }),

/***/ "./src/app/team-members/edit-team-member/edit-team-member.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/team-members/edit-team-member/edit-team-member.component.ts ***!
  \*****************************************************************************/
/*! exports provided: EditTeamMemberComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditTeamMemberComponent", function() { return EditTeamMemberComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _team_members_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../team-members.service */ "./src/app/team-members/team-members.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var EditTeamMemberComponent = /** @class */ (function () {
    function EditTeamMemberComponent(fb, router, route, teamMemberService) {
        this.fb = fb;
        this.router = router;
        this.route = route;
        this.teamMemberService = teamMemberService;
        this.memberId = this.route.snapshot.params.id;
        this.name = '';
        this.title = '';
        this.linkedinUrl = '';
        this.images = [];
        this.pinned = false;
        this.loading = false;
        this.selectedFiles = [];
        this.formData = new FormData();
        this.urls = [];
    }
    EditTeamMemberComponent.prototype.ngOnInit = function () {
        this.buildForm();
        this.getOne(this.memberId);
    };
    EditTeamMemberComponent.prototype.onFileChanged = function (event) {
        var _this = this;
        this.loading = true;
        this.selectedFiles = event.target.files;
        // this.formData.append('images', this.selectedFiles, this.selectedFiles.name);
        if (this.selectedFiles.length > 0) {
            var _loop_1 = function (i) {
                var reader = new FileReader();
                reader.onload = function (event) {
                    _this.formData.append('images', _this.selectedFiles[i]);
                    _this.urls.push(event.target.result);
                };
                reader.readAsDataURL(this_1.selectedFiles[i]);
            };
            var this_1 = this;
            for (var i = 0; i < this.selectedFiles.length; i++) {
                _loop_1(i);
            }
        }
    };
    EditTeamMemberComponent.prototype.update = function () {
        var _this = this;
        $('.content').find('*').attr('disabled', 'disabled');
        $('.content').find('a').click(function (e) { e.preventDefault(); });
        var data = {
            name: this.form.value.name,
            title: this.form.value.title,
            linkedinUrl: this.form.value.linkedinUrl,
            pinned: this.pinned,
        };
        this.formData.append('data', JSON.stringify(data));
        this.loading = true;
        this.teamMemberService
            .update(this.formData, this.memberId)
            .subscribe(function (result) {
            _this.loading = false;
            var returnUrl = _this.route.snapshot.queryParamMap.get('returnUrl');
            _this.router.navigate(['team-member']);
        }, function (error) {
            _this.loading = false;
        });
    };
    EditTeamMemberComponent.prototype.getOne = function (id) {
        var _this = this;
        this.loading = true;
        this.teamMemberService
            .getOne(id)
            .subscribe(function (result) {
            _this.name = result.name;
            _this.title = result.title;
            _this.linkedinUrl = result.linkedinUrl;
            _this.pinned = result.pinned;
            _this.loading = false;
            _this.buildForm();
        }, function (error) {
            _this.loading = false;
        });
    };
    EditTeamMemberComponent.prototype.buildForm = function () {
        this.form = this.fb.group({
            name: [this.name, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            title: [this.title, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            linkedinUrl: [this.linkedinUrl],
        });
    };
    EditTeamMemberComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-edit-team-member',
            template: __webpack_require__(/*! ./edit-team-member.component.html */ "./src/app/team-members/edit-team-member/edit-team-member.component.html"),
            styles: [__webpack_require__(/*! ./edit-team-member.component.css */ "./src/app/team-members/edit-team-member/edit-team-member.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _team_members_service__WEBPACK_IMPORTED_MODULE_3__["TeamMembersService"]])
    ], EditTeamMemberComponent);
    return EditTeamMemberComponent;
}());



/***/ }),

/***/ "./src/app/team-members/list-team-members/list-team-members.component.css":
/*!********************************************************************************!*\
  !*** ./src/app/team-members/list-team-members/list-team-members.component.css ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RlYW0tbWVtYmVycy9saXN0LXRlYW0tbWVtYmVycy9saXN0LXRlYW0tbWVtYmVycy5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/team-members/list-team-members/list-team-members.component.html":
/*!*********************************************************************************!*\
  !*** ./src/app/team-members/list-team-members/list-team-members.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<style>\r\n  .btn {\r\n    background-color: DodgerBlue; /* Blue background */\r\n    border: none; /* Remove borders */\r\n    color: white; /* White text */\r\n    font-size: 10px; /* Set a font size */\r\n    cursor: pointer; /* Mouse pointer on hover */\r\n  }\r\n\r\n  /* Darker background on mouse-over */\r\n  .btn:hover {\r\n    background-color: RoyalBlue;\r\n  }\r\n  .loader {\r\n    position: absolute;\r\n    left: 50%;\r\n    top: 50%;\r\n    height:60px;\r\n    width:60px;\r\n    margin:0px auto;\r\n    -webkit-animation: rotation .6s infinite linear;\r\n    -moz-animation: rotation .6s infinite linear;\r\n    -o-animation: rotation .6s infinite linear;\r\n    animation: rotation .6s infinite linear;\r\n    border-left:6px solid rgba(0,174,239,.15);\r\n    border-right:6px solid rgba(0,174,239,.15);\r\n    border-bottom:6px solid rgba(0,174,239,.15);\r\n    border-top:6px solid rgba(0,174,239,.8);\r\n    border-radius:100%;\r\n  }\r\n\r\n  @-webkit-keyframes rotation {\r\n    from {-webkit-transform: rotate(0deg);}\r\n    to {-webkit-transform: rotate(359deg);}\r\n  }\r\n  @-moz-keyframes rotation {\r\n    from {-moz-transform: rotate(0deg);}\r\n    to {-moz-transform: rotate(359deg);}\r\n  }\r\n  @-o-keyframes rotation {\r\n    from {-o-transform: rotate(0deg);}\r\n    to {-o-transform: rotate(359deg);}\r\n  }\r\n  @keyframes rotation {\r\n    from {transform: rotate(0deg);}\r\n    to {transform: rotate(359deg);}\r\n  }\r\n</style>\r\n<!-- Content Header (Page header) -->\r\n<section class=\"content-header\">\r\n  <h1>\r\n    Team Member <button routerLink=\"/team-member/create-team-member\" type=\"submit\" class=\"btn btn-primary pull-right\">New</button>\r\n  </h1>\r\n  <ol class=\"breadcrumb\">\r\n    <li><a routerLink=\"/dashboard\"><i class=\"fa fa-dashboard\"></i> Home</a></li>\r\n    <li><a routerLink=\"/team-member\">Team Member</a></li>\r\n  </ol>\r\n</section>\r\n\r\n<!-- Main content -->\r\n<div class=\"loader\" [hidden]=\"!loading\"></div>\r\n<section class=\"content\">\r\n  <!-- /.row -->\r\n  <div class=\"row\">\r\n    <div class=\"col-xs-12\">\r\n      <div class=\"box\">\r\n        <div class=\"box-header\">\r\n          <h3 class=\"box-title\"></h3>\r\n\r\n          <div class=\"box-tools\">\r\n            <div class=\"input-group input-group-sm\" style=\"width: 150px;\">\r\n              <input (ngModelChange)=\"filter($event)\" [(ngModel)]=\"nameFilter\"  type=\"text\" name=\"table_search\" class=\"form-control pull-right\" placeholder=\"Search\">\r\n\r\n              <div class=\"input-group-btn\">\r\n                <button type=\"submit\" class=\"btn btn-default\"><i class=\"fa fa-search\"></i></button>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <!-- /.box-header -->\r\n        <div class=\"box-body table-responsive no-padding\">\r\n          <table class=\"table table-hover\">\r\n            <thead><tr>\r\n              <th>Name</th>\r\n              <th>Title</th>\r\n              <th>LinkedinUrl</th>\r\n              <th>Pinned</th>\r\n              <th>Edit/Delete</th>\r\n            </tr>\r\n            </thead>\r\n            <tbody>\r\n            <tr *ngFor=\"let member of members\" class=\"gradeX odd content-table-payment cursor\" role=\"row\">\r\n              <td class=\"mn-130 sorting_1\">{{member.name}}</td>\r\n              <td class=\"mn-130 sorting_1\">{{member.title}}</td>\r\n              <td class=\"mn-130 sorting_1\">{{member.linkedinUrl}}</td>\r\n              <td class=\"mn-120 sorting_1\">\r\n                  <span *ngIf=\"member.pinned === true\"\r\n                        class=\"label label-success upper-case\">Pinned</span>\r\n                <span *ngIf=\"member.pinned === false\"\r\n                      class=\"label label-danger upper-case\">Pinned</span>\r\n              </td>\r\n              <td>\r\n                <button class=\"btn\" routerLink='/team-member/{{member._id}}/edit-team-member'><i class=\"fa fa-edit\"></i></button>\r\n                <button class=\"btn\" (click)=\"delete(member._id)\"><i class=\"fa fa-trash\"></i></button>\r\n              </td>\r\n            </tr>\r\n            </tbody>\r\n          </table>\r\n        </div>\r\n        <!-- /.box-body -->\r\n      </div>\r\n      <!-- /.box -->\r\n    </div>\r\n  </div>\r\n</section>\r\n<!-- /.content -->\r\n"

/***/ }),

/***/ "./src/app/team-members/list-team-members/list-team-members.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/team-members/list-team-members/list-team-members.component.ts ***!
  \*******************************************************************************/
/*! exports provided: ListTeamMembersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListTeamMembersComponent", function() { return ListTeamMembersComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _team_members_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../team-members.service */ "./src/app/team-members/team-members.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_3__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ListTeamMembersComponent = /** @class */ (function () {
    function ListTeamMembersComponent(router, route, teamMembersService) {
        this.router = router;
        this.route = route;
        this.teamMembersService = teamMembersService;
        this.ctrl = this;
        this.loading = false;
        this.members = [];
        this.nameFilter = '';
    }
    ListTeamMembersComponent.prototype.ngOnInit = function () {
        this.getAll('');
    };
    ListTeamMembersComponent.prototype.filter = function (value) {
        if (value !== '') {
            this.nameFilter = "name=" + value;
        }
        else {
            this.nameFilter = '';
        }
        this.getAll(this.nameFilter);
    };
    ListTeamMembersComponent.prototype.getAll = function (query) {
        var _this = this;
        this.loading = true;
        $('.content').find('*').attr('disabled', 'disabled');
        $('.content').find('a').click(function (e) { e.preventDefault(); });
        this.teamMembersService
            .getAll(query)
            .subscribe(function (result) {
            _this.loading = false;
            _this.members = result.teammembers;
            $('.content').find('*').removeAttr('disabled');
            $('.content').find('a').unbind('click');
            // const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
            // this.router.navigate(['news']);
            // setTimeout(() => {
            //   this.successMsg = 'News Created Successfully';
            // }, 2000);
        }, function (error) {
            _this.loading = false;
            $('.content').find('*').removeAttr('disabled');
            $('.content').find('a').unbind('click');
            // setTimeout(() => {
            //   this.errorMsg = error;
            // }, 2000);
        });
    };
    ListTeamMembersComponent.prototype.delete = function (id) {
        var _this = this;
        this.loading = true;
        sweetalert2__WEBPACK_IMPORTED_MODULE_3___default()({
            type: 'warning',
            title: 'Are you sure to Delete Team Member?',
            text: 'You will not be able to recover the data of Team Member',
            showCancelButton: true,
            confirmButtonColor: '#049F0C',
            cancelButtonColor: '#ff0000',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, keep it'
        }).then(function (result) {
            if (result.value) {
                _this.teamMembersService
                    .delete(id)
                    .subscribe(function (result) {
                    _this.loading = false;
                    _this.getAll('');
                }, function (error) {
                    _this.loading = false;
                    _this.getAll('');
                });
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_3___default()({
                    type: 'info',
                    title: 'Cancelled',
                    text: 'Your Team Member file is safe :)'
                });
            }
        });
    };
    ListTeamMembersComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-list-team-members',
            template: __webpack_require__(/*! ./list-team-members.component.html */ "./src/app/team-members/list-team-members/list-team-members.component.html"),
            styles: [__webpack_require__(/*! ./list-team-members.component.css */ "./src/app/team-members/list-team-members/list-team-members.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _team_members_service__WEBPACK_IMPORTED_MODULE_2__["TeamMembersService"]])
    ], ListTeamMembersComponent);
    return ListTeamMembersComponent;
}());



/***/ }),

/***/ "./src/app/team-members/team-members.service.ts":
/*!******************************************************!*\
  !*** ./src/app/team-members/team-members.service.ts ***!
  \******************************************************/
/*! exports provided: TeamMembersService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TeamMembersService", function() { return TeamMembersService; });
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../constants */ "./src/app/constants.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var TeamMembersService = /** @class */ (function () {
    function TeamMembersService(http) {
        this.http = http;
        this.apiUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_0__["environment"].apiBaseUrl;
        this.userId = null;
        this._isAuthenticated = new rxjs__WEBPACK_IMPORTED_MODULE_4__["BehaviorSubject"](false);
    }
    TeamMembersService.prototype.create = function (data) {
        return this.http.post(this.apiUrl + "/team-member", data, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    TeamMembersService.prototype.update = function (data, id) {
        return this.http.put(this.apiUrl + "/team-member/" + id, data, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    TeamMembersService.prototype.getOne = function (id) {
        return this.http.get(this.apiUrl + "/team-member/" + id, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    TeamMembersService.prototype.getAll = function (query) {
        return this.http.get(this.apiUrl + "/team-member?" + query, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    TeamMembersService.prototype.delete = function (id) {
        return this.http.delete(this.apiUrl + "/team-member/" + id, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    TeamMembersService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], TeamMembersService);
    return TeamMembersService;
}());



/***/ }),

/***/ "./src/app/users/create-user/create-user.component.css":
/*!*************************************************************!*\
  !*** ./src/app/users/create-user/create-user.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXJzL2NyZWF0ZS11c2VyL2NyZWF0ZS11c2VyLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/users/create-user/create-user.component.html":
/*!**************************************************************!*\
  !*** ./src/app/users/create-user/create-user.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<!-- Content Header (Page header) -->\r\n<section class=\"content-header\">\r\n  <h1>\r\n    User\r\n  </h1>\r\n  <ol class=\"breadcrumb\">\r\n    <li><a routerLink=\"/dashboard\"><i class=\"fa fa-dashboard\"></i> Home</a></li>\r\n    <li><a routerLink=\"/users\">User</a></li>\r\n    <li class=\"active\">Create</li>\r\n  </ol>\r\n</section>\r\n\r\n<!-- Main content -->\r\n<div class=\"loader\" [hidden]=\"!loading\"></div>\r\n\r\n<section class=\"content\">\r\n  <div class=\"row\">\r\n    <!-- left column -->\r\n    <div class=\"col-md-12\">\r\n      <!-- general form elements -->\r\n      <div class=\"box box-primary\">\r\n        <div class=\"box-header with-border\">\r\n          <h3 class=\"box-title\">Create New User</h3>\r\n        </div>\r\n        <!-- /.box-header -->\r\n        <!-- form start -->\r\n        <form id=\"form-jobs\" class=\"p-t-15\" role=\"form\" [formGroup]=\"form\" (ngSubmit)=\"create()\">\r\n          <div class=\"box-body\">\r\n            <div class=\"form-group has-feedback\">\r\n              <label for=\"exampleInputEmail1\">Email</label>\r\n              <input type=\"email\" formControlName=\"email\" class=\"form-control\" id=\"exampleInputEmail1\" placeholder=\"Enter Email\">\r\n              <span class=\"glyphicon glyphicon-envelope form-control-feedback\"></span>\r\n            </div>\r\n            <div class=\"form-group has-feedback\">\r\n              <label for=\"exampleInputPassword\">Password</label>\r\n              <input type=\"password\" formControlName=\"password\" class=\"form-control\" id=\"exampleInputPassword\" placeholder=\"Enter Password\">\r\n              <span class=\"glyphicon glyphicon-lock form-control-feedback\"></span>\r\n            </div>\r\n            <div class=\"form-group has-feedback\">\r\n              <label for=\"exampleInputFirstName\">First Name</label>\r\n              <input type=\"text\" formControlName=\"first_name\" class=\"form-control\" id=\"exampleInputFirstName\" placeholder=\"Enter First Name\">\r\n              <span class=\"glyphicon glyphicon-user form-control-feedback\"></span>\r\n            </div>\r\n            <div class=\"form-group has-feedback\">\r\n              <label for=\"exampleInputLastName\">Last Name</label>\r\n              <input type=\"text\" formControlName=\"last_name\" class=\"form-control\" id=\"exampleInputLastName\" placeholder=\"Enter Last Name\">\r\n              <span class=\"glyphicon glyphicon-user form-control-feedback\"></span>\r\n            </div>\r\n            <div class=\"form-group has-feedback\">\r\n              <label for=\"exampleInputMobile\">Mobile</label>\r\n              <input type=\"text\" formControlName=\"mobile\" class=\"form-control\" id=\"exampleInputMobile\" placeholder=\"Enter Mobile\">\r\n              <span class=\"glyphicon glyphicon-phone form-control-feedback\"></span>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label>Gender</label>\r\n              <select formControlName=\"gender\" class=\"form-control\">\r\n                <option [value]=\"gender\" *ngFor=\"let gender of genders\"[selected]=\"gender\">\r\n                  {{gender}}</option>\r\n              </select>\r\n            </div>\r\n            <label>Upload Image</label>\r\n            <div class=\"form-group\">\r\n              <img *ngFor='let url of urls'  [src]=\"url\" height=\"200\"> <br/>\r\n              <input type='file' (change)=\"onFileChanged($event)\" multiple>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label class=\"hover\">\r\n                <div class=\"icheckbox_flat-green hover\" [ngClass]=\"{'checked': is_active}\" aria-checked=\"true\" aria-disabled=\"false\" style=\"position: relative;\"><input [checked]=\"is_active\" (change)=\"is_active = !is_active\" [value]='is_active' type=\"checkbox\" class=\"flat-red\" checked=\"\" style=\"position: absolute; opacity: 0;\"><ins class=\"iCheck-helper\" style=\"position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;\"></ins></div>\r\n                Active\r\n              </label>\r\n            </div>\r\n          </div>\r\n          <!-- /.box-body -->\r\n\r\n          <div class=\"box-footer\">\r\n            <button type=\"submit\" class=\"btn btn-primary\">Save</button>\r\n          </div>\r\n        </form>\r\n      </div>\r\n      <!-- /.box -->\r\n\r\n    </div>\r\n    <!--/.col (left) -->\r\n  </div>\r\n  <!-- /.row -->\r\n</section>\r\n<!-- /.content -->\r\n"

/***/ }),

/***/ "./src/app/users/create-user/create-user.component.ts":
/*!************************************************************!*\
  !*** ./src/app/users/create-user/create-user.component.ts ***!
  \************************************************************/
/*! exports provided: CreateUserComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateUserComponent", function() { return CreateUserComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _users_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../users.service */ "./src/app/users/users.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CreateUserComponent = /** @class */ (function () {
    function CreateUserComponent(fb, router, route, userService) {
        this.fb = fb;
        this.router = router;
        this.route = route;
        this.userService = userService;
        this.ctrl = this;
        this.genders = ['Male', 'Female'];
        this.is_active = false;
        this.loading = false;
        this.selectedFiles = [];
        this.formData = new FormData();
        this.urls = [];
    }
    CreateUserComponent.prototype.ngOnInit = function () {
        this.buildForm();
    };
    Object.defineProperty(CreateUserComponent.prototype, "email", {
        get: function () {
            return this.form.get('email');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreateUserComponent.prototype, "password", {
        get: function () {
            return this.form.get('password');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreateUserComponent.prototype, "first_name", {
        get: function () {
            return this.form.get('first_name');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreateUserComponent.prototype, "last_name", {
        get: function () {
            return this.form.get('last_name');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreateUserComponent.prototype, "mobile", {
        get: function () {
            return this.form.get('mobile');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CreateUserComponent.prototype, "gender", {
        get: function () {
            return this.form.get('gender');
        },
        enumerable: true,
        configurable: true
    });
    CreateUserComponent.prototype.onFileChanged = function (event) {
        var _this = this;
        this.loading = true;
        this.selectedFiles = event.target.files;
        // this.formData.append('images', this.selectedFiles, this.selectedFiles.name);
        if (this.selectedFiles.length > 0) {
            var _loop_1 = function (i) {
                var reader = new FileReader();
                reader.onload = function (event) {
                    _this.formData.append('images', _this.selectedFiles[i]);
                    _this.urls.push(event.target.result);
                };
                reader.readAsDataURL(this_1.selectedFiles[i]);
            };
            var this_1 = this;
            for (var i = 0; i < this.selectedFiles.length; i++) {
                _loop_1(i);
            }
        }
    };
    CreateUserComponent.prototype.create = function () {
        var _this = this;
        this.loading = true;
        $('.content').find('*').attr('disabled', 'disabled');
        $('.content').find('a').click(function (e) { e.preventDefault(); });
        var data = {
            email: this.form.value.email,
            password: this.form.value.password,
            first_name: this.form.value.first_name,
            last_name: this.form.value.last_name,
            mobile: this.form.value.mobile,
            gender: this.form.value.gender,
            is_active: this.is_active
        };
        this.formData.append('data', JSON.stringify(data));
        this.userService
            .create(this.formData)
            .subscribe(function (result) {
            _this.loading = false;
            var returnUrl = _this.route.snapshot.queryParamMap.get('returnUrl');
            _this.router.navigate(['users']);
        }, function (error) {
            _this.loading = false;
        });
    };
    CreateUserComponent.prototype.buildForm = function () {
        this.form = this.fb.group({
            email: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            first_name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            last_name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            mobile: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            gender: [this.genders],
        });
    };
    CreateUserComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-create-user',
            template: __webpack_require__(/*! ./create-user.component.html */ "./src/app/users/create-user/create-user.component.html"),
            styles: [__webpack_require__(/*! ./create-user.component.css */ "./src/app/users/create-user/create-user.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _users_service__WEBPACK_IMPORTED_MODULE_3__["UsersService"]])
    ], CreateUserComponent);
    return CreateUserComponent;
}());



/***/ }),

/***/ "./src/app/users/edit-user/edit-user.component.css":
/*!*********************************************************!*\
  !*** ./src/app/users/edit-user/edit-user.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXJzL2VkaXQtdXNlci9lZGl0LXVzZXIuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/users/edit-user/edit-user.component.html":
/*!**********************************************************!*\
  !*** ./src/app/users/edit-user/edit-user.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<!-- Content Header (Page header) -->\r\n<section class=\"content-header\">\r\n  <h1>\r\n    User\r\n  </h1>\r\n  <ol class=\"breadcrumb\">\r\n    <li><a routerLink=\"/dashboard\"><i class=\"fa fa-dashboard\"></i> Home</a></li>\r\n    <li><a routerLink=\"/users\">User</a></li>\r\n    <li class=\"active\">Edit</li>\r\n  </ol>\r\n</section>\r\n\r\n<!-- Main content -->\r\n<div class=\"loader\" [hidden]=\"!loading\"></div>\r\n<section class=\"content\">\r\n  <div class=\"row\">\r\n    <!-- left column -->\r\n    <div class=\"col-md-12\">\r\n      <!-- general form elements -->\r\n      <div class=\"box box-primary\">\r\n        <div class=\"box-header with-border\">\r\n          <h3 class=\"box-title\">Edit User</h3>\r\n        </div>\r\n        <!-- /.box-header -->\r\n        <!-- form start -->\r\n        <form id=\"form-jobs\" class=\"p-t-15\" role=\"form\" [formGroup]=\"form\" (ngSubmit)=\"update()\">\r\n          <div class=\"box-body\">\r\n            <div class=\"form-group has-feedback\">\r\n              <label for=\"exampleInputEmail1\">Email</label>\r\n              <input type=\"email\" formControlName=\"email\" class=\"form-control\" id=\"exampleInputEmail1\" placeholder=\"Enter Email\">\r\n              <span class=\"glyphicon glyphicon-envelope form-control-feedback\"></span>\r\n            </div>\r\n            <div class=\"form-group has-feedback\">\r\n              <label for=\"exampleInputPassword\">Password</label>\r\n              <input type=\"password\" formControlName=\"password\" class=\"form-control\" id=\"exampleInputPassword\" placeholder=\"Enter Password\">\r\n              <span class=\"glyphicon glyphicon-lock form-control-feedback\"></span>\r\n            </div>\r\n            <div class=\"form-group has-feedback\">\r\n              <label for=\"exampleInputFirstName\">First Name</label>\r\n              <input type=\"text\" formControlName=\"first_name\" class=\"form-control\" id=\"exampleInputFirstName\" placeholder=\"Enter First Name\">\r\n              <span class=\"glyphicon glyphicon-user form-control-feedback\"></span>\r\n            </div>\r\n            <div class=\"form-group has-feedback\">\r\n              <label for=\"exampleInputLastName\">Last Name</label>\r\n              <input type=\"text\" formControlName=\"last_name\" class=\"form-control\" id=\"exampleInputLastName\" placeholder=\"Enter Last Name\">\r\n              <span class=\"glyphicon glyphicon-user form-control-feedback\"></span>\r\n            </div>\r\n            <div class=\"form-group has-feedback\">\r\n              <label for=\"exampleInputMobile\">Mobile</label>\r\n              <input type=\"text\" formControlName=\"mobile\" class=\"form-control\" id=\"exampleInputMobile\" placeholder=\"Enter Mobile\">\r\n              <span class=\"glyphicon glyphicon-phone form-control-feedback\"></span>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label>Gender</label>\r\n              <select formControlName=\"gender\" class=\"form-control\">\r\n                <option [value]=\"gender\" *ngFor=\"let gender of genders\">\r\n                  {{gender}}</option>\r\n              </select>\r\n            </div>\r\n            <label>Upload Image</label>\r\n            <div class=\"form-group\">\r\n              <img *ngFor='let url of urls'  [src]=\"url\" height=\"200\"> <br/>\r\n              <input type='file' (change)=\"onFileChanged($event)\" multiple>\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label class=\"hover\">\r\n                <div class=\"icheckbox_flat-green hover\" [ngClass]=\"{'checked': is_active}\" aria-checked=\"true\" aria-disabled=\"false\" style=\"position: relative;\"><input [checked]=\"is_active\" (change)=\"is_active = !is_active\" [value]='is_active' type=\"checkbox\" class=\"flat-red\" checked=\"\" style=\"position: absolute; opacity: 0;\"><ins class=\"iCheck-helper\" style=\"position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;\"></ins></div>\r\n                Active\r\n              </label>\r\n            </div>\r\n          </div>\r\n          <!-- /.box-body -->\r\n\r\n          <div class=\"box-footer\">\r\n            <button type=\"submit\" class=\"btn btn-primary\">Save</button>\r\n          </div>\r\n        </form>\r\n      </div>\r\n      <!-- /.box -->\r\n\r\n    </div>\r\n    <!--/.col (left) -->\r\n  </div>\r\n  <!-- /.row -->\r\n</section>\r\n<!-- /.content -->\r\n"

/***/ }),

/***/ "./src/app/users/edit-user/edit-user.component.ts":
/*!********************************************************!*\
  !*** ./src/app/users/edit-user/edit-user.component.ts ***!
  \********************************************************/
/*! exports provided: EditUserComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditUserComponent", function() { return EditUserComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _users_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../users.service */ "./src/app/users/users.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var EditUserComponent = /** @class */ (function () {
    function EditUserComponent(fb, router, route, userService) {
        this.fb = fb;
        this.router = router;
        this.route = route;
        this.userService = userService;
        this.ctrl = this;
        this.userId = this.route.snapshot.params.id;
        this.email = '';
        this.password = '';
        this.first_name = '';
        this.last_name = '';
        this.mobile = '';
        this.gender = '';
        this.image = '';
        this.is_active = false;
        this.loading = false;
        this.selectedFiles = [];
        this.genders = ['Male', 'Female'];
        this.formData = new FormData();
        this.urls = [];
    }
    EditUserComponent.prototype.ngOnInit = function () {
        this.buildForm();
        this.getOne(this.userId);
    };
    EditUserComponent.prototype.onFileChanged = function (event) {
        var _this = this;
        this.loading = true;
        this.selectedFiles = event.target.files;
        // this.formData.append('images', this.selectedFiles, this.selectedFiles.name);
        if (this.selectedFiles.length > 0) {
            var _loop_1 = function (i) {
                var reader = new FileReader();
                reader.onload = function (event) {
                    _this.formData.append('images', _this.selectedFiles[i]);
                    _this.urls.push(event.target.result);
                };
                reader.readAsDataURL(this_1.selectedFiles[i]);
            };
            var this_1 = this;
            for (var i = 0; i < this.selectedFiles.length; i++) {
                _loop_1(i);
            }
        }
    };
    EditUserComponent.prototype.update = function () {
        var _this = this;
        $('.content').find('*').attr('disabled', 'disabled');
        $('.content').find('a').click(function (e) { e.preventDefault(); });
        var data = {
            email: this.form.value.email,
            first_name: this.form.value.first_name,
            last_name: this.form.value.last_name,
            mobile: this.form.value.mobile,
            gender: this.form.value.gender,
            is_active: this.is_active
        };
        this.formData.append('data', JSON.stringify(data));
        this.loading = true;
        this.userService
            .update(this.formData, this.userId)
            .subscribe(function (result) {
            _this.loading = false;
            var returnUrl = _this.route.snapshot.queryParamMap.get('returnUrl');
            _this.router.navigate(['users']);
        }, function (error) {
            _this.loading = false;
        });
    };
    EditUserComponent.prototype.getOne = function (id) {
        var _this = this;
        this.loading = true;
        this.userService
            .getOne(id)
            .subscribe(function (result) {
            _this.email = result.email;
            _this.password = result.password;
            _this.first_name = result.first_name;
            _this.last_name = result.last_name;
            _this.mobile = result.mobile;
            _this.gender = result.gender;
            // this.image =  this.urls;
            _this.is_active = result.is_active;
            _this.loading = false;
            _this.buildForm();
        }, function (error) {
            _this.loading = false;
        });
    };
    EditUserComponent.prototype.buildForm = function () {
        this.form = this.fb.group({
            email: [this.email, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            password: [this.password, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            first_name: [this.first_name, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            last_name: [this.last_name, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            mobile: [this.mobile, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            gender: [this.gender],
        });
    };
    EditUserComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-edit-user',
            template: __webpack_require__(/*! ./edit-user.component.html */ "./src/app/users/edit-user/edit-user.component.html"),
            styles: [__webpack_require__(/*! ./edit-user.component.css */ "./src/app/users/edit-user/edit-user.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _users_service__WEBPACK_IMPORTED_MODULE_3__["UsersService"]])
    ], EditUserComponent);
    return EditUserComponent;
}());



/***/ }),

/***/ "./src/app/users/list-users/list-users.component.css":
/*!***********************************************************!*\
  !*** ./src/app/users/list-users/list-users.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXJzL2xpc3QtdXNlcnMvbGlzdC11c2Vycy5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/users/list-users/list-users.component.html":
/*!************************************************************!*\
  !*** ./src/app/users/list-users/list-users.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<style>\r\n  .btn {\r\n    background-color: DodgerBlue; /* Blue background */\r\n    border: none; /* Remove borders */\r\n    color: white; /* White text */\r\n    font-size: 10px; /* Set a font size */\r\n    cursor: pointer; /* Mouse pointer on hover */\r\n  }\r\n\r\n  /* Darker background on mouse-over */\r\n  .btn:hover {\r\n    background-color: RoyalBlue;\r\n  }\r\n  .loader {\r\n    position: absolute;\r\n    left: 50%;\r\n    top: 50%;\r\n    height:60px;\r\n    width:60px;\r\n    margin:0px auto;\r\n    -webkit-animation: rotation .6s infinite linear;\r\n    -moz-animation: rotation .6s infinite linear;\r\n    -o-animation: rotation .6s infinite linear;\r\n    animation: rotation .6s infinite linear;\r\n    border-left:6px solid rgba(0,174,239,.15);\r\n    border-right:6px solid rgba(0,174,239,.15);\r\n    border-bottom:6px solid rgba(0,174,239,.15);\r\n    border-top:6px solid rgba(0,174,239,.8);\r\n    border-radius:100%;\r\n  }\r\n\r\n  @-webkit-keyframes rotation {\r\n    from {-webkit-transform: rotate(0deg);}\r\n    to {-webkit-transform: rotate(359deg);}\r\n  }\r\n  @-moz-keyframes rotation {\r\n    from {-moz-transform: rotate(0deg);}\r\n    to {-moz-transform: rotate(359deg);}\r\n  }\r\n  @-o-keyframes rotation {\r\n    from {-o-transform: rotate(0deg);}\r\n    to {-o-transform: rotate(359deg);}\r\n  }\r\n  @keyframes rotation {\r\n    from {transform: rotate(0deg);}\r\n    to {transform: rotate(359deg);}\r\n  }\r\n</style>\r\n<!-- Content Header (Page header) -->\r\n<section class=\"content-header\">\r\n  <h1>\r\n    User <button routerLink=\"/users/create-user\" type=\"submit\" class=\"btn btn-primary pull-right\">New</button>\r\n  </h1>\r\n  <ol class=\"breadcrumb\">\r\n    <li><a routerLink=\"/dashboard\"><i class=\"fa fa-dashboard\"></i> Home</a></li>\r\n    <li><a routerLink=\"/users\">User</a></li>\r\n  </ol>\r\n</section>\r\n\r\n<!-- Main content -->\r\n<div class=\"loader\" [hidden]=\"!loading\"></div>\r\n<section class=\"content\">\r\n  <!-- /.row -->\r\n  <div class=\"row\">\r\n    <div class=\"col-xs-12\">\r\n      <div class=\"box\">\r\n        <div class=\"box-header\">\r\n          <h3 class=\"box-title\"></h3>\r\n\r\n          <div class=\"box-tools\">\r\n            <div class=\"input-group input-group-sm\" style=\"width: 150px;\">\r\n              <input (ngModelChange)=\"filter($event)\" [(ngModel)]=\"nameFilter\"  type=\"text\" name=\"table_search\" class=\"form-control pull-right\" placeholder=\"Search\">\r\n\r\n              <div class=\"input-group-btn\">\r\n                <button type=\"submit\" class=\"btn btn-default\"><i class=\"fa fa-search\"></i></button>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <!-- /.box-header -->\r\n        <div class=\"box-body table-responsive no-padding\">\r\n          <table class=\"table table-hover\">\r\n            <thead>\r\n            <tr>\r\n              <th>Email</th>\r\n              <th>First Name</th>\r\n              <th>Last Name</th>\r\n              <th>Mobile</th>\r\n              <th>Gender</th>\r\n              <th>Status</th>\r\n              <th>Edit/Delete</th>\r\n            </tr>\r\n            </thead>\r\n            <tbody>\r\n            <tr *ngFor=\"let user of users\" class=\"gradeX odd content-table-payment cursor\" role=\"row\">\r\n              <td class=\"mn-130 sorting_1\">{{user.email}}</td>\r\n              <td class=\"mn-130 sorting_1\">{{user.first_name}}</td>\r\n              <td class=\"mn-130 sorting_1\">{{user.last_name}}</td>\r\n              <td class=\"mn-130 sorting_1\">{{user.mobile}}</td>\r\n              <td class=\"mn-130 sorting_1\">{{user.gender}}</td>\r\n              <td class=\"mn-120 sorting_1\">\r\n                  <span *ngIf=\"user.is_active === true\"\r\n                        class=\"label label-success upper-case\">Active</span>\r\n                <span *ngIf=\"user.is_active === false\"\r\n                      class=\"label label-danger upper-case\">Active</span>\r\n              </td>\r\n              <td>\r\n                <button class=\"btn\" routerLink='/users/{{user._id}}/edit-user'><i class=\"fa fa-edit\"></i></button>\r\n                <button class=\"btn\" (click)=\"delete(user._id)\"><i class=\"fa fa-trash\"></i></button>\r\n              </td>\r\n            </tr>\r\n            </tbody>\r\n          </table>\r\n        </div>\r\n        <!-- /.box-body -->\r\n      </div>\r\n      <!-- /.box -->\r\n    </div>\r\n  </div>\r\n</section>\r\n<!-- /.content -->\r\n"

/***/ }),

/***/ "./src/app/users/list-users/list-users.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/users/list-users/list-users.component.ts ***!
  \**********************************************************/
/*! exports provided: ListUsersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListUsersComponent", function() { return ListUsersComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _users_users_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../users/users.service */ "./src/app/users/users.service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_3__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ListUsersComponent = /** @class */ (function () {
    function ListUsersComponent(router, route, userService) {
        this.router = router;
        this.route = route;
        this.userService = userService;
        this.ctrl = this;
        this.loading = false;
        this.users = [];
        this.nameFilter = '';
    }
    ListUsersComponent.prototype.ngOnInit = function () {
        this.getAll('');
    };
    ListUsersComponent.prototype.filter = function (value) {
        if (value !== '') {
            this.nameFilter = "email=" + value + "&first_name=" + value;
        }
        else {
            this.nameFilter = '';
        }
        this.getAll(this.nameFilter);
    };
    ListUsersComponent.prototype.getAll = function (query) {
        var _this = this;
        this.loading = true;
        $('.content').find('*').attr('disabled', 'disabled');
        $('.content').find('a').click(function (e) { e.preventDefault(); });
        this.userService
            .getAll(query)
            .subscribe(function (result) {
            _this.loading = false;
            _this.users = result.users;
            $('.content').find('*').removeAttr('disabled');
            $('.content').find('a').unbind('click');
            // const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
            // this.router.navigate(['news']);
            // setTimeout(() => {
            //   this.successMsg = 'News Created Successfully';
            // }, 2000);
        }, function (error) {
            _this.loading = false;
            $('.content').find('*').removeAttr('disabled');
            $('.content').find('a').unbind('click');
            // setTimeout(() => {
            //   this.errorMsg = error;
            // }, 2000);
        });
    };
    ListUsersComponent.prototype.delete = function (id) {
        var _this = this;
        this.loading = true;
        sweetalert2__WEBPACK_IMPORTED_MODULE_3___default()({
            type: 'warning',
            title: 'Are you sure to Delete User?',
            text: 'You will not be able to recover the data of User',
            showCancelButton: true,
            confirmButtonColor: '#049F0C',
            cancelButtonColor: '#ff0000',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, keep it'
        }).then(function (result) {
            if (result.value) {
                _this.userService
                    .delete(id)
                    .subscribe(function (result) {
                    _this.loading = false;
                    _this.getAll('');
                    // const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
                    // this.router.navigate(['news']);
                    // setTimeout(() => {
                    //   this.successMsg = 'News Created Successfully';
                    // }, 2000);
                }, function (error) {
                    _this.loading = false;
                    _this.getAll('');
                    // setTimeout(() => {
                    //   this.errorMsg = error;
                    // }, 2000);
                });
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_3___default()({
                    type: 'info',
                    title: 'Cancelled',
                    text: 'Your User file is safe :)'
                });
            }
        });
    };
    ListUsersComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-list-users',
            template: __webpack_require__(/*! ./list-users.component.html */ "./src/app/users/list-users/list-users.component.html"),
            styles: [__webpack_require__(/*! ./list-users.component.css */ "./src/app/users/list-users/list-users.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _users_users_service__WEBPACK_IMPORTED_MODULE_2__["UsersService"]])
    ], ListUsersComponent);
    return ListUsersComponent;
}());



/***/ }),

/***/ "./src/app/users/users.service.ts":
/*!****************************************!*\
  !*** ./src/app/users/users.service.ts ***!
  \****************************************/
/*! exports provided: UsersService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersService", function() { return UsersService; });
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../constants */ "./src/app/constants.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var UsersService = /** @class */ (function () {
    function UsersService(http) {
        this.http = http;
        this.apiUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_0__["environment"].apiBaseUrl;
        this.userId = null;
        this._isAuthenticated = new rxjs__WEBPACK_IMPORTED_MODULE_4__["BehaviorSubject"](false);
    }
    UsersService.prototype.create = function (data) {
        return this.http.post(this.apiUrl + "/users", data, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    UsersService.prototype.update = function (data, id) {
        return this.http.put(this.apiUrl + "/users/" + id, data, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    UsersService.prototype.getOne = function (id) {
        return this.http.get(this.apiUrl + "/users/" + id, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    UsersService.prototype.getAll = function (query) {
        return this.http.get(this.apiUrl + "/users?" + query, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    UsersService.prototype.delete = function (id) {
        return this.http.delete(this.apiUrl + "/users/" + id, { headers: { 'Authorization': localStorage.getItem(_constants__WEBPACK_IMPORTED_MODULE_3__["AUTH_TOKEN"]) } });
    };
    UsersService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], UsersService);
    return UsersService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false,
    apiBaseUrl: 'https://shrouded-ridge-65941.herokuapp.com/api',
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! D:\FOX\Talk\ytg-admin-panel\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map